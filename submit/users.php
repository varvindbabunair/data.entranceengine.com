<?php
include '../config.php';
$db = new Db();

$msg = 'msg=err';

if(isset($_POST['new-user-submit']) && $_POST['new-user-submit']=='Add User' ){
    $user = addslashes($_POST['name']);
    $password = md5($_POST['password']);
    $email = addslashes($_POST['email']);
    $phone = addslashes($_POST['phone']);
    $role = $_POST['user-role'];
    $privilage = $_POST['user-privilage'];
    
    $user_added = $db->query("INSERT INTO ".$role."_list (name,password,email,phone,last_login,cookie,admin_role) VALUES ('$user','$password','$email','$phone',CURDATE(),'','$privilage')") or die(mysqli_error($db->db_link));

    $user_id = mysqli_insert_id($db->db_link);

    if(isset($_POST['institute']) && $_POST['institute'] != '' && $_POST['institute'] != 0 ){
        $inst = $_POST['institute'];
        $db->query("UPDATE ".$role."_list SET institute = '$inst' WHERE id = '$user_id'");
    }
    
    $msg = ($user_added)?'useradd=success':'useradd=fail';
}

if(isset($_POST['new-password']) && trim($_POST['new-password']) != '' ){
    $role = $_POST['user-role'];
    $msg = 'passpdate=fail';
        $newpass = trim($_POST['new-password']);
        $pass_update_query = $db->query("UPDATE ".$role."_list SET password = '$newpass' WHERE id = '$_POST[changeid]'");
        $msg = ($pass_update_query)?'passupdate=success':'passupdate=fail';
}

if(isset($_POST['edit-user-submit']) && $_POST['edit-user-submit'] =='Save Changes'){
    $email = addslashes($_POST['email']);
    $phone = addslashes($_POST['phone']);
    $name = addslashes($_POST['name']);
    $id = addslashes($_POST['id']);
    $privilage = $_POST['user-privilage'];
    $role = $_POST['user-role'];
    $user_update = $db->query("UPDATE ".$role."_list SET email = '$email',phone = '$phone',name='$name', admin_role = '$privilage' WHERE id = '$id'") or die(mysqli_error($db->db_link));
    
    $msg = ($user_update)?'user-edit=success':'user-edit=fail';
}

if(isset($_POST['remove-user-submit']) && $_POST['remove-user-submit'] == 'Confirm Delete'){
    include '../includes/user.php';
    $user = new User();
    $id = $_POST['delete-user'];
    $del = FALSE;
    $role = $_POST['user-role'];
    if( $user->user_details['id'] != $id){
        $del = $db->query("DELETE FROM ".$role."_list WHERE id = '$id'");
    }
    $msg=($del)?'user-remove=success':'user-remove=fail';
}

if(isset($_POST['reset-user-submit']) && $_POST['reset-user-submit'] == 'Reset Password' && trim($_POST['new-password']) != '' ){
    $password = md5(addslashes($_POST['new-password']));
    $role = $_POST['user-role'];
    $id = $_POST['user-id'];
    $pass_update = $db->query("UPDATE ".$role."_list SET password = '$password' WHERE id = '$id'");
    
    $msg = ($pass_update)?'pass-update=success':'pass-update:fail';
}


$header = explode('&', $_SERVER['HTTP_REFERER']);
header("Location:".$header[0]."&".$msg);