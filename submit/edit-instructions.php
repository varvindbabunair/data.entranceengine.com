<?php
include('../config.php');
$db = new Db();
if(isset($_POST['instruction-submit']) && $_POST['instruction-submit'] == 'Add Instruction'){
    $title = addslashes(trim($_POST['instruction-title']));
    $description = addslashes(trim($_POST['instruction-description']));
    $instruction = addslashes(trim($_POST['instruction']));
    $insert_query = $db->query("INSERT INTO instruction_list (title, description, instruction) VALUES ('$title', '$description', '$instruction')");
    
    header('Location:../?page=instructions&add=success');
    die();
}


//Code to Delete Digital Material
if(isset($_POST['delete-submit']) && $_POST['delete-submit'] == 'Delete Material'){
    $id = $_POST['delete-id'];
    $delete = $db->query("DELETE FROM digital_list WHERE id = '$id'");
    $msg = ($delete)?'delete=success':'delete=fail';
}


//Code to edit digital material
if(isset($_POST['edit-submit']) && $_POST['edit-submit'] == 'Save Changes'){
    
    $title = addslashes($_POST['material-title']);
    $description = addslashes($_POST['material-description']);
    $id = $_POST['edit-id'];
    
    $notes_add = $db->query("UPDATE digital_list SET title = '$title', description = '$description' WHERE id = '$id'") or die(mysqli_error($db->db_link));

    $insert_id = $id;
    $ids = substr($_COOKIE['gr-app-user'], 0,10);
    if(isset($_FILES['question-file'])){
        $extension = pathinfo(basename($_FILES['question-file']['name']),PATHINFO_EXTENSION);
        if($extension != ''){
            $filename = $ids.'-question.'.$extension;
            move_uploaded_file($_FILES['question-file']['tmp_name'],'../digital/'.$filename);
            $db->query("UPDATE digital_list SET question_file='$filename' WHERE id = '$insert_id'");
        }
    }

    $ids = substr($_COOKIE['gr-app-user'], 10,20);
    if(isset($_FILES['answer-file'])){
        $extension = pathinfo(basename($_FILES['answer-file']['name']),PATHINFO_EXTENSION);
        if($extension != ''){
            $filename = $ids.'-answer.'.$extension;
            move_uploaded_file($_FILES['answer-file']['tmp_name'],'../digital/'.$filename);
            $db->query("UPDATE digital_list SET option_file='$filename' WHERE id = '$insert_id'");
        }
    }

    $ids = substr($_COOKIE['gr-app-user'], 20,30);
    if(isset($_FILES['hint-file'])){
        $extension = pathinfo(basename($_FILES['hint-file']['name']),PATHINFO_EXTENSION);
        if($extension != ''){
            $filename = $ids.'-hint.'.$extension;
            move_uploaded_file($_FILES['hint-file']['tmp_name'],'../digital/'.$filename);
            $db->query("UPDATE digital_list SET hint_file='$filename' WHERE id = '$insert_id'");
        }
    }

    $msg = ($notes_add)?'note-edit=success&id='.$_POST['edit-id']:'note-edit=fail&id='.$_POST['edit-id'];
}

$header = explode('&', $_SERVER['HTTP_REFERER']);
header("Location:".$header[0]."&".$msg);