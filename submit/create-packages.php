<?php
include '../config.php';

$db = new Db();

if(isset($_POST['package-name']) && trim($_POST['package-name'])!=''){
    
    
    $package_title = addslashes($_POST['package-name']);
    $package_description = addslashes($_POST['package-description']);
    
    $subscription_period = addslashes($_POST['subscription-period']);
    $for_sale = (isset($_POST['package-for-sale']))?(int)$_POST['package-for-sale']:0;
    $pricing = (int)$_POST['pricing'];
    $include_tax = (isset($_POST['include-tax']))?(int)$_POST['include-tax']:0;
    
    $subscription = '["any","'.$subscription_period.'"]';
    
    $package_features = addslashes($_POST['package-features']);
    
    $subject_list = addslashes($_POST['subject-list']);
    $chapter_list = addslashes($_POST['chapter-list']);
    $notes_list = addslashes($_POST['notes-list']);
    
    $user_group = $_POST['user-group'];
    $base = $_POST['base'];
    $upgrade_price = (int)$_POST['upgrade-pricing'];
    $package_type = $_POST['package-type'];
    $test_restriction = $_POST['test-restrictions'];
    
    
    $db->query("INSERT INTO package_list (title,image,main_header,description,features,subject_list,chapter_list,material_list,subscription,for_sale,pricing,include_taxes,user_group,base,upgrade_price,package_type,test_restriction)"
            . " VALUES ('$package_title','','','$package_description','$package_features','$subject_list','$chapter_list','$notes_list','$subscription','$for_sale','$pricing','$include_tax','$user_group','$base','$upgrade_price','$package_type','$test_restriction')")or die(mysqli_error($db->db_link));

    
    $insert_id = mysqli_insert_id($db->db_link);
    if(isset($_FILES['package-image'])){
        $extension = pathinfo(basename($_FILES['package-image']['name']),PATHINFO_EXTENSION);
        if($extension != ''){
        move_uploaded_file($_FILES['package-image']['tmp_name'], '../images/packages/'.$insert_id.'.'.$extension);
        $db->query("UPDATE package_list SET image='".$insert_id.".".$extension."' WHERE id = '$insert_id'");
        }else{
            $db->query("UPDATE package_list SET image='default.png' WHERE id = '$insert_id'");
        }
    }
}

 header("Location:".SITE_URL.'?page=packages');