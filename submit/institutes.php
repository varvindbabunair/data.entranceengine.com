<?php
error_reporting(E_ALL); 
ini_set('display_errors', 1);
include '../config.php';
include '../includes/user.php';
$user = new User;

$msg = 'msg=err';

if(isset($_POST['new-user-submit']) && $_POST['new-user-submit']=='Add Institute' ){

    $name = addslashes($_POST['inst-name']);
    $email = addslashes($_POST['inst-email']);
    $phone = addslashes($_POST['inst-phone']);
    $address = addslashes($_POST['inst-address']);
    $website = $_POST['inst-website'];
    $tagline = addslashes($_POST['inst-tagline']);
    $expiry = date('Y-m-d', strtotime($_POST['package-expiry']));

    
    $added_by = $user->user_details['id'];
    
    
    $inst_add = $user->query("INSERT INTO institute_list (name,email,phone,address,tagline,website,logo,instkey,added_by,added_date) "
            . "VALUES ('$name','$email','$phone','$address','$tagline','$expiry','$website','','$added_by',CURDATE())") or die(mysqli_error($user->db_link));
    
    $inst_id = mysqli_insert_id($user->db_link);
    
    $target_file = basename($_FILES['inst-logo']['name']);
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    $filename = md5( $inst_id . time() ) .'.'.$imageFileType;
    // $instkey = md5($inst_id . $phone . time());
    $instkey = trim($_POST['instkey']);
    if(trim($imageFileType) != ''){
        move_uploaded_file($_FILES['inst-logo']['tmp_name'], HOME_DIR.'/images/institutes/'.$filename) or die('Image upload failed');
        $user->query("UPDATE institute_list SET logo = '$filename', instkey = '$instkey' WHERE id = $inst_id");
    }else{
        $user->query("UPDATE institute_list SET instkey = '$instkey' WHERE id = $inst_id");
    }
    
    
    $user->query("UPDATE institute_list SET logo = '$filename', instkey = '$instkey' WHERE id = $inst_id");
    

    $msg = ($user_added )? 'instadd=success':'instadd=fail';
}

if(isset($_POST['new-admin-submit']) && $_POST['new-admin-submit']=='Add Institute Administrator' ){
    $username = addslashes($_POST['name']);
    $password = md5($_POST['password']);
    $email = addslashes($_POST['email']);
    $phone = addslashes($_POST['phone']);
    
    $institute = $_POST['instid'];
    

    $instadd = $user->query("INSERT INTO user_list (name, password, email, phone, regn_number, status, institute,cookie,course,batch) "
                . "VALUES ('$username','$password','$email','$phone','','admin','$institute','',0,0)") or die(mysqli_error($user->db_link));

    
//    mysqli_query($user->db_link, $q) or die(mysqli_error($user->db_link));
    
    $msg = ($instadd)?'adminadd=success':'adminadd=fail';
    
    
}


$header = explode('&', $_SERVER['HTTP_REFERER']);
header("Location:".$header[0]."&".$msg);
