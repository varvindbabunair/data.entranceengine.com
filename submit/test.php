<html>
    <head>
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css/font-awesome.css" />
        <link rel="stylesheet" href="../css/exam-result.css" />
        <script src="../js/jquery-1.9.1.js" ></script>
        <script src="../js/bootstrap.min.js"></script>
        
    </head>
    <body>
<?php
include '../config.php';
$db = new Db();

include '../includes/user.php';
$users = new User();

$test = $_POST['test'];
$answer = str_replace('\'', '"', $_POST['answer-string']);

if(isset($_POST['test-session'])){
$session = $_POST['test-session'];
$package = '';
$user = $_POST['user'];
if($users->user_details['role'] == 'student'){
    $db->query("INSERT INTO answer_list (test,package,test_time,session,user,answer) VALUES ('$test','$package',NOW(),'$session','$user','$answer')");
}
}

$test_get_query = $db->query("SELECT questions FROM test_list WHERE id = '$test'");

$test_written = mysqli_fetch_array($test_get_query);

$answered = json_decode($answer);
$original_answer = json_decode($test_written['questions']);

$qn_qry = "SELECT id,correct_answer FROM question_list WHERE";
$section_num = 0;
foreach ($original_answer as $section){
    $individual_answer_num =0;
    foreach ($section as $qn){
        $qn_id = $original_answer[$section_num][$individual_answer_num][0];
        $qn_qry .= " id = '$qn_id' OR";
        $individual_answer_num++;
    }
    $section_num++;
}
$qn_qry = trim($qn_qry,' OR');

$question_get_query = $db->query($qn_qry);

$ans_arr = array();
while($qn_info = mysqli_fetch_array($question_get_query)){
    $ans_arr[$qn_info['id']] = $qn_info['correct_answer'];
}

$positive_marks = 0;
$negative_marks = 0;

$questions_answered = 0;
$correctly_answered = 0;
$attended_question = 0;

$time_on_unanswered =0;
$time_on_correct = 0;
$time_on_wrong = 0;
$total_time = 0;

$section_num = 0;
foreach ($original_answer as $section_answer){
    $individual_answer_num =0;
    foreach ($section_answer as $individual_answer){
        $qn_id = $answered[$section_num][$individual_answer_num][0];
        if($answered[$section_num][$individual_answer_num][1] == $ans_arr[$qn_id]){
            $questions_answered++;
            $correctly_answered++;
            $total_time = $total_time + (int)$answered[$section_num][$individual_answer_num][2];
            $positive_marks = $positive_marks + (int)$original_answer[$section_num][$individual_answer_num][1];
            $attended_question++;
            $time_on_correct = $time_on_correct + (int)$answered[$section_num][$individual_answer_num][2];
        }else if($answered[$section_num][$individual_answer_num][1] == ''){
            $total_time = $total_time + (int)$answered[$section_num][$individual_answer_num][2];
            if((int)$answered[$section_num][$individual_answer_num][2] > 0){
                $attended_question++;
                $time_on_unanswered = $time_on_unanswered + (int)$answered[$section_num][$individual_answer_num][2];
            }
        }else if($answered[$section_num][$individual_answer_num][1] != $ans_arr[$qn_id]){
            $questions_answered++;
            $total_time = $total_time + (int)$answered[$section_num][$individual_answer_num][2];
            $negative_marks = $negative_marks + (int)$original_answer[$section_num][$individual_answer_num][2];
            $attended_question++;
            $time_on_wrong = $time_on_wrong + (int)$answered[$section_num][$individual_answer_num][2];
        }
        $individual_answer_num++;
    }
    $section_num++;
}
?>

        <div class="container" style="margin-top: 100px;">
            <div class="col-lg-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Total Marks</h3>
                    </div>
                    <div class="panel-body">
                        <h1 style="text-align: center"><?php echo $positive_marks - $negative_marks; ?></h1>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <h3>Results Analysis</h3>
                <hr>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Answering Overview</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-condensed">
                            <tr><th>Total Attended</th><td><?php echo $attended_question; ?></td></tr>
                            <tr><th>Total Answered</th><td><?php echo $questions_answered; ?></td></tr>
                            <tr><th>Correct Answers</th><td><?php echo $correctly_answered; ?></td></tr>
                            <tr><th>Wrong Answers</th><td><?php echo $questions_answered-$correctly_answered; ?></td></tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Marking Overview</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-condensed">
                            <tr><th>Marks for Correct Answers</th><td><?php echo $positive_marks; ?></td></tr>
                            <tr><th>Negative Marks for Wrong Answers</th><td><?php echo $negative_marks; ?></td></tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-2" >
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Accuracy</h3>
                    </div>
                    <div class="panel-body" >
                        <?php 
                        
                        $accuracy = ($questions_answered == 0)?$questions_answered:round(($correctly_answered/$questions_answered)*100,2);
                        
                        ?>
                         
                        <h1 style="text-align: center"><?php echo $accuracy.'%'; ?></h1>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Time Analysis</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <tr><th>Total Time Taken</th><td><?php echo $total_time.'s'; ?></td></tr>
                            <tr><th>Avg Time/Question</th><td><?php echo round($total_time/$attended_question,2).'s'; ?></td></tr>
                            <tr><th>Time on Unanswered Question</th><td><?php echo $time_on_unanswered.'s'; ?></td></tr>
                            <tr><th>Time on Correctly Answered Question</th><td><?php echo $time_on_correct.'s'; ?></td></tr>
                            <tr><th>Time on Wrongly Answered Question</th><td><?php echo $time_on_wrong.'s'; ?></td></tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <hr>
            </div>
            <div class="col-lg-12" style="text-align: center">
                <form method="POST" action="../pages/student/answersheet.php">
                    <input type="hidden" name="answers" value="<?php echo str_replace('"', '\'', $answer); ?>" />
                    <input type="hidden" name="test" value="<?php echo $test; ?>" />
                    <button class="btn btn-primary" type="submit"><span class="fa fa-list-alt"></span> View Answersheet</button>
                </form>
            </div>
        </div>
    </body>
</html>