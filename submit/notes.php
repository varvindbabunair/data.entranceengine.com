<?php
include '../config.php';
include '../includes/subjects.php';

$subjects = new Subjects();

if(isset($_POST['note-submit']) && $_POST['note-submit'] == 'Submit Note' ){
    $chapter = $_POST['chapter'];
    $title = addslashes($_POST['title']);
    $tags = addslashes($_POST['tags']);
    $section = addslashes($_POST['section']);
    $notes = addslashes($_POST['editor1']);
    
    $notes_add = $subjects->query("INSERT INTO notes_list (chapter,title,tags,note,section) VALUES ('$chapter','$title','$tags','$notes','$section')") or die(mysqli_error($subjects->db_link));

    $msg = ($notes_add)?'note-add=success':'note-add=fail';
}

if(isset($_POST['delete-submit']) && $_POST['delete-submit'] == 'Delete Synopsis'){
    $id = $_POST['delete-id'];
    $delete = $subjects->query("DELETE FROM notes_list WHERE id = '$id'");
    $msg = ($delete)?'delete=success':'delete=fail';
}

if(isset($_POST['edit-submit']) && $_POST['edit-submit'] == 'Save Changes'){
    $chapter = $_POST['chapter'];
    $title = addslashes($_POST['title']);
    $tags = addslashes($_POST['tags']);
    $section = addslashes($_POST['section']);
    $notes = addslashes($_POST['editor1']);
    $id = $_POST['edit-id'];
    
//    print_r($_POST);
    $notes_add = $subjects->query("UPDATE notes_list SET section = '$section',chapter='$chapter',title='$title',tags='$tags',note='$notes' WHERE id = '$id'") or die(mysqli_error($subjects->db_link));

    $msg = ($notes_add)?'note-edit=success&id='.$_POST['edit-id']:'note-edit=fail&id='.$_POST['edit-id'];
//    echo $msg;
}

$header = explode('&', $_SERVER['HTTP_REFERER']);
header("Location:".$header[0]."&".$msg);