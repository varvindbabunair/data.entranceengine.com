<?php
include_once '../includes/class.php';
echo 'test';
$class = new Classclass();
$msg='';

if(isset($_POST['submit-class']) && $_POST['submit-class'] == 'Submit Class' ){
    $title = addslashes($_POST['classname']);
    $code = addslashes($_POST['code']);
    if($class->class_add($title,$code)){
        $msg = 'class-add=success';
    }else{
        $msg= 'class-add=fail';
    }
}

if(isset($_POST['class-submit']) && $_POST['class-submit'] == 'Edit Class'){
    $title = addslashes($_POST['classeditname']);
    $id = $_POST['classeditid'];
    if($class->class_update($id,$title)){
        $msg = 'class-edit=success';
    }else{
        $msg= 'class-edit=fail';
    }
}

header('Location:../?page=class&'.$msg);