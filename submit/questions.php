<?php
include '../config.php';
include '../includes/user.php';
$user = new User;

if(isset($_POST['question-submit'])){
    $option = array();
//    print_r($_POST);
    $class_id = $_POST['class-name'];
    $subject_id = $_POST['subject-id'];
    $chapter_id = $_POST['chapter-id'];
    $topic_id = $_POST['topic-id'];
    $section = $_POST['section-id'];
    $difficulty = $_POST['difficulty'];
    $question_type = $_POST['answer-type'];
    $tags = addslashes($_POST['tags']);
    $source = addslashes($_POST['qn-source']);
    $reference = addslashes($_POST['qn-reference']);
    

    if($question_type == 'assertion'){
        $question = '[["'.addslashes($_POST['assertion-statement-1']).'"],["'.addslashes($_POST['assertion-statement-2']).'"]]';
    }else{
        $question = addslashes($_POST['text-question']);
    }

    if($question_type == 'assertion'){
        $correct_answer = addslashes($_POST['assertion-correct-answer']);
    }elseif ($question_type == 'numeric') {
        $correct_answer = addslashes($_POST['integer-answer']);
    }elseif($question_type == 'mmcq'){
        $correct_answer = filter_var(json_encode($_POST['correct-answer']), FILTER_SANITIZE_STRING);
    }else{
        $correct_answer = filter_var($_POST['correct-answer'], FILTER_SANITIZE_STRING);
    }

    if($question_type == 'assertion'){
        $answer_option = '[["A","Both Statement 1 and Statement 2 are true and Statement 2 is the correct reason for Statement 1"],["B"," Both Statement 1 and Statement 2 are true and Statement 2 is not the correct reason for Statement 1 "],["C"," Both Statement 1 and Statement 2 are false. "],["D"," Statement 1 is true but Statement 2 is false. "]]';
    }elseif ($question_type == 'numeric') {
        $answer_option = '';
    }else{
        $opts = array();
        $opts[0] = array('A',filter_var(addslashes($_POST['answer-a'])));
        $opts[1] = array('B',filter_var(addslashes($_POST['answer-b'])));
        $opts[2] = array('C',filter_var(addslashes($_POST['answer-c'])));
        $opts[3] = array('D',filter_var(addslashes($_POST['answer-d'])));
        $answer_option = json_encode($opts);
//        $answer_option = implode('', explode('
//', $answer_option));
//        $answer_option = rtrim($answer_option, "\0");
    }

    $hint = addslashes($_POST['text-hint']);
    $added_by = $user->user_details['id'];
    

    $user->query("INSERT INTO question_list(reference, source, class, subject, chapter, question_type, question, section, difficulty, tags, answer_option, correct_answer, hint, added_by, validated_by, approved, review_status, topic, add_date) "
            . "VALUES ('$reference','$source','$class_id', '$subject_id', '$chapter_id', '$question_type', '$question', '$section', '$difficulty', '$tags', '$answer_option', '$correct_answer', '$hint', '$added_by', '0', '0', '0', '$topic_id', CURDATE())") or die(mysqli_error($user->db_link));
    
    $msg = 'qn-add=success';
    $insertid = mysqli_insert_id($user->db_link);
    $uri = explode('?', $_SERVER['HTTP_REFERER']);
 header("Location:".$uri[0]."?page=edit-question&id=".$insertid."&".$msg);
        }


if(isset($_POST['qn-remove-submit']) && $_POST['qn-remove-submit'] == 'Delete Question'){
    $id = $_POST['qn-remove-id'];
    $user->query("DELETE FROM question_list WHERE id = '$id'");
    
    $msg='qnrem=1';
    $uri = explode('?', $_SERVER['HTTP_REFERER']);
 header("Location:".$uri[0]."?page=questions&".$msg);
}



if(isset($_POST['qn-edit-submit']) && $_POST['qn-edit-submit'] =='Save Changes' && isset($_POST['qn-edit-id']) && $_POST['qn-edit-id'] !=''){


    $option = array();
    $class_id = $_POST['class-name'];
    $subject_id = $_POST['subject-id'];
    $chapter_id = $_POST['chapter-id'];
    $section = $_POST['section-id'];
    $topic = $_POST['topic-id'];
    $difficulty = $_POST['difficulty-val'];
    $question_type = $_POST['answer-type'];
    $tags = addslashes($_POST['tags']);
    $source = addslashes($_POST['qn-source']);
    $reference = addslashes($_POST['qn-reference']);

    if($question_type == 'assertion'){
        $question = '[["'.addslashes($_POST['assertion-statement-1']).'"],["'.addslashes($_POST['assertion-statement-2']).'"]]';
    }else{
        $question = addslashes($_POST['text-question']);
    }

    if($question_type == 'assertion'){
        $correct_answer = $_POST['assertion-correct-answer'];
    }elseif ($question_type == 'numeric') {
        $correct_answer = $_POST['integer-answer'];
    }elseif($question_type == 'mmcq'){
        $correct_answer = $_POST['correct-answer'];
    }else{
        $correct_answer = $_POST['correct-answer'];
    }

    if($question_type == 'assertion'){
        $answer_option = '[["A","Both Statement 1 and Statement 2 are true and Statement 2 is the correct reason for Statement 1"],["B"," Both Statement 1 and Statement 2 are true and Statement 2 is not the correct reason for Statement 1 "],["C"," Both Statement 1 and Statement 2 are false. "],["D"," Statement 1 is true but Statement 2 is false. "]]';
    }elseif ($question_type == 'numeric') {
        $answer_option = '';
    }else{
        $opts = array();
        $opts[0] = array('A',filter_var(addslashes($_POST['answer-a'])));
        $opts[1] = array('B',filter_var(addslashes($_POST['answer-b'])));
        $opts[2] = array('C',filter_var(addslashes($_POST['answer-c'])));
        $opts[3] = array('D',filter_var(addslashes($_POST['answer-d'])));
        $answer_option = json_encode($opts);
        
    }

    $hint = addslashes($_POST['text-hint']);

    $qnid = $_POST['qn-edit-id'];


    $user->query("UPDATE question_list SET reference = '$reference', source = '$source', class = '$class_id', subject = '$subject_id', topic = '$topic', chapter = '$chapter_id', question_type = '$question_type', question = '$question', section = '$section', difficulty = '$difficulty', tags = '$tags', answer_option = '$answer_option', correct_answer = '$correct_answer', hint = '$hint' WHERE id = '$qnid'") or die(mysqli_error($user->db_link));
    
    $msg = 'qn-edit=success';
    $uri = explode('?', $_SERVER['HTTP_REFERER']);
    header("Location:".$uri[0]."?page=edit-question&id=".$qnid."&".$msg);

}

if(isset($_POST['qn-validate-submit'])){
    $qnid = $_POST['qn-validate-id'];
    $uid = $user->user_details['id'];
    $val = $user->query("UPDATE question_list SET validated_by = '$uid', validate_date = CURDATE() WHERE id = '$qnid'") or die(mysqli_error($user->db_link));
    $msg = ($val)?'qn-validate=success':'qn-validate=fail';
    $uri = explode('?', $_SERVER['HTTP_REFERER']);
    header("Location:".$uri[0]."?page=validate-questions&".$msg);
}