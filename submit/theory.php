<?php
include '../config.php';
include '../includes/subjects.php';
include '../includes/user.php';

$subjects = new Subjects();
$user = new User();

if(isset($_POST['synopsys-submit']) && $_POST['synopsys-submit'] == 'Submit Theory' ){
    $subject = $_POST['subject-id'];
    $chapter = $_POST['chapter-id'];
    $reference = $_POST['reference'];
    $class = $_POST['class-name'];
    $synopsys = addslashes($_POST['editor1']);
    $added_by = $user->user_details['id'];
    
    
    $notes_add = $subjects->query("INSERT INTO theory_list (class,subject,chapter,theory,added_by,validated_by,reference) "
            . "VALUES ('$class','$subject','$chapter','$synopsys','$added_by',0,'$reference')") or die(mysqli_error($subjects->db_link));

    $msg = ($notes_add)?'note-add=success':'note-add=fail';
    $uri = explode('?', $_SERVER['HTTP_REFERER']);
header("Location:".$uri[0]."?page=theory&".$msg);
}

if(isset($_POST['delete-submit']) && $_POST['delete-submit'] == 'Delete Theory'){
    $id = $_POST['delete-id'];
    $delete = $subjects->query("DELETE FROM theory_list WHERE id = '$id'");
    $msg = ($delete)?'delete=success':'delete=fail';
    $uri = explode('?', $_SERVER['HTTP_REFERER']);
header("Location:".$uri[0]."?page=theory&".$msg);
}

if(isset($_POST['edit-submit']) && $_POST['edit-submit'] == 'Save Changes'){
    $subject = $_POST['subject-id'];
    $chapter = $_POST['chapter-id'];
    $class = $_POST['class-name'];
    $reference = $_POST['reference'];
    $synopsys = addslashes($_POST['editor1']);
    $id = $_POST['edit-id'];
    
//    print_r($_POST);
    $notes_add = $subjects->query("UPDATE theory_list SET subject = '$subject',chapter='$chapter',reference='$reference',class='$class', theory='$synopsys' WHERE id = '$id'") or die(mysqli_error($subjects->db_link));

    $msg = ($notes_add)?'theory-edit=success&id='.$_POST['edit-id']:'theory-edit=fail&id='.$_POST['edit-id'];
//    echo $msg;
    $uri = explode('?', $_SERVER['HTTP_REFERER']);
    header("Location:".$uri[0]."?page=edit-theory&".$msg);
}

if(isset($_POST['qn-validate-submit'])){
    $qnid = $_POST['qn-validate-id'];
    $uid = $user->user_details['id'];
    $val = $user->query("UPDATE synopsys_list SET validated_by = '$uid', validated_date = CURDATE() WHERE id = '$qnid'") or die(mysqli_error($user->db_link));
    $msg = ($val)?'synopsys-validate=success':'synopsys-validate=fail';
    $uri = explode('?', $_SERVER['HTTP_REFERER']);
    header("Location:".$uri[0]."?page=validate-synopsys&".$msg);
}