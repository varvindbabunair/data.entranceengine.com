<?php
include '../config.php';
$db = new Db();

$msg = 'msg=err';

if(isset($_POST['new-user-submit']) && $_POST['new-user-submit']=='Add Institute' ){
    $user = addslashes($_POST['name']);
    $username = addslashes($_POST['username']);
    $password = md5($_POST['password']);
    $email = addslashes($_POST['email']);
    $phone = addslashes($_POST['phone']);
    
    $user_added = $db->query("INSERT INTO institute_list (name,username,password,email,phone,last_login,cookie) VALUES ('$user','$username','$password','$email','$phone',CURDATE(),'')") or die(mysqli_error($db->db_link));
    
    $msg = ($user_added)?'useradd=success':'useradd=fail';
}

if(isset($_POST['new-password']) && trim($_POST['new-password']) != '' && isset($_POST['old-password']) && trim($_POST['old-password']) !='' ){
    $msg = 'passpdate=fail';
    if(trim($_POST['new-password']) == trim($_POST['cnf-new-password'])){
        $newpass = trim($_POST['new-password']);
        $oldpass = trim($_POST['old-password']);
        $old_pass_select = $db->query("SELECT password from institute_list WHERE id = '$_POST[changeid]'");
        $old_pass = mysqli_fetch_array($old_pass_select);
        if(md5($oldpass) == $old_pass['password']){
            $newpass = md5($newpass);
            $pass_update_query = $db->query("UPDATE institute_list SET password = '$newpass' WHERE id = '$_POST[changeid]'");
            $msg = ($pass_update_query)?'passupdate=success':'passupdate=fail';
        }
    }
    
    
}

if(isset($_POST['edit-user-submit']) && $_POST['edit-user-submit'] =='Save Changes'){
    $email = addslashes($_POST['email']);
    $phone = addslashes($_POST['phone']);
    $name = addslashes($_POST['name']);
    $id = addslashes($_POST['id']);
    
    $user_update = $db->query("UPDATE institute_list SET email = '$email',phone = '$phone',name='$name' WHERE id = '$id'") or die(mysqli_error($db->db_link));
    
    $msg = ($user_update)?'user-edit=success':'user-edit=fail';
}

if(isset($_POST['remove-user-submit']) && $_POST['remove-user-submit'] == 'Confirm Delete'){
    include '../includes/user.php';
    $user = new User();
    $id = $_POST['delete-user'];
    $del = FALSE;
    if( $user->user_details['id'] != $id){
        $del = $db->query("DELETE FROM institute_list WHERE id = '$id'");
    }
    $msg=($del)?'user-remove=success':'user-remove=fail';
}

if(isset($_POST['reset-user-submit']) && $_POST['reset-user-submit'] == 'Reset Password' && trim($_POST['new-password']) != '' ){
    $password = md5(addslashes($_POST['new-password']));
    $id = $_POST['user-id'];
    $pass_update = $db->query("UPDATE institute_list SET password = '$password' WHERE id = '$id'");
    
    $msg = ($pass_update)?'pass-update=success':'pass-update:fail';
}


$header = explode('&', $_SERVER['HTTP_REFERER']);
if(sizeof($header) > 1){
    header("Location:".$_SERVER['HTTP_REFERER']."?".$msg);
}else{
    header("Location:".$_SERVER['HTTP_REFERER']."&".$msg);
}