<?php
include '../config.php';

$db = new Db();

if(isset($_POST['editid']) && trim($_POST['editid'])!=''){
    
    
    $id = $_POST['editid'];
    
    $package_title = addslashes($_POST['package-name']);
    $package_description = addslashes($_POST['package-description']);
    
    $subscription_period = addslashes($_POST['subscription-period']);
    $for_sale = (isset($_POST['package-for-sale']))?(int)$_POST['package-for-sale']:0;
    $pricing = (int)$_POST['pricing'];
    $include_tax = (isset($_POST['include-tax']))?(int)$_POST['include-tax']:0;
    
    $subscription = '["any","'.$subscription_period.'"]';
    
    $package_features = addslashes($_POST['package-features']);
    
    $subject_list = addslashes($_POST['subject-list']);
    $chapter_list = addslashes($_POST['chapter-list']);
    $notes_list = addslashes($_POST['notes-list']);
    
    $user_group = $_POST['user-group'];
    $base = $_POST['base'];
    $upgrade_price = $_POST['upgrade-pricing'];
    $package_type = $_POST['package-type'];
    $test_restriction = $_POST['test-restrictions'];
    
    
    $db->query("UPDATE package_list SET title = '$package_title',description='$package_description',features='$package_features',subject_list='$subject_list',chapter_list='$chapter_list',material_list='$notes_list',subscription='$subscription',for_sale='$for_sale',"
            . "pricing='$pricing',include_taxes='$include_tax',user_group='$user_group',base='$base',upgrade_price='$upgrade_price',package_type='$package_type',test_restriction='$test_restriction' WHERE id = '$id'")or die(mysqli_error($db->db_link));

    
    $insert_id = $id;
    if(isset($_FILES['package-image'])){
        $extension = pathinfo(basename($_FILES['package-image']['name']),PATHINFO_EXTENSION);
        if($extension != ''){
            $filename = $insert_id.'.'.$extension;
            move_uploaded_file($_FILES['package-image']['tmp_name'],'../images/packages/'.$filename);
            $db->query("UPDATE package_list SET image='$filename' WHERE id = '$insert_id'");
        }
    }
}

 header("Location:".$_SERVER['HTTP_REFERER']);