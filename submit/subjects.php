<?php
include '../config.php';
$db = new Db();
if(isset($_POST['subject-submit'])){
    $subject = addslashes(trim($_POST['subject-name']));
    $chapters = ($_POST['chapters'] !='')?addslashes($_POST['chapters']):'';
    $subject_check_query = $db->query("SELECT id FROM subject_list WHERE name = '$subject'");
    if(mysqli_num_rows($subject_check_query) == 0){
        $msg=($db->query("INSERT into subject_list (title) VALUES ('$subject')"))?"msg=01":"err=011";
        $id_get_query = $db->query("SELECT id FROM subject_list WHERE title = '$subject'");
        $ids = mysqli_fetch_array($id_get_query);
        $id = $ids['id'];
        if($chapters != ''){
            $chapter_list = explode(",", $chapters);
            foreach ($chapter_list as $chapter){
                $chapter = addslashes(trim($chapter));
                $msg=($db->query("INSERT into chapter_list (title,subject) VALUES ('$chapter','$id')"))?"msg=01":"err=012";
            }
        }
    }else{
        $msg = "err=01";
    }
}
if(isset($_POST['subject-edit-submit']) && $_POST['subject-edit-submit']=='Save Changes'){
    $subject = addslashes($_POST['edit-subject-name']);
    $id = $_POST['edit-id'];
    if(trim($subject) != ''){
        $msg = ($db->query("UPDATE subject_list SET title = '$subject' WHERE id = '$id'"))?"msg=02":"err=02";
    }
}
if(isset($_POST['chapter-add-submit']) && trim($_POST['chapter-add-submit']) == 'Add Chapters'){
    $chapters = explode(',', addslashes($_POST['chapter-names']));
    $subject = $_POST['subject-id'];
    $chapteradd = 0;
    foreach ($chapters as $chapter){
        $chapter = trim($chapter);
        $chapter_get_query = $db->query("SELECT id FROM chapter_list WHERE title = '$chapter' AND subject = '$subject'");
        if(mysqli_num_rows($chapter_get_query) == 0){
            $chapteradd = ($db->query("INSERT INTO chapter_list (title,subject) VALUES ('$chapter','$subject')"))?$chapteradd+1:$chapteradd;
        }
    }
    $msg = "chapterAdd=".$chapteradd;
}
if(isset($_POST['subject-remove-submit']) && trim($_POST['subject-remove-submit']) == 'Delete'){
    $subject = $_POST['subject-remove-id'];
    $chapter_get_query = $db->query("SELECT * FROM chapter_list WHERE subject = '$subject'");
    $chap_count = mysqli_num_rows($chapter_get_query);
    while($chapter = mysqli_fetch_array($chapter_get_query)){
        $db->query("UPDATE notes_list SET chapter = '0' WHERE chapter = '$chapter[id]'");
        $db->query("UPDATE video_list SET chapter = '0' WHERE chapter = '$chapter[id]'");
        $db->query("UPDATE question_list SET chapter = '0' WHERE chapter = '$chapter[id]'");
    }
    if($db->query("DELETE FROM chapter_list WHERE subject = '$subject'")){
        $msg="chaprem=".$chap_count;
        $db->query("DELETE FROM subject_list WHERE id = '$subject'");
    }
}
if(isset($_POST['chapter-edit-submit']) && $_POST['chapter-edit-submit'] == 'Save Changes'){
    $chapter = addslashes($_POST['edit-chapter-name']);
    $id = $_POST['chapter-edit-id'];
    if(trim($chapter) != ''){
        $msg = ($db->query("UPDATE chapter_list SET title = '$chapter' WHERE id = '$id'"))?"msg=04":"err=04";
    }
}
if(isset($_POST['chapter-remove-submit']) && trim($_POST['chapter-remove-submit']) == 'Remove Chapter'){
    $chapter = $_POST['chapter-remove-id'];
    $db->query("UPDATE notes_list SET chapter = '0' WHERE chapter = '$chapter'");
    $db->query("UPDATE video_list SET chapter = '0' WHERE chapter = '$chapter'");
    $db->query("UPDATE question_list SET chapter = '0' WHERE chapter = '$chapter'");
    $msg=($db->query("DELETE FROM chapter_list WHERE id = '$chapter'"))?'msg=05':'err=05';
}

if(isset($_POST['subject-add-submit']) && trim($_POST['subject-add-submit']) == 'Add Subject'){
    $subject = addslashes($_POST['subject-name']);
    $subject_add = $db->query("INSERT INTO subject_list (title) VALUES ('$subject')");
    $msg = ($subject_add)?'subject-add=success':'subject-add=fail';
}

if(isset($_POST['section-add-submit']) && trim($_POST['section-add-submit']) == 'Add Section'){
    $sections = explode(',', $_POST['section-names']);
    $chapter = $_POST['section-add-id'];
    foreach ($sections as $section){
        if(trim($section) != ''){
            $section = addslashes($section);
            $section_add = $db->query("INSERT INTO section_list (title,chapter) VALUES ('$section','$chapter')");
        }
    }
    $msg = 'section-add=success';
}

if(isset($_POST['subject-remove-submit']) && trim($_POST['subject-remove-submit']) == 'Remove Section'){
    $id = $_POST['remove-section'];
    $section_remove = $db->query("DELETE FROM section_list WHERE id = '$id'");
    $msg = ($section_remove)?'section-remove=success':'section-remove=fail';
}

$header = explode('&', $_SERVER['HTTP_REFERER']);
header("Location:".$header[0]."&".$msg);