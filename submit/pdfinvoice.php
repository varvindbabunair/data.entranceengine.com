<?php
include '../includes/packages.php';
include '../includes/user.php';
if(!isset($_POST['package'])){
    die('Some Error Occured');
}
$package = $_POST['package'];
$packages = new Packages();
$user = new User();
require_once('../includes/tcpdf/tcpdf.php');
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
$pdf->SetFont('dejavusans', '', 14, '', true);$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

    $package_select = $user->query("SELECT id,pricing,title,subscription,include_taxes,user_group,upgrade_price FROM package_list WHERE id = '$package'");
    
    if(mysqli_num_rows($package_select) == 0){
        die('<h3 style="margin-top:100px;text-align:center;">Some error occured</h3>');
    }
    
    if($_POST['package-upgrade'] == '1'){
        $package_select_query = $user->query("SELECT * FROM order_list WHERE package = '".$_POST['upgrade-from']."' AND status = '1'");
        $old_package_info = mysqli_fetch_array($package_select_query);
        $purchase_date = $old_package_info['purchase_date'];
        $sale_insert_query = $user->query("INSERT INTO order_list (user,package,order_date,purchase_date,upgrade) VALUES ('".$user->user_details['id']."','$package',CURDATE(),'$purchase_date','1')");
    }else{
        $sale_insert_query = $user->query("INSERT INTO order_list (user,package,order_date) VALUES ('".$user->user_details['id']."','".$package."',CURDATE())");
    }
    
    $order_id = mysqli_insert_id($user->db_link);
    
    $package_details = mysqli_fetch_array($package_select);
$price = ($_POST['package-upgrade'] != '1')?$package_details['pricing']:$packages->get_upgrade_price($_POST['upgrade-from'], $package_details['id']);
// Set some content to print
$html = '<h3>Registration Details</h3>
        <table>
        <tr><th>Id</th><td>'.$order_id.'</td></tr>
        <tr><th>Name</th><td>'.$user->user_details['name'].'</td></tr>
        <tr><th>Address</th><td>'.$user->user_details['address'].'</td></tr>
        <tr><th>City</th><td>'.$user->user_details['city'].'</td></tr>
        <tr><th>State</th><td>'.$user->user_details['state'].'</td></tr>
        <tr><th>Country</th><td>'.$user->user_details['country'].'</td></tr>
        <tr><th>Username</th><td>'.$user->user_details['username'].'</td></tr>
        <tr><th>Package Selected</th><td>'.$package_details['title'].'</td></tr>
        <tr><th><b>Total Amount to Pay</b></th><td><b>'.$price.'</b></td></tr>
        <tr><th>DD Number</th><td>__________________________</td></tr>
        </table>
 <hr>
 <p>Please fill in the DD number and send this document along with DD to</p>
 <p>The Coordinator<br>
 Brilliant Study Centre<br>
 Mutholy, Puliyannoor [P.O]<br>
 Pala, Kottayam - 686573
</p>
<p>
Demand Draft should be drawn in favor of "Brilliant Study Centre" payable at Pala.
</p>
';

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
