<?php
include '../config.php';
$db = new Db();

// print_r($_POST);
// die();
$num = 0;
$subject = $_POST['subject'];
$sql = $db->query("SELECT question_list.reference as refno, class_list.title as class, subject_list.title as subject, chapter_list.title as chapter, question_list.topic as topic, question_list.difficulty as difficulty, question_list.section as section, question_list.question as question, question_list.answer_option as options, question_list.correct_answer as correct_answer, question_list.hint as hint FROM `question_list` INNER JOIN class_list ON class_list.id = question_list.class INNER JOIN subject_list ON subject_list.id = question_list.subject INNER JOIN chapter_list ON chapter_list.id = question_list.chapter WHERE question_list.subject = $subject");

    while($p = mysqli_fetch_array($sql)) {
         $prod[$num]['refno']          = $p['refno'];
         $prod[$num]['class']        = $p['class'];
         $prod[$num]['subject'] = $p['subject'];
         $prod[$num]['chapter'] = $p['chapter'];
         if($p['topic'] == 0){
         	$prod[$num]['topic'] = 'unspecified';
         }else{
         	$topic = $p['topic'];
         	$topic_qry = $db->query("SELECT * FROM topic_list WHERE id = $topic");
         	$top = mysqli_fetch_array($topic_qry);
         	$prod[$num]['topic'] = $top['title'];
         }
         $prod[$num]['difficulty'] = $p['difficulty'];

         if($p['section'] == 0){
         	$prod[$num]['section'] = 'unspecified';
         }else{
         	$section = $p['section'];
         	$section_qry = $db->query("SELECT * FROM section_list WHERE id = $section");
         	$sec = mysqli_fetch_array($section_qry);
         	$prod[$num]['section'] = $sec['name'];
         }
         
         // $prod[$num]['section'] = $p['section'];
         $prod[$num]['question'] = $p['question'];
         $opts = json_decode(preg_replace("/[\r\n]+/", " ", $p['options']));

         $prod[$num]['opta'] = $opts[0][1];
         $prod[$num]['optb'] = $opts[1][1];
         $prod[$num]['optc'] = $opts[2][1];
         $prod[$num]['optd'] = $opts[3][1];

         $prod[$num]['correct_answer'] = $p['correct_answer'];
         $prod[$num]['hint'] = $p['hint'];
         $num++;        
    }
 
$output = fopen("php://output",'w') or die("Can't open php://output");
header("Content-Type:application/csv"); 
header("Content-Disposition:attachment;filename=qns.csv"); 
fputcsv($output, array('Reference No','Class','Subject','Chapter','Topic','Difficulty','Section','Question','Option A', 'Option B','Option C','Option D', 'Correct Answer','Hint'));
foreach($prod as $product) {
    fputcsv($output, $product);
}
fclose($output) or die("Can't close php://output");
?>
