<?php
error_reporting(0);
?>
<html>
<head>
    <script src="../js/jquery-1.9.1.js" ></script>
</head>
<body>
    <?php
    include '../config.php';
    include '../includes/user.php';
    include '../includes/packages.php';
    $db = new Db();
    $user = new User();
    
    $package = $_POST['package'];
    
    $packages = new Packages();
    
    $package_select = $user->query("SELECT id,pricing,title,subscription,include_taxes,user_group,upgrade_price FROM package_list WHERE id = '$package'");
    
    if(mysqli_num_rows($package_select) == 0){
        die('<h3 style="margin-top:100px;text-align:center;">Some error occured</h3>');
    }
    
    
    if($_POST['package-upgrade'] == '1'){
        $package_select_query = $user->query("SELECT * FROM order_list WHERE package = '".$_POST['upgrade-from']."' AND status = '1'");
        $old_package_info = mysqli_fetch_array($package_select_query);
        $purchase_date = $old_package_info['purchase_date'];
        $sale_insert_query = $user->query("INSERT INTO order_list (user,package,order_date,purchase_date,upgrade,status) VALUES ('".$user->user_details['id']."','$package',CURDATE(),'$purchase_date','1','0')")or die(mysqli_error($user->db_link));
    }else{
        $sale_insert_query = $user->query("INSERT INTO order_list (user,package,order_date,status,purchase_date,transaction_id,upgrade,upgrade_date,amount_paid,tax) VALUES ('".$user->user_details['id']."','".$package."',CURDATE(),'0',CURDATE(),'0','0',CURDATE(),'0.00','')")or die(mysqli_error($user->db_link));
    }
    
    $order_id = mysqli_insert_id($user->db_link);
    
    $package_details = mysqli_fetch_array($package_select);
    ?>
    <form id="paySubmit" action="https://secure.ebs.in/pg/ma/payment/request" method="post">

    <?php $hashData = '9ff08f6604005f1e2f0b54883dae4291'; ?>
    <input type="hidden" value="7609" name="account_id" />
    <?php $hashData .= '|7609'; ?>
    <input type="hidden" value="<?php echo $user->user_details['address']; ?>" name="address" />
    <?php $hashData .= '|'.$user->user_details['address']; ?>
    <?php $price = ($_POST['package-upgrade'] != '1')?$package_details['pricing']:$packages->get_upgrade_price($_POST['upgrade-from'], $package_details['id']); ?>
    <input type="hidden" value="<?php echo ($package_details['include_taxes'] == '1')?$price:$packages->get_principal($price, $package_details['include_taxes']); ?>" name="amount" />
    <?php $hashData .= '|';
            
    $hashData .= ($package_details['include_taxes'] == '1')?$price:$packages->get_principal($price, $package_details['include_taxes']); ?>
    
    <input type="hidden" value="0" name="channel" />
    <?php $hashData .= '|0'; ?>
    <input type="hidden" value="<?php echo $user->user_details['city']; ?>" name="city" />
    <?php $hashData .= '|'.$user->user_details['city']; ?>
    <input type="hidden" value="<?php echo $user->user_details['country']; ?>" name="country" />
    <?php $hashData .= '|'.$user->user_details['country']; ?>
    
    <input type="hidden" value="<?php echo $package_details['title']; ?>" name="description" />
    <?php $hashData .= '|'.$package_details['title']; ?>
    
    <input type="hidden" value="<?php echo $user->user_details['email']; ?>" name="email" />
    <?php $hashData .= '|'.$user->user_details['email']; ?>
    
    <input type="hidden" value="TEST" name="mode" />
    <?php $hashData .= '|TEST'; ?>
    
    <input type="hidden" value="<?php echo $user->user_details['name']; ?>" name="name" />
    <?php $hashData .= '|'.$user->user_details['name']; ?>
    
    <input type="hidden" value="9326" name="page_id"  />
    <?php $hashData .= '|9326'; ?>
    <input type="hidden" value="<?php echo $user->user_details['phone']; ?>" name="phone" />
    <?php $hashData .= '|'.$user->user_details['phone']; ?>
    <input type="hidden" value="<?php echo $user->user_details['postal_code']; ?>" name="postal_code" />
    <?php $hashData .= '|'.$user->user_details['postal_code']; ?>

    <input type="hidden" value="<?php echo $order_id; ?>" name="reference_no" />
    <?php $hashData .= '|'.$order_id; ?>
    <input type="hidden" value="<?php echo SITE_URL.'/response.php'; ?>" name="return_url" />
    <?php $hashData .= '|'.SITE_URL.'/response.php'; ?>

    <input type="hidden" value="<?php echo $user->user_details['state']; ?>" name="state" />
    <?php $hashData .= '|'.$user->user_details['state']; ?>

    
    <?php
    
	if (strlen($hashData) > 0) {  
	$hashvalue = strtoupper(md5($hashData));  
	}
	 
	$SecureHash = $hashvalue;

    ?>

    <input type="hidden" value="<?php echo $SecureHash; ?>" name="secure_hash"  />

</form>
    
    <script>
            $('form').submit();
    </script>
</body>
</html>