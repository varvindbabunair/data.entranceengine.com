<?php
include '../config.php';
$db = new Db();

if(isset($_POST['register-student']) && $_POST['register-student'] == 'Register Student' ){

    $name = addslashes($_POST['name']);
    $username = addslashes($_POST['username']);
    $password = md5($_POST['password']);
    $address = addslashes($_POST['address']);
    $city = addslashes($_POST['city']);
    $state = addslashes($_POST['state']);
    $country = addslashes($_POST['country']);
    $postal_code = addslashes($_POST['postal_code']);
    $phone = addslashes($_POST['phone']);
    $email = addslashes($_POST['email']);
    $user_group = addslashes($_POST['user_group']);
    $institute = $_POST['institute'];

    $user_exist_check = $db->query("SELECT * FROM student_list WHERE username = '$username' OR phone = '$phone' OR email = '$email'");
    if(mysqli_num_rows($user_exist_check) == 0){
        $qry = $db->query("INSERT INTO student_list (username,name,address,city,state,country,postal_code,phone,email,password,registration_date,student_group,cookie,last_login,packages,recover_key,institute) "
                . "VALUES ('$username','$name','$address','$city','$state','$country','$postal_code','$phone','$email','$password',CURDATE(),'$user_group','',CURDATE(),'','','$institute')");
    
        $msg = ($qry)?'user-add=success':'user-add=fail';
    }else{
        $msg = 'exist=1';
    }

}

if(isset($_POST['student-edit']) && $_POST['student-edit'] == 'Save Changes' ){
    $name = addslashes($_POST['name']);
    $username = addslashes($_POST['username']);
    $address = addslashes($_POST['address']);
    $city = addslashes($_POST['city']);
    $state = addslashes($_POST['state']);
    $country = addslashes($_POST['country']);
    $postal_code = addslashes($_POST['postal_code']);
    $phone = addslashes($_POST['phone']);
    $email = addslashes($_POST['email']);
    $success = $db->query("UPDATE student_list SET name = '$name',address = '$address',city = '$city',state = '$state',country = '$country',postal_code = '$postal_code',phone = '$phone',email = '$email' WHERE username = '$username'") or die(mysqli_error($db->db_link));
    $msg = ($success)?'user-edit=success':'user-edit=fail';
}

if(isset($_POST['student-id']) && $_POST['student-id'] != '' && isset($_POST['package-id']) && $_POST['package-id'] != '' ){
    $package = $_POST['package-id'];
    $student = $_POST['student-id'];
    if($_POST['purchase-type'] == 'activate'){
        $success = $db->query("INSERT INTO order_list (package,order_date,status,purchase_date,user,transaction_id,upgrade,upgrade_date,amount_paid,tax) VALUES ('$package',CURDATE(),'1',CURDATE(),'$student','0','0',CURDATE(),'0.00','')") or die(mysqli_error($db->db_link));
        $msg = ($success)?'package-activation=success':'package-activation=fail';
    }else if($_POST['purchase-type'] == 'upgrade'){
        $lower_package = $_POST['upgrade-from'];
        $upgrade_from_select_qry = $db->query("SELECT id,purchase_date FROM order_list WHERE package = '$lower_package' AND status = '1'");
        $purchase_date = mysqli_fetch_array($upgrade_from_select_qry);
        $db->query("UPDATE order_list SET status = '0' WHERE id = '$purchase_date[id]'");
        $success = $db->query("INSERT INTO order_list (package,order_date,status,purchase_date,user,transaction_id,upgrade,upgrade_date,amount_paid,tax) VALUES ('$package',CURDATE(),'1','$purchase_date[purchase_date]','$student','0','1',CURDATE(),'0.00','')");
    }
}

if(isset($_POST['student-remove']) && $_POST['student-remove'] =='Remove Student'){
    $id  = $_POST['student-id'];
    $rem = $db->query("DELETE FROM student_list WHERE id = '$id'");
    $msg = ($rem)?'student-del:success':'student-del:fail';
}


$header = explode('&', $_SERVER['HTTP_REFERER']);
header("Location:".$header[0]."&".$msg);