<?php
include '../config.php';
$db = new Db();

if(isset($_POST['package-delete']) && $_POST['package-delete'] == 'Confirm Removal'){
    $id  = $_POST['delete-id'];
    $base_sel_qry = $db->query("SELECT id,base,main_header FROM package_list WHERE id = '$id'");
    $base = mysqli_fetch_array($base_sel_qry);
    $del = $db->query("DELETE FROM package_list WHERE id = '$id'");
    $upd = $db->query("UPDATE package_list SET base = '$base[base]',main_header = '$base[main_header]' WHERE base = '$id'");
    
    if($del && $upd){
        $msg = 'delete=success';
    }
}

if(isset($_POST['package-copy']) && $_POST['package-copy'] == 'Confirm Copying' && isset($_POST['copy-id'])){
    $id = $_POST['copy-id'];
    $package_select_query = $db->query("SELECT * FROM package_list WHERE id = '$id'");
    $package = mysqli_fetch_array($package_select_query);
    
    $image = $package['image'];
    $title = $package['title'].'-Copy';
    $description = $package['description'];
    $features = $package['features'];
    $subject_list = $package['subject_list'];
    $chapter_list = $package['chapter_list'];
    $material_list = $package['material_list'];
    $subscription = $package['subscription'];
    $for_sale = 0;
    $pricing = $package['pricing'];
    $include_taxes = $package['include_taxes'];
    $user_group = $package['user_group'];
    $base = $package['base'];
    $upgrade_price = $package['upgrade_price'];
    $package_type = $package['package_type'];
    $test_restriction = $package['test_restriction'];
    $main_header = $package['main_header'];
    
    $insert = $db->query("INSERT INTO package_list (image,title,description,features,subject_list,chapter_list,material_list,subscription,for_sale,pricing,include_taxes,user_group,base,upgrade_price,package_type,test_restriction,main_header) VALUES"
            . "('$image','$title','$description','$features','$subject_list','$chapter_list','$material_list','$subscription','$for_sale','$pricing','$include_taxes','$user_group','$base','$upgrade_price','$package_type','$test_restriction','$main_header')");
    $msg= ($insert)?'copy=success':'copy=fail';
}

$header = explode('&', $_SERVER['HTTP_REFERER']);
header("Location:".$header[0]."&".$msg);