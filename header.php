<html !DOCTYPE>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.css" />
    <link rel="stylesheet" href="css/bootstrap-tagsinput.css" />
    <link rel="stylesheet" href="css/jquery.multiselect.css" />
    <link rel="stylesheet" href="css/jquery-ui.min.css" />
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link href="css/sb-admin.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/datatables.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

    <script src="js/jquery.min.js" ></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/datatables.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/bootstrap-tagsinput.js"></script>

    <script src="js/moment.min.js"></script>
    <script src="js/bootstrap-datetimepicker.js"></script>
    <script src="js/jquery.multiselect.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/script.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-MML-AM_CHTML' async></script>
    <?php if(isset($_GET['page'])){ ?>
    <script src="js/page/<?= $user->user_details['userrole'] ?>/<?= $_GET['page']; ?>.js"></script>
    <?php }else{
      ?>
      <script src="js/page/<?= $user->user_details['userrole'] ?>/dashboard.js"></script>
      <?php
      } 
      ?>
          
          <?php
          if(isset($_GET['page']) && $_GET['page']=='videos'){
          ?>
            <script type="text/javascript" src="//cdn.jsdelivr.net/npm/afterglowplayer@1.x"></script>
          <?php
          }
          ?>

    <script src="js/ckeditor/ckeditor.js"></script>
</head>
<body>
    <div id="wrapper" style="<?=($user->is_loggedin)?'':'padding-left:0px;' ?>">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="#">
                    <?php
                    if($user->is_loggedin){
                        if($user->user_details['role'] == 'admin'){
                            echo 'Entrance Engine';
                        }else{
                            echo $user->get_institute();
                        }
                    }
                    ?>
                </a>
            </div>
            <?php
            if($user->is_loggedin){
            ?>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $user->user_details['name'] ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="?page=settings"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <?php
            
                include 'pages/'.$user->user_details['userrole'].'/sidebar.php';
            }
            ?>
        </nav>
    
<div id="passChangeModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Password</h4>
      </div>
        <form method="POST" action="submit/users.php">
            <div class="modal-body">
                Enter Current Password :
                <input class="form-control" id="oldPass" name="old-password" type="password" />
                Enter New Password :
                <input class="form-control" id="newPass" name="new-password" type="password" />
                Confirm New Password :
                <input class="form-control" id="cnfnewPass" name="cnf-new-password" type="password" />
                <input type="hidden" name="changeid" value="<?php echo $user->user_details['id']; ?>" />
            </div>
            <div class="modal-footer">
                <button id="passwordChangeSubmit" class="btn btn-primary">Submit</button>
                <button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
        </form>
    </div>

  </div>
</div>