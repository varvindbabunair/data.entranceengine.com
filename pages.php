<?php

/* 
 * This page loads the page required
 */
?>
<?php
if($user->is_loggedin){
    ?>
     <!-- main right col -->
     <div id="page-wrapper" style="min-height: 100%;">

            <div class="container-fluid">

            
            </p>
            <?php
                if(!isset($_GET['page'])){
                    include 'pages/'.$user->user_details['userrole'].'/dashboard.php';
                }else{
                    include 'pages/'.$user->user_details['userrole'].'/'.$_GET['page'].'.php';
                }
            ?>
        </div>
        <!-- /main -->
    </div>

<div class="black-background-full" style="display: none;">
    <h3 style="text-align: center;margin-top:20%;"><b><span class="loading-msg">Loading</span> <span class="fa fa-spinner fa-spin"></span></b></h3>
</div>

<div id="messageModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="messageTitle" class="modal-title">Modal Header</h4>
      </div>
        <div  class="modal-body">
        <p id="messageBody" >Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<?php
}else{
    ?>
     <!-- main right col -->
        <div id="page-wrapper">

            <div class="container-fluid">
     <?php
    if(isset($_GET['page']) && $_GET['page']=='lost-pass'){
        include 'lost-pass.php';
    }elseif(isset($_GET['page']) && $_GET['page']=='recover-pass'){
        include 'recover-pass.php';
    }else{
        include 'login.php';
    }
    ?>
            </div>
        </div>
                <?php
             
                
}
?>

</div>