$(document).ready(function(){
    
    $('body').on('click', '.qn-view', function (){
        var qnid = $(this).attr('qnid');
        $.ajax({
            url:'ajax/qn-view-fetch-faculty.php',
            type:'POST',
            async: false,
            data:{
                qnid : qnid
            },
            success:function(result){
                $('.qn-content').html(result);
                MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            }
        });
            
    });

});
