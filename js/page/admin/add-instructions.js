$(document).ready(function(){

	CKEDITOR.replace( 'instruction', {
        toolbar: [
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
            { name: 'insert', items: ['Table', 'HorizontalRule', 'Smiley', 'SpecialChar','EqnEditor','Image' ] },
            '/',
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        ]
    });

    $('.media-modal-btn').click(function(){
        mediaFileReload();
    });

    $('form#img-submit').on('submit',(function(e){
        var formData = new FormData($(this)[0]);
                console.log(formData);
        $.ajax({
            url  : 'ajax/media-upload.php',
            type : 'POST',
            data : formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend:function(){
                $('.media-files').html('<p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>');
            },
            success:function(result){
                $('.media-files').html(result);
            },
            error:function(){
                mediaFileReload();
                $('.media-files').html('<p style="text-align: center"><span class="fa fa-exclamation"></span> Failed Uploading File.</p>'+$('.media-files').html(result))
            }
        }); 
        return false;
    }));


});

function mediaFileReload(){
    $.ajax({
        url:'ajax/media-fetch.php',
        type:'POST',
        cache:false,
        success:function(result){
            $('.media-files').html(result);
        }
    });
}