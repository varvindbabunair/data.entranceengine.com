$(document).ready(function(){
    
    $('.qn-remove').click(function(){
        $('#qn-remove-id').val($(this).attr('removeid'));
    });
        
        
        
    $('#qn-add-submit').click(function(){
        $('.option-number').val($('.answer-type:visible').length);
        if($('#chapter-select').val() == ''){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Incomplete field');
            $('#messageBody').html('Please choose chapter from the dropdown list of Chapters');
            $('#QnAddModal').modal('hide');
            $('#messageModal').modal('show');
            return null;
        }
        if($('.question-type').val() == 'image'){
            if($('.image-question').val() != ''){
                var filename = $(".image-question").val();
                var extension = filename.replace(/^.*\./, '');
                extension = extension.toLowerCase();
                switch (extension) {
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                        break;
                    default:
                        $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Wrong Image Format');
                        $('#messageBody').html('Please choose any valid image formats. Only png, jpg and JPEG image fprmats are allowed');
                        $('#QnAddModal').modal('hide');
                        $('#messageModal').modal('show');
                        return null;
                }
            }else{
                return null;
            }
        }
        if($('.question-type').val() == 'text'){
            var textAnswer =  CKEDITOR.instances['text-question'].getData()
            if(textAnswer.trim() == ''){
                $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Empty Question');
                $('#messageBody').html('Please enter the question, as empty questions are not allowes and might cause errors.');
                $('#QnAddModal').modal('hide');
                $('#messageModal').modal('show');
                return null;
            }
        }
        if($('.tags').val() == ''){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Incomplete field');
            $('#messageBody').html('Please add tags. Tags help to identify questions');
            $('#QnAddModal').modal('hide');
            $('#messageModal').modal('show');
            return null;
        }
        
        $('.option-table').children('tbody').children('tr').each(function(){
            if($(this).children('td').eq(1).children('.answer-type').val() == 'text'){
                if($(this).children('td').eq(2).children('.option-text').val() == ''){
                    $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Empty Option');
                    $('#messageBody').html('An Empty option is found. Please make sure that all options are filled');
                    $('#QnAddModal').modal('hide');
                    $('#messageModal').modal('show');
                    return null;
                }
            }else if($(this).children('td').eq(1).children('.answer-type').val() == 'image'){
                if($(this).children('td').eq(2).children('.option-image').val() == ''){
                    $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Empty Option');
                    $('#messageBody').html('An Empty option is found. Please make sure that all options are filled');
                    $('#QnAddModal').modal('hide');
                    $('#messageModal').modal('show');
                    return null;
                }else{
                    var filename = $(".option-image").val();
                    var extension = filename.replace(/^.*\./, '');
                    extension = extension.toLowerCase();
                    switch (extension) {
                        case 'jpg':
                        case 'jpeg':
                        case 'png':
                            break;
                        default:
                            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Wrong Image Format in Option');
                            $('#messageBody').html('Please choose any valid image formats. Only png, jpg and JPEG image formats are allowed');
                            $('#QnAddModal').modal('hide');
                            $('#messageModal').modal('show');
                            return null;
                    }
                }
            }
        });
        
        if(!$('[name="correct-answer"]:checked').val()){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Correct answer not specified');
            $('#messageBody').html('Correct answer for the question is not specified. Please choose the correct answerto complete the question.');
            $('#QnAddModal').modal('hide');
            $('#messageModal').modal('show');
        }
        
    });
    
    
    $('.qn-edit-submit').click(function(){
        $('.option-number').val($('.answer-type:visible').length);
    });
    
    $('.chapter-select').change(function(){
        var chapterid = $(this).val();
        $.ajax({
            url:'ajax/section-fetch.php',
            type:'POST',
            data:{
                chapterid : chapterid
            },
            success:function(result){
                $('.section-select').html(result);
            }
        });
    });
    
    
    $('body').on('click', '.qn-view', function (){
        var qnid = $(this).attr('qnid');
        $.ajax({
            url:'ajax/qn-view-fetch.php',
            type:'POST',
            async: false,
            data:{
                qnid : qnid
            },
            success:function(result){
                $('.qn-content').html(result);
                MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            }
        });
            
    });
$('#qn-table').dataTable( {
    "order": [[ 0, "desc" ]],
    "fnDrawCallback": function( oSettings ) {
      MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
    }
  } );
});
