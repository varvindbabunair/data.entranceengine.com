$(document).ready(function(){
    $('body').on('change','.answer-type',function(){
        if($(this).val() == 'image'){
            $('.option-image-'+$(this).attr('typeof')).show();
            $('.option-text-'+$(this).attr('typeof')).hide();
        }else if($(this).val() == 'text'){
            $('.option-image-'+$(this).attr('typeof')).hide();
            $('.option-text-'+$(this).attr('typeof')).show();
        }
    });
    
    $('body').on('change','.question-type',function(){
        if($(this).val() == 'image'){
            $('.image-question').show();
            $('.cke_editor_text-question').hide();
            $('.image-question').attr('required');
            $('.text-question').attr('required');
        }else if($(this).val() == 'text'){
            $('.image-question').hide();
            $('.cke_editor_text-question').show();
            $('.text-question').addAttr('required');
            $('.image-question').removeAttr('required');
        }
    });
    $('body').on('change','.hint-type',function(){
        if($(this).val() == 'image'){
            $('.image-hint').show();
            $('.cke_editor_text-hint').hide();
        }else if($(this).val() == 'text'){
            $('.image-hint').hide();
            $('.cke_editor_text-hint').show();
        }
    });
    $('body').on('click','.add-option:visible',function(){
        var val = $(this).parent('td').children('input').attr('value');
        var addelem = String.fromCharCode(val.charCodeAt() + 1);
        var addelemSmall = addelem.toLowerCase();
        var addRow = '<tr class="option-row-'+addelem+'"><td>Option '+addelem+'</td>\n\
                        <td><select name="answer-type-'+addelemSmall+'" typeof="'+addelem+'" class="form-control answer-type"><option value="text">Text</option><option value="image">Image</option></select></td>\n\
                        <td><input name="text-answer-'+addelemSmall+'" type="text" class="form-control option-text option-text-'+addelem+'" /> <input name="image-answer-'+addelemSmall+'" class="option-image option-image-'+addelem+'" style="display:none;" type="file" accept=".jpg,.png" /></td>\n\
                        <td><input type="radio" name="correct-answer" value="'+addelem+'" /><span style="margin-left:10px;" class="fa fa-plus add-option" title="Add Option"></span><span style="margin-left:10px;" class="fa fa-remove remove-option" title="Remove Option"></span></td></tr>';
        $(this).parent('td').parent('tr:last').after(addRow);
        $(this).parent('td').children('.remove-option').remove();
        $(this).remove();
    });
    
    $('body').on('click','.remove-option',function(){
        var val = $(this).parent('td').children('input').attr('value');
        var addelem = String.fromCharCode(val.charCodeAt() - 1);
        var addRow = '<span style="margin-left:10px;" class="fa fa-plus add-option" title="Add Option"></span><span style="margin-left:10px;" class="fa fa-remove remove-option" title="Remove Option"></span>';
//        alert($(this).parent('td').parent('tr').index() );
        if($(this).parent('td').parent('tr').index() > 1){
            $('input[value="'+addelem+'"]:visible').after(addRow);            
            $(this).parent('td').parent('tr').remove();
        }
    });
    
    CKEDITOR.replace( 'text-question', {
        toolbar: [
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
            { name: 'insert', items: ['Table', 'HorizontalRule', 'Smiley', 'SpecialChar' ] },
            '/',
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        ]
    });
        
    CKEDITOR.replace( 'text-hint', {
        toolbar: [
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote','-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
            { name: 'insert', items: ['Table', 'HorizontalRule', 'Smiley', 'SpecialChar' ] },
            '/',
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        ]
    });
    
    $('.qn-remove').click(function(){
        $('#qn-content').html('Qn: '+$(this).parent('td').parent('tr').children('td').eq(1).html());
        $('#no-of-tests').html($(this).parent('td').attr('noofTest'));
        $('#no-of-packages').html($(this).parent('td').attr('noofPackage'));
        $('#remTestAction').val('delete tests');
        $('#remPackageAction').val('delete tests from package');
        if($(this).parent('td').attr('noofTest') == '0'){
            $('#test-contain-action').hide();
        }else{
            $('#test-contain-action').show();
        }
        if($(this).parent('td').attr('noofPackage') == '0'){
            $('#package-contain-action').hide();
        }else{
            $('#package-contain-action').show();
        }
        $('#qn-remove-id').val($(this).attr('removeid'));
    });
        
    $('#remTestAction,#remPackageAction').change(function(){
        if($('#remTestAction').val() != 'delete tests' || $('#remPackageAction').val() != 'delete tests from package'){
            $('#delete-question').hide();
        }else{
            $('#delete-question').show();
        }
    });
        
    $('.qn-edit').click(function(){
        $.ajax({
            url:'ajax/questions.php',
            type:'POST',
            data:{
                editid : $(this).attr('editid')
            },
            success:function(result,status){
                $('#edit-qnBody').html(result);
                $('#tags-input').tagsinput('refresh');
                var addRow = '<span style="margin-left:10px;" class="fa fa-plus add-option" title="Add Option"></span><span style="margin-left:10px;" class="fa fa-remove remove-option" title="Remove Option"></span>';
                $('input[type="radio"]:last').after(addRow);  
                CKEDITOR.replace( 'text-question', {
                    toolbar: [
                        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
                        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote','-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                        { name: 'insert', items: ['Table', 'HorizontalRule', 'Smiley', 'SpecialChar' ] },
                        '/',
                        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                    ]
                });
                CKEDITOR.replace( 'text-hint', {
                    toolbar: [
                        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
                        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote','-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                        { name: 'insert', items: ['Table', 'HorizontalRule', 'Smiley', 'SpecialChar' ] },
                        '/',
                        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                    ]
                });
                
            },
            error:function(){
                $('#edit-qnBody').html('<p style="text-align:center;font-weight:bold;font-size:20px;"><span class="fa fa-exclamation-triangle"></span> Oops.... Some error occured while processing your request.</p>');
            }
        });
        $('#qn-editModal').modal('show');
    });
        
        
        
    $('#qn-add-submit').click(function(){
        if($('#chapter-select').val() == ''){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Incomplete field');
            $('#messageBody').html('Please choose chapter from the dropdown list of Chapters');
            $('#QnAddModal').modal('hide');
            $('#messageModal').modal('show');
            return null;
        }
        if($('.question-type').val() == 'image'){
            if($('.image-question').val() != ''){
                var filename = $(".image-question").val();
                var extension = filename.replace(/^.*\./, '');
                extension = extension.toLowerCase();
                switch (extension) {
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                        break;
                    default:
                        $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Wrong Image Format');
                        $('#messageBody').html('Please choose any valid image formats. Only png, jpg and JPEG image fprmats are allowed');
                        $('#QnAddModal').modal('hide');
                        $('#messageModal').modal('show');
                        return null;
                }
            }else{
                return null;
            }
        }
        if($('.question-type').val() == 'text'){
            var textAnswer =  CKEDITOR.instances['text-question'].getData()
            if(textAnswer.trim() == ''){
                $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Empty Question');
                $('#messageBody').html('Please enter the question, as empty questions are not allowes and might cause errors.');
                $('#QnAddModal').modal('hide');
                $('#messageModal').modal('show');
                return null;
            }
        }
        if($('.tags').val() == ''){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Incomplete field');
            $('#messageBody').html('Please add tags. Tags help to identify questions');
            $('#QnAddModal').modal('hide');
            $('#messageModal').modal('show');
            return null;
        }
        
        $('.option-table').children('tbody').children('tr').each(function(){
            if($(this).children('td').eq(1).children('.answer-type').val() == 'text'){
                if($(this).children('td').eq(2).children('.option-text').val() == ''){
                    $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Empty Option');
                    $('#messageBody').html('An Empty option is found. Please make sure that all options are filled');
                    $('#QnAddModal').modal('hide');
                    $('#messageModal').modal('show');
                    return null;
                }
            }else if($(this).children('td').eq(1).children('.answer-type').val() == 'image'){
                if($(this).children('td').eq(2).children('.option-image').val() == ''){
                    $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Empty Option');
                    $('#messageBody').html('An Empty option is found. Please make sure that all options are filled');
                    $('#QnAddModal').modal('hide');
                    $('#messageModal').modal('show');
                    return null;
                }else{
                    var filename = $(".option-image").val();
                    var extension = filename.replace(/^.*\./, '');
                    extension = extension.toLowerCase();
                    switch (extension) {
                        case 'jpg':
                        case 'jpeg':
                        case 'png':
                            break;
                        default:
                            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Wrong Image Format in Option');
                            $('#messageBody').html('Please choose any valid image formats. Only png, jpg and JPEG image formats are allowed');
                            $('#QnAddModal').modal('hide');
                            $('#messageModal').modal('show');
                            return null;
                    }
                }
            }
        });
        
        
        if(!$('[name="correct-answer"]:checked').val()){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Correct answer not specified');
            $('#messageBody').html('Correct answer for the question is not specified. Please choose the correct answerto complete the question.');
            $('#QnAddModal').modal('hide');
            $('#messageModal').modal('show');
        }
        
    });
});