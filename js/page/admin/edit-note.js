 $(document).ready(function(){
    $('.chapter-select').change(function(){
        var chapterid = $(this).val();
        $.ajax({
            url:'ajax/section-fetch.php',
            type:'POST',
            data:{
                chapterid : chapterid
            },
            success:function(result){
                $('.section-select').html(result);
            }
        });
    });
    

    
    
    $('.media-modal-btn').click(function(){
        mediaFileReload();
    });
    
    $('form#img-submit').on('submit',(function(e){
            var formData = new FormData($(this)[0]);
                    console.log(formData);
            $.ajax({
                url  : 'ajax/media-upload.php',
                type : 'POST',
                data : formData,
                cache:false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $('.media-files').html('<p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>');
                },
                success:function(result){
                    $('.media-files').html(result);
                },
                error:function(){
                    mediaFileReload();
                    $('.media-files').html('<p style="text-align: center"><span class="fa fa-exclamation"></span> Failed Uploading File.</p>'+$('.media-files').html(result))
                }
            }); 
            return false;
        
    }));
    
    
 });
 
 function mediaFileReload(){
    $.ajax({
        url:'ajax/media-fetch.php',
        type:'POST',
        cache:false,
        success:function(result){
            $('.media-files').html(result);
        }
    });
}