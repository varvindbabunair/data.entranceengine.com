$(document).ready(function(){
    $('.class-list').change(function(){
        var pass_val = $(this).val();
        $.ajax({
            url:'ajax/get-subject-qnadd.php',
            type:'POST',
            data:{
                class : pass_val
            },
            beforeSend:function(){
                $('.subjects,.chapters').hide();
                $('.subjects,.subjects-head').show();
                $('.subjects').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.subjects').html(result);
            },
            error:function(){
                $('.subjects').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });

    $('body').on('click','.subject-list',function(){
        var pass_val = $(this).attr('subject_var');
        $('.subject-list').removeClass('selected');
        $(this).addClass('selected');
        $.ajax({
            url:'ajax/get-chapters-qnadd.php',
            type:'POST',
            data:{
                class : pass_val
            },
            beforeSend:function(){
                $('.chapters').hide();
                $('.chapters,.chapters-head').show();
                $('.chapters').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.chapters').html(result);
            },
            error:function(){
                $('.chapters').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });


    $('body').on('click','.chapter-list',function(){
        $('.chapter-list').removeClass('selected');
        $(this).addClass('selected');
    });
    
    
    $('.media-modal-btn').click(function(){
    mediaFileReload();
    });
    
    $('form#img-submit').on('submit',(function(e){
            var formData = new FormData($(this)[0]);
                    console.log(formData);
            $.ajax({
                url  : 'ajax/media-upload.php',
                type : 'POST',
                data : formData,
                cache:false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $('.media-files').html('<p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>');
                },
                success:function(result){
                    $('.media-files').html(result);
                },
                error:function(){
                    mediaFileReload();
                    $('.media-files').html('<p style="text-align: center"><span class="fa fa-exclamation"></span> Failed Uploading File.</p>'+$('.media-files').html(result))
                }
            }); 
            return false;
        
    }));
    
    $('#theory-edit-submit').click(function(e){
        var err = 0;
        
        $('.subject-id').val($('li.subject-list.selected').attr('subject_var'));
        $('.chapter-id').val($('li.chapter-list.selected').attr('subject_var'));
        
        if($('.chapter-id').val().trim() == ''){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Incomplete field');
            $('#messageBody').html('Please Select chapter of the inserted quiestion.');
            $('#messageModal').modal('show');

            err = 1;
        }

        if($('.subject-id').val().trim() == ''){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Incomplete field');
            $('#messageBody').html('Please Select Subject of the inserted quiestion.');
            $('#messageModal').modal('show');

            err = 1;
        }
        if($('.class-name').val().trim() == ''){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Incomplete field');
            $('#messageBody').html('Please Select Class of the inserted quiestion.');
            $('#messageModal').modal('show');

            err = 1;
        }
        
        if(err == 1){
            e.preventDefault();
            return false;
        }
    });
    
});

function mediaFileReload(){
    $.ajax({
        url:'ajax/media-fetch.php',
        type:'POST',
        cache:false,
        success:function(result){
            $('.media-files').html(result);
        }
    });
}