$(document).ready(function(){
    
    var examid = $('#getid').val();
    var session= $('#getsession').val();
    var package= $('#getpackage').val();
    var user= $('#getuser').val();
    $.ajax({
        url:'../../ajax/test.php',
        type:'POST',
        data:{
            examid  : examid,
            session : session,
            package : package,
            user    : user
        },
        beforeSend:function(){

        },
        success:function(result,status){
            $('body').html(result);
        },
        error:function(){
            $('body').html('result 1');
        }
    });
    
//    $("body").on("contextmenu",function(e){
//        return false;
//    });
    
    
    $('body').on('click','.palette-btn',function(){
        if($(this).hasClass('btn-default')){
            $(this).removeClass('btn-default');
            $(this).addClass('btn-danger');
        }
        if($('#palette-'+$('.question:visible').attr('question-id')).hasClass('btn-danger')){
            $('input[type="radio"]:visible').attr('checked',false);
        }
        $('.question:visible').hide();
        $($(this).attr('data-target')).show();
    });
    
    

    
    $('body').on('click','#clear-answer',function(){
        $('.question:visible').children('ul').children('.answer').children('input').attr('checked',false);
//        $('#palette-'+$('.question:visible').attr('id'))
        if($($('li.answer:visible').eq(0).attr('data-target')).hasClass('btn-success')){
            $($('li.answer:visible').eq(0).attr('data-target')).removeClass('btn-success');
            $($('li.answer:visible').eq(0).attr('data-target')).addClass('btn-danger');
        }else if($($('li.answer:visible').eq(0).attr('data-target')).hasClass('btn-info')){
            $($('li.answer:visible').eq(0).attr('data-target')).removeClass('btn-info');
            $($('li.answer:visible').eq(0).attr('data-target')).addClass('btn-danger');
        }
    });


    $('body').on('click','#review-answer',function(){
        if($($('li.answer:visible').eq(0).attr('data-target')).hasClass('btn-success')){
            $($('li.answer:visible').eq(0).attr('data-target')).removeClass('btn-success');
            $($('li.answer:visible').eq(0).attr('data-target')).addClass('btn-info');
        }else if($($('li.answer:visible').eq(0).attr('data-target')).hasClass('btn-danger')){
            $($('li.answer:visible').eq(0).attr('data-target')).removeClass('btn-danger');
            $($('li.answer:visible').eq(0).attr('data-target')).addClass('btn-info');
        }else if($($('li.answer:visible').eq(0).attr('data-target')).hasClass('btn-default')){
            $($('li.answer:visible').eq(0).attr('data-target')).removeClass('btn-default');
            $($('li.answer:visible').eq(0).attr('data-target')).addClass('btn-info');
        }
        
        var currentelemNo = $('.question:visible').index() + 1;
        if($('.section:visible').children('.question').length == currentelemNo){
             $('.question:visible').hide();
             $('.section:visible').children('.question').eq(0).show();
        }else{
            $('.question:visible').hide();
            $('.section:visible').children('.question').eq(currentelemNo).show();
            if($($('li.answer:visible').eq(0).attr('data-target')).hasClass('btn-default')){
                $($('li.answer:visible').eq(0).attr('data-target')).removeClass('btn-default');
                $($('li.answer:visible').eq(0).attr('data-target')).addClass('btn-danger');
            }
        }
//        alert($('.question:visible').index());
    });
    
    
   $('body').on('mouseenter', '.dropdownbtn', function() {
       if($($(this).attr('section-target')).attr('attended') == 'true'){
            $(this).parent('.btn-group').children('ul').children('li').eq(1).children('a').children('.btn-danger').html($($(this).attr('data-target')).eq(1).children('.btn-danger').length);
            $(this).parent('.btn-group').children('ul').children('li').eq(3).children('a').children('.btn-info').html($($(this).attr('data-target')).eq(1).children('.btn-info').length);
            $(this).parent('.btn-group').children('ul').children('li').eq(0).children('a').children('.btn-default').html($($(this).attr('data-target')).eq(1).children('.btn-default').length);
            $(this).parent('.btn-group').children('ul').children('li').eq(2).children('a').children('.btn-success').html($($(this).attr('data-target')).eq(1).children('.btn-success').length);
        }else{
            $(this).parent('.btn-group').children('ul').children('li').eq(1).children('a').children('.btn-danger').html('0');
            $(this).parent('.btn-group').children('ul').children('li').eq(3).children('a').children('.btn-info').html('0');
            $(this).parent('.btn-group').children('ul').children('li').eq(0).children('a').children('.btn-default').html('0');
            $(this).parent('.btn-group').children('ul').children('li').eq(2).children('a').children('.btn-success').html('0');
        }
        $(this).parent('.btn-group').children('ul').children('li').eq(5).children('a').children('strong').children('span').html($($(this).attr('data-target')).eq(1).children('.btn').length);
        $($(this).attr('drop-target')).stop(true, true).delay(200).fadeIn(500);
   }).on('mouseleave', '.dropdownbtn', function() {
       $($(this).attr('drop-target')).stop(true, true).delay(200).fadeOut(500);
   });
   
   
   $('body').on('click', '.dropdownbtn', function() {
      if(!$(this).hasClass('btn-primary')){
        $('.dropdownbtn').removeClass('btn-primary');
        $('.dropdownbtn').addClass('btn-default');
        $(this).removeClass('btn-default');
        $(this).addClass('btn-primary');
        $($(this).attr('section-target')).attr('attended','true');
        $('.section').hide();
        $($(this).attr('data-target')).show();
      }
   });
   

    var html = $('#left-time').html()
    setInterval(function(){ 
         if($('#left-time').html() != '00:00'){
             $('.question:visible').attr('time-spent',parseInt($('.question:visible').attr('time-spent'))+1);
         }
    }, 1000);

        
    $('body').on('click','#view-next',function(){
        
        var currentelemNo = $('.question:visible').index() + 1;
        if($($('input:visible[type="radio"]:checked').parent('li').attr('data-target')).hasClass('btn-danger')){
            $($('input:visible[type="radio"]:checked').parent('li').attr('data-target')).removeClass('btn-danger');
            $($('input:visible[type="radio"]:checked').parent('li').attr('data-target')).addClass('btn-success');
        }else if($($('input:visible[type="radio"]:checked').parent('li').attr('data-target')).hasClass('btn-info')){
            $($('input:visible[type="radio"]:checked').parent('li').attr('data-target')).removeClass('btn-info');
            $($('input:visible[type="radio"]:checked').parent('li').attr('data-target')).addClass('btn-success');
        }
        
        if($('.section:visible').children('.question').length == currentelemNo){
            $('.question:visible').hide();
            $('.section:visible').children('.question').eq(0).show();
        }else{
            
            if($('.section:visible').eq(1).children('.palette-btn').eq(currentelemNo).hasClass('btn-default')){
                $('.section:visible').eq(1).children('.palette-btn').eq(currentelemNo).removeClass('btn-default');
                $('.section:visible').eq(1).children('.palette-btn').eq(currentelemNo).addClass('btn-danger');
            }
            $('.question:visible').hide();
            $('.section:visible').children('.question').eq(currentelemNo).show();
        }
    });


    $('body').on('click','#submitExam',function(){
        var cnf = confirm('You are about to submit the exam. Are you sure you want to submit.');
        if(cnf)
        testExpired();
    });



});

$(document).keydown(function(e){
  var key = e.charCode || e.keyCode;
  var allowedkey = [48,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105,110];
  if($.inArray(key,allowedkey) == -1){
      e.preventDefault();
  }
});


function testExpired(){
    var answerString = '[';
    $('.question-section').each(function(){
        answerString = answerString +'[';
        $(this).children('.question').each(function(){
            answerString = answerString+'["'+ $(this).attr('question-id')+'","';
            if($('input[name=answer-'+$(this).attr('question-id')+']:checked').val()==null){
                answerString = answerString+'';
            }else{
                answerString = answerString+$('input[name=answer-'+$(this).attr('question-id')+']:checked').val();
            }
            answerString = answerString+'","'+$(this).attr('time-spent')+'"],';
        });
        answerString = answerString.replace(/,\s*$/, "");
        answerString = answerString +'],';
    });
    answerString = answerString.replace(/,\s*$/, "");
    answerString = answerString +']';
//    alert(answerString);
    $('input[name="answer-string"]').val(answerString);
    $('#test-submit').submit();
}

