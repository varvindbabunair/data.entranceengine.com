$(document).ready(function(){
    $('.subject-select').change(function(){
        var subjectid = $(this).val();
        $.ajax({
            url:'ajax/get-chapter-option.php',
            type:'POST',
            data:{
                subjectid : subjectid
            },
            success:function(result){
                $('.chapter-select').html(result);
                $('#sortable1').html('');
                $('#sortable2').html('');
            }
        });
    });

    $('body').on('change', '.chapter-select', function() {
        var chapterid = $(this).val();
        $.ajax({
            url:'ajax/get-topic-option.php',
            type:'POST',
            data:{
                chapterid : chapterid
            },
            success:function(result){
                $('.topic-select').html(result);
                $('#sortable1').html('');
                $('#sortable2').html('');
            }
        });
    });

    $( "#sortable1" ).sortable({
        update: function() {
            $('#sortable1Count').html($('#sortable1 li').length);
            $('#sortable2Count').html($('#sortable2 li').length);
        }
    });

    $('body').on('change', '.topic-select', function(){
        var topic = $(this).val();
        $.ajax({
            url:'ajax/get-qns-test-create.php',
            type:'POST',
            data:{
                topic : topic
            },
            success:function(result){
                $('#sortable1').html(result);
                $('#sortable2').html('');
                MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
                $('#sortable1Count').html($('#sortable1 li').length);
            }
        });
    });


    $('#create-test').click(function(){
        var sectionTime = $('.total-exam-time').val();
        var sectionTitle = $('.subject-select option:selected').text();
        var sectionrule = ["any",sectionTime];
        var qnser = $('#sortable2').sortable('serialize',{attribute:'question',key: 'qn',expression: /(.+)/ });
        var questions = qnser.replace(/&qn=/g,',').replace('qn=','');
        var subject = $('.subject-select').val();
        var chapter = $('.chapter-select').val();
        var topic = $('.topic-select').val();
        var testName = $('.test-name').val();
        var tags = $('.tags').val();

        $.ajax({
            url:'ajax/create-test.php',
            type:'POST',
            data:{
                title : testName,
                sectionrule : JSON.stringify(sectionrule),
                sectiontitle : JSON.stringify([sectionTitle]),
                questions : questions,
                subject :subject,
                tags : tags,
                chapter : chapter,
                topic : topic,
                type : 'topic'
            },
            beforeSend:function(){
                $('.loading-msg').html('Creating Test... ');
                $('.black-background-full').show();
            },
            success:function(result,status){
                alert('successfully created test');
                window.location.replace('?page=app-topic-test');
            },
            error:function(){
                alert('Some Error occured while creating test');
            }
        });
    });
    
});
