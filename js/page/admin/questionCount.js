$(document).ready(function(){

    $('body').on('click','.subject-list',function(){
        var pass_val = $(this).attr('subject_var');
        $('.subject-list').removeClass('selected');
        $(this).addClass('selected');
        $.ajax({
            url:'ajax/get-chaptersCount.php',
            type:'POST',
            data:{
                class : pass_val
            },
            beforeSend:function(){
                $('.chapters,.sections,.topics').hide();
                $('.chapters').show();
                $('.chapters').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.chapters').html(result);
            },
            error:function(){
                $('.chapters').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });

    $('body').on('click','.chapter-list',function(){
        var pass_val = $(this).attr('subject_var');
        $('.chapter-list').removeClass('selected');
        $(this).addClass('selected');
        $.ajax({
            url:'ajax/get-topicsCount.php',
            type:'POST',
            data:{
                class : pass_val
            },
            beforeSend:function(){
                $('.sections,.topics').hide();
                $('.topics').show();
                $('.topics').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.topics').html(result);
            },
            error:function(){
                $('.topics').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });
    
    $('body').on('click','.topic-list',function(){
        var pass_val = $(this).attr('subject_var');
        $('.topic-list').removeClass('selected');
        $(this).addClass('selected');
        $.ajax({
            url:'ajax/get-sections.php',
            type:'POST',
            data:{
                class : pass_val
            },
            beforeSend:function(){
                $('.sections').show();
                $('.sections').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.sections').html(result);
            },
            error:function(){
                $('.sections').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });


    // $('.class-list').change(function(){
    //     var pass_val = $(this).val();
    //     $.ajax({
    //         url:'ajax/get-subjectCount.php',
    //         type:'POST',
    //         async: true,
    //         data:{
    //             class : pass_val
    //         },
    //         beforeSend:function(){
    //             $('.subjects').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
    //         },
    //         success:function(result,status){
    //             $('.subjects').html(result);
    //         },
    //         error:function(){
    //             $('.subjects').html('<p>Oops!... Some Error Occured</p>');
    //         }
    //     });
    // });

    $('.class-list').change(function(){
        var pass_val = $(this).val();
        $.ajax({
            url:'ajax/get-chaptersCount.php',
            type:'POST',
            async: true,
            data:{
                class : pass_val
            },
            success:function(result,status){
                $('.subjects').show();
                $('.chapter').html(result);
            },
            error:function(){
                $('.subjects').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });

    $('.class-list').change(function(){
        var pass_val = $(this).val();
        $.ajax({
            url:'ajax/get-qnCount.php',
            type:'POST',
            async: true,
            data:{
                class : pass_val
            },
            success:function(result,status){
                
                $('.numb').html(result);
            },
            error:function(){
                $('.numb').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });

    $('.class-list').change(function(){
        var pass_val = $(this).val();
        $.ajax({
            url:'ajax/getsubjectpanel.php',
            type:'POST',
            
            data:{
                class : pass_val
            },
            success:function(result,status){
                
                $('.subjectpanel').html(result);
            },
            error:function(){
                $('.numb').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });
// table click event
    $('body').on('click','.chap-list',function(){
        var pass_val = $(this).attr('chapter_id');
        $.ajax({
            url:'ajax/get-qnCount.php',
            type:'POST',
            data:{
                class : pass_val
            },
            beforeSend:function(){
                $('.sections').show();
                $('.sections').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.numb').html(result);
            },
            error:function(){
                $('.sections').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });

    $('body').on('click','.chap-list',function(){
        var pass_val = $(this).attr('chapter_id');
        $.ajax({
            url:'ajax/get-chaptername.php',
            type:'POST',
            async:true,
            data:{
                class : pass_val
            },
            success:function(result,status){
                $('.chapter-title').html(result);
            },
            error:function(){
                $('.chapter-title').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });
    
    $('body').on('click','.chap-list',function(){
        var chapter_id = $(this).attr('chapter_id');
        $.ajax({
            url:'ajax/chaptercount.php',
            type:'POST',
            data:{
                chapter_id : chapter_id
            },
            success:function(result,status){
                $('.chapter-count').html(result);
            },
            error:function(){
                $('.chapter-title').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });
    
    $('body').on('click','.add-subject',function(event){
        event.preventDefault();
        var form = $('.subject-add-form')[0];
        var data = new FormData(form);
        $.ajax({
            type: "post",
            enctype: 'multipart/form-data',
            url: "ajax/add-subjects.php",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (result) {
                if(result == 'success'){
                    var classid = $('.class-list').val();
                    $.ajax({
                        url:'ajax/get-subject.php',
                        type:'POST',
                        data:{
                            class : classid
                        },
                        beforeSend:function(){
                            $('.chapters,.sections,.topics').hide();
                            $('.subjects').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
                        },
                        success:function(result,status){
                            $('.subjects').html(result);
                        },
                        error:function(){
                            $('.subjects').html('<p>OOps!... Some Error Occured</p>');
                        }
                    });
                }
            }
        });
    });
    
    $('body').on('click','.add-chapter',function(event){
        event.preventDefault();
        var form = $('.chapter-add-form')[0];
        var data = new FormData(form);
        var classid = $('.subject-select').val();
        $.ajax({
            type: "post",
            enctype: 'multipart/form-data',
            url: "ajax/add-chapter.php",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (result) {
                if(result == 'success'){
                    $.ajax({
                        url:'ajax/get-chapters.php',
                        type:'POST',
                        data:{
                            class : classid
                        },
                        beforeSend:function(){
                            $('.sections,.topics').hide();
                            $('.chapters').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
                        },
                        success:function(result,status){
                            $('.chapters').html(result);
                        },
                        error:function(){
                            $('.chapters').html('<p>OOps!... Some Error Occured</p>');
                        }
                    });
                }
            }
        });
    });
    
    $('body').on('click','.add-topic',function(event){
        event.preventDefault();
        var form = $('.topic-add-form')[0];
        var data = new FormData(form);
        var classid = $('.topic-select').val();
        $.ajax({
            type: "post",
            enctype: 'multipart/form-data',
            url: "ajax/add-topics.php",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (result) {
                if(result == 'success'){
                    $.ajax({
                        url:'ajax/get-topics.php',
                        type:'POST',
                        data:{
                            class : classid
                        },
                        beforeSend:function(){
                            $('.sections').hide();
                            $('.topics').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
                        },
                        success:function(result,status){
                            $('.topics').html(result);
                        },
                        error:function(){
                            $('.topics').html('<p>OOps!... Some Error Occured</p>');
                        }
                    });
                }
            }
        });
    });
    
    $('body').on('click','.add-section',function(event){
        event.preventDefault();
        var form = $('.section-add-form')[0];
        var data = new FormData(form);
        var classid = $('.section-select').val();
        $.ajax({
            type: "post",
            enctype: 'multipart/form-data',
            url: "ajax/add-sections.php",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (result) {
                if(result == 'success'){
                    $.ajax({
                        url:'ajax/get-sections.php',
                        type:'POST',
                        data:{
                            class : classid
                        },
                        beforeSend:function(){
                            $('.sections').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
                        },
                        success:function(result,status){
                            $('.sections').html(result);
                        },
                        error:function(){
                            $('.sections').html('<p>OOps!... Some Error Occured</p>');
                        }
                    });
                }
            }
        });
    });
});