$(document).ready(function(){
    $('.view-user').click(function(){
        var id = $(this).attr('editid');
        $.ajax({
            url:'ajax/admin-user-edit.php',
            type:'POST',
            data:{
                id : id
            },
            beforeSend:function(){
                $('.edit-body').html('<p style="text-align:center;"><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.edit-body').html(result);
            }
        });
    });
    
    $('.delete-user').click(function(){
        $('.delete-user').val($(this).attr('removeid'));
    });
    
    $('.reset-user').click(function(){
        $('.user-id').val($(this).attr('resetid'));
    });
});