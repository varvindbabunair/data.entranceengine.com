$(document).ready(function(){
    $('.subject-menu').click(function(){
        $('.subject-menu').removeClass('active');
        $(this).addClass('active');
        $('.package-contents').hide();
        $('.material-list').hide();
        $($(this).attr('data-target')).show();
        $('li.chapter-name').removeClass('active');
        $('li.test-name').removeClass('active');
        $('ul.nav-stacked').find('li:visible:first').addClass('active');
        $($('ul.nav-stacked:visible').find('li:first').attr('data-target')).show();
        $('li.material-name').removeClass('active');
        $('ul.pagination:visible').children('li').eq(0).addClass('active');
        
        var contentString = $(this).attr('data-target').split('-');
        var content = (contentString[0]=='#test')?$(this).attr('data-target'):$('ul.nav-stacked:visible').children('li:first').attr('data-target');
//        alert(content);
        var contentarr = content.split('-');
        content = (contentarr[0] == '#chapter')?$('ul.pagination:visible').children('li:first').attr('data-target'):content;
        $.ajax({
            url:'ajax/package.php',
            type:'POST',
            data:{
                content : content,
                package : $('#package').val(),
                user : $('#user-id').val()
            },
            beforeSend:function(){
                $('.content:visible').html('<p style="text-align:center;"><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.content:visible').html(result);
            },
            error:function(){
                $('.black-background-full').hide();
                $('.content:visible').html('');
                $('.modal-title').html('Error');
                $('.modal-message').html('Some Error occured while processing your request');
                $('#package-error-modal').modal('show');
            }
        });
    });
    
    $('.chapter-name,.test-name').click(function(){
        $('.chapter-name').removeClass('active');
        $('.test-name').removeClass('active');
        $(this).addClass('active');
        $('.material-list').hide();
        $($(this).attr('data-target')).show();
        $('li.material-name').removeClass('active');
        $('ul.pagination:visible').children('li').eq(0).addClass('active');
        
        var contentString = $(this).attr('data-target').split('-');
        var content = (contentString[0]=='#test')?$(this).attr('data-target'):$('ul.pagination:visible').children('li').eq(0).attr('data-target');
        
        $.ajax({
            url:'ajax/package.php',
            type:'POST',
            data:{
                content : content,
                package : $('#package').val(),
                user : $('#user-id').val()
            },
            beforeSend:function(){
                $('.content:visible').html('<p style="text-align:center;"><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.content:visible').html(result);
            },
            error:function(){
                $('.black-background-full').hide();
                $('.content:visible').html('');
                $('.modal-title').html('Error');
                $('.modal-message').html('Some Error occured while processing your request');
                $('#package-error-modal').modal('show');
            }
        });
    });
    
    $('.material-name').click(function(){
        $('.material-name').removeClass('active');
        $(this).addClass('active');
        
        $.ajax({
            url:'ajax/package.php',
            type:'POST',
            data:{
                content : $(this).attr('data-target'),
                package : $('#package').val(),
                user : $('#user-id').val()
            },
            beforeSend:function(){
                $('.content:visible').html('<p style="text-align:center;"><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.content:visible').html(result);
            },
            error:function(){
                $('.content:visible').html('');
                $('.modal-title').html('Error');
                $('.modal-message').html('Some Error occured while processing your request');
                $('#package-error-modal').modal('show');
            }
        });
    });
    
     
    $('.content').on('click', '.exam-open', function (){
        window.open('', 'examWindow','height='+screen.height+',width='+screen.width+',toolbar=0,location=0, directories=no, top=0 ,left=0 , channelmode=yes , titlebar=no , status=no, menubar=no, scrollbars=yes, resizable=yes, fullscreen=yes , copyhistory=no');
        $('#examForm'+$(this).attr('examid')).submit();
    });
    
    
});

function viewAnswersheet(){
    window.open('', 'answerSheetWindow','height='+screen.height+',width='+screen.width+',toolbar=0,location=0, directories=no, top=0 ,left=0 , channelmode=yes , titlebar=no , status=no, menubar=no, scrollbars=yes, resizable=yes, fullscreen=yes , copyhistory=no');
    $(this).form.submit();
};

function viewResult(){
    window.open('', 'resultWindow','height='+screen.height+',width='+screen.width+',toolbar=0,location=0, directories=no, top=0 ,left=0 , channelmode=yes , titlebar=no , status=no, menubar=no, scrollbars=yes, resizable=yes, fullscreen=yes , copyhistory=no');
    $(this).form.submit();
};