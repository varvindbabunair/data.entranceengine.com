$(document).ready(function(){
    $('.package-view').click(function(){
        $('#packageId').val($(this).attr('viewid'));
        if($(this).hasClass('upgrade')){
            $('#packageUpgrade').val('1');
            $('#upgradeFrom').val($(this).attr('upgradefrom'));
        }else{
            $('#packageUpgrade').val('0');
            $('#upgradeFrom').val('0');
        }
        
        var packageid = $(this).attr('viewid');
        var action = ($(this).hasClass('upgrade'))?'upgrade':'view';
        var studentGroup  = $(this).attr('viewgroup');
        $.ajax({
            url: 'ajax/dashboard.php',
            type:'POST',
            data : {
            	packageid : packageid,
            	action: action,
                viewGroup : studentGroup
            },
            success:function(result,status){
            	$('.view-modal-body').html(result);
       			$('#viewModal').modal("show");
            },
            error:function(){
            	alert('error');
            }
        });
    });
});