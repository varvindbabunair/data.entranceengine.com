$(document).ready(function(){
    $('input[name=paymode]').click(function(){
        if($(this).val() == 'net-banking'){
            $('#invoiceForm').attr('action','submit/payment.php');
            $('#invoiceForm').attr('target','');
            $('.pay-msg').html('Continue to make payment');
        }else if($(this).val() == 'dd-direct'){
            $('#invoiceForm').attr('action','submit/pdfinvoice.php');
            $('#invoiceForm').attr('target','_blank');
            $('.pay-msg').html('Click to Download Form');
        }
    });
});