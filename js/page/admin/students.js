$(document).ready(function(){
    get_students();
    $('.limit,.group,.search').change(function(){
        get_students();
    });
    
    $('body').on('click', '.viewDetails', function (){
        var student = $(this).attr('target-user');
        var action = 'view';
        $('#viewModal').modal('show');
        student_details(student,action);
    });
    
    $('body').on('click', '.editDetails', function (){
        var student = $(this).attr('target-user');
        var action = 'edit';
        $('#editModal').modal('show');
        student_details(student,action);
    });
    
    $('body').on('click', '.removeStudent', function (){
        $('#removeModal').modal('show');
        $('.remove-content').html('Are you sure you want to delete <b>'+$(this).attr('target-name')+'</b> from database');
        $('.remove-id').val($(this).attr('target-user'));
    });
    
    $('body').on('click', '.viewPackages', function (){
        $('#packageModal').modal('show');
        var student = $(this).attr('target-user');
        $.ajax({
            url:'ajax/student-package.php',
            type:'POST',
            data:{
                student  : student
            },
            beforeSend:function(){
                $('.package-content').html('<p style="text-align: center;"><span class="fa fa-spin fa-spinner"></span> Loading...</p>');
            },
            success:function(result){
                $('.package-content').html(result);
            },
            error:function(){
                $('.package-content').html('<h3 style="text-align:center;">some error occured</h3>');
            }
        });
    });
    
});

function student_details(student,action){
        var action = action;
        var student= student;
        $.ajax({
            url:'ajax/student-details.php',
            type:'POST',
            data:{
                student  : student,
                action : action
            },
            beforeSend:function(){
                $('.student-'+action).html('<p style="text-align: center;"><span class="fa fa-spin fa-spinner"></span> Loading...</p>');
            },
            success:function(result){
                $('.student-'+action).html(result);
            },
            error:function(){
                $('.student-'+action).html('<h3 style="text-align:center;">some error occured</h3>');
            }
        });
}

function get_students(){
    var limit = $('.limit').val();
    var group = $('.group').val();
    var search = $('.search').val();
    $.ajax({
        url:'ajax/student-user.php',
        type:'POST',
        data:{
            limit  : limit,
            group : group,
            search : search
        },
        beforeSend:function(){
            $('#student-list').html('<p style="text-align: center;"><span class="fa fa-spin fa-spinner"></span> Loading...</p>');
        },
        success:function(result){
            $('#student-list').html(result);
        },
        error:function(){
            $('#student-list').html('<h3 style="text-align:center;">some error occured</h3>');
        }
    });
}