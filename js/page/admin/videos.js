$(document).ready(function(){
    	
       $('.vid-play').click(function(){
           var vidid = $(this).attr('data-target');
           $.ajax({
                    url:'ajax/vid-fetch.php',
                    type:'POST',
                    data:{
                        vidid  : vidid
                    },
                    success:function(result,status){
                        $('#vidModal').modal('show');
                        $('.video-modal').html(result);
                    },
                    error:function(){
                        $('body').html('some error occured');
                    }
                });
           });
           
           $('#vidModal').on('hide.bs.modal', function () {
               $('iframe').attr('src','');
            });
        
       $('.qn-remove').click(function(){
        $('#qn-remove-id').val($(this).attr('removeid'));
    });
});