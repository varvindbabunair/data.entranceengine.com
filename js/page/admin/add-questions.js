$(document).ready(function(){
    $('.class-list').change(function(){
        var pass_val = $(this).val();
        $.ajax({
            url:'ajax/get-subject-qnadd.php',
            type:'POST',
            data:{
                class : pass_val
            },
            beforeSend:function(){
                $('.subjects,.chapters,.topics,.sections,.subjects-head,.chapters-head,.topics-head,.sections-head').hide();
                $('.subjects,.subjects-head').show();
                $('.subjects').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.subjects').html(result);
            },
            error:function(){
                $('.subjects').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });

    $('body').on('click','.subject-list',function(){
        var pass_val = $(this).attr('subject_var');
        $('.subject-list').removeClass('selected');
        $(this).addClass('selected');
        $.ajax({
            url:'ajax/get-chapters-qnadd.php',
            type:'POST',
            data:{
                class : pass_val
            },
            beforeSend:function(){
                $('.chapters,.sections,.topics,.chapters-head,.sections-head,.topics-head').hide();
                $('.chapters,.chapters-head').show();
                $('.chapters').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.chapters').html(result);
            },
            error:function(){
                $('.chapters').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });


    $('body').on('click','.chapter-list',function(){
        var pass_val = $(this).attr('subject_var');
        $('.chapter-list').removeClass('selected');
        $(this).addClass('selected');
        $.ajax({
            url:'ajax/get-topics-qnadd.php',
            type:'POST',
            data:{
                class : pass_val
            },
            beforeSend:function(){
                $('.topics,.sections,.topics-head,.sections-head').hide();
                $('.topics,.topics-head').show();
                $('.topics').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.topics').html(result);
            },
            error:function(){
                $('.topics').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });
    
    $('body').on('click','.topic-list',function(){
        var pass_val = $(this).attr('subject_var');
        $('.topic-list').removeClass('selected');
        $(this).addClass('selected');
        $.ajax({
            url:'ajax/get-sections-qnadd.php',
            type:'POST',
            data:{
                class : pass_val
            },
            beforeSend:function(){
                $('.sections,.sections-head').show();
                $('.sections').html('<p><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.sections').html(result);
            },
            error:function(){
                $('.sections').html('<p>OOps!... Some Error Occured</p>');
            }
        });
    });
    

    $('body').on('click','.section-list',function(){
        var pass_val = $(this).attr('subject_var');
        $('.section-list').removeClass('selected');
        $(this).addClass('selected');
        
    });

    

    $('#qn-add-submit').click(function(e){
        var err = 0;
        
        $('.class-name').val($('li.class-list.selected').attr('class_var'));
        $('.subject-id').val($('li.subject-list.selected').attr('subject_var'));
        $('.chapter-id').val($('li.chapter-list.selected').attr('subject_var'));
        $('.section-id').val($('li.section-list.selected').attr('subject_var'));
        $('.topic-id').val($('li.topic-list.selected').attr('subject_var'));
        $('.difficulty-val').val($('li.difficulty-list.selected').attr('class_var'));



        if($('.chapter-id').val().trim() == ''){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Incomplete field');
            $('#messageBody').html('Please Select chapter of the inserted quiestion.');
            // $('#QnAddModal').modal('hide');
            $('#messageModal').modal('show');

            err = 1;
        }

        if($('.subject-id').val().trim() == ''){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Incomplete field');
            $('#messageBody').html('Please Select Subject of the inserted quiestion.');
            // $('#QnAddModal').modal('hide');
            $('#messageModal').modal('show');

            err = 1;
        }
        if($('.class-name').val().trim() == ''){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Incomplete field');
            $('#messageBody').html('Please Select Class of the inserted quiestion.');
            // $('#QnAddModal').modal('hide');
            $('#messageModal').modal('show');

            err = 1;
        }
        if($('.tags').val() == ''){
            $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Incomplete field');
            $('#messageBody').html('Please add tags. Tags help to identify questions');
            // $('#QnAddModal').modal('hide');
            $('#messageModal').modal('show');

            err = 1;
        }  

        if($('.answer-category').val()=='numeric' || $('.answer-category').val()=='smcq' || $('.answer-category').val()=='mcq' || $('.answer-category').val()=='match'){
            var textQn =  CKEDITOR.instances['text-question'].getData();
            if(textQn.trim() == ''){
                $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Empty Question');
                $('#messageBody').html('Please Enter the question.');
                // $('#QnAddModal').modal('hide');
                $('#messageModal').modal('show');
                err = 1;
            }
        }else if($('.answer-category').val()=='assertion'){
            var assertion1 =  CKEDITOR.instances['asssertion-statement-1'].getData();
            var assertion2 =  CKEDITOR.instances['asssertion-statement-2'].getData();
            if(assertion1.trim() == '' || assertion2.trim() == ''){
                $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Empty Question Prompts');
                $('#messageBody').html('Please enter both the question prompts before submitting the question.');
                // $('#QnAddModal').modal('hide');
                $('#messageModal').modal('show');
                err = 1;
            }
        }

        if($('.answer-category').val()=='numeric'){
            if($('input.integer-answer').val() == ''){
                $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Empty Option');
                $('#messageBody').html('An Empty numeric answer. Please enter a value.');
                // $('#QnAddModal').modal('hide');
                $('#messageModal').modal('show');
                err = 1;
            }
        }else if($('.answer-category').val()=='smcq' || $('.answer-category').val()=='mmcq' || $('.answer-category').val()=='match'){
            var answerA =  CKEDITOR.instances['answer-a'].getData();
            var answerB =  CKEDITOR.instances['answer-b'].getData();
            var answerC =  CKEDITOR.instances['answer-c'].getData();
            var answerD =  CKEDITOR.instances['answer-d'].getData();
            if(answerA.trim() == '' || answerB.trim() == '' || answerC.trim() == '' || answerD.trim() == ''){
                $('#messageTitle').html('<span class="fa fa-exclamation-triangle"></span> Empty Option');
                $('#messageBody').html('An Empty option is found. Please enter all the options.');
                // $('#QnAddModal').modal('hide');
                $('#messageModal').modal('show');
                err = 1;
            }
        }

        
        if(err == 1){
            e.preventDefault();
            return false;
        }
    });
    
    
    $('.media-modal-btn').click(function(){
        mediaFileReload();
    });
    
    $('form#img-submit').on('submit',(function(e){
            var formData = new FormData($(this)[0]);
                    console.log(formData);
            $.ajax({
                url  : 'ajax/media-upload.php',
                type : 'POST',
                data : formData,
                cache:false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $('.media-files').html('<p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>');
                },
                success:function(result){
                    $('.media-files').html(result);
                },
                error:function(){
                    mediaFileReload();
                    $('.media-files').html('<p style="text-align: center"><span class="fa fa-exclamation"></span> Failed Uploading File.</p>'+$('.media-files').html(result))
                }
            }); 
            return false;
        
    }));

    $('.answer-category').change(function(){
        if ($(this).val() == 'smcq') {
            $('.mcq-answer').show();
            $('.integer-answer').hide();
            $('.assertion-question').hide();
            $('.text-question').show();
            $('.answer-option').attr('type','radio');
            $('.answer-option').attr('name','correct-answer');
            $('.assertion-answer').hide();
            $('.mcq-answer').show();
        }else if ($(this).val() == 'numeric'){
            $('.mcq-answer').hide();
            $('.integer-answer').show();
            $('.assertion-answer').hide();
            $('.assertion-question').hide();
            $('.text-question').show();
        }else if ($(this).val() == 'mmcq') {
            $('.mcq-answer').show();
            $('.integer-answer').hide();
            $('.assertion-question').hide();
            $('.text-question').show();
            $('.answer-option').attr('type','checkbox');
            $('.answer-option').attr('name','correct-answer[]');
            $('.assertion-answer').hide();
            $('.mcq-answer').show();
        }else if ($(this).val() == 'assertion') {
            $('.mcq-answer').show();
            $('.integer-answer').hide();
            $('.assertion-answer').show();
            $('.assertion-question').show();
            $('.text-question').hide();
            $('.mcq-answer').hide();
        }else if ($(this).val() == 'match') {
            $('.mcq-answer').show();
            $('.integer-answer').hide();
            $('.assertion-question').hide();
            $('.text-question').show();
            $('.answer-option').attr('type','radio');
            $('.answer-option').attr('name','correct-answer');
            $('.assertion-answer').hide();
            $('.mcq-answer').show();
        }
    });
    
    
    
});

function mediaFileReload(){
    $.ajax({
        url:'ajax/media-fetch.php',
        type:'POST',
        cache:false,
        success:function(result){
            $('.media-files').html(result);
        }
    });
}