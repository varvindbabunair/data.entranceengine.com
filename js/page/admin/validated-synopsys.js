$(document).ready(function(){
       
    $('body').on('click', '.qn-view', function (){
        var qnid = $(this).attr('qnid');
        $.ajax({
            url:'ajax/synopsys-view-fetch.php',
            type:'POST',
            async: false,
            data:{
                qnid : qnid
            },
            success:function(result){
                $('.qn-content').html(result);
                MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            }
        });
            
    });
    $('#qn-table').dataTable( {
        "order": [[ 0, "desc" ]],
        "fnDrawCallback": function( oSettings ) {
          MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
        }
      } );   
        
});