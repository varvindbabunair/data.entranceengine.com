$(document).ready(function(){
    $(".datetimepicker").datetimepicker();
      
    $('.test-container').sortable({
        receive: function( event, ui ) {
            if($(ui.item).hasClass('notes')){
                $(ui.sender).sortable('cancel');
            }else if($(ui.item).hasClass('chapter')){
                $(ui.sender).sortable('cancel');
            }else if($(ui.item).hasClass('subject')){
                $(ui.sender).sortable('cancel');
            }
            
//            $(ui.item).children('.test-date-picker').hide();
        },
        connectWith: ".connectsort"
    });
    $('.chapter-container').sortable({
        receive: function( event, ui ) {
            if($(ui.item).hasClass('notes')){
                $(ui.sender).sortable('cancel');
            }else if($(ui.item).hasClass('subject')){
                $(ui.sender).sortable('cancel');
            }
            if($(ui.item).parent('div') != $(ui.sender).parent('div') && !$(ui.item).hasClass('test')){
                $(ui.sender).sortable('cancel');
            }
//            $(ui.item).children('.test-date-picker').show();
        },
        connectWith: ".connectsort"
    });
    $('.package-container').sortable({
        receive: function( event, ui ) {
            if($(ui.item).hasClass('notes')){
                $(ui.sender).sortable('cancel');
            }else if($(ui.item).hasClass('chapter')){
                $(ui.sender).sortable('cancel');
            }
            if($(ui.item).parent('div') != $(ui.sender).parent('div') && !$(ui.item).hasClass('test')){
                $(ui.sender).sortable('cancel');
            }
//            $(ui.item).children('.test-date-picker').show();
        },
        connectWith: ".connectsort"
    });
    $('.notes-container').sortable({
        receive: function( event, ui ) {
            if($(ui.item).hasClass('chapter')){
                $(ui.sender).sortable('cancel');
            }else if($(ui.item).hasClass('subject')){
                $(ui.sender).sortable('cancel');
            }
            if($(ui.item).parent('div') != $(ui.sender).parent('div') && !$(ui.item).hasClass('test')){
                $(ui.sender).sortable('cancel');
            }
//            $(ui.item).children('.test-date-picker').show();
        },
        connectWith: ".connectsort"
    });
    
    $('.subject-check').change(function(){
        if($(this).is(":checked")){ 
            $(this).parent('.subject-name-container').removeClass('strikeout');
            $(this).parent('.subject-name-container').parents('.subject').children('.chapter-container').show();
        }else{
            $(this).parent('.subject-name-container').addClass('strikeout');
            $(this).parent('.subject-name-container').parents('.subject').children('.chapter-container').hide();
        }
    });
    $('.chapter-check').change(function(){
        if($(this).is(":checked")){
            $(this).parent('div').removeClass('strikeout');
            $(this).parent('div').parents('.chapter').children('.notes-container').show();
        }else{
            $(this).parent('div').addClass('strikeout');
            $(this).parent('div').parents('.chapter').children('.notes-container').hide();
        }
    });
    $('.note-check').change(function(){
        if($(this).is(":checked")){
            $(this).parent('div').removeClass('strikeout');
        }else{
            $(this).parent('div').addClass('strikeout');
        }
    });
    
    CKEDITOR.replace( 'package-features', {
        toolbar: [
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
            { name: 'insert', items: ['Table', 'HorizontalRule', 'Smiley', 'SpecialChar' ] },
            '/',
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        ]
    });
    
    $('.create-package-button').click(function(){
        var chapterSortedArray = [];
        var notesSortedArray = [];
        var sorted = $('.package-container').sortable('serialize',{attribute:'id'});
//        alert(sorted);
        $('.subject').each(function(){
            if(!$(this).children('.subject-name-container').children('input').is(':checked')){
                var replaceStr = $(this).attr('id').replace('_','[]=');
                sorted = sorted.replace(replaceStr,'');
            }else{
                var notesLong = '';
                var chapterSorted = $(this).children('.chapter-container').sortable('serialize',{attribute:'id'});
                $(this).children('.chapter-container').children('.chapter').each(function(){
                    if(!$(this).children('.chapter-name-container').children('input').is(':checked')){
                        chapterSorted = chapterSorted.replace($(this).attr('id').replace('_','[]='),'');
                    }else{
                        var notesSorted = $(this).children('.notes-container').sortable('serialize',{attribute:'id'});
                        $(this).children('.notes-container').children('.notes').each(function(){
                            if(!$(this).children('.note-check').is(':checked')){
                                notesSorted = notesSorted.replace($(this).attr('id').replace('_','[]='),'');
                            }
                        });
                        var notesSortedAr = notesSorted.split('&');
                        notesSortedAr = notesSortedAr.filter(function(v){return v!=='';});
                        notesSorted = notesSortedAr.join('"],["');
                        if(notesSorted != ''){
                            notesSorted = '["'+notesSorted+'"]';
                            var notesSortedAr = notesSorted.split('[]=');
                            notesSorted = notesSortedAr.join('","');
                            notesLong = notesLong + '],[' + notesSorted;
                        }else{
                            notesSorted = '[]';
                            var notesSortedAr = notesSorted.split('[]=');
                            notesSorted = notesSortedAr.join('","');
                            notesLong = notesLong + '],[' + notesSorted;
                        }
                    }
                });
                if(notesLong != ''){
                    notesLong = notesLong.replace('],','') + ']';
                }
                notesSortedArray.push(notesLong);
                var chapterSortedArr = chapterSorted.split('&');
                chapterSortedArr = chapterSortedArr.filter(function(v){return v!=='';});
                chapterSorted = chapterSortedArr.join('"],["');
                if(chapterSorted != ''){
                    chapterSorted = '["'+chapterSorted+'"]';
                    var chapterSortedArr = chapterSorted.split('[]=');
                    chapterSorted = chapterSortedArr.join('","');
                }
                chapterSortedArray.push(chapterSorted);
            }
        });
        var sortedArr = sorted.split('&');
        sortedArr = sortedArr.filter(function(v){return v!=='';});
        sorted = sortedArr.join('"],["');
        if(sorted != ''){
            sorted = '[["'+sorted+'"]]';
            var sortedArr = sorted.split('[]=');
            sorted = sortedArr.join('","');
        }
        $('#subject-list').val(sorted);
//        alert(sorted);
        
        var chapterSortedString = chapterSortedArray.join('],[');
        if(chapterSortedString != ''){
            chapterSortedString = '[['+chapterSortedString+']]';
        }
        $('#chapter-list').val(chapterSortedString);
//        alert(chapterSortedString);
        
        var notesLongList = notesSortedArray.join('],[');
        notesLongList = '[[' + notesLongList + ']]';
        $('#notes-list').val(notesLongList);
//        alert(notesLongList);

//        return false;
    });
    
    $('#user-group').change(function(){
        var id = $(this).val();
        $.ajax({
            url:'ajax/package-option.php',
            type:'POST',
            data:{
                id : id
            },
            success:function(result,status){
                $('#base-package').html(result);
            },
            error:function(){
                $('.black-background-full').hide();
                $('.content:visible').html('');
                $('.modal-title').html('Error');
                $('.modal-message').html('Some Error occured while processing your request');
                $('#package-error-modal').modal('show');
            }
        });
    });
    
     $('#base-package').change(function(){
         if($(this).val() != 0){
             $('.upgrade-pricing').show();
         }else{
             $('.upgrade-pricing').hide();
         }
     });
});