$(document).ready(function(){
    	
       $('.user-id, .days').change(function(){
           var user = $('.user-id').val();
           var days = $('.days').val();
           $.ajax({
                    url:'ajax/get-report.php',
                    type:'POST',
                    data:{
                        user: user,
                        days: days
                    },
                    beforeSend:function(){
                        $('.panel-body').html('<h6 style="text-align:center"><span class="fa fa-spin fa-spinner"></span> Loading...</h6>');
                    },
                    success:function(result){
                        $('.panel-body').html(result);
                    },
                    error:function(){
                        $('body').html('some error occured');
                    }
                });
           });
           
});