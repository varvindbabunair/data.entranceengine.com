$(document).ready(function(){
    
    $('.view-package').click(function(){
        var id = $(this).attr('viewid');
        $.ajax({
            url:'ajax/view-package.php',
            type:'POST',
            data:{
                id : id
            },
            beforeSend:function(){
                $('.view-content').html('<p style="text-align:center;"><span class="fa fa-spinner fa-spin"></span> Loading...</p>');
            },
            success:function(result,status){
                $('.view-content').html(result);
            }
        });
    });
    
    $('.remove-package').click(function(){
        var id = $(this).attr('removeid');
        $('.delete-id').val(id);
    });
    
    $('.copy-package').click(function(){
        var id = $(this).attr('copyid');
        $('.copy-id').val(id);
    });
    
});