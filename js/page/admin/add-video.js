$(document).ready(function(){
    $('.chapter-select').change(function(){
        var chapterid = $(this).val();
        $.ajax({
            url:'ajax/section-fetch.php',
            type:'POST',
            data:{
                chapterid : chapterid
            },
            success:function(result){
                $('.section-select').html(result);
            }
        });
    });
    
    CKEDITOR.replace( 'editor1', {
        toolbar: [
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
            { name: 'insert', items: ['Table', 'HorizontalRule', 'Smiley', 'SpecialChar','EqnEditor','Image' ] },
            '/',
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        ]
    });
});