$(document).ready(function(){
	$('.qn-review').click(function(){
        var qnid = $(this).attr('qnid');
        $.ajax({
            url:'ajax/qn-review-fetch.php',
            type:'POST',
            data:{
                qnid : qnid
            },
            success:function(result){
                $('.qn-review-content').html(result);
            }
        });
    });

    $('.qn-view').click(function(){
        var qnid = $(this).attr('qnid');
        $.ajax({
            url:'ajax/qn-view-fetch.php',
            type:'POST',
            data:{
                qnid : qnid
            },
            success:function(result){
                $('.qn-content').html(result);
            }
        });
    });
});