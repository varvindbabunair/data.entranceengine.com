$(document).ready(function(){
    $('.test-subjects').val("");
    $('.test-tags').val("");
    $('.section-name').hide();
    $('.section-rule').val('any');
    
    $(function () {
        $('#datetimepicker6').datetimepicker();
        $('#datetimepicker7').datetimepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
    
    CKEDITOR.replace( 'instruction', {
        toolbar: [
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote','-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
            { name: 'insert', items: ['Table', 'HorizontalRule', 'Smiley', 'SpecialChar' ] },
            '/',
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        ]
    });
    
    
    $('#general-instruction-check').change(function(){
        if($('#general-instruction-check').is(':checked')){
            CKEDITOR.instances['instruction'].setData($('.general-instruction').html());
        }else{
            CKEDITOR.instances['instruction'].setData('');
        }
    });
    
    
    $('.test-sections').change(function(){
        var totalSections = $( 'div.section' ).size();
        var sectionRequired = $(this).val();
        
        if(totalSections > 1){
            $('div.added-section').remove();
        }
        if(sectionRequired > 1){
            var htmlString = '';
            for(var j = 1;j<sectionRequired;j++){
                var addHtml = '<div class="section added-section" style="width:100%; overflow: hidden;">' + $('div.section').eq(0).html() + '</div>';
                
                var htmlArray = addHtml.split('<div class="clearfix"></div>');
                $('#selectChoose select').val("");
                addHtml = htmlArray[0]+ $('#selectChoose').html()+htmlArray[2];
                
                var nextval = j+1;
                addHtml = addHtml.replace(/test-subjects1/g,'test-subjects'+nextval);
                addHtml = addHtml.replace(/test-tags1/g,'test-tags'+nextval);
                
                var sortval1 = (j*2)+1;
                addHtml = addHtml.replace(/sortable1/g,'sortable'+sortval1);
                var sortval2 = (j*2)+2;
                addHtml = addHtml.replace(/sortable2/g,'sortable'+sortval2);
                
                htmlString = htmlString + addHtml;
            }
            $('.section').after(htmlString);
            
            for(var k = 1;k<sectionRequired;k++){
                $('.ui-sortable').eq(k*2).html("");
                $('.ui-sortable').eq(k*2+1).html("");
            }
        }
        var totalTitle = $( '.section-number' ).size();
        for(var i = 0;i<totalTitle;i++){
            var j = i+1;
            $( '.section-number').eq(i).html(j);
        }
        if(totalTitle == 1){
            $('.section-name').hide();
            $('.section-rule').val('any');
            $('.section-indtime').hide();
            $('.exam-time').show();
        }else{
            $('.section-name').show();
        }
        
    });
    
    $('.section-rule').change(function(){
        if($(this).val() == 'continuous'){
            $('.exam-time').hide();
            $('.section-indtime').show();
        }else{
            $('.exam-time').show();
            $('.section-indtime').hide();
        }
    });
    
    
    
    $('#save-test').click(function(){
        var id = $('.edit-id').val();
        var noofSection = $('.test-sections').val();
        
        var sectionTime =  $('.total-exam-time').val();
        var temptitle = [];
        $('.section-title').each(function(){
            temptitle.push(this.value);
        });
        var sectionTitle = temptitle;
        var sectionrule = ["any",sectionTime];
        var globalPlus = $('#correct-answer-mark').val();
        var globalMinus = ($('#wrong-answer-mark-type').val() == 'percent') ? globalPlus*$('#wrong-answer-mark').val()/100 : $('#wrong-answer-mark').val(); 
        var qn=[];
        var qnSection=[];
        
        var Instruction = CKEDITOR.instances['instruction'].getData();
        var minusMark = 0;
        var plusMark = 0;
        
        for(var num=0;num < noofSection;num++){
            var serid = (num+1)*2;
            var qnser = $('#sortable'+ serid).sortable('serialize',{attribute:'id'});
            var qnarr = qnser.toString().split('&');
            var qnPlusarr = [];
            var qnMinusarr = [];
            for(var n=0;n<qnarr.length;n++){
                var qnSelect = '#'+qnarr[n].replace('[]=','_');
                var qnSelectsubPlus = qnSelect+'_plus';
                var telem = '#sortable'+ serid;
                telem = telem.toString();
                var plusElem = $(telem).children(qnSelect).children('.mark').children('.input-group').children(qnSelectsubPlus);
                plusMark =( plusElem.val()== '' || plusElem.val()== 0)? globalPlus:plusElem.val();
                qnPlusarr.push(plusMark);
                var qnSelectsubMinus = qnSelect+'_minus';
                var minusElem = $('#sortable'+ serid).children(qnSelect).children('.mark').children('.input-group').children(qnSelectsubMinus);
                minusMark =( minusElem.val()== '' || minusElem.val()== 0)?globalMinus:minusElem.val();
                qnMinusarr.push(minusMark);
                qnarr[n] = qnarr[n].replace('qn[]=','');
                
                qn.push(JSON.stringify([qnarr[n],plusMark,minusMark]));
//                alert('test');
            }
            qnSection.push(JSON.stringify(qn));
            qn = [];
        }
        var title = $('.test-name').val();
        var tags = $('.tags').val();
        $.ajax({
            url:'ajax/edit-test.php',
            type:'POST',
            data:{
                testname : title,
                sectionrule : JSON.stringify(sectionrule),
                sectiontitle : JSON.stringify(sectionTitle),
                questions : JSON.stringify(qnSection),
                instruction : Instruction,
                tags : tags,
                editid : id
            },
            beforeSend:function(){
                $('.loading-msg').html('Creating Test... ');
                $('.black-background-full').show();
            },
            success:function(result,status){
                $('.black-background-full').hide();
                $('.modal-title').html('Status '+status);
                $('.modal-message').html(result);
                $('#create-test-modal').modal('show');
            },
            error:function(){
                $('.black-background-full').hide();
                $('.modal-title').html('Error');
                $('.modal-message').html('Some Error occured while processing your request');
                $('#create-test-modal').modal('show');
            }
        });
    });
    
});
