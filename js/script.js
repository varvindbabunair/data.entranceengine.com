$(document).ready(function(){
    /* off-canvas sidebar toggle */
    $('[data-toggle=offcanvas]').click(function() {
        $('.row-offcanvas').toggleClass('active');
        $('.collapse').toggleClass('in').toggleClass('hidden-xs').toggleClass('visible-xs');
    });
    
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $('#passwordChangeSubmit').click(function(e){
        if($('#oldPass').val() != '' && $('#newPass').val() != '' && $('#cnfnewPass').val() != '' ){
            if($('#cnfnewPass').val() != $('#newPass').val()){
                alert('The new password and the confirmation password doesn\'t match');
                e.preventDefault();
            }else{
                $(this).parent('div').parent('form').submit();
            }
        }else{
            e.preventDefault();
        }
    });
    
    $('.datatable').DataTable({
        "order": [[ 0, "desc" ]]
    });
    
    
});
