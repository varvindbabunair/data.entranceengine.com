<html>
    <head>
        <link rel="stylesheet" href="../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../css/font-awesome.css" />
        <link rel="stylesheet" href="../../css/exam.css" />
        <script src="../../js/jquery-1.9.1.js" ></script>
        <script src="../../js/bootstrap.min.js"></script>
        <script src="../../js/page/exam-script.js"></script>
        <?php if(isset($_POST['session']) && isset($_SERVER['HTTP_REFERER'])){ ?>
        <script src="../../js/jquery.countdownTimer.min.js"></script>
        <?php } ?>
    </head>
    <body>
        <div class="col-lg-12" style="text-align: center;margin-top:20%">
            <?php echo (isset($_POST['session']) && isset($_SERVER['HTTP_REFERER']))?'<span class="fa fa-spinner fa-spin"></span> Loading...':'<span class="fa fa-exclamation-triangle"></span> <strong>Some Error Occured.</strong> Unable to load Exam'; ?>
            
        </div>
        <?php echo '<input id="getid" type="hidden" value="'.$_POST['examid'].'" />'; ?>
        <?php echo '<input id="getsession" type="hidden" value="'.$_POST['session'].'" />'; ?>
        <?php echo '<input id="getuser" type="hidden" value="'.$_POST['user'].'" />'; ?>
    </body>
    
    
</html>