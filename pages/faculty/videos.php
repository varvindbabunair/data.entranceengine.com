<?php
    include 'includes/materials.php';
    $videos = new Materials();
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Video <small> All Videos</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-alt"></i> Video
                </li>
                <li class="active">
                    <i class="fa fa-list-alt"></i> All Videos
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    


    
    <div class="row" style="">
        <div class="col-lg-12">
            <form  acton="" method="POST">
            <div class="col-lg-2">
                Display :
                <select name="display-number" onchange="this.form.submit()" class="form-control">
                    <option <?php echo (isset($_POST['display-number']) && $_POST['display-number'] == '100')?'selected':''; ?> value="100">100</option>
                    <option <?php echo (isset($_POST['display-number']) && $_POST['display-number'] == '500')?'selected':''; ?> value="500">500</option>
                    <option <?php echo (isset($_POST['display-number']) && $_POST['display-number'] == '1000')?'selected':''; ?> value="1000">1000</option>
                </select>
            </div>
            <div class="col-lg-3">
                Choose a Chapter :
                <select name="display-chapter" onchange="this.form.submit()" id="chapter-select" name="chapter" class="form-control" >
                    <option value="all">All Chapters</option>
                    <?php
                    $subject_list = $videos->get_subject_list();
                    foreach($subject_list as $subject){
                    ?>
                    <optgroup label="<?php echo $subject['title'] ?>">
                    <?php
                    $chapter_list = $videos->get_chapter_list($subject['id']);
                    foreach ($chapter_list as $chapter){
                    ?>
                        <option <?php echo (isset($_POST['display-chapter']) && $_POST['display-chapter'] == $chapter['id'])?'selected':''; ?> value="<?php echo $chapter['id']; ?>"><?php echo $chapter['title']; ?></option>
                    <?php
                    }
                    ?>
                    </optgroup>
                    <?php
                    }
                    ?>
                </select>
            </div>
                <div class="col-lg-2">
                    Section :
                    <select name="display-section" class="form-control" onchange="this.form.submit()">
                        <?php
                        if(!isset($_POST['display-chapter']) || $_POST['display-chapter'] =='all'){
                            echo '<option value="" >No chapter Selected</option>';
                        }else{
                            $cid = $_POST['display-chapter'];
                            $section_get_query = $videos->query("SELECT * FROM section_list WHERE chapter = '$cid'");
                            echo '<option value="" >Select Section</option>';
                            while($section = mysqli_fetch_array($section_get_query)){
                                echo '<option ';
                                echo ($_POST['display-section'] == $section['id'])?'selected':'';
                                echo ' value="'.$section['id'].'" >'.$section['title'].'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
            </form>
        
        </div>   
    </div>
    <div class="row">
        <div class="col-lg-12" style="margin-top:20px;margin-bottom:50px;" >
            
            <table class="table table-hover table-condensed table-striped" style="font-size:12px;" >
                <thead><th>#</th><th>Video Title</th><th>Subject</th><th>Chapter</th><th>Section</th><th>Option</th></thead>
            <?php
            if(isset($_POST['display-number']) && isset($_POST['display-chapter']) && isset($_POST['display-tag'])){
                
                $video_select_query = "SELECT * FROM video_list";
                
                $chapter = trim($_POST['display-chapter']);
                $chapter_part = '';
                if($chapter != 'all'){
                    $chapter_part = "chapter = '".$chapter."'";
                    $video_select_query .= " WHERE ".$chapter_part;
                }
                if(isset($_POST['display-section']) && $_POST['display-section'] !=''){
                    $section = $_POST['display-section'];
                    $section_part = '';
                    if($section != ''){
                        $section_part = "section = '".$section."'";
                        $video_select_query .= " AND ".$chapter_part;
                    }
                }
                $tag = 'all';
                if($tag != 'all'){
                    $tagspart = "tags LIKE '%".$tag."%'";
                    if($chapter != 'all'){
                        $video_select_query .= " AND ".$tagspart;
                    }else{
                        $video_select_query .= " WHERE ".$tagspart;
                    }
                }
                $limit = $_POST['display-number'];
                
                $video_select_query .= " ORDER BY id DESC LIMIT ".$limit;
            }else{
                $video_select_query = "SELECT * FROM video_list ORDER BY id DESC LIMIT 100";
            }
            $video_get_query = $db->query($video_select_query)or die(mysqli_error($db->db_link));
            $cnt = 1;
            while($video_info = mysqli_fetch_array($video_get_query)){
            ?>
            <tr>
                <td>
                    <?php echo $cnt++; ?>
                </td>
                <td>
                    <?php
                        echo $video_info['title'];
                    ?>
                </td>
                <td>
                    <?php echo $videos->get_subject_name($video_info['chapter']); ?>
                </td>
                <td>
                    <?php echo $videos->get_chapter_name($video_info['chapter']).""; ?>
                </td>
                <td>
                    <?php echo $videos->get_section_name($video_info['section']).""; ?>
                </td>
                <td noofTest="<?php echo $videos->get_noof_test($video_info['id']); ?>" noofPackage="<?php echo $videos->get_noof_package($video_info['id']); ?>">
                    <button class="btn btn-primary vid-play btn-xs" data-target="<?=$video_info['id']?>" ><span class="fa fa-play"></span> Play Video</button>
                </td>
            </tr>
            <?php
            }
            ?>
            </table>
            
        </div>
  
</div>

<div id="qn-removeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remove Videos</h4>
      </div>
        <form method="POST" action="submit/videos.php" >
            <div class="modal-body">
                <p><strong>Are you sure you want to remove the following Video from the database ?</strong></p>
                <p>This action will permanently remove video from the database. This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" value="" id="qn-remove-id" name="delete-id" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <input type="submit" name="delete-submit" id="delete-video" class="btn btn-primary" value="Delete Video" />
            </div>
        </form>
    </div>

  </div>
</div>

    
<!-- Video Play Modal -->
<div id="vidModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Video Details</h4>
      </div>
      <div class="modal-body video-modal">
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>