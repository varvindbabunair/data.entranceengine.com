<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="<?=(!isset($_GET['page']) || $_GET['page']=='dashboard')?'active':''?>">
                        <a href="?page=dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='questions')?'active':''?>">
                        <a href="?page=questions"><i class="fa fa-fw fa-list-alt"></i> Questions</a>
                    </li>
                    <li class="<?=(isset($_GET['page']) && ($_GET['page']=='test-dashboard' || $_GET['page']=='tests' || $_GET['page']=='create-test' || $_GET['page']=='edit-test' ))?'active ':''?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#tests"><i class="fa fa-fw fa-edit"></i> Test <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="tests" class="collapse <?=(isset($_GET['page']) && ($_GET['page']=='test-dashboard' || $_GET['page']=='tests' || $_GET['page']=='create-test' || $_GET['page']=='edit-test' ))?'in':''?>">
                            <li>
                                <a href="?page=test-dashboard"><i class="fa fa-fw fa-dashboard"></i> Test Dashboard</a>
                            </li>
                            <li>
                                <a href="?page=tests"><i class="fa fa-fw fa-edit"></i> All Tests</a>
                            </li>
                            <li>
                                <a href="?page=create-test"><i class="fa fa-fw fa-plus"></i> Create Test</a>
                            </li>
                            <li>
                                <a href="?page=quick-create-test"><i class="fa fa-fw fa-plus"></i> Quick Create Test</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='notes')?'active':''?>">
                        <a href="?page=notes"><i class="fa fa-fw fa-sticky-note"></i> Synopsis</a>
                    </li>
                   <?php
                   if($user->user_details['status'] == 'admin'){
                   ?>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='videos')?'active':''?>">
                        <a href="?page=videos"><i class="fa fa-fw fa-play"></i> Videos</a>
                    </li>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='faculty')?'active':''?>">
                        <a href="?page=faculty"><i class="fa fa-fw fa-user"></i> Faculty</a>
                    </li>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='students')?'active':''?>">
                        <a href="?page=students"><i class="fa fa-fw fa-users"></i> Students</a>
                    </li>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='student-batch')?'active':''?>">
                        <a href="?page=student-batch"><i class="fa fa-fw fa-flag-checkered"></i> Student Batches</a>
                    </li>
                    <?php
                   }
                    ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->

