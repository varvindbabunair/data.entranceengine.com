<?php
include 'includes/materials.php';
$tests = new Materials();
?>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    Name for Test :
    <input class="form-control test-name" name="test-name" type="text" />
</div>
<div style="width:100%;overflow:hidden">
    <div class="exam-time col-lg-2">
        Total Time of Exam
        <div class="input-group">
            <input type="text" class="form-control total-exam-time" aria-describedby="basic-addon2">
            <span class="input-group-addon" id="basic-addon2">minutes</span>
        </div>
    </div>
</div>
<div style="width:100%;">
    <div class="col-lg-4">
        Student Group :
        <select class="form-control user-group" name="user_group">
                    <?php 
                    $inst = $user->user_details['institute'];
                    $group_select_query = $db->query("SELECT * FROM student_group WHERE institute = '$inst'");
                    while($group = mysqli_fetch_array($group_select_query)){
                        echo '<option value="'.$group['id'].'">'.$group['name'].'</option>';
                    }
                    ?>
        </select>
        <br>
        <input type="checkbox" name="qn-shuffle"> Shuffle Questions in each section<br>
        <input type="checkbox" name="option-shuffle"> Shuffle Options for each question


    </div>
    <script>
        $('.tags').tagsinput();
    </script>


<div class="row">

    <div class='col-sm-6'>
        Date and Time Of Exam
        <div class="form-group">
            <div class='input-group date' id='datetimepicker1'>
                <input type='text' class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker();
        });
    </script>
</div>



</div>

<div style="width: 100%"><hr></div>

<div style="width: 100%">
    <div class="col-lg-3">
        <h4 style="text-align: center;text-decoration: underline">Physics</h4>
        <select class="form-control">
            <option>All</option>
            <option>Class 11 only</option>
            <option>Class 12 only</option>
            <option>None</option>
        </select>
        <h5 style="text-decoration: underline">Class11</h5>
        <?php
        $chapters = $tests->get_chapter_list(1);
        foreach($chapters as $chapter){
            echo '<div class="form-control" style="overflow:hidden;margin:2px;height:auto;">';
            echo '<input name="" type="checkbox"  />';
            echo $chapter['title'];
            echo '</div>';
        }
        ?>
        <h5 style="text-decoration: underline">Class12</h5>
        <?php
        $chapters = $tests->get_chapter_list(5);
        foreach($chapters as $chapter){
            echo '<div class="form-control" style="overflow:hidden;margin:2px;height:auto;">';
            echo '<input name="" type="checkbox"  />';
            echo $chapter['title'];
            echo '</div>';
        }
        ?>
        
    </div>
    <div class="col-lg-3">
        <h4 style="text-align: center;text-decoration: underline">Chemistry</h4>
        <select class="form-control">
            <option>All</option>
            <option>Class 11 only</option>
            <option>Class 12 only</option>
            <option>None</option>
        </select>
        <h5 style="text-decoration: underline">Class11</h5>
        <?php
        $chapters = $tests->get_chapter_list(2);
        foreach($chapters as $chapter){
            echo '<div class="form-control" style="overflow:hidden;margin:2px;height:auto;">';
            echo '<input name="" type="checkbox"  />';
            echo $chapter['title'];
            echo '</div>';
        }
        ?>
        <h5 style="text-decoration: underline">Class12</h5>
        <?php
        $chapters = $tests->get_chapter_list(6);
        foreach($chapters as $chapter){
            echo '<div class="form-control" style="overflow:hidden;margin:2px;height:auto;">';
            echo '<input name="" type="checkbox"  />';
            echo $chapter['title'];
            echo '</div>';
        }
        ?>
        
    </div>
    <div class="col-lg-3">
        <h4 style="text-align: center;text-decoration: underline">Biology</h4>
        <select class="form-control">
            <option>All</option>
            <option>Class 11 only</option>
            <option>Class 12 only</option>
            <option>None</option>
        </select>
        <h5 style="text-decoration: underline">Class11</h5>
        <?php
        $chapters = $tests->get_chapter_list(3);
        foreach($chapters as $chapter){
            echo '<div class="form-control" style="overflow:hidden;margin:2px;height:auto;">';
            echo '<input name="" type="checkbox"  />';
            echo $chapter['title'];
            echo '</div>';
        }
        ?>
        <h5 style="text-decoration: underline">Class12</h5>
        <?php
        $chapters = $tests->get_chapter_list(8);
        foreach($chapters as $chapter){
            echo '<div class="form-control" style="overflow:hidden;margin:2px;height:auto;">';
            echo '<input name="" type="checkbox"  />';
            echo $chapter['title'];
            echo '</div>';
        }
        ?>
        
    </div>
    <div class="col-lg-3">
        <h4 style="text-align: center;text-decoration: underline">Mathematics</h4>
        <select class="form-control">
            <option>All</option>
            <option>Class 11 only</option>
            <option>Class 12 only</option>
            <option>None</option>
        </select>
        <h5 style="text-decoration: underline">Class11</h5>
        <?php
        $chapters = $tests->get_chapter_list(4);
        foreach($chapters as $chapter){
            echo '<div class="form-control" style="overflow:hidden;margin:2px;height:auto;">';
            echo '<input name="" type="checkbox"  />';
            echo $chapter['title'];
            echo '</div>';
        }
        ?>
        <h5 style="text-decoration: underline">Class12</h5>
        <?php
        $chapters = $tests->get_chapter_list(9);
        foreach($chapters as $chapter){
            echo '<div class="form-control" style="overflow:hidden;margin:2px;height:auto;">';
            echo '<input name="" type="checkbox"  />';
            echo $chapter['title'];
            echo '</div>';
        }
        ?>
        
    </div>
</div>
<div style="width: 100%;">
    <div class="col-lg-12">
        <hr>
    </div>
    <div class="col-lg-4">
        <h4><u>Instructions to candidates</u></h4>
        <select class="form-control">
            <option>Select Instruction templates</option>
        </select>
    </div>
    
    <div class="col-lg-12">
       <br> 
        
        <textarea id="instruction" class="form-control" name="instruction"><?php echo $tests->get_test_settings('general-instruction'); ?></textarea>
    </div>
</div>

<div class="col-lg-12" style="margin-bottom:50px;margin-top:50px;text-align: center">
    <button id="create-test" class="btn btn-lg btn-primary"><b><span class="fa fa-plus"></span> Create Test</b></button>
</div>

<!-- Modal -->
<div id="create-test-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" >Modal Header</h4>
      </div>
      <div class="modal-body">
          <p class="modal-message">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
          <form action="?page=tests" method="POST">
              <input type="submit" class="btn btn-default" onclick="this.form.submit()" data-dismiss="modal" value="Close" />
          </form>
      </div>
    </div>

  </div>
</div>


<div class="general-instruction" style="display:none;">
    <?php
    echo $tests->get_test_settings('general-instruction');
    ?>
</div>