<?php
    include 'includes/materials.php';
    $test = new Materials();
?>

<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Tests <small> All Tests</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-list-alt"></i> Tests
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    
        
            <table class="table table-hover" style="width: 100%;" >
                <thead><th>id</th><th>Title</th><th>Tags</th><th>Options</th></thead>
        <?php
        if(isset($_POST['test-subjects']) && $_POST['test-subjects'] != '' ) {
            $par = implode(',', $_POST['test-subjects']);
        }else{
            $par = 'all';
        }
        $test_list = $test->get_test_list($par);

        foreach($test_list as $tests){
            ?>
                <tbody>
            <tr>
            <td><?php echo $tests['id']; ?></td>
            <td><?php echo $tests['title']; ?></td>
            <td><?php echo $tests['tags']; ?></td>
            <td>
                
                <button class="btn btn-xs btn-primary test-delete" data-toggle="modal" data-target="#removeModal" remove="<?php echo $tests['id']; ?>" ><span class="fa fa-remove"></span> Remove</button>
                <a href="?page=edit-test&id=<?php echo $tests['id']; ?>" ><button class="btn btn-xs btn-primary" ><span class="fa fa-edit"></span> Edit</button></a>
                
                <button class="exam-open btn btn-primary btn-xs exam-open" examid="<?= $tests['id'] ?>"> <span class="fa fa-edit"></span> View Test</button>
                <form id="examForm<?= $tests['id'] ?>" method="POST" action="pages/faculty/test.php" target="examWindow">
                    <input name="user" value="<?= $user->user_details['id'] ?>" type="hidden">
                    <input name="examid" value="<?= $tests['id'] ?>" type="hidden">
                    <input name="session" value="<?= $user->user_details['cookie'] ?>" type="hidden">
                </form>
            </td>
            </tr>
                </tbody>
            <?php
        }
        ?>

        </table>

    
</div>


<div id="removeModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remove Test</h4>
      </div>
      <div class="modal-body delete-modal delete-content">
          Are you sure you want to remove this test.
      </div>
      <div class="modal-footer">
          
        <form action="submit/tests.php" method="POST">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="hidden" value="" class="delete-id" name="delete-id"  />
            <input type="submit" class="btn btn-primary" name="delete-submit" value="Confirm Delete" />
        </form>
      </div>
    </div>

  </div>
</div>