<div class="col-lg-4">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <span class="badge pull-right" data-toggle="modal" data-target="#groupAddModal" ><span class="fa fa-plus"></span> Add User Group</span>
            <h3 class="panel-title">Student Group</h3>
        </div>
        <div class="panel-body">
            <table class="table table-condensed">
                <th>#</th><th>Group Name</th>
            <?php
            $inst = $user->user_details['institute'];
            $group_fetch_query = $db->query("SELECT * FROM student_group WHERE institute = '$inst'");
            $cnt = 1;
            while ($group = mysqli_fetch_array($group_fetch_query)){
                echo '<tr>';
                echo '<td>'.$cnt.'</td>';
                echo '<td>'.$group['name'].'</td>';
                echo '</tr>';
                $cnt++;
            }
            ?>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="groupAddModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add User Group</h4>
      </div>
        <form method="POST" action="submit/user-settings.php">
        <div class="modal-body">
            Group Name
            <input class="form-control" name="grp-name"  />
            <input type="hidden" name="institute" value="<?= $user->user_details['institute'] ?>" />
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-primary" value="Add Group" name="grp-add" />
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
    </div>

  </div>
</div>