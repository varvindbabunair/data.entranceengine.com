<?php
include 'includes/materials.php';
$tests = new Materials();
?>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    Name for Test :
    <input class="form-control test-name" name="test-name" type="text" />
</div>
<div style="width:100%;overflow:hidden">
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
        Number of sections :
        <input class="form-control test-sections" name="test-sections" type="number" value="1" min="1" />
    </div>
</div>
<div style="width:100%;overflow:hidden">
<div class="exam-time col-lg-2">
        Total Time of Exam
        <div class="input-group">
          <input type="text" class="form-control total-exam-time" aria-describedby="basic-addon2">
          <span class="input-group-addon" id="basic-addon2">minutes</span>
        </div>
    </div>
</div>
<div style="width:100%;">
    <div class="col-lg-4">
        Tags :
        <input class="form-control tags" />

        Student Group :
        <select class="form-control user-group" name="user_group">
                    <?php 
                    $inst = $user->user_details['institute'];
                    $group_select_query = $db->query("SELECT * FROM student_group WHERE institute = '$inst'");
                    while($group = mysqli_fetch_array($group_select_query)){
                        echo '<option value="'.$group['id'].'">'.$group['name'].'</option>';
                    }
                    ?>
        </select>
        <br>
        <input type="checkbox" name=""> Shuffle Questions in each section


    </div>
    <script>
        $('.tags').tagsinput();
    </script>


<div class="row">

        <div class='col-sm-6'>
            Date and Time Of Exam
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>
    </div>



</div>


<div class="section" style="width:100%; overflow: hidden;">
    <div class="col-lg-12"><hr></div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 " style="text-align:center;"><h3>Section <span class="section-number"></span></h3></div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <span class="">Section Title</span>
        <input name="section-name" type="text" class="form-control section-title"  />
    </div>
    
    <div style="width: 100%;overflow: hidden;">
        <div class="col-lg-12"><h4><u>Marking Scheme</u></h4></div>
        <div class="col-lg-3">
            Mark for Each Correct Answer :
            <input class="form-control" value="4" type="number" id="correct-answer-mark" name="correct-answer-mark" />
        </div>
        <div class="col-lg-3">
            Negative Mark for Each Wrong Answer :
            <div class="input-group">
                <input style="width: 50%;" value="1" type="number" id="wrong-answer-mark" class="form-control" />
                <select style="width: 50%;" id="wrong-answer-mark-type" class="form-control">
                    <option value="mark">Marks</option>
                    <option value="percent">in %</option>
                </select>
            </div>
        </div>
        <div class="col-lg-3 section-indtime" style="display: none;">
            Time For Section
            <div class="input-group">
                <input type="text" id="time-for-section" class="form-control each-section-time" />
                <span class="input-group-addon" id="basic-addon2">minutes</span>
            </div>
        </div>
        <div class="col-lg-12"><hr></div>
    </div>

    
        <div class="clearfix"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        Select Subject:
        <select name="test-subjects[]" id="test-subjects1" class="form-control test-subjects" multiple="multiple"  >
            <?php
            $subject_list = $tests->get_subject_list();
            foreach ($subject_list as $subject){
                ?>
            <optgroup label="<?php echo $subject['title'];  ?>">
            <?php 
            $chapter_list = $tests->get_chapter_list($subject['id']);
            foreach($chapter_list as $chapter){
                echo '<option value="'.$chapter['id'].'">'.$chapter['title'].'</option>';
                }
            ?>
            </optgroup>
            <?php

            }
            ?>
        </select>
        </div>
        
        <div class="clearfix"></div>
    


    <div class="col-lg-12" style="margin-top: 30px;">
        <div class="col-lg-5 col-md-5 question-list">
            <h4 style="text-align:center"><u>Availiable Questions</u></h4>
            <ul id="sortable1" class="connectedSortable ">

            </ul>
        </div>
        <div class="col-md-1 col-lg-1">
            <button class="btn btn-warning form-control" style="margin-top:50%;"> &Rrightarrow;</button>
        </div>
        <div class="col-lg-6 col-md-6 question-list">
            <h4 style="text-align:center"><u>Selected Questions</u></h4>
            <ul id="sortable2" class="connectedSortable ">

            </ul>
        </div>
    </div>
    
    
    
    
    <script>
        
    
    $('#test-subjects1').multiselect({
        columns: 2,
        search : true,
        selectAll : true,
        selectGroup : true,
        texts: {
            placeholder: 'Select chapters',
            selectAll:'Select all Chapters',
            search:'Search Chapters'
        }
    });
    
    

          
          
    $('#test-subjects1').change(function(){
        var subjects = $(this).val();
        var tags = '';
        var excludes = $( "#sortable2" ).sortable( "serialize", { attribute: "id" });
        $.post("ajax/create-test.php",{
            tag : tags,
            subject : subjects,
            exclude : excludes
        },
        function(responseTxt){
            $('#sortable1').html(responseTxt);
        });
    });
    
    
   
    
    
    
            $( "#sortable1, #sortable2" ).sortable({
            connectWith: ".connectedSortable"
          }).enableSelection();
    
    
    
    $( "#sortable2" ).sortable({
        receive: function( event, ui ) {
            ui.item.children(".mark").show();
            ui.item.children(".order").show();
            
            var totalElements = ui.item.parent('ul').children('li').size();
            for(var i = 0;i<totalElements;i++){
                var j = i+1;
                ui.item.parent('ul').children('li').eq(i).children('.order').html("<b>" + j + "</b>");
            }
        },
        stop: function( event, ui ) {
//            var idstring = $( "#sortable2" ).sortable( "serialize", { attribute: "id" });
            var totalElements = ui.item.parent('ul').children('li').size();
            for(var i = 0;i<totalElements;i++){
                var j = i+1;
            ui.item.parent('ul').children('li').eq(i).children('.order').html("<b>" + j + "</b>");
            }

        }
    });
    
    $( "#sortable1" ).sortable({
        receive: function( event, ui ) {
            ui.item.children(".mark").hide();
            ui.item.children(".order").hide();
            var totalElements = $( "#sortable2" ).children('li').size();
            for(var i = 0;i<totalElements;i++){
                var j = i+1;
                $( "#sortable2" ).children('li').eq(i).children('.order').html("<b>" + j + "</b>");
            }
        }
    });
        
        </script>
    
</div>

<div id="selectChoose" style="display: none;">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        Select Subject:
        <select name="test-subjects[]" id="test-subjects1" class="form-control test-subjects" multiple="multiple"  >
            <?php
            $subject_list = $tests->get_subject_list();
            foreach ($subject_list as $subject){
                ?>
            <optgroup label="<?php echo $subject['title'];  ?>">
            <?php 
            $chapter_list = $tests->get_chapter_list($subject['id']);
            foreach($chapter_list as $chapter){
                echo '<option value="'.$chapter['id'].'">'.$chapter['title'].'</option>';
                }
            ?>
            </optgroup>
            <?php

            }
            ?>
        </select>
    </div>
        
    </div>
<div style="width: 100%;"><hr></div>

<div style="width: 100%;">
    
    <div class="col-lg-4">
        Select Batch :
        <select name="student-group" class="form-control stud-group" >
            <?php
            $inst = $user->user_details['institute'];
            $group_list = $tests->query("SELECT * FROM student_group WHERE institute = '$inst'");
            while($group = mysqli_fetch_array($group_list)){
                echo '<option value="'.$group['id'].'">'.$group['name'].'</option>';
            }
            ?>
        </select>
    </div>
    
    <div class="col-lg-12">
        <hr>
        <h4><u>Instructions to candidates</u></h4>
        <input type="checkbox" checked="true" id="general-instruction-check" name="general-instruction" /> Same as General Instructions
        <textarea id="instruction" class="form-control" name="instruction"><?php echo $tests->get_test_settings('general-instruction'); ?></textarea>
    </div>
</div>

<div class="col-lg-12" style="margin-bottom:50px;margin-top:50px;text-align: center">
    <button id="create-test" class="btn btn-lg btn-primary"><b><span class="fa fa-plus"></span> Create Test</b></button>
</div>

<!-- Modal -->
<div id="create-test-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" >Modal Header</h4>
      </div>
      <div class="modal-body">
          <p class="modal-message">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
          <form action="?page=tests" method="POST">
              <input type="submit" class="btn btn-default" onclick="this.form.submit()" data-dismiss="modal" value="Close" />
          </form>
      </div>
    </div>

  </div>
</div>


<div class="general-instruction" style="display:none;">
    <?php
    echo $tests->get_test_settings('general-instruction');
    ?>
</div>