<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Faculty <small></small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-user"></i> Faculty
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <div class="col-lg-12">
        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#userAddModal"><span class="fa fa-plus"></span> Add Faculty</button>
    </div>
    <div class="col-lg-12" style="margin-top:20px;">
    <div class="panel panel-primary">
        <div class="panel-heading">
            
            <h4 class="panel-title">Users</h4>
        </div>
        <div class="panel-body">
            <table class="table table-condensed table-hover">
                <thead><th>Name</th><th>Contact</th><th>Options</th></thead>
            <?php
            $users = $user->get_users('faculty');
            foreach ($users as $individual){
                ?>
            <tr>
                <td><?php echo $individual['name']; ?></td>
                <td><span class="fa fa-envelope"></span> <?php echo $individual['email']; ?><br><span class="fa fa-phone"></span> <?php echo $individual['phone']; ?></td>
                <td>
                    <button class="btn btn-xs btn-primary view-user" editid="<?php echo $individual['id']; ?>" data-toggle="modal" data-target="#userEditModal"><span class="fa fa-edit"></span> Edit</button>
                    <button class="btn btn-xs btn-primary delete-user" removeid="<?php echo $individual['id']; ?>" data-toggle="modal" data-target="#userRemoveModal"><span class="fa fa-remove"></span> Delete</button>
                    <button class="btn btn-xs btn-primary reset-user" resetid="<?php echo $individual['id']; ?>" data-toggle="modal" data-target="#userResetModal"><span class="fa fa-lock"></span> Reset Password</button>
                </td>
            </tr>
                <?php
            }
            ?>
            </table>
        </div>
    </div>
</div>


<div id="userAddModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New User</h4>
      </div>
        <form action="submit/users.php" method="POST">
            <div class="modal-body">
                Name :
                <input class="form-control" value="" name="name"/>
                Username :
                <input class="form-control" value="" name="username"/>
                Password :
                <input class="form-control" type="password" value="" name="password"/>
                Confirm Password :
                <input class="form-control" type="password" value="" name="cnf-password"/>
                Email :
                <input class="form-control" type="email" value="" name="email"/>
                Phone :
                <input class="form-control" type="" value="" name="phone"/>
                <input type="hidden" value="faculty" name="user-role"/>
                <input type="hidden" value="<?= $user->user_details['institute'] ?>" name="institute"/>
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" name="new-user-submit" class="btn btn-primary" value="Add User" />
            </div>
        </form>
    </div>

  </div>
</div>


<div id="userEditModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit User Details</h4>
      </div>
        <form action="submit/users.php" method="POST">
            <div class="modal-body edit-body">
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" name="edit-user-submit" class="btn btn-primary" value="Save Changes" />
            </div>
        </form>
    </div>

  </div>
</div>


<div id="userRemoveModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remove User</h4>
      </div>
        <form action="submit/users.php" method="POST">
            <div class="modal-body">
                <h3>Are you sure you want to delete this user. ?</h3>
                <input type="hidden" value="" name="delete-user" class="delete-user" />
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" name="remove-user-submit" class="btn btn-primary" value="Confirm Delete" />
            </div>
        </form>
    </div>

  </div>
</div>


<div id="userResetModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reset User Password</h4>
      </div>
        <form action="submit/users.php" method="POST">
            <div class="modal-body edit-body">
                New Password :
                <input type="text" class="form-control" name="new-password" />
                <input type="hidden"  name="user-id" class="user-id" />
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" name="reset-user-submit" class="btn btn-primary" value="Reset Password" />
            </div>
        </form>
    </div>

  </div>
</div>