<?php
    include 'includes/materials.php';
    $notes = new Materials();
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Notes <small></small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-sticky-note"></i> Notes
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    
    
<div class="row">
    <div class="col-lg-12">
        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#notesModal"><span class="fa fa-plus"></span> Add Notes</button>
    </div>
</div>

    <div class="row">
<div class="col-xs-12" style="margin-top:20px;">
            <table class="table table-bordered">
                <thead><th>Subject</th><th>Chapter</th><th>Title</th><th>Options</th></thead>
            <?php
            $subject_list = $notes->get_subject_list();
            if(sizeof($subject_list) != 0){
            foreach($subject_list as $subject_name){
                if($notes->get_notes_count($subject_name['id']) == 0){
                    continue;
                }
                echo '<tr><td rowspan="'.$notes->get_notes_count($subject_name['id']).'">'.$subject_name['title'].'</td>';
                
                $chapter_list = $notes->get_chapter_list($subject_name['id']);
                $i =0;
                foreach ($chapter_list as $chapter){
                    $note_list = $notes->get_notes($chapter['id']);
                    if(sizeof($note_list) == 0){
                        continue;
                    }
                    if($i == 0){
                        echo '<td rowspan="'.sizeof($note_list).'">'.$chapter['title'].'</td>';
                    }else{
                        echo '<tr><td rowspan="'.sizeof($note_list).'">'.$chapter['title'].'</td>';
                    }
                    $j=0;
//                    print_r($note_list);
                    foreach($note_list as $note){
                        if($j ==0){
                            echo '<td>'.$note['title'].'<br><small>'.$note['tags'].'</small></td><td><button class="btn btn-xs btn-primary note-edit" data-toggle="modal" edit="'.$note['id'].'" data-target="#noteEditModal" ><span class="fa fa-edit"></span> Edit</button>&nbsp;<button class="btn btn-xs btn-primary note-remove" data-toggle="modal" remove="'.$note['id'].'" data-target="#noteDeleteModal" ><span class="fa fa-remove"></span> Remove</button></td></tr>';
                        }else{
                            echo '<tr><td>'.$note['title'].'<br><small>'.$note['tags'].'</small></td><td><button class="btn btn-xs btn-primary note-edit" data-toggle="modal" edit="'.$note['id'].'" data-target="#noteEditModal" ><span class="fa fa-edit"></span> Edit</button>&nbsp;<button class="btn btn-xs btn-primary note-remove" data-toggle="modal" remove="'.$note['id'].'" data-target="#noteDeleteModal" ><span class="fa fa-remove"></span> Remove</button></td></tr>';
                        }
                        $j++;
                    }
                    $i++;
                }
                
            }
            }
            ?>
            </table>

</div>
    </div>
<div id="notesModal" class="modal fade" role="dialog">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title"><span  class="fa fa-plus"></span> Add Notes</h3>
                </div>
                <form action="submit/notes.php" method="POST" >
                <div class="modal-body">
                        Select Chapter :
                        <select name="chapter" class="form-control">
                            <option>Select from Chapter List</option>
                          <?php
                          $subject_list = $notes->get_subject_list();
                          foreach($subject_list as $subjects){
                              echo '<optgroup label="'.$subjects['title'].'">';
                              $chapter_list = $notes->get_chapter_list($subjects['id']);
                              foreach($chapter_list as $chapter){
                                  echo '<option value="'.$chapter['id'].'">'.$chapter['title'].'</option>';
                              }
                              echo '</optgroup>';
                          }
                          ?>
                        </select>
                        Notes Title:
                        <input name="title" type="text" class="form-control" />
                        Tags :
                        <input name="tags" class="form-control tags" />
                        <script>
                            $('.tags').tagsinput();
                          </script>

                        Notes :
                        <textarea class="form-control" name="editor1"></textarea>
                        
                  
                </div>
                    <div class="modal-footer">
                        <input type="submit" name="note-submit" value="Submit Note" class="btn btn-primary" />
                    </div>
                    </form>
            </div>
        </div>
</div>

<div id="noteEditModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="fa fa-edit"></span> Edit Modal</h4>
      </div>
        <form action="submit/notes.php" method="POST">
      <div class="modal-body edit-content">
          
      </div>
      <div class="modal-footer">
          <input type="submit" class="btn btn-primary" value="Save Changes" name="edit-submit" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
        </form>
    </div>

  </div>
</div>

<div id="noteDeleteModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Notes</h4>
      </div>
      <div class="modal-body delete-content">
          Are you sure you want to delete this note.
      </div>
      <div class="modal-footer">
          <form action="submit/notes.php" method="POST" >
              <input type="hidden" value="" name="delete-id" class="delete-id"  />
              <input type="submit" name="delete-submit" class="btn btn-primary" value="Delete Note" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </form>
      </div>
    </div>
  </div>
</div>