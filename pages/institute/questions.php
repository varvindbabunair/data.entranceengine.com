<?php
include 'includes/questions.php';
$questions = new Questions();

?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Questions <small>All Questions</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-alt"></i> Questions
                </li>
                <li class="active">
                    <i class="fa fa-list"></i> All Questions
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    


    
    <div class="row" style="">
        <div class="col-lg-12">
            <form  acton="" method="POST">
            <div class="col-lg-2">
                Display :
                <select name="display-number" onchange="this.form.submit()" class="form-control">
                    <option <?php echo (isset($_POST['display-number']) && $_POST['display-number'] == '100')?'selected':''; ?> value="100">100</option>
                    <option <?php echo (isset($_POST['display-number']) && $_POST['display-number'] == '500')?'selected':''; ?> value="500">500</option>
                    <option <?php echo (isset($_POST['display-number']) && $_POST['display-number'] == '1000')?'selected':''; ?> value="1000">1000</option>
                </select>
            </div>
            <div class="col-lg-3">
                Choose a Chapter :
                <select name="display-chapter" onchange="this.form.submit()" id="chapter-select" name="chapter" class="form-control" >
                    <option value="all">All Chapters</option>
                    <?php
                    $subject_list = $questions->get_subject_list();
                    foreach($subject_list as $subject){
                    ?>
                    <optgroup label="<?php echo $subject['title'] ?>">
                    <?php
                    $chapter_list = $questions->get_chapter_list($subject['id']);
                    foreach ($chapter_list as $chapter){
                    ?>
                        <option <?php echo (isset($_POST['display-chapter']) && $_POST['display-chapter'] == $chapter['id'])?'selected':''; ?> value="<?php echo $chapter['id']; ?>"><?php echo $chapter['title']; ?></option>
                    <?php
                    }
                    ?>
                    </optgroup>
                    <?php
                    }
                    ?>
                </select>
            </div>
                <div class="col-lg-2">
                    Section :
                    <select name="display-section" class="form-control" onchange="this.form.submit()">
                        <?php
                        if(!isset($_POST['display-chapter']) || $_POST['display-chapter'] =='all'){
                            echo '<option value="" >No chapter Selected</option>';
                        }else{
                            $cid = $_POST['display-chapter'];
                            $section_get_query = $questions->query("SELECT * FROM section_list WHERE chapter = '$cid'");
                            echo '<option value="" >Select Section</option>';
                            while($section = mysqli_fetch_array($section_get_query)){
                                echo '<option ';
                                echo ($_POST['display-section'] == $section['id'])?'selected':'';
                                echo ' value="'.$section['id'].'" >'.$section['title'].'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
            
            </form>
        
             </div>   
    </div>
    <div class="row">
        <div class="col-lg-12" style="margin-top:20px;margin-bottom:50px;" >
            
            <table class="table table-hover table-condensed table-striped" style="font-size:12px;" >
                <thead><th>#</th><th>Question</th><th>Subject</th><th>Chapter</th><th>Section</th><th>Option</th></thead>
            <?php
            if(isset($_POST['display-number']) && isset($_POST['display-chapter']) && isset($_POST['display-tag'])){
                
                $question_select_query = "SELECT * FROM question_list";
                
                $chapter = trim($_POST['display-chapter']);
                $chapter_part = '';
                if($chapter != 'all'){
                    $chapter_part = "chapter = '".$chapter."'";
                    $question_select_query .= " WHERE ".$chapter_part;
                }
                if(isset($_POST['display-section']) && $_POST['display-section'] !=''){
                    $section = $_POST['display-section'];
                    $section_part = '';
                    if($section != ''){
                        $section_part = "section = '".$section."'";
                        $question_select_query .= " AND ".$chapter_part;
                    }
                }
                
                $limit = $_POST['display-number'];
                
                $question_select_query .= " ORDER BY id DESC LIMIT ".$limit;
            }else{
                $question_select_query = "SELECT * FROM question_list ORDER BY id DESC LIMIT 100";
            }
            $question_get_query = $db->query($question_select_query)or die(mysqli_error($db->db_link));
            $cnt = 1;
            while($question_info = mysqli_fetch_array($question_get_query)){
            ?>
            <tr>
                <td>
                    <?php echo $cnt++; ?>
                </td>
                <td>
                    <?php
                    if($question_info['question_type'] == 'image'){
                        echo '<img style="width:100%;" src="images/questions/'.$question_info['question'].'"/>';
                    }else{
                        echo $question_info['question'];
                    }
                    ?>
                </td>
                <td>
                    <?php echo $questions->get_subject_name($question_info['chapter']); ?>
                </td>
                <td>
                    <?php echo $questions->get_chapter_name($question_info['chapter']).""; ?>
                </td>
                <td>
                    <?php echo $questions->get_section_name($question_info['section']).""; ?>
                </td>
                <td>
                    <button class="btn btn-xs btn-primary"><span class="fa fa-eye"></span> View</button>
                </td>
            </tr>
            <?php
            }
            ?>
            </table>
            
        </div>
  
</div>

<div id="qn-removeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remove Question</h4>
      </div>
        <form method="POST" action="submit/questions.php" >
            <div class="modal-body">
                <p><strong>Are you sure you want to remove the following question from the database ?</strong></p>
                <p>This action will permanently remove question from the database. This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" value="" id="qn-remove-id" name="qn-remove-id" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <input type="submit" name="qn-remove-submit" id="delete-question" class="btn btn-primary" value="Delete Question" />
            </div>
        </form>
    </div>

  </div>
</div>


<div id="qn-editModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Question</h4>
      </div>
        <form action="submit/questions.php" method="POST" enctype="multipart/form-data">
            <div id="edit-qnBody" class="modal-body" style="overflow:hidden;">
                <p style="text-align:center;"><span class="fa fa-spinner fa-spin"></span> Loading...</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" class="option-number" value="5" name="option-number" />
                <input type="hidden" name="qn-edit-id" value="" class="qn-edit-id" />
                <input type="submit" class="btn btn-primary qn-edit-submit" name="qn-edit-submit" value="Save Changes" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
  </div>
</div>