<?php
    include 'includes/materials.php';
    $videos = new Materials();
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Videos <small></small>
            </h1>
        </div>
    </div>
    <!-- /.row -->
    

    <div class="row" >
<div class="col-xs-12">
            <table class="table table-bordered">
                <thead><th>Subject</th><th>Chapter</th><th>Title</th><th>Options</th></thead>
            <?php
            $subject_list = $videos->get_subject_list();
            if(sizeof($subject_list) != 0){
            foreach($subject_list as $subject_name){
                if($videos->get_video_count($subject_name['id']) == 0){
                    continue;
                }
                echo '<tr><td rowspan="'.$videos->get_video_count($subject_name['id']).'">'.$subject_name['title'].'</td>';
                
                $chapter_list = $videos->get_chapter_list($subject_name['id']);
                $i =0;
                foreach ($chapter_list as $chapter){
                    $video_list = $videos->get_videos($chapter['id']);
                    if(sizeof($video_list) == 0){
                        continue;
                    }
                    if($i == 0){
                        echo '<td rowspan="'.sizeof($video_list).'">'.$chapter['title'].'</td>';
                    }else{
                        echo '<tr><td rowspan="'.sizeof($video_list).'">'.$chapter['title'].'</td>';
                    }
                    $j=0;
//                    print_r($video_list);
                    foreach($video_list as $video){
                        if($j ==0){
                            echo '<td>'.$video['title'].'<br><small>'.$video['tags'].'</small></td><td><button class="btn btn-xs btn-primary video-edit" data-toggle="modal" edit="'.$video['id'].'" data-target="#videoEditModal" ><span class="fa fa-edit"></span> Edit</button>&nbsp;<button class="btn btn-xs btn-primary video-remove" data-toggle="modal" remove="'.$video['id'].'" data-target="#videoDeleteModal"><span class="fa fa-remove"></span> Delete</button>'
                                    . '<button class="btn btn-primary vid-play btn-xs" data-target="'.$video['id'].'" ><span class="fa fa-play"></span> Play Video</button>'
                                    . '</td></tr>';
                        }else{
                            echo '<tr><td>'.$video['title'].'<br><small>'.$video['tags'].'</small></td><td><button class="btn btn-xs btn-primary video-edit" data-toggle="modal" edit="'.$video['id'].'" data-target="#videoEditModal" ><span class="fa fa-edit"></span> Edit</button>&nbsp;<button class="btn btn-xs btn-primary video-remove" data-toggle="modal" remove="'.$video['id'].'" data-target="#videoDeleteModal"><span class="fa fa-remove"></span> Delete</button>'
                                    . '<button class="btn btn-primary vid-play btn-xs" data-target="'.$video['id'].'" ><span class="fa fa-play"></span> Play Video</button>'
                                    . '</td></tr>';
                        }
                        $j++;
                    }
                    $i++;
                }
            }
            }
            ?>
            </table>
        
</div>
    </div>

<!-- Modal -->
<div id="videoModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
          <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="fa fa-plus"></span> Add Video</h4>
      </div>
            <form action="submit/videos.php" enctype="multipart/form-data" method="POST" >
                <div class="modal-body">
                  Select Chapter :
                  <select name="chapter" class="form-control">
                      <option>Select from Chapter List</option>
                    <?php
                    $subject_list = $videos->get_subject_list();
                    foreach($subject_list as $subjects){
                        echo '<optgroup label="'.$subjects['title'].'">';
                        $chapter_list = $videos->get_chapter_list($subjects['id']);
                        foreach($chapter_list as $chapter){
                            echo '<option value="'.$chapter['id'].'">'.$chapter['title'].'</option>';
                        }
                        echo '</optgroup>';
                    }
                    ?>
                  </select>
                  Video Title:
                  <input name="title" type="text" class="form-control" />
                  Video Link / Upload:
                  <div class="input-group">
                      <input type="text" class="form-control" name="vid-link" id="basic-url" aria-describedby="basic-addon3" placeholder="Paste video link">
                    <span class="input-group-addon" id="basic-addon3"> OR </span>
                    <input type="file" name="upload_vid" class="vid form-control" accept=".mp4,.mpeg4,.flv,.wmv" />
                    
                  </div>
                  Tags :
                  <input name="tags" class="form-control tags" />
                  <script>
                      $('.tags').tagsinput();
                    </script>

                    Description :
                    <textarea class="form-control" name="description"></textarea>
                  Notes :
                  <textarea class="form-control" name="editor1"></textarea>
                  <br>
                </div>
                <div class="modal-footer">
                      <input type="submit" name="video-submit" value="Submit Video" class="btn btn-primary" />
                  </div>
            </form>
        </div>
    </div>
</div>


<!-- Video Play Modal -->
<div id="vidModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body video-modal">
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<div id="videoEditModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="fa fa-edit"></span> Edit Video Details</h4>
      </div>
        <form action="submit/videos.php" method="POST" enctype="multipart/form-data">
      <div class="modal-body edit-content">
          
      </div>
      <div class="modal-footer">
          <input type="submit" class="btn btn-primary" value="Save Changes" name="edit-submit" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
        </form>
    </div>

  </div>
</div>

<div id="videoDeleteModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Videos</h4>
      </div>
      <div class="modal-body delete-content">
          Are you sure you want to delete this video.
      </div>
      <div class="modal-footer">
          <form action="submit/videos.php" method="POST" >
              <input type="hidden" value="" name="delete-id" class="delete-id"  />
              <input type="submit" name="delete-submit" class="btn btn-primary" value="Delete Video" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </form>
      </div>
    </div>
  </div>
</div>