<?php
include 'includes/packages.php';
$packages = new Packages();
$package = $packages->get_package_details($_POST['package']);
?>

<div class="col-lg-3">
    <p></p>
</div>
<div class="col-lg-6" >
    <div class="col-lg-3">
        <img class="thumbnail" style="max-width: 100%" src="images/packages/<?php echo $package['image']; ?>" />
    </div>
    <div class="col-lg-9">
        <h3><?php echo $package['title']; ?></h3>
        <p><?php echo $package['description']; ?></p>
        <?php 
        $pricing = ($_POST['package-upgrade'] == '1')?$packages->get_upgrade_price($_POST['package-upgrade'],$package['id']):$package['pricing']; ?>
    </div>
    <div class="col-lg-12">
        <table class="table table-condensed">
            <tbody>
                <tr><th>Package Pricing</th><td style="text-align:right;"><span class="fa fa-inr"></span> <?php echo ($package['include_taxes'] == '1')?$packages->get_principal($pricing,$package['include_taxes']):$pricing; ?></td></tr>
                <?php
                $taxes = $packages->get_taxes();
                foreach ($taxes as $tax){
                    echo '<tr>';
                    echo '<th>'.$tax[0].'</th>';
                    echo ($tax[1]=='amount')?'<td style="text-align:right"><span class="fa fa-inr"></span> '.$tax[2].'</td>':'<td style="text-align:right"><span class="fa fa-inr"></span> '.round(($packages->get_principal($pricing,$package['include_taxes']) * $tax[2]/100),2).'</td>';
                    echo '</tr>';
                }
                ?>
                <tr><th>Total Price :</th><th> <span class="fa fa-inr"></span> <?php echo ($package['include_taxes'] == '1')?$pricing:$packages->get_principal($pricing, $package['include_taxes']); ?></th></tr>
            </tbody>
        </table>
        
        <hr>
        
        <h3 style="border-bottom: 1px solid #ccc;">Select Payment Mode :</h3>
        <form>
            <div class=" well well-sm" >
                <input type="radio" name="paymode" value="net-banking" checked="checked" /> Net Banking / Credit Card / Debit Card
            </div>
            <div class="well well-sm" >
                <input type="radio" name="paymode" value="dd-direct" /> DD / Bank account Transfer
            </div>
        </form>
        <div style="width: 100%;" ">
            <form id="invoiceForm" action="submit/payment.php" method="POST" > 
                <input type="hidden" value="<?php echo $_POST['package']; ?>" name="package" />
                <input type="hidden" value="<?php echo $_POST['package-upgrade']; ?>" name="package-upgrade" />
                <?php if($_POST['package-upgrade'] == '1'){ ?>
                <input type="hidden" value="<?php echo $_POST['upgrade-from']; ?>" name="upgrade-from" />
                <?php } ?>
                <button id="paycnf" class="btn btn-warning pull-right btn-lg"><span class="fa fa-forward"></span> <span class="pay-msg">Continue to make Payment</span></button>
            </form>
        </div>
</div>
</div>
