<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="<?=(!isset($_GET['page']) || $_GET['page']=='dashboard')?'active':''?>">
                        <a href="?page=dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='tests')?'active':''?>">
                        <a href="?page=tests"><i class="fa fa-fw fa-edit"></i> Tests</a>
                    </li>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='notes')?'active':''?>">
                        <a href="?page=notes"><i class="fa fa-fw fa-sticky-note"></i> Synopsis</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->

