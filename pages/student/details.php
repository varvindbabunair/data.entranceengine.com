<div class="col-lg-4">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><span class="fa fa-user"></span> My Profile</h3>
        </div>
        <div class="panel-body">
            <table class="table">
                <tr><th>Name</th><td><?php echo $user->user_details['name']; ?></td></tr>
                <tr><th>Address</th><td>
                    <?php echo $user->user_details['name']; ?><br>
                        <?php echo $user->user_details['address']; ?>,
                            <?php echo $user->user_details['city']; ?>,
                                <?php echo $user->user_details['state']; ?> - <?php echo $user->user_details['postal_code']; ?>,
                    <?php
                    $ctry = $user->user_details['country'];
                    $country_select_query = $db->query("SELECT * FROM country_list WHERE code = '$ctry'");
                    $country = mysqli_fetch_array($country_select_query);
                    echo $country['name'];
                    ?>
                    </td></tr>
                <tr><th>Phone</th><td><?php echo $user->user_details['phone']; ?></td></tr>
                <tr><th>Email</th><td><?php echo $user->user_details['email']; ?></td></tr>
            </table>
        </div>
    </div>
</div>