<?php
    include 'includes/materials.php';
    $test = new Materials();
?>

<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Tests <small> All Tests</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-list-alt"></i> Tests
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    
        
            <table class="table table-hover" style="width: 100%;" >
                <thead><th>id</th><th>Title</th><th>Options</th></thead>
        <?php
        if(isset($_POST['test-subjects']) && $_POST['test-subjects'] != '' ) {
            $par = implode(',', $_POST['test-subjects']);
        }else{
            $par = 'all';
        }
        $test_list = $test->get_test_list($par);

        foreach($test_list as $tests){
            ?>
                <tbody>
            <tr>
            <td><?php echo $tests['id']; ?></td>
            <td><?php echo $tests['title']; ?></td>
            <td>
                
                <button class="exam-open btn btn-primary btn-xs exam-open" examid="<?= $tests['id'] ?>"> <span class="fa fa-edit"></span> Begin Exam</button>
                <form id="examForm<?= $tests['id'] ?>" method="POST" action="pages/faculty/test.php" target="examWindow">
                    <input name="user" value="<?= $user->user_details['id'] ?>" type="hidden">
                    <input name="examid" value="<?= $tests['id'] ?>" type="hidden">
                    <input name="session" value="<?= $user->user_details['cookie'] ?>" type="hidden">
                </form>
            </td>
            </tr>
                </tbody>
            <?php
        }
        ?>

        </table>

    
</div>

