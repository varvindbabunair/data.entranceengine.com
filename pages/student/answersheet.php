<html>
    <head>
        <link rel="stylesheet" href="../../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../../css/font-awesome.css" />
        <link rel="stylesheet" href="../../css/exam-result.css" />
        <script src="../../js/jquery-1.9.1.js" ></script>
        <script src="../../js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <?php
            include '../../config.php';
            $db = new Db();
            
            $answer = json_decode(str_replace('\'', '"', $_REQUEST['answers']));
            $test = $_REQUEST['test'];
            
            $test_get_query = $db->query("SELECT * FROM test_list WHERE id = '$test'");
            $questions_list_get = mysqli_fetch_array($test_get_query);
            $original_answer = json_decode($questions_list_get['questions']);

            $qn_qry = "SELECT * FROM question_list WHERE";
            $section_num = 0;
            foreach ($original_answer as $section){
                $individual_answer_num =0;
                foreach ($section as $qn){
                    $qn_id = $original_answer[$section_num][$individual_answer_num][0];
                    $qn_qry .= " id = '$qn_id' OR";
                    $individual_answer_num++;
                }
                $section_num++;
            }
            $qn_qry = trim($qn_qry,' OR');

            $question_get_query = $db->query($qn_qry);

            $qn_arr = array();
            while($qn_info = mysqli_fetch_array($question_get_query)){
                $qn_arr[$qn_info['id']] = array('question_type'=>$qn_info['question_type'],'question'=>$qn_info['question'],'answer_option'=>$qn_info['answer_option'],'correct_answer'=>$qn_info['correct_answer'],'hint_type'=>$qn_info['hint_type'],'hint'=>$qn_info['hint']);
            }
            
            $section_list = json_decode($questions_list_get['section_title']);
            
            echo '<h2 style="font-weight:bold;text-align:center;">Answersheet : '.$questions_list_get['title'].'</h2>';
            
            $section_num = 0;
            foreach ($original_answer as $answer_section){
                $qn_num = 0;
                echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h4 style="font-weight:bold;text-align:center;" >Section  : '.$section_list[$section_num].'</h4><hr></div>';
                foreach($answer_section as $ans_org){
                    ?>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="font-size: 16px;">
                <strong><?php echo $qn_num+1; ?></strong>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="font-size: 16px;">
                <?php
                echo ($qn_arr[$ans_org[0]]['question_type'] == 'image')? '<img src="../../images/questions/'.$qn_arr[$ans_org[0]]['question'].'" />':$qn_arr[$ans_org[0]]['question'];
                ?>
                <ul style="list-style-type: none;">
                    <?php
                    $answer_list = json_decode($qn_arr[$ans_org[0]]['answer_option']);
                    foreach ($answer_list as $answer_option){
                        echo '<li>';
//                        echo ($answer[$section_num][$qn_num][1] != $answer_option[0] || $answer[$section_num][$qn_num][1] == '')?'<span class="fa fa-square"':'<span class="fa fa-check-square-o"';
//                        echo ($answer_option[0] == $qn_arr[$ans_org[0]]['correct_answer'])?' style="color:green;" ':' ';
//                        echo ' ></span> &nbsp;';
                        if($answer[$section_num][$qn_num][1] == $answer_option[0]){
                            if($answer_option[0] == $qn_arr[$ans_org[0]]['correct_answer']){
                                echo '<span class="fa fa-check-square-o" style="color:green;"></span> &nbsp;';
                            }else{
                                echo '<span class="fa fa-remove" style="color:red;"></span> &nbsp;';
                            }
                        }else{
                            if($answer_option[0] == $qn_arr[$ans_org[0]]['correct_answer']){
                                echo '<span class="fa fa-square" style="color:green"></span> &nbsp;';
                            }else{
                                echo '<span class="fa fa-square" ></span> &nbsp;';
                            }
                        }
                        echo ($answer_option[1] == 'image')?'<img src="../../images/answers/'.$answer_option[2].'" />': $answer_option[2];
                        echo '</li>';
                    }
                    ?>
                </ul>
                <?php
                if(trim($qn_arr[$ans_org[0]]['hint']) !=''){
                ?>
                <h4 style="font-weight: bold;border-bottom:1px solid #ccc;">Hint:</h4>
                <?php
                echo ($qn_arr[$ans_org[0]]['hint_type'] == 'image')? '<img src="../../images/hints/'.$qn_arr[$ans_org[0]]['hint'].'" />':$qn_arr[$ans_org[0]]['hint'];
                }
                ?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="font-size: 16px;">
                <table>
                    <tr><th>Mark for Correct Answer : </th><td>&nbsp; <?php echo $ans_org[1]; ?></td></tr>
                    <tr><th>Negative Mark : </th><td>&nbsp; <?php echo $ans_org[2]; ?></td></tr>
                </table>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><hr></div>
            <?php
            $qn_num++;
                }
                $section_num++;
            }
            ?>
        </div>
    </body>
</html>