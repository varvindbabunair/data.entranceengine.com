<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Edit Digital Materials
            </h1>
            <ol class="breadcrumb">
                <li>
                    Digital Materials
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Edit Digital Materials
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <?php
    if(!isset($_GET['id']) && $_GET['id']==''){
        die('<h4>Some Error Occured</h4>');
    }
    $id = $_GET['id'];
    $material_select = $db->query("SELECT * FROM digital_list WHERE id = '$id'");
    if(mysqli_num_rows($material_select) == 0){
        die('<h4>Some Error Occured</h4>');
    }
    $material = mysqli_fetch_array($material_select);
    ?>

<div class="row">
    <form enctype="multipart/form-data" method="POST" action="submit/digital.php">
        <div class="col-md-6">
            Title :
            <input type="text" value="<?= $material['title'] ?>" name="material-title" class="form-control" required="">
            Description :
            <textarea name="material-description" class="form-control" required=""><?= $material['description'] ?></textarea>

            Questions File :
            <div class="input-group">
              <input type="File" name="question-file" class="form-control"  accept=".pdf"aria-describedby="basic-addon2">
              <span class="input-group-addon" id="basic-addon2"><a target="_blank" href="digital/<?= $material['question_file'] ?>" >View Original File</a></span>
            </div>
            Options File :
            <div class="input-group">
              <input type="File" name="answer-file" class="form-control"  accept=".pdf"aria-describedby="basic-addon2">
              <span class="input-group-addon" id="basic-addon2"><a target="_blank" href="digital/<?= $material['option_file'] ?>" >View Original File</a></span>
            </div>
            Hints File :
            <div class="input-group">
              <input type="File" name="hint-file" class="form-control"  accept=".pdf"aria-describedby="basic-addon2">
              <span class="input-group-addon" id="basic-addon2"><a target="_blank" href="digital/<?= $material['hint_file'] ?>" >View Original File</a></span>
            </div>

            <input type="hidden" name="edit-id" value="<?= $material['id'] ?>">
            <p style="text-align: center;"><input style="margin-top: 20px;" class="btn btn-primary" type="submit" name="edit-submit" value="Save Changes"  ></p>
        </div>
    </form>
</div>