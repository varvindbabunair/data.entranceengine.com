<?php
include 'includes/materials.php';
$tests = new Materials();
?>

<div class="col-lg-12">
    <h2><span class="fa fa-cogs"></span> General Exam Settings</h2>
    <hr>
</div>
<div class="col-lg-2">
    General positive Mark
    <div class="input-group">
        <input type="text" class="form-control" value="<?php echo $tests->get_test_settings('general-positive-mark'); ?>">
      <span class="input-group-btn">
          <button class="btn btn-secondary " type="button"><span class="fa fa-edit"></span> Update</button>
      </span>
    </div>
</div>
<div class="col-lg-2">
    General negative Mark
    <div class="input-group">
        <input type="text" class="form-control" value="<?php echo $tests->get_test_settings('general-negative-mark'); ?>">
      <span class="input-group-btn">
          <button class="btn btn-secondary " type="button"><span class="fa fa-edit"></span> Update</button>
      </span>
    </div>
</div>

<div class="col-lg-12">
        <hr>
        <h4><u>Instructions to candidates</u></h4>
        <input type="checkbox" id="general-instruction-check" name="general-instruction" /> Same as General Instructions
        <textarea id="instruction" class="form-control" name="instruction"><?php echo get_test_settings('general-instruction'); ?></textarea>
    </div>