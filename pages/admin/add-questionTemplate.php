<?php
include 'includes/questions.php';
$questions = new Questions();

?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Questions <small> Question Template</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-list-alt"></i> Questions
                </li>
                <li class="active">
                    <i class="fa fa-plus"></i>  Question Template
                </li>
            </ol>
        </div>
    </div>
    <?php
            if($_GET['qn-add'] == 'success'){
            ?>
            <div class="alert alert-success">
                Template Successfully added.
            </div>
            <?php
            }
            ?>
    <!-- /.row -->
    <form id="qn-addForm" enctype="multipart/form-data" method="POST" action="submit/questionTemplate.php">
        
        <div class="row">
            <div class="col-md-4">
                
                Class :
                <select class="form-control class-list" name="class-name">
                    <option>Select Class</option>
                <?php
                $class_select_query = $db->query("SELECT * FROM class_list");
                while ($class = mysqli_fetch_array($class_select_query)) {
                    echo '<option code="'.$class['code'].'" value="'.$class['id'].'">';
                    echo $class['title'];
                    echo '</option>';
                }

                ?>
                </select>
            </div>
        </div>
        <br>
        <div class="" style="width: 100%;overflow: hidden;min-height: 370px;">
            <div class="col-lg-3 subjects-head" style="display:none;padding:0px;margin:0px;"><strong>Subjects</strong>
                <div class="subjects" style="height:310px;border: 1px solid #ccc;padding: 0px;display: none;overflow-y: scroll;"></div>
                
            </div>
            <div class="col-lg-3 chapters-head" style="display:none;padding:0px;margin:0px;"><strong>Chapters</strong>
                <div class="chapters" style="height:310px;border: 1px solid #ccc;padding: 0px;display: none;overflow-y: scroll;"></div>
                
            </div>
            <div class="col-lg-3 topics-head" style="display:none;padding:0px;margin:0px;"><strong>Topics</strong>
                <div class="topics" style="height:310px;border: 1px solid #ccc;padding: 0px;display: none;overflow-y: scroll;"></div>
                
            </div>
            <div class="col-lg-3 sections-head" style="display:none;padding:0px;margin:0px;"><strong>Section</strong>
                <div class="sections" style="height:310px;border: 1px solid #ccc;padding: 0px;display: none;overflow-y: scroll;"></div>
                
            </div>
        </div>
<div class="hidden-fields">
    <input type="hidden" class="subject-id" name="subject-id" value="">
    <input type="hidden" class="chapter-id" name="chapter-id" value="">
    <input type="hidden" class="topic-id" name="topic-id" value="">
    <input type="hidden" class="section-id" name="section-id" value="">
</div>
<div class="row">
    <div class="col-lg-12"><hr></div>
</div>
        <div class="row">
            <div class="col-lg-3">
                Difficulty :
                <select class="form-control" name="difficulty">
                    <option value="easy">Easy</option>
                    <option value="medium">Medium</option>
                    <option value="hard">Hard</option>
                </select>
                Question Type :
                <select name="answer-type" class="form-control answer-category">
                    <option value="smcq">Single Answer MCQ</option>
                    <option value="mmcq">Multiple Answer MCQ</option>
                    <option value="numeric">Numeric Answer</option>
                    <option value="assertion">Assertion Reason</option>
                    <option value="match">Match the following</option>
                </select>
                Source :
                <select class="form-control" name="qn-source">
                    <option value="Aakash">Aakash</option>
                    <option value="Allen">Allen</option>
                    <option value="Arihant">Arihant</option>
                    <option value="Balagi">Balaji</option>
                    <option value="Byjus">Byjus</option>
                    <option value="Cengage">Cengage</option>
                    <option value="Chaitanya">Chaitanya</option>
                    <option value="DC Pande">DC Pande</option>
                    <option value="Dinesh">Dinesh</option>
                    <option value="Disha">Disha</option>
                    <option value="Errorless">Errorless</option>
                    <option value="Etoos">Etoos</option>
                    <option value="Fitjee">Fitjee</option>
                    <option value="GRB">GRB</option>
                    <option value="HCV">HCV</option>
                    <option value="Motion classes">Motion classes</option>
                    <option value="MTG">MTG</option>
                    <option value="Newton Classes">Newton Classes</option>
                    <option value="Resonance">Resonance</option>
                    <option value="Tata">Tata</option>
                    <option value="Truemans">Truemans</option>
                    <option value="Vidya Mandir">Vidya Mandir</option>
                    <option value="Wiley">Wiley</option>
                </select>
                Reference Number :
                <!-- <input class="form-control" name="qn-reference" /> -->

                <span style= "display:flex">
                    <input class="form-control" style="margin-right:10px" name="qn-reference" placeholder="Code" />
                    <input class="form-control" style="margin-right:10px" name="code-start" placeholder="Start"/> 
                    <input class="form-control" style="margin-right:10px" name="code-end" placeholder="End"/>
                </span>

            </div>

            <div class="col-lg-12" style="margin-top: 20px;">
                Tags :
                <input class="tags" name="tags" data-role="tagsinput" required="" value="JEE Mains, NEET " />
            </div>

        </div>
        <div class="row">
        <div class="col-lg-12" style="text-align: center;margin-top: 20px;margin-bottom: 50px;">
          <button id="qn-add-submit" type="submit" value="submit" name="question-submit" class="btn btn-primary"> Create Template</button>
        </div>
</div>
        
    </form>

    
    
    <!-- Modal -->
<div id="mediaModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Media Modal</h4>
      </div>
      <div class="modal-body">
          <form id="img-submit" method="POST" action="" enctype="multipart/form-data">
              <div style="overflow: hidden;">
              <input class="pull-left" type="file" name="media-upload" accept=".png,.jpg,.gif" />
              <input type="hidden" name="test" value="test" />
              <input type="submit" class="btn btn-sm btn-primary pull-right" value="Add Image" />
              </div>
          </form>
          <hr>
          <h5 style="border-bottom: 1px solid #ccc;">Existing Images</h5>
          <div class="media-files" style="width: 100%;max-height: 400px;overflow: hidden;overflow-y: scroll;">
            <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

