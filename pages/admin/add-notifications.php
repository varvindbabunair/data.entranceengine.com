<!-- Page Heading -->

<div class="row">

<div class="col-lg-12">

    <h1 class="page-header">

        Notification <small> Add Notifications</small>

    </h1>

    <ol class="breadcrumb">

        <li>

            <i class="fa fa-list-alt"></i> Notifications

        </li>

        <li class="active">

            <i class="fa fa-plus"></i> Add Notifications

        </li>

    </ol>

</div>

</div>

<!-- /.row -->



<div class="row">

<div class="col-lg-8">

    <form method="POST" action="submit/notifications.php">

        Title :

        <input class="form-control" type="text" name="notification-title">

        Description :

        <textarea class="form-control" name="notification-description"></textarea>

       

        Url :

        <input class="form-control" type="text" name="notification-url">

       

        <p style="text-align: center;"><br><input class="btn btn-primary" type="submit" name="notification-submit" value="Add notification"></p>

    </form>

</div>

</div>