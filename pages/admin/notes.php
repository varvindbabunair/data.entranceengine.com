<?php
    include 'includes/materials.php';
    $notes = new Materials();
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Synopsis <small> All Synopsis</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-alt"></i> Synopsis
                </li>
                <li class="active">
                    <i class="fa fa-list-alt"></i> All Synopsis
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    


    
    <div class="row" style="">
        <div class="col-lg-12">
            <form  acton="" method="POST">
            <div class="col-lg-2">
                Display :
                <select name="display-number" onchange="this.form.submit()" class="form-control">
                    <option <?php echo (isset($_POST['display-number']) && $_POST['display-number'] == '100')?'selected':''; ?> value="100">100</option>
                    <option <?php echo (isset($_POST['display-number']) && $_POST['display-number'] == '500')?'selected':''; ?> value="500">500</option>
                    <option <?php echo (isset($_POST['display-number']) && $_POST['display-number'] == '1000')?'selected':''; ?> value="1000">1000</option>
                </select>
            </div>
            <div class="col-lg-3">
                Choose a Chapter :
                <select name="display-chapter" onchange="this.form.submit()" id="chapter-select" name="chapter" class="form-control" >
                    <option value="all">All Chapters</option>
                    <?php
                    $subject_list = $notes->get_subject_list();
                    foreach($subject_list as $subject){
                    ?>
                    <optgroup label="<?php echo $subject['title'] ?>">
                    <?php
                    $chapter_list = $notes->get_chapter_list($subject['id']);
                    foreach ($chapter_list as $chapter){
                    ?>
                        <option <?php echo (isset($_POST['display-chapter']) && $_POST['display-chapter'] == $chapter['id'])?'selected':''; ?> value="<?php echo $chapter['id']; ?>"><?php echo $chapter['title']; ?></option>
                    <?php
                    }
                    ?>
                    </optgroup>
                    <?php
                    }
                    ?>
                </select>
            </div>
                <div class="col-lg-2">
                    Section :
                    <select name="display-section" class="form-control" onchange="this.form.submit()">
                        <?php
                        if(!isset($_POST['display-chapter']) || $_POST['display-chapter'] =='all'){
                            echo '<option value="" >No chapter Selected</option>';
                        }else{
                            $cid = $_POST['display-chapter'];
                            $section_get_query = $notes->query("SELECT * FROM section_list WHERE chapter = '$cid'");
                            echo '<option value="" >Select Section</option>';
                            while($section = mysqli_fetch_array($section_get_query)){
                                echo '<option ';
                                echo ($_POST['display-section'] == $section['id'])?'selected':'';
                                echo ' value="'.$section['id'].'" >'.$section['title'].'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
            <div class="col-lg-2">
                Choose a tag :
                <select name="display-tag" onchange="this.form.submit()" id="chapter-select" name="chapter" class="form-control" >
                    <option value="all">All Tags</option>
                    <?php
                    $tag_list = $notes->get_tag_list();
                    foreach($tag_list as $tag){
                    ?>
                    <option <?php echo (isset($_POST['display-tag']) && $_POST['display-tag'] == $tag)?'selected':''; ?> value="<?php echo $tag; ?>"><?php echo $tag; ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            </form>
        
            <a href="?page=add-notes"><button class="btn btn-primary pull-right"><span class="fa fa-plus "></span> Add Synopsis</button></a>
        </div>   
    </div>
    <div class="row">
        <div class="col-lg-12" style="margin-top:20px;margin-bottom:50px;" >
            
            <table class="table table-hover table-condensed table-striped" style="font-size:12px;" >
                <thead><th>#</th><th>Notes Title</th><th>Subject</th><th>Chapter</th><th>Section</th><th>Tags</th><th>Added By</th><th>Option</th></thead>
            <?php
            if(isset($_POST['display-number']) && isset($_POST['display-chapter']) && isset($_POST['display-tag'])){
                
                $note_select_query = "SELECT * FROM notes_list";
                
                $chapter = trim($_POST['display-chapter']);
                $chapter_part = '';
                if($chapter != 'all'){
                    $chapter_part = "chapter = '".$chapter."'";
                    $note_select_query .= " WHERE ".$chapter_part;
                }
                if(isset($_POST['display-section']) && $_POST['display-section'] !=''){
                    $section = $_POST['display-section'];
                    $section_part = '';
                    if($section != ''){
                        $section_part = "section = '".$section."'";
                        $note_select_query .= " AND ".$chapter_part;
                    }
                }
                $tag = $_POST['display-tag'];
                if($tag != 'all'){
                    $tagspart = "tags LIKE '%".$tag."%'";
                    if($chapter != 'all'){
                        $note_select_query .= " AND ".$tagspart;
                    }else{
                        $note_select_query .= " WHERE ".$tagspart;
                    }
                }
                $limit = $_POST['display-number'];
                
                $note_select_query .= " ORDER BY id DESC LIMIT ".$limit;
            }else{
                $note_select_query = "SELECT * FROM notes_list ORDER BY id DESC LIMIT 100";
            }
            $note_get_query = $db->query($note_select_query)or die(mysqli_error($db->db_link));
            $cnt = 1;
            while($note_info = mysqli_fetch_array($note_get_query)){
            ?>
            <tr>
                <td>
                    <?php echo $cnt++; ?>
                </td>
                <td>
                    <?php
                        echo $note_info['title'];
                    ?>
                </td>
                <td>
                    <?php echo $notes->get_subject_name($note_info['chapter']); ?>
                </td>
                <td>
                    <?php echo $notes->get_chapter_name($note_info['chapter']).""; ?>
                </td>
                <td>
                    <?php echo $notes->get_section_name($note_info['section']).""; ?>
                </td>
                <td>
                    <?php echo $note_info['tags']; ?>
                </td>
                <td>
                    <?php echo $user->get_name($note_info['added_by']); ?>
                </td>
                <td noofTest="<?php echo $notes->get_noof_test($note_info['id']); ?>" noofPackage="<?php echo $notes->get_noof_package($note_info['id']); ?>">
                    <a href="?page=edit-note&id=<?= $note_info['id'] ?>"><button class="btn btn-primary btn-xs" ><span  class="fa fa-edit"></span> Edit</button></a>
                    <button class="btn btn-primary btn-xs qn-remove" data-toggle="modal" data-target="#qn-removeModal" removeid="<?php echo $note_info['id']; ?>" ><span class="fa fa-remove"></span> Remove</button>
                    <button class="btn btn-xs btn-primary qn-view" qnid="<?= $note_info['id'] ?>" data-toggle="modal" data-target="#qnviewModal" ><span class="fa fa-eye"></span> View</button>
                </td>
            </tr>
            <?php
            }
            ?>
            </table>
            
        </div>
  
</div>

<div id="qn-removeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remove Synopsis</h4>
      </div>
        <form method="POST" action="submit/notes.php" >
            <div class="modal-body">
                <p><strong>Are you sure you want to remove the following synopsis from the database ?</strong></p>
                <p>This action will permanently remove synopsis from the database. This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" value="" id="qn-remove-id" name="delete-id" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <input type="submit" name="delete-submit" id="delete-note" class="btn btn-primary" value="Delete Synopsis" />
            </div>
        </form>
    </div>

  </div>
</div>

    
    <!-- Modal -->
<div id="qnviewModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Notes</h4>
      </div>
      <div class="modal-body qn-content">
          <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>