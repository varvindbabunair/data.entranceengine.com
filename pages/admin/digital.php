<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Add Digital Materials
            </h1>
            <ol class="breadcrumb">
                <li>
                    Digital Materials
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<table class="table table-condensed">
			<tr><th>id</th><th>Title</th><th>Question</th><th>Options</th><th>Hint</th><th></th></tr>
			<?php
			$db_select = $db->query("SELECT * FROM digital_list");
			while($digital_material = mysqli_fetch_array($db_select)){
			?>
			<tr>
				<td><?= $digital_material['id'] ?></td>
				<td>
					<h4><?= $digital_material['title'] ?></h4>
					<p><?= $digital_material['description'] ?></p>
				</td>
				<td><a target="_blank" href="digital/<?= $digital_material['question_file'] ?>"><button class="btn btn-warning btn-xs"> Question File</button></a></td>
				<td><a target="_blank" href="digital/<?= $digital_material['option_file'] ?>"><button class="btn btn-warning btn-xs"> Option File</button></a></td>
				<td><a target="_blank" href="digital/<?= $digital_material['hint_file'] ?>"><button class="btn btn-warning btn-xs"> Hint File</button></a></td>
				<td>
					<a href="?page=edit-digital&id=<?= $digital_material['id'] ?>"><button class="btn btn-primary btn-xs">Edit</button> </a>
					<button class="btn btn-primary btn-xs dg-remove" removeid="<?= $digital_material['id'] ?>" data-toggle="modal" data-target="#dg-removeModal">Delete</button>
				</td>
			</tr>
			<?php
			}
			?>
		</table>
	</div>
</div>


<div id="dg-removeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remove Material</h4>
      </div>
        <form method="POST" action="submit/digital.php" >
            <div class="modal-body">
                <p><strong>Are you sure you want to remove the digital material from the database ?</strong></p>
                <p>This action will permanently remove digital material from the database. This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" value="" id="dg-remove-id" name="delete-id" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <input type="submit" name="delete-submit" id="delete-digital" class="btn btn-primary" value="Delete Material" />
            </div>
        </form>
    </div>

  </div>
</div>