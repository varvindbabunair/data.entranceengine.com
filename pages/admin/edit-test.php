<?php
if(!isset($_GET['id']) || trim($_GET['id']) ==''){
    die('<h3 style="text-align:center">Some Error Occured</h3>');
}
include 'includes/materials.php';
$tests = new Materials();
$id = $_GET['id'];

$test = $tests->get_test_details($id);
$question_arr = json_decode($test['questions']);

$section_num = json_decode($test['section_title']);
?>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    Name for Test :
    <input class="form-control test-name" name="test-name" type="text" value="<?php echo $test['title']; ?>" />
    <input type="hidden" name="id" class="edit-id" value="<?php echo $id; ?>" />
</div>
<div style="width:100%;overflow:hidden">
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
        Number of sections :
        <input class="form-control test-sections" name="test-sections" type="number" value="<?php echo sizeof($section_num); ?>" min="1" />
    </div>
</div>
<div style="width:100%;overflow:hidden">
<div class="exam-time col-lg-2">
        Total Time of Exam
        <div class="input-group">
            <input type="text" class="form-control total-exam-time" aria-describedby="basic-addon2" value="<?php $section_rule = json_decode($test['section_rule']); echo $section_rule[1]; ?>" >
          <span class="input-group-addon" id="basic-addon2">minutes</span>
        </div>
    </div>
</div>
<div style="width:100%;overflow:hidden">
    <div class="col-lg-4">
        Tags :
        <input class="form-control tags" value="<?php echo $test['tags']; ?>" type="text" />
    </div>
    <script>
        $('.tags').tagsinput('refresh');
    </script>
</div>

<?php
$section_num = 1;
foreach($question_arr as $question){
    
?>

<div class="section <?php echo ($section_num != 1)?'added-section':''; ?>" style="width:100%; overflow: hidden;">
    <div class="col-lg-12"><hr></div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 " style="text-align:center;"><h3>Section <span class="section-number"><?php echo ($section_num > 1 || sizeof($question_arr) > 1 )?$section_num:''; ?></span></h3></div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <span class="">Section Title</span>
        <input name="section-name" type="text" class="form-control section-title" value="<?php $section_title = json_decode($test['section_title']); echo $section_title[$section_num-1]; ?>" />
    </div>
    
    <div style="width: 100%;overflow: hidden;">
        <div class="col-lg-12"><h4><u>Marking Scheme</u></h4></div>
        <div class="col-lg-3">
            Mark for Each Correct Answer :
            <input class="form-control" type="number" id="correct-answer-mark" value="4" name="correct-answer-mark" />
        </div>
        <div class="col-lg-3">
            Negative Mark for Each Wrong Answer :
            <div class="input-group">
                <input style="width: 50%;" type="number" id="wrong-answer-mark" class="form-control" value="1" />
                <select style="width: 50%;" id="wrong-answer-mark-type" class="form-control">
                    <option value="mark">Marks</option>
                    <option value="percent">in %</option>
                </select>
            </div>
        </div>
        <div class="col-lg-3 section-indtime" style="display: none;">
            Time For Section
            <div class="input-group">
                <input type="text" id="time-for-section" class="form-control each-section-time" />
                <span class="input-group-addon" id="basic-addon2">minutes</span>
            </div>
        </div>
        <div class="col-lg-12"><hr></div>
    </div>

    
        <div class="clearfix"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        Select Subject:
        <select name="test-subjects[]" id="test-subjects<?php echo $section_num; ?>" class="form-control test-subjects" multiple="multiple"  >
            <?php
            $subject_list = $tests->get_subject_list();
            foreach ($subject_list as $subject){
                ?>
            <optgroup label="<?php echo $subject['title'];  ?>">
            <?php 
            $chapter_list = $tests->get_chapter_list($subject['id']);
            foreach($chapter_list as $chapter){
                echo '<option value="'.$chapter['id'].'">'.$chapter['title'].'</option>';
                }
            ?>
            </optgroup>
            <?php

            }
            ?>
        </select>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        Select Tags :
        <select name="test-tags[]" id="test-tags<?php echo $section_num; ?>" class="form-control test-tags" multiple="multiple">
            <?php
            $tags = $tests->get_tag_list();

            foreach($tags as $tag){
                echo '<option value="'.$tag.'">'.$tag.'</option>';
            }
            ?>
        </select>
        </div>
        <div class="clearfix"></div>
    


    <div class="col-lg-12" style="margin-top: 30px;">
        <div class="col-lg-5 col-md-5 question-list">
            <h4 style="text-align:center"><u>Availiable Questions</u></h4>
            <ul id="sortable<?php echo (($section_num -1)*2)+1; ?>" class="connectedSortable ">

            </ul>
        </div>
        <div class="col-md-1 col-lg-1">
            <button class="btn btn-warning form-control" style="margin-top:50%;"> &Rrightarrow;</button>
        </div>
        <div class="col-lg-6 col-md-6 question-list">
            <h4 style="text-align:center"><u>Selected Questions</u></h4>
            <ul id="sortable<?php echo (($section_num -1)*2)+2; ?>" class="connectedSortable ">
                <?php
                $qn_num = 1;
                foreach($question as $qn){
                    $test_qn = $tests->get_question($qn[0]);
                    ?>
                <li style="overflow: hidden;" id="qn_<?php echo $test_qn['id']; ?>" class="ui-state-default">
                    <div class="col-lg-1 order" style=""><b><?php echo $qn_num; ?></b></div>
                    <div class="col-lg-8">
                        <?php
                                            echo ($test_qn['question_type'] == 'text')?$test_qn['question']:'<img style="width:100%" src="images/questions/'.$test_qn['question'].'" />';
                        ?>
                    </div>
                    <div style="" class="col-lg-3 pull-right mark">
                        <div class="input-group input-group-sm">
                        <input id="qn_<?php echo $test_qn['id']; ?>_plus" value="<?php echo ($qn[1] != 4)?$qn[1]:''; ?>" minval="0" style="width:50%" class="form-control" placeholder="+" title="Marks for correct Answer other than global" type="number">
                        <input id="qn_<?php echo $test_qn['id']; ?>_minus" value="<?php echo ($qn[1] != 4)?$qn[2]:''; ?>" minval="0" style="width:50%" class="form-control" placeholder="-" title="Negative Marks for wrong Answer other than global" type="number">
                      </div>
                    </div>
                </li>
                <?php
                $qn_num++;
                }
                ?>
            </ul>
        </div>
    </div>
    
    
    
    
    <script>
        
    
    $('#test-subjects<?php echo $section_num; ?>').multiselect({
        columns: 2,
        search : true,
        selectAll : true,
        selectGroup : true,
        texts: {
            placeholder: 'Select chapters',
            selectAll:'Select all Chapters',
            search:'Search Chapters'
        }
    });
    $('#test-tags<?php echo $section_num; ?>').multiselect({
        columns: 3,
        texts: {
            placeholder: 'Select Tags'
        }
    });
    

          
          
    $('#test-subjects<?php echo $section_num; ?>').change(function(){
        var subjects = $(this).val();
        var tags = $('.test-tags').val();
        var excludes = $( "#sortable<?php echo (($section_num -1)*2)+2; ?>" ).sortable( "serialize", { attribute: "id" });
        $.post("ajax/create-test.php",{
            tag : tags,
            subject : subjects,
            exclude : excludes
        },
        function(responseTxt){
            $('#sortable<?php echo (($section_num -1)*2)+1; ?>').html(responseTxt);
        });
    });
    
    
    $('#test-tags<?php echo $section_num; ?>').change(function(){
        var subjects = $('.test-subjects').val();
        var tags = $(this).val();
        var excludes = $( "#sortable<?php echo (($section_num -1)*2)+2; ?>" ).sortable( "serialize", { attribute: "id" });
        if(excludes == null){
            excludes = '';
        }
        $.post("ajax/create-test.php",{
            tag : tags,
            subject : subjects,
            exclude : excludes
        },
        function(responseTxt){
            $('#sortable<?php echo (($section_num -1)*2)+1; ?>').html(responseTxt);
        });
    });
    
    
    
            $( "#sortable<?php echo (($section_num -1)*2)+1; ?>, #sortable<?php echo $section_num*($section_num -1)+2; ?>" ).sortable({
            connectWith: ".connectedSortable"
          }).enableSelection();
    
    
    
    $( "#sortable<?php echo (($section_num -1)*2)+2; ?>" ).sortable({
        receive: function( event, ui ) {
            ui.item.children(".mark").show();
            ui.item.children(".order").show();
            
            var totalElements = ui.item.parent('ul').children('li').size();
            for(var i = 0;i<totalElements;i++){
                var j = i+1;
                ui.item.parent('ul').children('li').eq(i).children('.order').html("<b>" + j + "</b>");
            }
        },
        stop: function( event, ui ) {
//            var idstring = $( "#sortable2" ).sortable( "serialize", { attribute: "id" });
            var totalElements = ui.item.parent('ul').children('li').size();
            for(var i = 0;i<totalElements;i++){
                var j = i+1;
            ui.item.parent('ul').children('li').eq(i).children('.order').html("<b>" + j + "</b>");
            }

        }
    });
    
    $( "#sortable<?php echo (($section_num -1)*2)+1; ?>" ).sortable({
        receive: function( event, ui ) {
            ui.item.children(".mark").hide();
            ui.item.children(".order").hide();
            var totalElements = $( "#sortable<?php echo (($section_num -1)*2)+2; ?>" ).children('li').size();
            for(var i = 0;i<totalElements;i++){
                var j = i+1;
                $( "#sortable<?php echo (($section_num -1)*2)+2; ?>" ).children('li').eq(i).children('.order').html("<b>" + j + "</b>");
            }
        }
    });
        
        </script>
    
</div>

<?php 
$section_num++;
}
?>

<div id="selectChoose" style="display: none;">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        Select Subject:
        <select name="test-subjects[]" id="test-subjects1" class="form-control test-subjects" multiple="multiple"  >
            <?php
            $subject_list = $tests->get_subject_list();
            foreach ($subject_list as $subject){
                ?>
            <optgroup label="<?php echo $subject['title'];  ?>">
            <?php 
            $chapter_list = $tests->get_chapter_list($subject['id']);
            foreach($chapter_list as $chapter){
                echo '<option value="'.$chapter['id'].'">'.$chapter['title'].'</option>';
                }
            ?>
            </optgroup>
            <?php

            }
            ?>
        </select>
    </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        Select Tags :
        <select name="test-tags[]" id="test-tags1" class="form-control test-tags" multiple="multiple">
            <?php
            $tags = $tests->get_tag_list();

            foreach($tags as $tag){
                echo '<option value="'.$tag.'">'.$tag.'</option>';
            }
            ?>
        </select>
        </div>
    </div>
<div style="width: 100%;">
    
    
    
    <div class="col-lg-12">
        <hr>
        <h4><u>Instructions to candidates</u></h4>
        <input type="checkbox"  id="general-instruction-check" name="general-instruction" /> Same as General Instructions
        <textarea id="instruction" class="form-control" name="instruction"><?php echo $test['instruction']; ?></textarea>
    </div>
</div>

<div class="col-lg-12" style="margin-bottom:50px;margin-top:50px;text-align: center">
    <button id="save-test" class="btn btn-lg btn-primary"><b><span class="fa fa-save"></span> Save Changes</b></button>
</div>

<!-- Modal -->
<div id="create-test-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" >Modal Header</h4>
      </div>
      <div class="modal-body">
          <p class="modal-message">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div class="general-instruction" style="display:none;">
    <?php
    echo $tests->get_test_settings('general-instruction');
    ?>
</div>