<?php
    include 'includes/materials.php';
    $notes = new Materials();
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Synopsis <small> Add Synopsis</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-alt"></i> Synopsis
                </li>
                <li class="active">
                    <i class="fa fa-list-alt"></i> Add Synopsis
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

<div class="row">
    
            <form action="submit/notes.php" method="POST" >
                <div class="col-lg-6">
                  Select Chapter :
                  <select name="chapter" class="form-control chapter-select">
                      <option>Select from Chapter List</option>
                    <?php
                    $subject_list = $notes->get_subject_list();
                    foreach($subject_list as $subjects){
                        echo '<optgroup label="'.$subjects['title'].'">';
                        $chapter_list = $notes->get_chapter_list($subjects['id']);
                        foreach($chapter_list as $chapter){
                            echo '<option value="'.$chapter['id'].'">'.$chapter['title'].'</option>';
                        }
                        echo '</optgroup>';
                    }
                    ?>
                  </select>
                  
                  Section :
                <select name="section" class="form-control section-select">
                    <option>Select Section</option>
                </select>
                </div>
                <div class="col-lg-12">
                  Notes Title:
                  <input name="title" type="text" class="form-control" />
                  Tags :
                  <input name="tags" class="form-control tags" />
                  <script>
                      $('.tags').tagsinput();
                    </script>
                    <div class="col-lg-12" style="margin-top: 20px;">
                <div class="btn btn-xs btn-primary pull-right media-modal-btn" data-toggle="modal" data-target="#mediaModal"><span class="fa fa-plus"></span> Add Media</div>
            </div>
                  Notes :
                  <textarea class="form-control ckeditor" name="editor1"></textarea>
                  <br>
                  <div style="text-align: center;">
                      <input type="submit" name="note-submit" value="Submit Note" class="btn btn-primary" />
                  </div>
                  </div>
            </form>
    
</div>
    
        
    <!-- Modal -->
<div id="mediaModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Media Modal</h4>
      </div>
      <div class="modal-body">
          <form id="img-submit" method="POST" action="" enctype="multipart/form-data">
              <div style="overflow: hidden;">
              <input class="pull-left" type="file" name="media-upload" accept=".png,.jpg,.gif" />
              <input type="hidden" name="test" value="test" />
              <input type="submit" class="btn btn-sm btn-primary pull-right" value="Add Image" />
              </div>
          </form>
          <hr>
          <h5 style="border-bottom: 1px solid #ccc;">Existing Images</h5>
          <div class="media-files" style="width: 100%;max-height: 400px;overflow: hidden;overflow-y: scroll;">
            <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

