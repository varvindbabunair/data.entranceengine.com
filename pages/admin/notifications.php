<!-- Page Heading -->

<div class="row">

<div class="col-lg-12">

    <h1 class="page-header">

        Notifications <small> All Notifications</small>

    </h1>

    <ol class="breadcrumb">

        <li>

            <i class="fa fa-list-alt"></i> Notifications

        </li>

        <li class="active">

            <i class="fa fa-list-alt"></i> All Notifications

        </li>

    </ol>

</div>

</div>

<!-- /.row -->



<div class="row">

<div class="col-lg-12">

<table class="table table-bordered">

    <thead><th>Id</th><th>Title</th><th>Option</th> </thead>

    <?php

    $notification_select = $db->query("SELECT * FROM notifications_list WHERE 1 ORDER BY id DESC");

    while($notification = mysqli_fetch_array($notification_select)){

        ?>

        <tr>

            <td><?= $notification['id'] ?></td>

            <td><h4><?= $notification['title'] ?></h4><p><?= $notification['description'] ?></p>

            <?php if($notification['url'] != "") { ?><p><b>Url: </b><?= $notification['url'] ?></p><?php } ?>

            </td>

             <td>

                <button class="btn btn-xs btn-primary view-noti" editid="<?php echo $notification['id']; ?>" data-toggle="modal" data-target="#notiEditModal"><span class="fa fa-edit"></span> Edit</button>

                <button class="btn btn-xs btn-primary delete-noti" removeid="<?php echo $notification['id']; ?>" data-toggle="modal" data-target="#notiRemoveModal"><span class="fa fa-remove"></span> Delete</button>

            </td>

        </tr>

        <?php

    }

    ?>

</table>

</div>

</div>







<div id="notiEditModal" class="modal fade" role="dialog">

<div class="modal-dialog modal-md">



<!-- Modal content-->

<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal">&times;</button>

<h4 class="modal-title">Edit Notification Details</h4>

</div>

<form action="submit/notifications.php" method="POST">

    <div class="modal-body notification-body">

       

    </div>

    <div class="modal-footer">

     

      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      <input type="submit" name="edit-submit" class="btn btn-primary" value="Save Changes" />

    </div>

</form>

</div>



</div>

</div>





<div id="notiRemoveModal" class="modal fade" role="dialog">

<div class="modal-dialog modal-md">



<!-- Modal content-->

<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal">&times;</button>

<h4 class="modal-title">Remove Notification</h4>

</div>

<form action="submit/notifications.php" method="POST">

    <div class="modal-body">

        <h3>Are you sure you want to delete this notification. ?</h3>

        <input type="hidden" value="" name="delete-id" class="delete-id" />

    </div>

    <div class="modal-footer">

        

      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      <input type="submit" name="remove-notification-submit" class="btn btn-primary" value="Confirm Delete" />

    </div>

</form>

</div>



</div>

</div>







<script type="text/javascript">



$(document).ready(function(){





$('.view-noti').click(function(){

var id = $(this).attr('editid');

$.ajax({

    url:'ajax/notification-view-edit.php',

    type:'POST',

    data:{

        id : id

    },

    beforeSend:function(){

        $('.notification-body').html('<p style="text-align:center;"><span class="fa fa-spinner fa-spin"></span> Loading...</p>');

    },

    success:function(result,status){

        $('.notification-body').html(result);

    }

});

});



$('.delete-noti').click(function(e){



$('.delete-id').val($(this).attr('removeid'));

});



});

</script>