<?php

    include 'includes/app_user.php';

    $user = new app_user();

?>

 

<!-- Page Heading -->

    <div class="row">

        <div class="col-lg-12">

            <h1 class="page-header">

                Users <small>App Users</small>

            </h1>

            <ol class="breadcrumb">

                <li class="active">

                    <i class="fa fa-user"></i> App Users

                </li>

            </ol>

        </div>

    </div>

    <!-- /.row -->

 

    <div class="col-lg-12">

        <?php /*<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#userAddModal"><span class="fa fa-plus"></span> Add New Users</button>*/ ?>

    </div>

    <div class="col-lg-12" style="margin-top:20px;">

    <div class="panel panel-primary">

        <div class="panel-heading">

           

            <h4 class="panel-title">App Users</h4>

        </div>

        <div class="panel-body">

            <table class="table table-condensed table-hover">

                <thead><th>Name</th><th>Contact</th> <th>Options</th></thead>

            <?php

            $users = $user->get_users('app_user');

            foreach ($users as $individual){

                ?>

            <tr>

                <td><?php echo $individual['name'].' '.$individual['last_name']; ?></td>

                <td><span class="fa fa-envelope"></span> <?php echo $individual['email']; ?><br><span class="fa fa-phone"></span> <?php echo $individual['phone']; ?></td>

                 

                <td>

                     <button class="btn btn-xs btn-primary view-app-user" editid="<?php echo $individual['id']; ?>" data-toggle="modal" data-target="#userViewModal"><span class="fa fa-edit"></span> View

                     </button>

                 

                     

                     <button class="btn btn-xs btn-primary delete-app-user" removeid="<?php echo $individual['id']; ?>" data-toggle="modal" data-target="#userRemoveModal"><span class="fa fa-remove"></span> Delete</button>

                   

                </td>

            </tr>

                <?php

            }

            ?>

            </table>

        </div>

    </div>

</div>

 

 

<div id="userAddModal" class="modal fade" role="dialog">

  <div class="modal-dialog modal-md">

 

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Add New User</h4>

      </div>

        <form action="submit/users.php" method="POST">

            <div class="modal-body">

                Name :

                <input class="form-control" value="" name="name"/>

                Email :

                <input class="form-control" type="email" value="" name="email"/>

                Password :

                <input class="form-control" type="text" value="" name="password"/>

                Phone :

                <input class="form-control" type="" value="" name="phone"/>

                User Role :

                <select name="user-privilage" class="form-control">

                    <option value="admin">Administrator</option>

                    <option value="data-entry">Data Entry</option>

                </select>

                <input type="hidden"  name="user-role" value="admin" />

 

            </div>

            <div class="modal-footer">

              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

              <input type="submit" name="new-user-submit" class="btn btn-primary" value="Add User" />

            </div>

        </form>

    </div>

 

  </div>

</div>

 

 

<div id="userViewModal" class="modal fade" role="dialog">

  <div class="modal-dialog modal-md">

 

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">App User Details</h4>

      </div>

       

            <div class="modal-body view-app-user-body">

               

            </div>

            <div class="modal-footer">

                <input type="hidden"  name="user-role" value="admin" />

              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 

            </div>

       

    </div>

 

  </div>

</div>

 

 

<div id="userRemoveModal" class="modal fade" role="dialog">

  <div class="modal-dialog modal-md">

 

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Remove User</h4>

      </div>

        <form action="submit/users.php" method="POST">

            <div class="modal-body">

                <h3>Are you sure you want to delete this user. ?</h3>

                <input type="hidden" value="" name="delete-user" class="delete-user" />

            </div>

            <div class="modal-footer">

                <input type="hidden"  name="user-role" value="admin" />

              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

              <input type="submit" name="remove-user-submit" class="btn btn-primary" value="Confirm Delete" />

            </div>

        </form>

    </div>

 

  </div>

</div>

 

 

<div id="userResetModal" class="modal fade" role="dialog">

  <div class="modal-dialog modal-sm">

 

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Reset User Password</h4>

      </div>

        <form action="submit/users.php" method="POST">

            <div class="modal-body">

                New Password :

                <input type="text" class="form-control" name="new-password" />

                <input type="hidden"  name="user-id" class="user-id" />

                <input type="hidden"  name="user-role" value="admin" />

            </div>

            <div class="modal-footer">

              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

              <input type="submit" name="reset-user-submit" class="btn btn-primary" value="Reset Password" />

            </div>

        </form>

    </div>

 

  </div>

</div>

 

<script type="text/javascript">

   

    $(document).ready(function(){

    $('.view-app-user').click(function(){

        var id = $(this).attr('editid');

        $.ajax({

            url:'ajax/app-user-view.php',

            type:'POST',

            data:{

                id : id

            },

            beforeSend:function(){

                $('.view-app-user-body').html('<p style="text-align:center;"><span class="fa fa-spinner fa-spin"></span> Loading...</p>');

            },

            success:function(result,status){

                $('.view-app-user-body').html(result);

            }

        });

    });

   

    $('.delete-app-user').click(function(){

        $('.delete-user').val($(this).attr('removeid'));

    });

   

});

</script>