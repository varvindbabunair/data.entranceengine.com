<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-list-alt fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <?php
                            $qn_vlcnt_fetch = $user->query("SELECT id FROM question_list");
                        ?>
                        <div class="huge"><?= mysqli_num_rows($qn_vlcnt_fetch) ?></div>
                        <div>Questions</div>
                    </div>
                </div>
            </div>
            <a href="?page=questions">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    
</div>

<div class="row">
<div class="col-sm-3">
    User :
    <select class="form-control user-id" >
        <option value="all">All Users</option>
        <?php
        $users = $user->get_users('admin');
            foreach ($users as $individual){
                echo '<option value="'.$individual['id'].'">';
                echo $individual['name'];
                echo '</option>';
            }
        ?>
    </select>
</div>
<div class="col-sm-3">
    Days :
    <select class="form-control days">
        <option value="7">Last 7 days</option>
        <option value="30">Last 30 days</option>
    </select>
</div>
</div>

<div class="row">
<div class="col-sm-12" style="margin-top:20px;">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Questions Summary</h3>
            </div>
            <div class="panel-body">
                <?php
                $add_qry = $db->query("SELECT question_list.add_date, COUNT(question_list.id) as total_added FROM question_list WHERE question_list.add_date > DATE_SUB(CURDATE(), INTERVAL 7 DAY) GROUP BY question_list.add_date ORDER BY add_date ASC");
                $val_qry = $db->query("SELECT question_list.validate_date, COUNT(question_list.id) as total_validated FROM question_list WHERE validated_by != 0 AND question_list.validate_date > DATE_SUB(CURDATE(), INTERVAL 7 DAY) GROUP BY question_list.validate_date ORDER BY validate_date ASC");
                $data = array();
                while ($add = mysqli_fetch_array($add_qry)){
                    $data[$add['add_date']]['added'] = $add['total_added'];
                }
                while ($add = mysqli_fetch_array($val_qry)){
                    $data[$add['validate_date']]['validated'] = $add['total_validated'];
                }
                ?>
                <div id="myfirstchart" style="height: 250px;"></div>
                <script>
                new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'myfirstchart',
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.
                    
                    data: [
                        <?php
                        $str = "";
                foreach ($data as $key=>$value){
                    $str .= "{dates:'".date('d M Y', strtotime($key))."', entered: ";
                    $str .= (isset($data[$key]['added']))?$value['added']:0;
                    $str .= ", validated: ";
                    $str .= (isset($data[$key]['validated']))?$value['validated']:0;
                    $str .= "},";
                    
                }
                echo trim($str, ',');
                      ?>         
                    ],
                    
                    // The name of the data record attribute that contains x-values.
                    xkey: 'dates',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['entered','validated'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Questions Entered','Questions Validated']
                  });
                </script>
            </div>
        </div>
    </div>
</div>