<?php
    include 'includes/materials.php';
    $notes = new Materials();
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Synopsis <small> Add Synopsis</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-sticky-note"></i> Synopsis
                </li>
                <li class="active">
                    <i class="fa fa-plus"></i> Add Synopsis
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <div class="row" style="margin-bottom:50px;">
    
            <form action="submit/synopsys.php" method="POST" >
                <div class="col-lg-3">
                  Class :
                <select class="form-control class-list" name="class-name">
                    <option>Select Class</option>
                <?php
                $class_select_query = $db->query("SELECT * FROM class_list");
                while ($class = mysqli_fetch_array($class_select_query)) {
                    echo '<option code="'.$class['code'].'" value="'.$class['id'].'">';
                    echo $class['title'];
                    echo '</option>';
                }

                ?>
                </select>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12">
                    <br>
        <div class="" style="width: 100%;overflow: hidden;min-height: 170px;">
            <div class="col-lg-3 subjects-head" style="display:none;padding:0px;margin:0px;"><strong>Subjects</strong>
                <div class="subjects" style="border: 1px solid #ccc;height: 150px;padding: 0px;display: none;overflow-y: scroll;"></div>
                
            </div>
            <div class="col-lg-3 chapters-head" style="display:none;padding:0px;margin:0px;"><strong>Chapters</strong>
                <div class="chapters" style="border: 1px solid #ccc;height: 150px;padding: 0px;display: none;overflow-y: scroll;"></div>
                
            </div>
        </div>
                </div>
                <br>
                
                <div class="col-lg-4">
                    Reference :
                    <input class="form-control" name="reference" value="" />
                </div>
            <div class="col-lg-12" style="margin-top: 20px;">
                <div class="btn btn-xs btn-primary pull-right media-modal-btn" data-toggle="modal" data-target="#mediaModal"><span class="fa fa-plus"></span> Add Media</div>
            </div>
                <div class="col-lg-12" style="margin-top: 20px;">
                  Notes :
                  <textarea class="form-control ckeditor" name="editor1"></textarea>
                  <br>
                  <div style="text-align: center;">
                      <input type="hidden" class="subject-id" name="subject-id" />
                      <input type="hidden" class="chapter-id" name="chapter-id" />
                      <input type="submit" id="synopsys-add-submit" name="synopsys-submit" value="Submit Synopsys" class="btn btn-primary" />
                  </div>
                  </div>
            </form>
    
</div>
    
        
    <!-- Modal -->
<div id="mediaModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Media Modal</h4>
      </div>
      <div class="modal-body">
          <form id="img-submit" method="POST" action="" enctype="multipart/form-data">
              <div style="overflow: hidden;">
              <input class="pull-left" type="file" name="media-upload" accept=".png,.jpg,.gif" />
              <input type="hidden" name="test" value="test" />
              <input type="submit" class="btn btn-sm btn-primary pull-right" value="Add Image" />
              </div>
          </form>
          <hr>
          <h5 style="border-bottom: 1px solid #ccc;">Existing Images</h5>
          <div class="media-files" style="width: 100%;max-height: 400px;overflow: hidden;overflow-y: scroll;">
            <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

