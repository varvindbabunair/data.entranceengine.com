<div class="col-lg-4">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><span class="fa fa-user"></span> My Profile</h3>
        </div>
        <div class="panel-body">
            <table class="table">
                <tr><th>Name</th><td><?php echo $user->user_details['name']; ?></td></tr>
                <tr><th>Phone</th><td><?php echo $user->user_details['phone']; ?></td></tr>
                <tr><th>Email</th><td><?php echo $user->user_details['email']; ?></td></tr>
            </table>
        </div>
    </div>
</div>