<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="<?=(!isset($_GET['page']) || $_GET['page']=='dashboard')?'active':''?>">
                        <a href="?page=dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <?php
                    if($user->user_details['admin_role']=='admin'){
                    ?>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='class')?'active':''?>">
                        <a href="?page=class"><i class="fa fa-fw fa-clipboard"></i> Class</a>
                    </li>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='subjects')?'active':''?>">
                        <a href="?page=subjects"><i class="fa fa-fw fa-bar-chart-o"></i> Syllabus</a>
                    </li>
                    <?php
                    }
                    ?>
                    <li class="<?=(isset($_GET['page']) && ($_GET['page']=='question-dashboard' || $_GET['page']=='questions' || $_GET['page']=='review-questions' || $_GET['page']=='validate-questions' || $_GET['page']=='validated-questions' || $_GET['page']=='add-questions' || $_GET['page']=='edit-question' ))?'active ':''?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#questions"><i class="fa fa-fw fa-list-alt"></i> Questions <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="questions" class="collapse <?=(isset($_GET['page']) && ( $_GET['page']=='questions' || $_GET['page']=='review-questions' || $_GET['page']=='add-questions' || $_GET['page']=='edit-question' || $_GET['page']=='validated-questions' || $_GET['page']=='validate-questions'))?'in':''?>">
                            
                            <li>
                                <a href="?page=questions"><i class="fa fa-fw fa-list-alt"></i> All Questions</a>
                            </li>
                    <?php
                    if($user->user_details['admin_role']=='admin'){
                    ?>
                            <li>
                                <a href="?page=review-questions"><i class="fa fa-fw fa-exclamation-circle"></i> Questions for review</a>
                            </li>
                    <?php
                    }
                    ?>
                            <li>
                                <a href="?page=validate-questions"><i class="fa fa-fw fa-database"></i> Questions for validation</a>
                            </li>
                            <li>
                                <a href="?page=validated-questions"><i class="fa fa-fw fa-check"></i> Validated Questions</a>
                            </li>
                            <li>
                                <a href="?page=sort-questions"><i class="fa fa-fw fas fa-sort"></i> Sort Questions</a>
                            </li>
                            <li>
                                <a href="?page=add-questions"><i class="fa fa-fw fa-plus"></i> Add Question</a>
                            </li>
                            <li>
                                <a href="?page=add-questionTemplate"><i class="fa fa-fw fa-sticky-note-o"></i> Create Template</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?=(isset($_GET['page']) && ( $_GET['page']=='synopsys' || $_GET['page']=='edit-synopsys' || $_GET['page']=='add-synopsys' || $_GET['page']=='review-synopsys' || $_GET['page']=='validate-synopsys' || $_GET['page']=='validated-synopsys' ))?'active ':''?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#synopsys"><i class="fa fa-fw fa-sticky-note"></i> Synopsis <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="synopsys" class="collapse <?=(isset($_GET['page']) && ( $_GET['page']=='synopsys' || $_GET['page']=='edit-synopsys' || $_GET['page']=='add-synopsys' || $_GET['page']=='review-synopsys' || $_GET['page']=='validate-synopsys' || $_GET['page']=='validated-synopsys' ))?'in':''?>">
                            <li>
                                <a href="?page=synopsys"><i class="fa fa-fw fa-sticky-note-o"></i> All Synopsis</a>
                            </li>
                    <?php
                    if($user->user_details['admin_role']=='admin'){
                    ?>
                            <li>
                                <a href="?page=review-synopsys"><i class="fa fa-fw fa-exclamation-circle"></i> Synopsis for Review</a>
                            </li>
                    <?php
                    }
                    ?>
                            <li>
                                <a href="?page=validate-synopsys"><i class="fa fa-fw fa-database"></i> Synopsis for Validation</a>
                            </li>
                            <li>
                                <a href="?page=validated-synopsys"><i class="fa fa-fw fa-check"></i> Validated Synopsis</a>
                            </li>
                            <li>
                                <a href="?page=add-synopsys"><i class="fa fa-fw fa-plus"></i> Add Synopsis</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?=(isset($_GET['page']) && ( $_GET['page']=='theory' || $_GET['page']=='edit-theory' || $_GET['page']=='add-theory' || $_GET['page']=='review-theory' || $_GET['page']=='validate-theory' || $_GET['page']=='validated-theory' ))?'active ':''?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#theory"><i class="fa fa-fw fa-sticky-note"></i> Theory <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="theory" class="collapse <?=(isset($_GET['page']) && ( $_GET['page']=='theory' || $_GET['page']=='edit-theory' || $_GET['page']=='add-theory' || $_GET['page']=='review-theory' || $_GET['page']=='validate-theory' || $_GET['page']=='validated-theory' ))?'in':''?>">
                            <li>
                                <a href="?page=theory"><i class="fa fa-fw fa-sticky-note-o"></i> All Theory</a>
                            </li>
                    <?php
                    if($user->user_details['admin_role']=='admin'){
                    ?>
                            <li>
                                <a href="?page=review-theory"><i class="fa fa-fw fa-exclamation-circle"></i> Theory for Review</a>
                            </li>
                    <?php
                    }
                    ?>
                            <li>
                                <a href="?page=validate-theory"><i class="fa fa-fw fa-database"></i> Theory for Validation</a>
                            </li>
                            <li>
                                <a href="?page=validated-theory"><i class="fa fa-fw fa-check"></i> Validated Theory</a>
                            </li>
                            <li>
                                <a href="?page=add-theory"><i class="fa fa-fw fa-plus"></i> Add Theory</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?=(isset($_GET['page']) && ( $_GET['page']=='notes' || $_GET['page']=='edit-notes' || $_GET['page']=='add-notes' || $_GET['page']=='review-notes' || $_GET['page']=='validate-notes' || $_GET['page']=='validated-notes' ))?'active ':''?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#notes"><i class="fa fa-fw fa-newspaper-o"></i> Notes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="notes" class="collapse <?=(isset($_GET['page']) && ( $_GET['page']=='notes' || $_GET['page']=='edit-notes' || $_GET['page']=='add-notes' || $_GET['page']=='review-notes' || $_GET['page']=='validate-notes' || $_GET['page']=='validated-notes' ))?'in':''?>">
                            <li>
                                <a href="?page=notes"><i class="fa fa-fw fa-newspaper-o"></i> All Notes</a>
                            </li>
                    <?php
                    if($user->user_details['admin_role']=='admin'){
                    ?>
                            <li>
                                <a href="?page=review-notes"><i class="fa fa-fw fa-exclamation-circle"></i> Notes for Review</a>
                            </li>
                    <?php
                    }
                    ?>
                            <li>
                                <a href="?page=validate-notes"><i class="fa fa-fw fa-database"></i> Notes for Validation</a>
                            </li>
                            <li>
                                <a href="?page=validated-notes"><i class="fa fa-fw fa-check"></i> Validated Notes</a>
                            </li>
                            <li>
                                <a href="?page=add-notes"><i class="fa fa-fw fa-plus"></i> Add Notes</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?=(isset($_GET['page']) && ( $_GET['page']=='videos' || $_GET['page']=='edit-videos' || $_GET['page']=='add-videos' || $_GET['page']=='review-videos' || $_GET['page']=='validate-videos' || $_GET['page']=='validated-videos' ))?'active ':''?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#videos"><i class="fa fa-fw fa-play-circle-o"></i> Videos <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="videos" class="collapse <?=(isset($_GET['page']) && ( $_GET['page']=='videos' || $_GET['page']=='edit-videos' || $_GET['page']=='add-videos' || $_GET['page']=='review-videos' || $_GET['page']=='validate-videos' || $_GET['page']=='validated-videos' ))?'in':''?>">
                            <li>
                                <a href="?page=videos"><i class="fa fa-fw fa-play-circle-o"></i> All Videos</a>
                            </li>
                    <?php
                    if($user->user_details['admin_role']=='admin'){
                    ?>
                            <li>
                                <a href="?page=review-videos"><i class="fa fa-fw fa-exclamation-circle"></i> Videos for Review</a>
                            </li>
                    <?php
                    }
                    ?>
                            <li>
                                <a href="?page=validate-videos"><i class="fa fa-fw fa-database"></i> Videos for Validation</a>
                            </li>
                            <li>
                                <a href="?page=validated-videos"><i class="fa fa-fw fa-check"></i> Validated Videos</a>
                            </li>
                            <li>
                                <a href="?page=add-videos"><i class="fa fa-fw fa-plus"></i> Add Videos</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?=(isset($_GET['page']) && ($_GET['page']=='digital-dashboard' || $_GET['page']=='digital' || $_GET['page']=='add-digital' ))?'active ':''?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#digital"><i class="fa fa-fw fa-internet-explorer"></i> Digital Material <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="digital" class="collapse <?=(isset($_GET['page']) && ($_GET['page']=='digital-dashboard' || $_GET['page']=='digital' || $_GET['page']=='add-digital' ))?'in':''?>">
                            <li class="active">
                                <a href="?page=digital-dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="?page=digital"><i class="fa fa-fw fa-internet-explorer"></i> All Digital Materials</a>
                            </li>
                            <li>
                                <a href="?page=add-digital"><i class="fa fa-fw fa-plus"></i> Add Digital Materials</a>
                            </li>
                        </ul>
                    </li>
                    <?php
                    if($user->user_details['admin_role']=='admin'){
                    ?>
                    <li class="<?=(isset($_GET['page']) && ($_GET['page']=='instructions' || $_GET['page']=='add-instructions' || $_GET['page']=='edit-instructions' ))?'active ':''?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#instructions"><i class="fa fa-fw fa-bullhorn"></i> Instuctions <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="instructions" class="collapse <?=(isset($_GET['page']) && ($_GET['page']=='instructions' || $_GET['page']=='add-instructions' || $_GET['page']=='edit-instructions' ))?'in':''?>">
                            <li>
                                <a href="?page=instructions"><i class="fa fa-fw fa-bullhorn"></i> All Instructions</a>
                            </li>
                            <li>
                                <a href="?page=add-instructions"><i class="fa fa-fw fa-plus"></i> Add Instruction</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='institutes')?'active':''?>">
                        <a href="?page=institutes"><i class="fa fa-fw fa-institution"></i> Institutes</a>
                    </li>
                    
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='users')?'active':''?>">
                        <a href="?page=users"><i class="fa fa-fw fa-user"></i> User</a>
                    </li>
                    
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='report')?'active':''?>">
                        <a href="?page=report"><i class="fa fa-fw fa-line-chart"></i> Report</a>
                    </li>
                    <?php
                    }
                    ?>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='darshana')?'active':''?>">
                        <a href="?page=darshana"><i class="fa fa-fw fa-clipboard"></i> Darshana Questions</a>
                    </li>
                    <li class="<?=(isset($_GET['app-chapter-test']) || isset($_GET['create-app-chapter-test']) || isset($_GET['app-topic-test']) || isset($_GET['create-app-topic-test']))?'active ':''?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#app-qns"><i class="fa fa-fw fa-question-circle"></i> App Tests <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="app-qns" class="collapse <?=(isset($_GET['app-chapter-test']) || isset($_GET['create-app-chapter-test']) || isset($_GET['app-topic-test']) || isset($_GET['create-app-topic-test']))?'in':''?>">
                            <li>
                                <a href="?page=app-chapter-test"><i class="fa fa-fw fa-question"></i> App Chapter Tests</a>
                            </li>
                            <li>
                                <a href="?page=create-app-chapter-test"><i class="fa fa-fw fa-plus"></i> Create App Chapter Tests</a>
                            </li>
                            <li>
                                <a href="?page=app-topic-test"><i class="fa fa-fw fa-question"></i> App Topic Tests</a>
                            </li>
                            <li>
                                <a href="?page=create-app-topic-test"><i class="fa fa-fw fa-plus"></i> Create App Topic Test</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='app-users')?'active':''?>">

                        <a href="?page=app-users"><i class="fa fa-fw fa-user"></i>App Users</a>

                    </li>
                    <li class="<?=(isset($_GET['page']) && ($_GET['page']=='notifications' || $_GET['page']=='add-notifications' || $_GET['page']=='edit-notifications' ))?'active ':''?>">

                        <a href="javascript:;" data-toggle="collapse" data-target="#notifications"><i class="fa fa-fw fa-bullhorn"></i> Notifications <i class="fa fa-fw fa-caret-down"></i></a>

                        <ul id="notifications" class="collapse <?=(isset($_GET['page']) && ($_GET['page']=='notifications' || $_GET['page']=='add-notifications' || $_GET['page']=='edit-notifications' ))?'in':''?>">

                            <li>

                                <a href="?page=notifications"><i class="fa fa-fw fa-bullhorn"></i> All Notifications</a>

                            </li>

                            <li>

                                <a href="?page=add-notifications"><i class="fa fa-fw fa-plus"></i> Add Notifications</a>

                            </li>

                        </ul>

                    </li>
                    <li class="<?=(isset($_GET['page']) && $_GET['page']=='questionCount')?'active':''?>">
                        <a href="?page=questionCount"><i class="fa fa-clock-o"></i> Questions Count</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->

