<?php
include 'includes/questions.php';
$questions = new Questions();

?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Questions <small>Edit Question</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-alt"></i> Questions
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Edit Question
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    
    
    <?php
if(!isset($_GET['id']) && trim($_GET['id'])==''){
    die('<h4 style="text-align:centre">Some Error Occured !</h4>');
}
$qid = $_GET['id'];
$question = $questions->get_question($qid);
?>
    
<div>
    <div class="row">
        <div class="col-lg-12">
            <?php
            if(isset($_GET['qn-edit']) && $_GET['qn-edit'] == 'success'){
            ?>
            <div class="alert alert-success">
                <strong>Question Edited Sussessfully....
            </div>
            <?php
            }else if(isset($_GET['qn-edit']) && $_GET['qn-edit'] == 'fail'){
            ?>
            <div class="alert alert-danger">
                <strong>Question Edit Failed...
            </div>
            <?php
            }
            ?>
            <?php
            if(isset($_GET['qn-add']) && $_GET['qn-add'] == 'success'){
            ?>
            <div class="alert alert-success">
                <strong>Question Successfully added. You can now continue editing the question.
            </div>
            <?php
            }
            ?>
        </div>
    </div>
        
        <form id="qn-addForm" enctype="multipart/form-data" method="POST" action="submit/questions.php">
            <div class="row">
                <div class="col-md-4">

                    Class :
                    <select class="form-control class-list" name="class-name">
                        <option>Select Class</option>
                    <?php
                    $class_select_query = $db->query("SELECT * FROM class_list");
                    while ($class = mysqli_fetch_array($class_select_query)) {
                        echo '<option code="'.$class['code'].'" value="'.$class['id'].'"';
                        if($class['id'] == $question['class'] ){
                            echo ' selected ';
                        }
                        echo '>'.$class['title'];
                        echo '</option>';
                    }

                    ?>
                    </select>
                    <br>
                </div>
            </div> 
            <div class="">
        <div class="" style="width: 100%;overflow: hidden;min-height: 370px;">
            <div class="col-lg-3 subjects-head" style="padding:0px;margin:0px;"><strong>Subjects</strong>
                <div class="subjects" style="height:310px;border: 1px solid #ccc;padding: 0px;overflow-y: scroll;">
                    <ul style="list-style-type: none;padding:0px;margin:0px;">
                    <?php
                    $subject_select_query = $db->query("SELECT * FROM subject_list WHERE class = '$question[class]' ");
                    $selected_subject = '';
                    while ($subject = mysqli_fetch_array($subject_select_query)) {
                        echo '<li class="subject-list';
                        if($question['subject'] == $subject['id']){
                            echo ' selected';
                            $selected_subject = $subject['id'];
                        }
                        echo '" subject_var="'.$subject['id'].'">';
                        echo $subject['title'];
                        echo '</li>';
                    }
                    ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 chapters-head" style="padding:0px;margin:0px;"><strong>Chapters</strong>
                <div class="chapters" style="height:310px;border: 1px solid #ccc;padding: 0px;overflow-y: scroll;">
                    <ul style="list-style-type: none;padding:0px;margin:0px;">
                    <?php
                    $chapter_select_query = $db->query("SELECT * FROM chapter_list WHERE subject = '$question[subject]' ");
                    $selected_chapter = '';
                    while ($chapter = mysqli_fetch_array($chapter_select_query)) {
                        echo '<li class="chapter-list';
                        if($question['chapter'] == $chapter['id']){
                            echo ' selected';
                            $selected_chapter = $chapter['id'];
                        }
                        echo '" subject_var="'.$chapter['id'].'">';
                        echo $chapter['title'];
                        echo '</li>';
                    }
                    ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 topics-head" style="padding:0px;margin:0px;"><strong>Topics</strong>
                <div class="topics" style="height:310px;border: 1px solid #ccc;padding: 0px;overflow-y: scroll;">
                    <ul style="list-style-type: none;padding:0px;margin:0px;">
                    <?php
                    $chapter_select_query = $db->query("SELECT * FROM topic_list WHERE chapter = '$question[chapter]' ");
                    $selected_chapter = '';
                    echo '<li class="topic-list';
                    if($question['topic'] == 0){
                        echo ' selected';
                        $selected_chapter = 0;
                    }
                    echo '" subject_var="0"> Unspecified</li>';
                    while ($chapter = mysqli_fetch_array($chapter_select_query)) {
                        echo '<li class="topic-list';
                        if($question['topic'] == $chapter['id']){
                            echo ' selected';
                            $selected_chapter = $chapter['id'];
                        }
                        echo '" subject_var="'.$chapter['id'].'">';
                        echo $chapter['title'];
                        echo '</li>';
                    }
                    ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 sections-head" style="padding:0px;margin:0px;"><strong>Section</strong>
                <div class="sections" style="height:310px;border: 1px solid #ccc;padding: 0px;overflow-y: scroll;">
                    <ul style="list-style-type: none;padding:0px;margin:0px;">
                    <?php
                    $section_select_query = $db->query("SELECT * FROM section_list WHERE topic = '$question[topic]' ");
                    $selected_topic = '';
                    echo '<li class="section-list';
                    if($question['section'] == 0){
                        echo ' selected';
                        $selected_topic = 0;
                    }
                    echo '" subject_var="0"> Unspecified</li>';
                    while ($section = mysqli_fetch_array($section_select_query)) {
                        echo '<li class="section-list';
                        if($question['section'] == $section['id']){
                            echo ' selected';
                        }
                        echo '" subject_var="'.$section['id'].'">';
                        echo $section['name'];
                        echo '</li>';
                    }
                    ?>
                    </ul>
                </div>
            </div>
        </div>
            </div>
<div class="hidden-fields">
    <input type="hidden" class="subject-id" name="subject-id" value="<?= $question['subject'] ?>">
    <input type="hidden" class="chapter-id" name="chapter-id" value="<?= $question['chapter'] ?>">
    <input type="hidden" class="section-id" name="section-id" value="<?= $question['section'] ?>">
    <input type="hidden" class="topic-id" name="topic-id" value="<?= $question['topic'] ?>">
</div>
<div class="row">
    <div class="col-lg-12"><hr></div>
</div>
        <div class="row">
            <div class="col-lg-3">
                Difficulty :
                <select class="form-control" name="difficulty-val">
                    <option value="easy" <?= ($question['difficulty'] == 'easy' )?'selected':'' ?> >Easy</option>
                    <option value="medium" <?= ($question['difficulty'] == 'medium' )?'selected':'' ?> >Medium</option>
                    <option value="hard" <?= ($question['difficulty'] == 'hard' )?'selected':'' ?> >Hard</option>
                </select>
                Question Type :
                <select name="answer-type" class="form-control answer-category">
                    <option value="smcq" <?= ($question['question_type'] == 'smcq')?'selected':'' ?> >Single Answer MCQ</option>
                    <option value="mmcq" <?= ($question['question_type'] == 'mmcq')?'selected':'' ?> >Multiple Answer MCQ</option>
                    <option value="numeric" <?= ($question['question_type'] == 'numeric')?'selected':'' ?> >Numeric Answer</option>
                    <option value="assertion" <?= ($question['question_type'] == 'assertion')?'selected':'' ?> >Assertion Reason</option>
                    <option value="match" <?= ($question['question_type'] == 'match')?'selected':'' ?> >Match the following</option>
                </select>
                Source :
                <select class="form-control" name="qn-source">
                    <option <?= ($question['source'] == 'Aakash')?'selected':'' ?> value="Aakash">Aakash</option>
                    <option <?= ($question['source'] == 'Allen')?'selected':'' ?> value="Allen">Allen</option>
                    <option <?= ($question['source'] == 'Arihant')?'selected':'' ?> value="Arihant">Arihant</option>
                    <option <?= ($question['source'] == 'Balagi')?'selected':'' ?> value="Balagi">Balaji</option>
                    <option <?= ($question['source'] == 'Byjus')?'selected':'' ?> value="Byjus">Byjus</option>
                    <option <?= ($question['source'] == 'Cengage')?'selected':'' ?> value="Cengage">Cengage</option>
                    <option <?= ($question['source'] == 'Chaitanya')?'selected':'' ?> value="Chaitanya">Chaitanya</option>
                    <option <?= ($question['source'] == 'DC Pande')?'selected':'' ?> value="DC Pande">DC Pande</option>
                    <option <?= ($question['source'] == 'Dinesh')?'selected':'' ?> value="Dinesh">Dinesh</option>
                    <option <?= ($question['source'] == 'Disha')?'selected':'' ?> value="Disha">Disha</option>
                    <option <?= ($question['source'] == 'Errorless')?'selected':'' ?> value="Errorless">Errorless</option>
                    <option <?= ($question['source'] == 'Etoos')?'selected':'' ?> value="Etoos">Etoos</option>
                    <option <?= ($question['source'] == 'Fitjee')?'selected':'' ?> value="Fitjee">Fitjee</option>
                    <option <?= ($question['source'] == 'GRB')?'selected':'' ?> value="GRB">GRB</option>
                    <option <?= ($question['source'] == 'HCV')?'selected':'' ?> value="HCV">HCV</option>
                    <option <?= ($question['source'] == 'Motion Classes')?'selected':'' ?> value="Motion classes">Motion classes</option>
                    <option <?= ($question['source'] == 'MTG')?'selected':'' ?> value="MTG">MTG</option>
                    <option <?= ($question['source'] == 'Newton Classes')?'selected':'' ?> value="Newton Classes">Newton Classes</option>
                    <option <?= ($question['source'] == 'Resonance')?'selected':'' ?> value="Resonance">Resonance</option>
                    <option <?= ($question['source'] == 'Tata')?'selected':'' ?> value="Tata">Tata</option>
                    <option <?= ($question['source'] == 'Truemans')?'selected':'' ?> value="Truemans">Truemans</option>
                    <option <?= ($question['source'] == 'Vidya Mandir')?'selected':'' ?> value="Vidya Mandir">Vidya Mandir</option>
                    <option <?= ($question['source'] == 'Wiley')?'selected':'' ?> value="Wiley">Wiley</option>
                </select>
                Reference Number :
                <input class="form-control" name="qn-reference" value="<?= stripslashes($question['reference']) ?>" />
            </div>

            <div class="col-lg-12" style="margin-top: 20px;">
                Tags :
                <input class="tags" name="tags" data-role="tagsinput" required="" value="<?= $question['tags'] ?>" />
            </div>

            <div class="col-lg-12" style="margin-top: 20px;">
                <div class="btn btn-xs btn-primary pull-right media-modal-btn" data-toggle="modal" data-target="#mediaModal"><span class="fa fa-plus"></span> Add Media</div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-question" style="<?= ($question['question_type'] == 'assertion')?'display:none':''; ?>">
                Question :
                <textarea name="text-question" class="form-control ckeditor"  ><?= ($question['question_type'] != 'assertion')?$question['question']:''; ?></textarea>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 assertion-question" style="<?= ($question['question_type'] != 'assertion')?'display:none':''; ?>">
                Question Statement 1:
                <?php
                $assertion1 = '';
                $assertion2 = '';
                if($question['question_type'] == 'assertion'){
                    $question_statement = json_decode(preg_replace('/[\r\n]+/', '', $question['question']));
                    $assertion1 = $question_statement[0][0];
                    $assertion2 = $question_statement[1][0];
                }
                ?>
                <textarea name="assertion-statement-1" class="form-control ckeditor"  ><?= $assertion1 ?></textarea>
            </div>
            <div class="col-lg-12 assertion-question" style="margin-top: 20px;">
                <div class="btn btn-xs btn-primary pull-right media-modal-btn" data-toggle="modal" data-target="#mediaModal"><span class="fa fa-plus"></span> Add Media</div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 assertion-question" style="<?= ($question['question_type'] != 'assertion')?'display:none':''; ?>">
                Question Statement 2:
                <textarea name="assertion-statement-2" class="form-control ckeditor" ><?= $assertion2 ?></textarea>
            </div>
        </div>
<div class="row">
<?php
$answerA = '';
$answerB = '';
$answerC = '';
$answerD = '';
if($question['question_type'] == 'mmcq' || $question['question_type'] == 'smcq' ){
    $answers = json_decode(preg_replace("/[\r\n]+/", " ", $question['answer_option']));
    $answerA = $answers[0][1];
    $answerB = $answers[1][1];
    $answerC = $answers[2][1];
    $answerD = $answers[3][1];
}
?>
        <div class="col-lg-12 mcq-answer" style="<?= ($question['question_type'] == 'mmcq' ||  $question['question_type'] == 'smcq' )?'':'display:none' ?>">
                <h4>Answer Options</h4>
                    <table class="table option-table"><tbody>
                            
                        <tr class="option-row-A">
                            <td><input type="<?= ($question['question_type'] == 'mmcq')?'checkbox':'radio' ?>" name="correct-answer" value="A" 
                                <?php
                                if($question['question_type'] == 'mmcq'){
                                    $answer_options = json_decode($question['correct_answer']);
                                    if(in_array('A', $answer_options)){
                                        echo 'checked';
                                    }
                                }elseif($question['question_type'] == 'smcq'){
                                    if($question['correct_answer'] == 'A'){
                                        echo 'checked';
                                    }
                                }
                                ?>
                            class="answer-option" > &nbsp; Option-A</td>
                            <td>
                                <textarea name="answer-a" type="text" class="form-control ckeditor text-answer option-text-A"><?= $answerA ?></textarea></td>
                            <td><div style="overflow: hidden;" class="btn btn-xs btn-primary pull-right media-modal-btn" data-toggle="modal" data-target="#mediaModal"><span class="fa fa-plus"></span> Add Media</div></td>
                        </tr>
                        <tr class="option-row-B">
                            <td><input type="<?= ($question['question_type'] == 'mmcq')?'checkbox':'radio' ?>" name="correct-answer" value="B" 
                                <?php
                                if($question['question_type'] == 'mmcq'){
                                    $answer_options = json_decode($question['correct_answer']);
                                    if(in_array('B', $answer_options)){
                                        echo 'checked';
                                    }
                                }elseif($question['question_type'] == 'smcq'){
                                    if($question['correct_answer'] == 'B'){
                                        echo 'checked';
                                    }
                                }
                                ?>
                             class="answer-option" > &nbsp; Option-B</td>
                            <td>
                                
                                <textarea name="answer-b" type="text" class="form-control ckeditor text-answer option-text-B"><?= $answerB ?></textarea></td>
                            <td>
                                <div style="overflow: hidden;" class="btn btn-xs btn-primary pull-right media-modal-btn" data-toggle="modal" data-target="#mediaModal"><span class="fa fa-plus"></span> Add Media</div>
                            </td>
                        </tr>
                        <tr class="option-row-C">
                            <td><input type="<?= ($question['question_type'] == 'mmcq')?'checkbox':'radio' ?>" name="correct-answer" value="C" 
                                <?php
                                if($question['question_type'] == 'mmcq'){
                                    $answer_options = json_decode($question['correct_answer']);
                                    if(in_array('C', $answer_options)){
                                        echo 'checked';
                                    }
                                }elseif($question['question_type'] == 'smcq'){
                                    if($question['correct_answer'] == 'C'){
                                        echo 'checked';
                                    }
                                }
                                ?>
                             class="answer-option" > &nbsp; Option-C</td>
                            <td>
                                <textarea name="answer-c" type="text" class="form-control ckeditor text-answer option-text-C"><?= $answerC ?></textarea></td>
                            <td><div style="overflow: hidden;" class="btn btn-xs btn-primary pull-right media-modal-btn" data-toggle="modal" data-target="#mediaModal"><span class="fa fa-plus"></span> Add Media</div></td>
                        </tr>
                        <tr class="option-row-D">
                            <td><input type="<?= ($question['question_type'] == 'mmcq')?'checkbox':'radio' ?>" name="correct-answer" value="D"
                                <?php
                                if($question['question_type'] == 'mmcq'){
                                    $answer_options = json_decode($question['correct_answer']);
                                    if(in_array('D', $answer_options)){
                                        echo 'checked';
                                    }
                                }elseif($question['question_type'] == 'smcq'){
                                    if($question['correct_answer'] == 'D'){
                                        echo 'checked';
                                    }
                                }
                                ?>
                             class="answer-option" > &nbsp; Option-D</td>
                            <td>
                                
                                <textarea name="answer-d" type="text" class="form-control ckeditor text-answer option-text-D"><?= $answerD ?></textarea></td>
                            <td><div style="overflow: hidden;" class="btn btn-xs btn-primary pull-right media-modal-btn" data-toggle="modal" data-target="#mediaModal"><span class="fa fa-plus"></span> Add Media</div></td>
                        </tr>
                    </tbody></table>
            </div>

            <div class="col-lg-12 assertion-answer" style="<?= ($question['question_type'] == 'assertion' )?'':'display:none' ?>">
                <h4>Answer Choices</h4>
                    <table class="table option-table"><tbody>
                            
                        <tr class="option-row-A">
                            <td><input type="radio" name="assertion-correct-answer" value="A" <?= ($question['correct_answer'] == 'A')?'checked':'' ?> class="assertion-answer-option" > &nbsp; Option-A</td>
                            <td>
                            Both Statement 1 and Statement 2 are true and Statement 2 is the correct reason for Statement 1
                            </td>
                        </tr>
                        <tr class="option-row-B">
                            <td><input type="radio" name="assertion-correct-answer" value="B" <?= ($question['correct_answer'] == 'B')?'checked':'' ?> class="assertion-answer-option" > &nbsp; Option-B</td>
                            <td>
                            Both Statement 1 and Statement 2 are true and Statement 2 is not the correct reason for Statement 1
                            </td>
                        </tr>
                        <tr class="option-row-C">
                            <td><input type="radio" name="assertion-correct-answer" value="C" <?= ($question['correct_answer'] == 'C')?'checked':'' ?> class="assertion-answer-option" > &nbsp; Option-C</td>
                            <td>
                            Both Statement 1 and Statement 2 are false.
                            </td>
                        </tr>
                        <tr class="option-row-D">
                            <td><input type="radio" name="assertion-correct-answer" value="D" <?= ($question['correct_answer'] == 'D')?'checked':'' ?> class="assertion-answer-option" > &nbsp; Option-D</td>
                            <td>
                            Statement 1 is true but Statement 2 is false.
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="col-lg-6 integer-answer" style="<?= ($question['question_type'] == 'numeric' )?'':'display:none' ?>">
                <h4>Answer Value</h4>
                <input type="number" class="form-control integer-answer" name="integer-answer" value="<?= ($question['question_type'] == 'numeric')?$question['correct_answer']:'' ?>" >
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Hint 
                <div class="btn btn-xs btn-primary pull-right media-modal-btn" data-toggle="modal" data-target="#mediaModal"><span class="fa fa-plus"></span> Add Media</div>
                </h4>
                <textarea name="text-hint" class="form-control text-hint ckeditor"><?= $question['hint'] ?></textarea>
                <input type="file" class="image-hint" style="display:none;" name="image-hint" accept=".jpg,.png" />
            </div>
</div>

<div class="row">
        <div class="col-lg-12" style="text-align: center;margin-top: 20px;margin-bottom: 50px;">
            <input type="hidden" name="qn-edit-id" value="<?= $_GET['id'] ?>">
            <input type="submit" id="qn-edit-submit" name="qn-edit-submit" class="btn btn-primary" value="Save Changes" />
          
        </div>
</div>
    </form>
    </div>
    
    
    <!-- Modal -->
<div id="mediaModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Media Modal</h4>
      </div>
      <div class="modal-body">
          <form id="img-submit" method="POST" action="" enctype="multipart/form-data">
              <div style="overflow: hidden;">
              <input class="pull-left" type="file" name="media-upload" accept=".png,.jpg,.gif" />
              <input type="hidden" name="test" value="test" />
              <input type="submit" class="btn btn-sm btn-primary pull-right" value="Add Image" />
              </div>
          </form>
          <hr>
          <h5 style="border-bottom: 1px solid #ccc;">Existing Images</h5>
          <div class="media-files" style="width: 100%;max-height: 400px;overflow-x: hidden;overflow-y: scroll;">
            <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

