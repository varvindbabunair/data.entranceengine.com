<?php
include 'includes/questions.php';
$questions = new Questions();

?>

<!-- Page Heading -->
<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Questions <small> CSV format of questions</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-alt"></i> Questions
                </li>
                <li class="active">
                    <i class="fa fa-list-alt"></i> Csv Format
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
    	<form action="submit/darshana.php" method="POST" >
	    	<div class="col-lg-3">
	    		<select name="subject" class="form-control classval">
	    			<optgroup label="Class 11">
	    				<option value="1" >Physics</option>
	    				<option value="2" >Chemistry</option>
	    				<option value="4" >Maths</option>
	    				<option value="3" >Biology</option>
	    			</optgroup>
	    			<optgroup label="Class 12">
	    				<option value="5" >Physics</option>
	    				<option value="6" >Chemistry</option>
	    				<option value="9" >Maths</option>
	    				<option value="8" >Biology</option>
	    			</optgroup>
	    		</select>
	    	</div>
	    	<div class="col-lg-3">
	    		<button type="submit" class="btn btn-primary form-control">Generate Qn in CSV</button>
	    	</div>
	    </form>
    </div>

