<?php
    include 'includes/subjects.php';
    $subject = new Subjects();
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Question Count
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    Question Count
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <div class="row" style="margin-bottom: 20px;">
    <div class="col-md-4">
        Class :
        <select class="form-control class-list">
            <option value="0">Select Class</option>
            <?php
            foreach ($subject->all_class() as $class) {
            ?>
            <option value="<?= $class['id'] ?>"><?= $class['title'] ?></option>
            <?php
            }
            ?>
        </select>
    </div>
</div>

<div class="subjectpanel"></div>

<div class="row">
        <hr>
        <div class="col-lg-12" style="margin-top:20px;margin-bottom:50px;" >
            
            <table id="qn-table" class="table table-hover table-condensed table-striped table-bordered custom-font" style="font-size:12px;" >
                <thead class="subjects" style="display:none">
                    <th>Physics</th>
                    <th>Chemistry</th>
                    <th>Biology</th>
                    <th>Mathematics</th>
                </thead>
        
            <tbody class="chapter">
                
            </tbody>
         
            </table>
            
        </div>
  
</div>

    <div id="chviewModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div style="padding:15px">
        <h2 class="chapter-title"></h2>
        <p class="chapter-count"></p>
      </div>
      <div id="qn-content" class="modal-body">
        <!-- <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p> -->
        <table class="table">
            <thead>
            <tr>
                <th>Topic Name</th>
                <th>Number of questions</th>
            </tr>
            </thead>
            <tbody class="numb">
            
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>