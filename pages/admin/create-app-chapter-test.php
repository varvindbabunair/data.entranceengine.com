<?php
include 'includes/materials.php';
$tests = new Materials();
?>
<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        Name for Test :
        <input class="form-control test-name" name="test-name" type="text" />
    </div>
</div>
<div class="row">
    <div class="exam-time col-lg-2">
        Total Time of Exam
        <div class="input-group">
          <input type="text" class="form-control total-exam-time" aria-describedby="basic-addon2">
          <span class="input-group-addon" id="basic-addon2">minutes</span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        Tags :
        <input class="form-control tags" />
        <br>

    </div>
    <script>
        $('.tags').tagsinput();
    </script>
</div>

<div class="row">
    <div class="col-lg-3">
        Select Subject :
        <select class="form-control subject-select">
            <option value="0" >Select Subject</option>
            <optgroup label="Class 11">
                <option value="1">Physics</option>
                <option value="2">Chemistry</option>
                <option value="4">Maths</option>
                <option value="3">Biology</option>
            </optgroup>
            <optgroup label="Class 12">
                <option value="5">Physics</option>
                <option value="6">Chemistry</option>
                <option value="9">Maths</option>
                <option value="8">Biology</option>
            </optgroup>
        </select>
    </div>
    <div class="col-lg-3">
        Select Chapter :
        <select class="form-control chapter-select">
            <option>Select Chapter</option>
        </select>
    </div>
</div>

<div class="row section" >
    <div class="col-lg-12" style="margin-top: 30px;">
        <div class="col-lg-5 col-md-5 question-list">
            <h4 style="text-align:center"><u>Availiable Questions (<span id="sortable1Count"></span>)</u></h4>
            <ul id="sortable1" class="connectedSortable ">

            </ul>
        </div>
        <div class="col-md-1 col-lg-1">
            <button class="btn btn-warning form-control" style="margin-top:50%;"> &Rrightarrow;</button>
        </div>
        <div class="col-lg-6 col-md-6 question-list">
            <h4 style="text-align:center"><u>Selected Questions (<span id="sortable2Count"></span>)</u></h4>
            <ul id="sortable2" class="connectedSortable ">

            </ul>
        </div>
    </div>
    
    
    
    
    <script>
    $('#test-subjects1').change(function(){
        var subjects = $(this).val();
        var tags = '';
        var excludes = $( "#sortable2" ).sortable( "serialize", { attribute: "id" });
        $.post("ajax/create-test.php",{
            tag : tags,
            subject : subjects,
            exclude : excludes
        },
        function(responseTxt){
            $('#sortable1').html(responseTxt);
        });
    });

      $( "#sortable1, #sortable2" ).sortable({
        connectWith: ".connectedSortable"
      }).enableSelection();

    $( "#sortable2" ).sortable({
        receive: function( event, ui ) {
            ui.item.children(".mark").show();
            ui.item.children(".order").show();
            
            var totalElements = ui.item.parent('ul').children('li').size();
            for(var i = 0;i<totalElements;i++){
                var j = i+1;
                ui.item.parent('ul').children('li').eq(i).children('.order').html("<b>" + j + "</b>");
            }
        },
        stop: function( event, ui ) {
//            var idstring = $( "#sortable2" ).sortable( "serialize", { attribute: "id" });
            var totalElements = ui.item.parent('ul').children('li').size();
            for(var i = 0;i<totalElements;i++){
                var j = i+1;
            ui.item.parent('ul').children('li').eq(i).children('.order').html("<b>" + j + "</b>");
            }

        }
    });
    
    $( "#sortable1" ).sortable({
        receive: function( event, ui ) {
            ui.item.children(".mark").hide();
            ui.item.children(".order").hide();
            var totalElements = $( "#sortable2" ).children('li').size();
            for(var i = 0;i<totalElements;i++){
                var j = i+1;
                $( "#sortable2" ).children('li').eq(i).children('.order').html("<b>" + j + "</b>");
            }
        }
    });
        
</script>
    
</div>

<div style="width: 100%;"><hr></div>



<div class="col-lg-12" style="margin-bottom:50px;margin-top:50px;text-align: center">
    <button id="create-test" class="btn btn-lg btn-primary"><b><span class="fa fa-plus"></span> Create Test</b></button>
</div>

<!-- Modal -->
<div id="create-test-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" >Modal Header</h4>
      </div>
      <div class="modal-body">
          <p class="modal-message">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
          <button  class="btn btn-default"  data-dismiss="modal" >Close</button>>
      </div>
    </div>

  </div>
</div>

