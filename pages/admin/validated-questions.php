<?php
include 'includes/questions.php';
$questions = new Questions();

?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Questions <small> Validated Questions</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-alt"></i> Questions
                </li>
                <li class="active">
                    <i class="fa fa-check"></i> Validated Questions
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12" style="margin-top:20px;margin-bottom:50px;" >
            
            <table id="qn-table" class="table table-hover table-condensed table-striped" style="font-size:12px;" >
                <thead><th>#</th><th>Reference</th><th>Question</th><th>Subject</th><th>Chapter</th><th>Tags</th><th>Validated By</th><th>Option</th></thead>
            <?php
            $added_user = $user->user_details['id'];
            if($user->user_details['admin_role'] == 'admin'){
            $question_select_query = "SELECT question_list.id,question_list.reference,question_list.question_type, question_list.question,question_list.tags, class_list.title as class_name, subject_list.title as subject_name, chapter_list.title as chapter_name, admin_list.name as added_user "
                    . "FROM question_list "
                    . "INNER JOIN class_list ON question_list.class = class_list.id "
                    . "INNER JOIN subject_list ON question_list.subject = subject_list.id "
                    . "INNER JOIN chapter_list ON question_list.chapter = chapter_list.id "
                    . "INNER JOIN admin_list ON (question_list.validated_by = admin_list.id) "
                    . "WHERE validated_by != 0";
            }else{
                $question_select_query = "SELECT question_list.id,question_list.reference,question_list.question_type, question_list.question,question_list.tags, class_list.title as class_name, subject_list.title as subject_name, chapter_list.title as chapter_name, admin_list.name as added_user "
                    . "FROM question_list "
                    . "INNER JOIN class_list ON question_list.class = class_list.id "
                    . "INNER JOIN subject_list ON question_list.subject = subject_list.id "
                    . "INNER JOIN chapter_list ON question_list.chapter = chapter_list.id "
                    . "INNER JOIN admin_list ON (question_list.validated_by = admin_list.id) "
                    . "WHERE validated_by != 0 AND added_by = $added_user";
            }
            $question_get_query = $db->query($question_select_query)or die(mysqli_error($db->db_link));
            $cnt = 1;
            while($question_info = mysqli_fetch_array($question_get_query)){
            ?>
            <tr>
                <td>
                    <?php echo $question_info['id']; ?>
                </td>
                <td>
                    <?php echo $question_info['reference']; ?>
                </td>
                <td>
                    <?php
                    if($question_info['question_type'] == 'assertion'){
                        $qns = preg_replace("/[\r\n]+/", " ", $question_info['question']);
                        $question_statements = json_decode($qns);
                        echo '<strong>Statement 1</strong><br>';
                        echo $question_statements[0][0].'<br>';
                        echo '<strong>Statement 2</strong><br>';
                        echo $question_statements[1][0].'<br>';
                    }else{
                        echo $question_info['question'];
                    }
                    ?>
                </td>
                <td>
                    <?= $question_info['subject_name'] ?>
                </td>
                <td>
                    <?= $question_info['chapter_name'] ?>
                </td>
                <td>
                    <?= $question_info['tags']; ?>
                </td>
                <td>
                    <?= $question_info['added_user'] ?>
                </td>
                <td>
                    <a href="?page=edit-question&id=<?= $question_info['id'] ?>"><button class="btn btn-primary btn-xs" ><span  class="fa fa-edit"></span> Edit</button></a>
                    <button class="btn btn-xs btn-primary qn-view" qnid="<?= $question_info['id'] ?>" data-toggle="modal" data-target="#qnviewModal" ><span class="fa fa-eye"></span> View & Review</button>
                </td>
            </tr>
            <?php
            }
            ?>
            </table>
            
        </div>
  
</div>

<div id="qn-removeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remove Question</h4>
      </div>
        <form method="POST" action="submit/questions.php" >
            <div class="modal-body">
                <p><strong>Are you sure you want to remove the following question from the database ?</strong></p>
                <p>This action will permanently remove question from the database. This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" value="" id="qn-remove-id" name="qn-remove-id" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <input type="submit" name="qn-remove-submit" id="delete-question" class="btn btn-primary" value="Delete Question" />
            </div>
        </form>
    </div>

  </div>
</div>

    
    <!-- Modal -->
<div id="qnviewModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div id="qn-content" class="modal-body qn-content">
          <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    