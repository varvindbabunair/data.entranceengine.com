<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <span class="badge pull-right" data-toggle="modal" data-target="#registerModal"><span class="fa fa-plus"></span> Register New Student</span>
            <h3 class="panel-title"><span class="fa fa-users"></span> Student</h3>
        </div>
        <div class="panel-body">
            <div class="col-lg-1">
                Display :
                <select class="form-control limit">
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </div>
            <div class="col-lg-2">
                Sort By :
                <select class="form-control group">
                    <option value="all" >All</option>
                    <?php
                    $user_group_query = $db->query("SELECT * FROM student_group WHERE 1");
                    while($user_group = mysqli_fetch_array($user_group_query)){
                        echo '<option value="'.$user_group['id'].'">'.$user_group['name'].'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-3">
                Search
                <div class="input-group ">
                <input type="text" class="form-control search" placeholder="" >
                <span class="input-group-btn ">
                    <button class="btn btn-primary" type="button"><span class="fa fa-search"></span> &nbsp;</button>
                </span>
              </div>
            </div>
            
            
            <div class="col-lg-12">
                <hr>
            </div>
            
            <div class="col-lg-12" id="student-list" >
                <p style="text-align: center;"><span class="fa fa-spin fa-spinner"></span> Loading...</p>
            </div>
        </div>
    </div>
</div>


<div id="registerModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="fa fa-user-plus"></span> Register New User</h4>
      </div>
        <form action="submit/student-users.php" method="POST">
            <div class="modal-body">
                Name :
		<input name="name" class="form-control" type="text">
		username :
		<input name="username" class="form-control" type="text">
		password :
		<input name="password" class="form-control" type="text">
		Address :
		<input name="address" class="form-control" type="text">
		City :
		<input name="city" class="form-control" type="text">
		State :
		<input name="state" class="form-control" type="text">
		Country :
		<select name="country" class="form-control">
                    <?php 
                    $country_select_query = $db->query("SELECT * FROM country_list WHERE 1");
                    while($country = mysqli_fetch_array($country_select_query)){
                        echo '<option value="'.$country['code'].'">'.$country['name'].'</option>';
                    }
                    ?>
		</select>
		Postal Code :
		<input name="postal_code" class="form-control" type="text">
		Phone :
		<input name="phone" class="form-control" type="text">
		Email :
		<input name="email" class="form-control" type="text">
		
		Student Group :
		<select class="form-control" name="user_group">
                    <?php 
                    $group_select_query = $db->query("SELECT * FROM student_group WHERE 1");
                    while($group = mysqli_fetch_array($group_select_query)){
                        echo '<option value="'.$group['id'].'">'.$group['name'].'</option>';
                    }
                    ?>
		</select>
		
		
            </div>
            <div class="modal-footer">
                <input class="btn btn-primary" name="register-student" value="Register Student" type="submit">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>

  </div>
</div>

<div id="viewModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="fa fa-user"></span> View Student Details</h4>
      </div>
      <div class="student-view modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="fa fa-edit"></span> Edit User Details...</h4>
      </div>
        <form method="POST" action="submit/student-users.php">
            <div class="student-edit modal-body">
                
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" name="student-edit" value="Save Changes" />
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>

  </div>
</div>

<div id="removeModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="fa fa-remove"></span> Remove Student</h4>
      </div>
        <form action="submit/student-users.php" method="POST" >
      <div class="modal-body remove-content">
          
      </div>
      <div class="modal-footer">
          <input type="hidden" class="remove-id" name="student-id" value=""  />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary"  name="student-remove" value="Remove Student"  />
      </div>
        </form>
    </div>

  </div>
</div>

<div id="packageModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title "><span class="fa fa-archive"></span> Packages availiable for User</h4>
      </div>
        <div class="modal-body package-content" style="overflow: hidden;">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>