<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Questions <small> Review Questions</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-alt"></i> Questions
                </li>
                <li class="active">
                    <i class="fa fa-question-circle"></i> Review Questions
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    
<?php
include 'includes/questions.php';

$question = new Questions();

$get_review_list = $question->query("SELECT DISTINCT question FROM review_list WHERE status = 0");
?>

<table class="table table-striped table-condensed">
    <thead>
        <tr><th>#</th><th>Question</th><th>Options</th></tr>
    </thead>
    <tbody>
        <?php
        $cnt = 1;
        while($qn_reviewed = mysqli_fetch_array($get_review_list)){
            $qn = $question->get_question($qn_reviewed['question']);
            ?>
        <tr>
            <td><?= $cnt++ ?></td>
            <td><?= $qn['question'] ?></td>
            <td>
                <button class="btn btn-xs btn-primary qn-view" qnid="<?= $qn['id'] ?>" data-toggle="modal" data-target="#qnviewModal" ><span class="fa fa-eye"></span> View Question</button>

                <a href="?page=edit-question&id=<?= $qn['id'] ?>"><button class="btn btn-primary btn-xs" ><span  class="fa fa-edit"></span> Edit</button></a>

                <button class="btn btn-primary btn-xs qn-review" qnid="<?= $qn['id'] ?>" data-toggle="modal" data-target="#qnreviewModal" ><span class="fa fa-cog"></span> View Reviews</button>
            </td>
        </tr>
        <?php
        }
        ?>
    </tbody>
</table>


    <!-- Modal -->
<div id="qnreviewModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Question</h4>
      </div>
      <div class="modal-body qn-review-content">
          <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

    <!-- Modal -->
<div id="qnviewModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Question</h4>
      </div>
      <div class="modal-body qn-content">
          <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>