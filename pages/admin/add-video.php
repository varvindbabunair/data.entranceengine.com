<?php
    include 'includes/materials.php';
    $videos = new Materials();
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Video <small> Add Video</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-alt"></i> Videos
                </li>
                <li class="active">
                    <i class="fa fa-list-alt"></i> Add Video
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

<div class="row">
    <div class="col-lg-6">
            <form action="submit/videos.php" enctype="multipart/form-data" method="POST" >
                  Select Chapter :
                  <select name="chapter" class="form-control chapter-select">
                      <option>Select from Chapter List</option>
                    <?php
                    $subject_list = $videos->get_subject_list();
                    foreach($subject_list as $subjects){
                        echo '<optgroup label="'.$subjects['title'].'">';
                        $chapter_list = $videos->get_chapter_list($subjects['id']);
                        foreach($chapter_list as $chapter){
                            echo '<option value="'.$chapter['id'].'">'.$chapter['title'].'</option>';
                        }
                        echo '</optgroup>';
                    }
                    ?>
                  </select>
                  Section :
                <select name="section" class="form-control section-select">
                    <option>Select Section</option>
                </select>
                  
                  Video Link / Upload:
                  <div class="input-group">
                      <input type="text" class="form-control" name="vid-link" id="basic-url" aria-describedby="basic-addon3" placeholder="Paste video link">
                    <span class="input-group-addon" id="basic-addon3"> OR </span>
                    <input type="file" name="upload_vid" class="vid form-control" accept=".mp4,.mpeg4,.flv,.wmv" />
                    
                  </div>
    </div>
    <div class="col-sm-12">
        
        Video Title:
                  <input name="title" type="text" class="form-control" />
                  Tags :
                  <input name="tags" class="form-control tags" />
                  <script>
                      $('.tags').tagsinput();
                    </script>

                    Description :
                    <textarea class="form-control" name="description"></textarea>
                  Notes :
                  <textarea class="form-control" name="editor1"></textarea>
                  <br>
                  <div style="text-align: center;">
                      <input type="submit" name="video-submit" value="Submit Video" class="btn btn-primary" />
                  </div>
            </form>
        </div>
    </div>

    
        
    <!-- Modal -->
<div id="mediaModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Media Modal</h4>
      </div>
      <div class="modal-body">
          <form id="img-submit" method="POST" action="" enctype="multipart/form-data">
              <div style="overflow: hidden;">
              <input class="pull-left" type="file" name="media-upload" accept=".png,.jpg,.gif" />
              <input type="hidden" name="test" value="test" />
              <input type="submit" class="btn btn-sm btn-primary pull-right" value="Add Image" />
              </div>
          </form>
          <hr>
          <h5 style="border-bottom: 1px solid #ccc;">Existing Images</h5>
          <div class="media-files" style="width: 100%;max-height: 400px;overflow: hidden;overflow-y: scroll;">
            <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

