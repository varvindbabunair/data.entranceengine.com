<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Instructions <small> All Instructions</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-alt"></i> Instructions
                </li>
                <li class="active">
                    <i class="fa fa-list-alt"></i> All Instructions
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    
<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered">
            <thead><th>Id</th><th>Title</th><th>Option</th></thead>
            <?php
            $instruction_select = $db->query("SELECT * FROM instruction_list WHERE 1 ORDER BY id DESC");
            while($instruction = mysqli_fetch_array($instruction_select)){
                ?>
                <tr>
                    <td><?= $instruction['id'] ?></td>
                    <td><h4><?= $instruction['title'] ?></h4><p><?= $instruction['description'] ?></p></td>
                    <td>
                        <button class="btn btn-xs btn-primary">Edit</button>
                        <button class="btn btn-xs btn-primary">Delete</button>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
</div>