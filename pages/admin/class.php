<?php
    include 'includes/class.php';
    $class = new Classclass();
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Class <small> The classes for Subjects to be added</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-alt"></i> Class
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">All Classes</h3>
            </div>
            <div class="panel-body">
                <?php
                $allclass = $class->all_class();
                foreach($allclass as $class_det){
                    echo '<div class="well well-sm">'.$class_det['title'].'<button class="btn btn-danger btn-xs pull-right class-edit" edit-id="'.$class_det['id'].'" edit-title="'.$class_det['title'].'">Edit</button> </div>';
                }
                ?>
            </div>
        </div>
    </div>
    
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Add Class</h3>
            </div>
            <div class="panel-body">
                <form action="submit/class-submit.php" method="POST">
                    Class Name :
                    <input name="classname" class="form-control" />
                    Code :
                    <input name="code" class="form-control" />
                    <br>
                    <input type="submit" name="submit-class" value="Submit Class" class="form-control btn btn-primary" />
                </form>
            </div>
        </div>
    </div>
    
</div>
  
    
    
        
    <!-- Modal -->
<div id="classModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Class</h4>
      </div>
      <div class="modal-body">
            <form method="POST" action="submit/class-submit.php" >
                Class Name:
                <input name="classeditname" class="form-control classeditname" />
                <input name="classeditid" type="hidden" class="form-control classeditid" />
                <br>
                <input type="submit" name="class-submit" value="Edit Class" class="form-control btn btn-primary" />
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    
    <!-- Delete Modal -->
<div id="classModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Class</h4>
      </div>
      <div class="modal-body">
            <form method="POST" action="submit/class-submit.php" >
                Class Name:
                <input name="classeditname" class="form-control classeditname" />
                <input name="classeditid" type="hidden" class="form-control classeditid" />
                <br>
                <input type="submit" name="class-submit" value="Edit Class" class="form-control btn btn-primary" />
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>