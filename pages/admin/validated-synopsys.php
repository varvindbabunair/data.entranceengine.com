<?php
    include 'includes/materials.php';
    $notes = new Materials();
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Synopsys <small> Validated</small>
            </h1>
            <ol class="breadcrumb">
                <li class="">
                    <i class="fa fa-sticky-note"></i> Synopsys
                </li>
                <li class="active">
                    <i class="fa fa-check"></i> Validated
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    
  
    <br>

    <div class="row">
        <div class="col-lg-12" style="margin-top:20px;margin-bottom:50px;" >
            
            <table class="table table-hover table-condensed table-striped datatable" style="font-size:12px;" >
                <thead><th>#</th><th>Reference</th><th>Class</th><th>Subject</th><th>Chapter</th><th>Validated By</th><th>Option</th></thead>
            <?php
            if($user->user_details['admin_role'] == 'admin'){
                $note_select_query = "SELECT synopsys_list.*, class_list.title as class_name, subject_list.title as subject_name, chapter_list.title as chapter_name, admin_list.name as validated_user "
                        . "FROM synopsys_list "
                        . "INNER JOIN class_list ON synopsys_list.class = class_list.id "
                        . "INNER JOIN subject_list ON synopsys_list.subject = subject_list.id "
                        . "INNER JOIN chapter_list ON synopsys_list.chapter = chapter_list.id "
                        . "INNER JOIN admin_list ON synopsys_list.validated_by = admin_list.id "
                        . "WHERE validated_by != 0 ORDER BY id DESC";
            }else{
                $note_select_query = "SELECT synopsys_list.*, class_list.title as class_name, subject_list.title as subject_name, chapter_list.title as chapter_name, admin_list.name as validated_user "
                        . "FROM synopsys_list "
                        . "INNER JOIN class_list ON synopsys_list.class = class_list.id "
                        . "INNER JOIN subject_list ON synopsys_list.subject = subject_list.id "
                        . "INNER JOIN chapter_list ON synopsys_list.chapter = chapter_list.id "
                        . "INNER JOIN admin_list ON synopsys_list.validated_by = admin_list.id "
                        . "WHERE added_by = $uid AND validated_by != 0 ORDER BY id DESC";
            }
            $note_get_query = $db->query($note_select_query)or die(mysqli_error($db->db_link));
            $cnt = 1;
            while($note_info = mysqli_fetch_array($note_get_query)){
            ?>
            <tr>
                <td>
                    <?= $note_info['id'] ?>
                </td>
                <td>
                    <?= $note_info['reference'] ?>
                </td>
                <td>
                    <?= $note_info['class_name'] ?>
                </td>
                <td>
                    <?= $note_info['subject_name'] ?>
                </td>
                <td>
                    <?= $note_info['chapter_name'] ?>
                </td>
                <td>
                    <?= $note_info['validated_user'] ?>
                </td>
                <td>
                    <a href="?page=edit-synopsys&id=<?= $note_info['id'] ?>"><button class="btn btn-primary btn-xs" ><span  class="fa fa-edit"></span> Edit</button></a>
                    <button class="btn btn-xs btn-primary qn-view" qnid="<?= $note_info['id'] ?>" data-toggle="modal" data-target="#qnviewModal" ><span class="fa fa-eye"></span> View</button>
                </td>
            </tr>
            <?php
            }
            ?>
            </table>
            
        </div>
  
</div>

<div id="qn-removeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remove Synopsis</h4>
      </div>
        <form method="POST" action="submit/synopsys.php" >
            <div class="modal-body">
                <p><strong>Are you sure you want to remove the following synopsis from the database ?</strong></p>
                <p>This action will permanently remove synopsis from the database. This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" value="" id="qn-remove-id" name="delete-id" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <input type="submit" name="delete-submit" id="delete-note" class="btn btn-primary" value="Delete Synopsis" />
            </div>
        </form>
    </div>

  </div>
</div>

    
    <!-- Modal -->
<div id="qnviewModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Notes</h4>
      </div>
      <div class="modal-body qn-content">
          <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>