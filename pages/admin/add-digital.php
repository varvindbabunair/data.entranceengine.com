<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Add Digital Materials
            </h1>
            <ol class="breadcrumb">
                <li>
                    Digital Materials
                </li>
                <li class="active">
                    <i class="fa fa-plus"></i> Add Digital Materials
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

<div class="row">
    <form enctype="multipart/form-data" method="POST" action="submit/digital.php">
        <div class="col-md-6">
            Title :
            <input type="text" name="material-title" class="form-control" required="">
            Description :
            <textarea name="material-description" class="form-control" required=""></textarea>

            Questions File :
            <input type="File" name="question-file" class="form-control" required="" accept=".pdf" >
            Options File :
            <input type="File" name="answer-file" class="form-control" required="" accept=".pdf" >
            Hints File :
            <input type="File" name="hint-file" class="form-control" required="" accept=".pdf" >

            <p style="text-align: center;"><input style="margin-top: 20px;" class="btn btn-primary" type="submit" name="material-submit" value="Submit Material"  ></p>
        </div>
    </form>
</div>