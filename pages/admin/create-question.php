<?php
include 'includes/questions.php';
$questions = new Questions();

?>
<form id="qn-addForm" enctype="multipart/form-data" method="POST" action="submit/questions.php">
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                Choose a Chapter :
                <select id="chapter-select" name="chapter" class="form-control chapter-select" >
                    <option value="0">Choose from the list</option>
                    <?php
                    $subject_list = $questions->get_subject_list();
                    foreach($subject_list as $subject){
                    ?>
                    <optgroup label="<?php echo $subject['title'] ?>">
                    <?php
                    $chapter_list = $questions->get_chapter_list($subject['id']);
                    foreach ($chapter_list as $chapter){
                    ?>
                        <option value="<?php echo $chapter['id']; ?>"><?php echo $chapter['title']; ?></option>
                    <?php
                    }
                    ?>
                    </optgroup>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                Question Type:
                <select class="form-control question-type" name="question-type">
                    <option value="text">Text</option>
                    <option value="image">Image</option>
                </select>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                Question :
                <textarea name="text-question" class="form-control text-question" required ></textarea>
                <input type="file" class="image-question" style="display:none;" name="image-question" accept=".jpg,.png" />
            </div>
                <div class="col-lg-6">
                Section :
                <select name="section" class="form-control section-select">
                    <option>Select Section</option>
                </select>
                </div>
                <div class="col-lg-12">
                Tags :
                <input class="tags" name="tags" data-role="tagsinput" required="" value="GRLS,NEET,JEE MAINS,JEE ADVANCED,AIIMS,BITSAT,JIPMER,NSEC,ALLEN,RESONANCE,AKASH,CHAITANYA,NARAYAN,FIITJEE,KEAM,UCEED,MTG,ARIHANT,CENGAGE,DINESH,DISHA,GRB,BALAJI,TRUEMAN,PRADEEP" />
                </div>
    <div class="col-lg-6">
        <select name="answer-type" class="form-control">
            <option value="mcq">Multiple Choice Question</option>
            <option value="nueric">Numeric</option>
        </select>
    </div>
                <div class="col-lg-12">
                <h4>Answer</h4>
                
                <div class="">
                <table class="table option-table"><tbody>
                        
                    <tr class="option-row-A">
                        <td>Option A</td>
                        <td><select name="answer-type-a" typeof="A" class="form-control answer-type"><option value="text">Text</option><option value="image">Image</option><option value="src">Image Source</option><option value="equation">Equation</option></select></td>
                        <td><input name="text-answer-a" type="text" class="form-control option-text option-text-A" /> <input name="image-answer-a" class="option-image option-image-A" style="display:none;" type="file" accept=".jpg,.png" /></td>
                        <td><input type="radio" name="correct-answer" value="A" checked="" /></td>
                    </tr>
                    <tr class="option-row-B">
                        <td>Option B</td>
                        <td><select name="answer-type-b" typeof="B" class="form-control answer-type"><option value="text">Text</option><option value="image">Image</option><option value="src">Image Source</option><option value="equation">Equation</option></select></td>
                        <td><input name="text-answer-b" type="text" class="form-control option-text option-text-B" /> <input name="image-answer-b" class="option-image option-image-B" style="display:none;" type="file" accept=".jpg,.png" /></td>
                        <td><input type="radio" name="correct-answer" value="B" /></td>
                    </tr>
                    <tr class="option-row-C">
                        <td>Option C</td>
                        <td><select name="answer-type-c" typeof="C" class="form-control answer-type"><option value="text">Text</option><option value="image">Image</option><option value="src">Image Source</option><option value="equation">Equation</option></select></td>
                        <td><input name="text-answer-c" type="text" class="form-control option-text option-text-C" /> <input name="image-answer-c" class="option-image option-image-C" style="display:none;" type="file" accept=".jpg,.png" /></td>
                        <td><input type="radio" name="correct-answer" value="C" /></td>
                    </tr>
                    <tr class="option-row-D">
                        <td>Option D</td>
                        <td><select name="answer-type-d" typeof="D" class="form-control answer-type"><option value="text">Text</option><option value="image">Image</option><option value="src">Image Source</option><option value="equation">Equation</option></select></td>
                        <td><input name="text-answer-d" type="text" class="form-control option-text option-text-D" /> <input name="image-answer-d" class="option-image option-image-D" style="display:none;" type="file" accept=".jpg,.png" /></td>
                        <td><input type="radio" name="correct-answer" value="D" /><span style="margin-left:10px;" class="fa fa-plus add-option" title="Add Option"></span><span style="margin-left:10px;" class="fa fa-remove remove-option" title="Remove Option"></span></td>
                    </tr>
                </tbody></table>
                    </div>
            </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                Hint Type:
                <select class="form-control hint-type" name="hint-type">
                    <option value="text">Text</option>
                    <option value="image">Image</option>
                </select>
            </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Hint</h4>
                <textarea name="text-hint" class="form-control text-hint"></textarea>
                <input type="file" class="image-hint" style="display:none;" name="image-hint" accept=".jpg,.png" />
            </div>
                
                <input type="hidden" class="option-number" value="5" name="option-number" />
            
      
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button id="qn-add-submit" type="submit" value="submit" name="question-submit" class="btn btn-primary">Submit Question</button>
        
    </form>