<?php
    include 'includes/materials.php';
    $notes = new Materials();
    
    $notes = new Materials();
$editid = (isset($_GET['id']))?$_GET['id']:'';
if($editid == ''){
    die('Some Error Occured');
}
$notes_get_query = "SELECT * FROM synopsys_list WHERE id = $editid";

$edit_note = $notes->query($notes_get_query);
if(mysqli_num_rows($edit_note) == 0){
    die('Some Error Occured');
}
$note = mysqli_fetch_array($edit_note);
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Synopsis <small> Edit Synopsis</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-alt"></i> Synopsis
                </li>
                <li class="active">
                    <i class="fa fa-list-alt"></i> Edit Synopsis
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <div class="row" style="margin-bottom:50px;">
    
            <form action="submit/synopsys.php" method="POST" >
                <div class=>
                    <div class="col-md-4">

                        Class :
                        <select class="form-control class-list" name="class-name">
                            <option>Select Class</option>
                        <?php
                        $class_select_query = $db->query("SELECT * FROM class_list");
                        while ($class = mysqli_fetch_array($class_select_query)) {
                            echo '<option code="'.$class['code'].'" value="'.$class['id'].'"';
                            if($class['id'] == $note['class'] ){
                                echo ' selected ';
                            }
                            echo '>'.$class['title'];
                            echo '</option>';
                        }

                        ?>
                        </select>
                        <br>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12">
                    <br>
        <div class="" style="width: 100%;overflow: hidden;min-height: 170px;">
            <div class="col-lg-3 subjects-head" style="padding:0px;margin:0px;"><strong>Subjects</strong>
                <div class="subjects" style="height:170px;border: 1px solid #ccc;padding: 0px;overflow-y: scroll;">
                    <ul style="list-style-type: none;padding:0px;margin:0px;">
                    <?php
                    $subject_select_query = $db->query("SELECT * FROM subject_list WHERE class = '$note[class]' ");
                    $selected_subject = '';
                    while ($subject = mysqli_fetch_array($subject_select_query)) {
                        echo '<li class="subject-list';
                        if($note['subject'] == $subject['id']){
                            echo ' selected';
                            $selected_subject = $subject['id'];
                        }
                        echo '" subject_var="'.$subject['id'].'">';
                        echo $subject['title'];
                        echo '</li>';
                    }
                    ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 chapters-head" style="height:210px;padding:0px;margin:0px;"><strong>Chapters</strong>
                <div class="chapters" style="height:170px;border: 1px solid #ccc;padding: 0px;overflow-y: scroll;">
                    <ul style="list-style-type: none;padding:0px;margin:0px;">
                    <?php
                    $chapter_select_query = $db->query("SELECT * FROM chapter_list WHERE subject = '$note[subject]' ");
                    $selected_chapter = '';
                    while ($chapter = mysqli_fetch_array($chapter_select_query)) {
                        echo '<li class="chapter-list';
                        if($note['chapter'] == $chapter['id']){
                            echo ' selected';
                            $selected_chapter = $chapter['id'];
                        }
                        echo '" subject_var="'.$chapter['id'].'">';
                        echo $chapter['title'];
                        echo '</li>';
                    }
                    ?>
                    </ul>
                </div>
            </div>
        </div>
                </div>
                <br>
                <div class="col-lg-4">
                    <input class="form-control" name="reference" value="<?= $note['reference'] ?>" />
                </div>
            <div class="col-lg-12" style="margin-top: 20px;">
                <div class="btn btn-xs btn-primary pull-right media-modal-btn" data-toggle="modal" data-target="#mediaModal"><span class="fa fa-plus"></span> Add Media</div>
            </div>
                <div class="col-lg-12" style="margin-top: 20px;">
                  Notes :
                  <textarea class="form-control ckeditor" name="editor1"><?= $note['synopsys'] ?></textarea>
                  <br>
                  <div style="text-align: center;">
                      <input type="hidden" class="subject-id" name="subject-id" />
                      <input type="hidden" class="chapter-id" name="chapter-id" />
                      <input type="hidden" name="edit-id" value="<?= $_GET['id'] ?>" />
                      <input type="submit" id="synopsys-edit-submit" name="edit-submit" value="Save Changes" class="btn btn-primary" />
                  </div>
                  </div>
            </form>
    
</div>
    
        
    <!-- Modal -->
<div id="mediaModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Media Modal</h4>
      </div>
      <div class="modal-body">
          <form id="img-submit" method="POST" action="" enctype="multipart/form-data">
              <div style="overflow: hidden;">
              <input class="pull-left" type="file" name="media-upload" accept=".png,.jpg,.gif" />
              <input type="hidden" name="test" value="test" />
              <input type="submit" class="btn btn-sm btn-primary pull-right" value="Add Image" />
              </div>
          </form>
          <hr>
          <h5 style="border-bottom: 1px solid #ccc;">Existing Images</h5>
          <div class="media-files" style="width: 100%;max-height: 400px;overflow: hidden;overflow-y: scroll;">
            <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


