<?php
    include 'includes/subjects.php';
    $subject = new Subjects();
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Syllabus
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    Syllabus
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <div class="row" style="margin-bottom: 20px;">
    <div class="col-md-4">
        Class :
        <select class="form-control class-list">
            <option value="0">Select Class</option>
            <?php
            foreach ($subject->all_class() as $class) {
            ?>
            <option value="<?= $class['id'] ?>"><?= $class['title'] ?></option>
            <?php
            }
            ?>
        </select>
    </div>
</div>
    

    <div class="col-lg-3 subjects" style="border: 1px solid #ccc;padding: 0px;display: none;">

    </div>

    <div class="col-lg-3 chapters" style="border: 1px solid #ccc;padding: 0px;display: none;">

    </div>

    <div class="col-lg-3 topics" style="border: 1px solid #ccc;padding: 0px;display: none;">

    </div>

    <div class="col-lg-3 sections" style="border: 1px solid #ccc;padding: 0px;display: none;">

    </div>