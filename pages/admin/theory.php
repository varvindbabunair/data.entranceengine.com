<?php
    include 'includes/materials.php';
    $notes = new Materials();
?>
<!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Theory <small> Overview</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-sticky-note"></i> Theory
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    
   
<div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-sticky-note fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <?php
                                $uid = $user->user_details['id'];
                            if($user->user_details['admin_role'] == 'admin'){
                                $qn_cnt_fetch = $notes->query("SELECT id FROM theory_list");
                            }else{
                                $qn_cnt_fetch = $notes->query("SELECT id FROM theory_list WHERE added_by = $uid");
                            }
                            ?>
                            <div class="huge"><?= mysqli_num_rows($qn_cnt_fetch) ?></div>
                            <div>Theory</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    
    <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-question fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <?php
                            if($user->user_details['admin_role'] == 'admin'){
                                $qn_vlcnt_fetch = $notes->query("SELECT id FROM theory_list WHERE validated_by = '0'");
                            }else{
                                $qn_vlcnt_fetch = $notes->query("SELECT id FROM theory_list WHERE validated_by = '0' AND added_by = $uid");
                            }
                            
                            ?>
                            <div class="huge"><?= mysqli_num_rows($qn_vlcnt_fetch) ?></div>
                            <div>Un Validated Theory</div>
                        </div>
                    </div>
                </div>
                <a href="?page=questions-validate">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-question fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <?php
                            if($user->user_details['admin_role'] == 'admin'){
                                $qn_apcnt_fetch = $notes->query("SELECT id FROM theory_list WHERE validated_by != 0");
                            }else{
                                $qn_apcnt_fetch = $notes->query("SELECT id FROM theory_list WHERE validated_by != 0 AND added_by = $uid");
                            }
                            
                            ?>
                            <div class="huge"><?= mysqli_num_rows($qn_apcnt_fetch) ?></div>
                            <div>Approved Theory</div>
                        </div>
                    </div>
                </div>
                <a href="?page=questions-approve">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-support fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <?php
                            if($user->user_details['admin_role'] == 'admin'){
                                $qn_rwcnt_fetch = $notes->query("SELECT id FROM theory_list WHERE review_status = '1'");
                            }else{
                                $qn_rwcnt_fetch = $notes->query("SELECT id FROM theory_list WHERE review_status = '1' AND added_by = $uid");
                            }
                            
                            ?>
                            <div class="huge"><?= mysqli_num_rows($qn_rwcnt_fetch) ?></div>
                            <div>Questions for Review</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <a href="?page=add-theory"><button class="btn btn-primary pull-right"><span class="fa fa-plus"></span> Add Theory</button></a>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-12" style="margin-top:20px;margin-bottom:50px;" >
            
            <table class="table table-hover table-condensed table-striped datatable" style="font-size:12px;" >
                <thead><th>#</th><th>Reference</th><th>Class</th><th>Subject</th><th>Chapter</th><th>Added By</th><th>Option</th></thead>
            <?php
            if($user->user_details['admin_role'] == 'admin'){
                $note_select_query = "SELECT theory_list.*, class_list.title as class_name, subject_list.title as subject_name, chapter_list.title as chapter_name, admin_list.name as added_user "
                        . "FROM theory_list "
                        . "INNER JOIN class_list ON theory_list.class = class_list.id "
                        . "INNER JOIN subject_list ON theory_list.subject = subject_list.id "
                        . "INNER JOIN chapter_list ON theory_list.chapter = chapter_list.id "
                        . "INNER JOIN admin_list ON theory_list.added_by = admin_list.id "
                        . "ORDER BY id DESC";
            }else{
                $note_select_query = "SELECT theory_list.*, class_list.title as class_name, subject_list.title as subject_name, chapter_list.title as chapter_name, admin_list.name as added_user "
                        . "FROM theory_list "
                        . "INNER JOIN class_list ON theory_list.class = class_list.id "
                        . "INNER JOIN subject_list ON theory_list.subject = subject_list.id "
                        . "INNER JOIN chapter_list ON theory_list.chapter = chapter_list.id "
                        . "INNER JOIN admin_list ON theory_list.added_by = admin_list.id "
                        . "WHERE added_by = $uid ORDER BY id DESC";
            }
            $note_get_query = $db->query($note_select_query)or die(mysqli_error($db->db_link));
            $cnt = 1;
            while($note_info = mysqli_fetch_array($note_get_query)){
            ?>
            <tr>
                <td>
                    <?= $note_info['id'] ?>
                </td>
                <td>
                    <?= $note_info['reference'] ?>
                </td>
                <td>
                    <?= $note_info['class_name'] ?>
                </td>
                <td>
                    <?= $note_info['subject_name'] ?>
                </td>
                <td>
                    <?= $note_info['chapter_name'] ?>
                </td>
                <td>
                    <?= $note_info['added_user'] ?>
                </td>
                <td>
                    <a href="?page=edit-theory&id=<?= $note_info['id'] ?>"><button class="btn btn-primary btn-xs" ><span  class="fa fa-edit"></span> Edit</button></a>
                    <button class="btn btn-primary btn-xs qn-remove" data-toggle="modal" data-target="#qn-removeModal" removeid="<?php echo $note_info['id']; ?>" ><span class="fa fa-remove"></span> Remove</button>
                    <button class="btn btn-xs btn-primary qn-view" qnid="<?= $note_info['id'] ?>" data-toggle="modal" data-target="#qnviewModal" ><span class="fa fa-eye"></span> View</button>
                </td>
            </tr>
            <?php
            }
            ?>
            </table>
            
        </div>
  
</div>

<div id="qn-removeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Remove Theory</h4>
      </div>
        <form method="POST" action="submit/theory.php" >
            <div class="modal-body">
                <p><strong>Are you sure you want to remove the following theory from the database ?</strong></p>
                <p>This action will permanently remove theory from the database. This action is irreversible.</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" value="" id="qn-remove-id" name="delete-id" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <input type="submit" name="delete-submit" id="delete-note" class="btn btn-primary" value="Delete Theory" />
            </div>
        </form>
    </div>

  </div>
</div>

    
    <!-- Modal -->
<div id="qnviewModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Notes</h4>
      </div>
      <div class="modal-body qn-content">
          <p style="text-align: center"><span class="fa fa-cog fa-spin"></span> Loading...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>