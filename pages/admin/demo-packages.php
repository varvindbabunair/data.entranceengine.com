<?php
include_once 'includes/packages.php';
$packages = new Packages();
?>

<div class="col-lg-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class='panel-title'>Total Packages</h3>
        </div>
        <div class="panel-body">
            <h3 style="text-align: center"><strong><?php echo $packages->get_package_num(); ?></strong></h3>
        </div>
    </div>
</div>

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Packages</h3>
        </div>
        <div class="panel-body">
            <table class="table table-condensed table-hover">
                <thead><th>Id</th><th>Package</th><th>Options</th></thead>
                <tbody>
                    <?php
                    $package_list = $packages->get_demo_packages('all');
                    foreach($package_list as $package){
                        ?>
                    <tr>
                        <td><?php echo $package['id']; ?></td>
                        <td>
                            <img src="images/packages/<?php echo $package['image']; ?>" class="img-thumbnail pull-left" style="margin:0px 20px 5px 0px;" alt="Cinque Terre" width="80" height="80"> 
                            <p><strong><?php echo $package['title']; ?></strong></p>
                            <p><?php echo $package['description']; ?></p>
                        </td>
                        <td>
                            <span class="fa fa-eye" title="View"></span>
                            <span class="fa fa-edit" title="Edit"></span>
                            <span class="fa fa-remove" title="Remove"></span>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>