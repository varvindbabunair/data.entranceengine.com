<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Chapter Tests <small>All chapter tests for app</small>
        </h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12" style="margin-top:20px;">

        <div class="panel-body">
            <table class="table table-condensed table-hover dataTable">
                <thead><th>Test Name</th><th>Subject</th><th>Chapter</th><th>Topic</th><th>Tags</th><th>Options</th></thead>
                <?php
                $tests_list = $db->query("SELECT ttl.id, ttl.title, ttl.tags, sl.title as subject, cl.title as chapter, tl.title as topic
                                    FROM app_topic_test_list ttl
                                    LEFT JOIN chapter_list cl on ttl.chapter = cl.id
                                    LEFT JOIN subject_list sl on ttl.subject = sl.id
                                    LEFT JOIN topic_list tl on ttl.topic = tl.id");
                while ($test = mysqli_fetch_array($tests_list)){
                    ?>
                    <tr>
                        <td>
                            <?php echo $test['title']; ?>
                        </td>
                        <td>
                            <?php echo $test['subject']; ?>
                        </td>
                        <td>
                            <?php echo $test['chapter']; ?>
                        </td>
                        <td>
                            <?php echo $test['topic']; ?>
                        </td>
                        <td>
                            <?php echo $test['tags']; ?>
                        </td>
                        <td>
                            <button class="btn btn-xs btn-primary">Edit</button>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </div>
</div>



<div id="userAddModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Institute</h4>
            </div>
            <form action="submit/institutes.php" method="POST" enctype="multipart/form-data" >
                <div class="modal-body">
                    <h3>Institute Details</h3>
                    Logo / Icon Image (150 * 150 px):
                    <input class="form-control" value="" type="file" accept=".jpg,.png" name="inst-logo"/>
                    Name of Institute *:
                    <input class="form-control inst-name" value="" name="inst-name"/>
                    Institute Tagline :
                    <input class="form-control" value="" name="inst-tagline"/>
                    <div class="row">
                        <div class="col-lg-12">
                            Institute Website :
                            <input type="text" class="form-control" name="inst-website" placeholder="Start with http://" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Package Expiry Date *:
                            <input class="form-control package-expiry" value="<?= date('Y-m-d', strtotime('+1 year')) ?>" type="date" name="package-expiry"/>
                        </div>
                        <div class="col-lg-6">
                            Institute Key :
                            <input class="form-control" type="text" name="instkey" required="" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Email *:
                            <input class="form-control inst-email" value="" name="inst-email" type="email" />
                        </div>
                        <div class="col-lg-6">
                            Phone *:
                            <input class="form-control inst-phone" type="text" name="inst-phone">
                        </div>
                    </div>
                    Address *:
                    <textarea class="form-control inst-address" name="inst-address"></textarea>
                    <br>


                    <input type="submit" name="new-user-submit" class="btn btn-primary inst-submit pull-right" value="Add Institute" />

                </div>
            </form>
        </div>

    </div>
</div>



<div id="adminAddModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Institute</h4>
            </div>
            <form action="submit/institutes.php" method="POST" enctype="multipart/form-data" >
                <div class="modal-body">
                    <h3>Institute Administrator Details</h3>
                    Name *:
                    <input class="form-control name" value="" name="name"/>
                    Email *:
                    <input class="form-control email" type="email" value="" name="email"/>
                    Phone *:
                    <input class="form-control phone" type="" value="" name="phone"/>
                    Password *:
                    <input class="form-control pass" type="password" value="" name="password"/>
                    Confirm Password *:
                    <input class="form-control cnf-pass" value="" name="cnf-password"/>
                    <br>
                    <input type="hidden" name="instid" class="instid" value="" />

                    <input type="submit" name="new-admin-submit" class="btn btn-primary inst-submit pull-right" value="Add Institute Administrator" />
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>

    </div>
</div>


<div id="userEditModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Institute Details</h4>
            </div>
            <form action="submit/institutes.php" method="POST">
                <div class="modal-body edit-body">

                </div>
                <div class="modal-footer">
                    <input type="hidden"  name="user-role" value="institute" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" name="edit-user-submit" class="btn btn-primary" value="Save Changes" />
                </div>
            </form>
        </div>

    </div>
</div>


<div id="userRemoveModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Remove Institute</h4>
            </div>
            <form action="submit/institutes.php" method="POST">
                <div class="modal-body">
                    <h3>Are you sure you want to delete this Institutes. ?</h3>
                    <input type="hidden" value="" name="delete-user" class="delete-user" />
                </div>
                <div class="modal-footer">
                    <input type="hidden"  name="user-role" value="institute" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" name="remove-user-submit" class="btn btn-primary" value="Confirm Delete" />
                </div>
            </form>
        </div>

    </div>
</div>


<div id="userResetModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reset Institute Password</h4>
            </div>
            <form action="submit/institutes.php" method="POST">
                <div class="modal-body edit-body">
                    New Password :
                    <input type="text" class="form-control" name="new-password" />
                    <input type="hidden"  name="user-id" class="user-id" />
                    <input type="hidden"  name="user-role" value="institute" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" name="reset-user-submit" class="btn btn-primary" value="Reset Password" />
                </div>
            </form>
        </div>

    </div>
</div>