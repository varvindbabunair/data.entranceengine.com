<?php
include_once dirname(__FILE__).'/materials.php';
class Packages extends Materials{
    public $package_details;
    public $subject_list;
    public $chapter_list;
    public $material_list;
    public $package_contents;
    
    public function __construct($packageid=null) {
        parent::__construct();
        if($packageid){
            
            $package_get_id  = parent::query("SELECT * FROM package_list WHERE id = '$packageid'");
            $this->package_details = (mysqli_num_rows($package_get_id) == 0)? NULL:mysqli_fetch_array($package_get_id);


            $this->subject_list = json_decode($this->package_details['subject_list']);
            $query = "";
            foreach ($this->subject_list as $subject){
                $query .= "SELECT id,'".$subject[0]."' as type,title FROM ".$subject[0]."_list WHERE id = '".$subject[1]."'";
                $query .= " UNION ";
            }
            $this->chapter_list = json_decode($this->package_details['chapter_list']);
            foreach ($this->chapter_list as $chapter_id_list){
                foreach ($chapter_id_list as $chapter){
                    $query .= "SELECT id,'".$chapter[0]."' as type,title FROM ".$chapter[0]."_list WHERE id = '".$chapter[1]."'";
                    $query .= " UNION ";
                }
            }
            $this->material_list = json_decode($this->package_details['material_list']);
            foreach ($this->material_list as $materialid_list){
                foreach ($materialid_list as $material_ids){
                    foreach ($material_ids as $material){
                        if(isset($material[0])){
                            $query .= "SELECT id,'".$material[0]."' as type,title FROM ".$material[0]."_list WHERE id = '".$material[1]."'";
                            $query .= " UNION ";
                        }
                    }
                }
            }
            $query = trim($query," UNION");
$ret = array();
            if($query !=''){
            $get_items_query = $this->query($query);
            
            while ($item = mysqli_fetch_array($get_items_query)){
                array_push($ret, $item);
            }
            }
            $this->package_contents = $ret;
            
        }
         
    }
    
    function get_package_details($packageid){
        $package_get_query = parent::query("SELECT id,title,image,upgrade_price,include_taxes,description,base,pricing,subscription FROM package_list WHERE id = '$packageid'");
        return mysqli_fetch_array($package_get_query);
    }
    
    function subscribed_packages($userid){
        $subscribed_packages_get_query = $this->query("SELECT id,package,purchase_date FROM order_list WHERE user = '$userid' AND status = '1'");
        $package_list = array();
        while($package = mysqli_fetch_array($subscribed_packages_get_query)){
            $purchased_date = date_create($package['purchase_date']);
            $package_details = $this->get_package_details($package['package']);
            $today = date_create(date('Y-m-d'));
            $curdays = date_diff($purchased_date, $today);
            $gone_days = (string)$curdays->format("%a");
            $valid_days = json_decode($package_details['subscription']);
            
            if($gone_days < (int)$valid_days[1]){
                array_push($package_list,$package_details);
            }
            
        }
        $arrcnt = sizeof($package_list);
        for($i=0;$i<$arrcnt;$i++){
            $package_check_id = $package_list[$i]['id'];
            for($j=$i+1;$j<$arrcnt;$j++){
                $package_bottom_base = $package_list[$j]['base'];
                if($package_check_id == $package_bottom_base){
                    $package_list[$i] = $package_list[$j];
                    $i--;
                    break;
                }
            }
        }
        $returnarr = array_unique($package_list, SORT_REGULAR);
        return $returnarr;
    }
    
    function get_upgrade_packages($packageid){
        $returnarray = array();
        $i=0;
        do{
            $get_higher_package_query = $this->query("SELECT id,title,description,base,pricing,include_taxes FROM package_list WHERE base = '$packageid'");
            if (mysqli_num_rows($get_higher_package_query) == 0){
                break;
            }
            $array = mysqli_fetch_array($get_higher_package_query);
            array_push($returnarray, $array);
            $packageid = $array['id'];
        }while(1);
        return $returnarray;
    }
    
    function get_downgrade_packages($packageid){
        $returnarray = array();
        $i=0;
        do{
            $get_lower_package_query = $this->query("SELECT id,title,description,base,pricing,include_taxes FROM package_list WHERE id = '$packageid'");
            if (mysqli_num_rows($get_lower_package_query) == 0){
                break;
            }
            $array = mysqli_fetch_array($get_lower_package_query);
            array_push($returnarray, $array);
            $packageid = $array['base'];
        }while($packageid != 0);
        return $returnarray;
    }
    
    function get_live_packages(){
        $package_get_query = $this->query("SELECT * FROM package_list WHERE package_type = 'live'");
        $returnarr = array();
        while($package = mysqli_fetch_array($package_get_query)){
            array_push($returnarr, $package);
        }
        return $returnarr;
    }
    
    function get_package_list($exclude,$student){
        $group_get_query = $this->query("SELECT student_group FROM student_list WHERE id = '$student'");
        $group_get = mysqli_fetch_array($group_get_query);
        $group = $group_get['student_group'];
        if($exclude == NULL){
            $package_get_query = $this->query("SELECT * FROM package_list WHERE for_sale = '1' AND package_type = 'live' AND user_group = '$group'");
        }else{
            $query = "SELECT * FROM package_list WHERE for_sale = '1' AND package_type = 'live' AND user_group = '$group' AND ";
            $exclude_list = explode(',', $exclude);
            foreach ($exclude_list as $exclude_id){
                $upgrade_list = $this->get_upgrade_packages($exclude_id);
                $downgrade_list = $this->get_downgrade_packages($exclude_id);
                
                foreach($downgrade_list as $list){
                    $excid = $list['id'];
                    $query .= "id != '$excid' AND ";
                }
                foreach($upgrade_list as $list){
                    $excid = $list['id'];
                    $query .= "id != '$excid' AND ";
                }
            }
            $query = trim($query," AND ");
            $package_get_query = $this->query($query);
        }
        $returnarr = array();
        while($package = mysqli_fetch_array($package_get_query)){
            array_push($returnarr, $package);
        }
        return $returnarr;
    }
    
    function get_upgrade_price($fromId,$toId){
        $upgrade_price = 0;
        do{
            $get_lower_package_query = $this->query("SELECT id,title,description,base,upgrade_price FROM package_list WHERE id = '$toId' AND base != '0'");
            if (mysqli_num_rows($get_lower_package_query) == 0 || $fromId == $toId){
                break;
            }
            $array = mysqli_fetch_array($get_lower_package_query);
            $toId = $array['base'];
            $upgrade_price = $upgrade_price + (int)$array['upgrade_price'];
        }while(1);
        return $upgrade_price;
    }
    
    function get_title($type,$id){
        $qry = $this->query("SELECT title FROM ".$type."_list WHERE id = '$id'");
        $title = mysqli_fetch_array($qry);
        return $title['title'];
    }
    function get_tags($type,$id){
        $qry = $this->query("SELECT tags FROM ".$type."_list WHERE id = '$id'");
        $tags = mysqli_fetch_array($qry);
        return explode(',', $tags['tags']);
    }
    
    function get_package_num(){
        $package_num_query = $this->query("SELECT COUNT(id) as count FROM package_list WHERE 1");
        $package_qry = mysqli_fetch_array($package_num_query);
        return $package_qry['count'];
    }
    
    function get_days_left($packageid,$userid){
        $packageid = ($this->package_details)?$this->package_details['id']:$packageid;
        if($this->package_details['id'] == $packageid){
            $dates = json_decode($this->package_details['subscription']);
            $date_valid = $dates[1];
        }else{
            $package_subscription_query = $this->query("SELECT subscription FROM package_list WHERE id = '".$packageid."'");
            $date_get = mysqli_fetch_array($package_subscription_query);
            $dates = json_decode($date_get['subscription']);
            $date_valid = $dates[1];
        }
        
        $package_buy_date_query = $this->query("SELECT purchase_date FROM order_list WHERE user = '$userid' AND user = '$userid' AND status = '1'");
        
        $purchased_date_qry = mysqli_fetch_array($package_buy_date_query);
        $purchased_date = date_create($purchased_date_qry['purchase_date']);
        $today = date_create(date('Y-m-d'));
        $curdays = date_diff($purchased_date, $today);
        $gone_days = (string)$curdays->format("%a");
        return ((int)$gone_days < (int)$date_valid)?(int)$date_valid-(int)$gone_days:0 ;
    }
        
    function get_item_title($type,$id){
        foreach ($this->package_contents as $content){
            if($content['id']==$id && $content['type']==$type){
                return $content['title'];
            }
        }
    }
    
    function get_taxes(){
        $tax_get_query = $this->query("SELECT value FROM package_settings WHERE name = 'taxes'");
        $tax_result = mysqli_fetch_array($tax_get_query);
        return json_decode($tax_result['value']);
    }
    
    function get_principal($amount,$include){
        $taxes = $this->get_taxes();
        $tax_amount = 0;
        $tax_percent = 0;
        foreach($taxes as $tax){
            if($tax[1] == 'amount'){
                $tax_amount += (int)$tax[2];
            }else if($tax[1] == 'percent'){
                $tax_percent += (int)$tax[2];
            }
        }
        $deducted_amount = ($include == '1')?(int)$amount - $tax_amount:(int)$amount;
        $principal = ($include == '1')?($deducted_amount*100)/($tax_percent+100):((int)$amount + ((int)$amount*$tax_percent/100))+$tax_amount;
        return round($principal,2);
    }
    
    function get_base_packages($student_group,$exclude){
        $returnarr = array();
        if($exclude == NULL || trim($exclude) == ''){
            $package_get_query = $this->query("SELECT id,image,for_sale,title,description,pricing,include_taxes,main_header FROM package_list WHERE for_sale = '1' AND package_type='live' AND base = '0' AND user_group='$student_group'");
        }else{
            $query = "SELECT id,image,for_sale,title,description,pricing,include_taxes,main_header FROM package_list WHERE for_sale = '1' AND package_type='live' AND base = '0' AND user_group='$student_group' AND ";
            $exclude_list = explode(',', $exclude);
            foreach ($exclude_list as $exclude_id){
                $upgrade_list = $this->get_upgrade_packages($exclude_id);
                $downgrade_list = $this->get_downgrade_packages($exclude_id);
                foreach($downgrade_list as $list){
                    $query .= "id != '$list[id]' AND ";
                }
                foreach($upgrade_list as $list){
                    $query .= "id != '$list[id]' AND ";
                }
            }
            $query = trim($query," AND ");
            $package_get_query = $this->query($query);
        }
        while($package = mysqli_fetch_array($package_get_query)){
            array_push($returnarr, $package);
        }
        return $returnarr;
    }
    
    function get_demo_packages($group){
        $returnarr = array();
        if($group = 'all'){
            $package_get_query = $this->query("SELECT id,image,title,description FROM package_list WHERE package_type='demo'");
        }else{
            $package_get_query = $this->query("SELECT id,image,title,description FROM package_list WHERE package_type='demo' AND user_group='$group'");
        }
        
        while($package = mysqli_fetch_array($package_get_query)){
            array_push($returnarr, $package);
        }
        return $returnarr;
    }
    function get_base($package){
        do{
            $get_lower_package_query = $this->query("SELECT id,title,description,base,upgrade_price FROM package_list WHERE id = '$package' AND base != '0'");
            if (mysqli_num_rows($get_lower_package_query) == 0){
                break;
            }
            $array = mysqli_fetch_array($get_lower_package_query);
            $package = $array['base'];
        }while(1);
        return $package;
    }
    function get_tests_attended($package,$user){
        $count = $this->query("SELECT DISTINCT test FROM answer_list WHERE package = '$package' AND user = '$user'");
        return mysqli_num_rows($count);
    }
    function get_all_packages($instid){
        $group_select = $this->query("SELECT * FROM student_group WHERE institute = '$instid'");
        $retarr = array();
        while($grp = mysqli_fetch_array($group_select)){
            $pack_select = $this->query("SELECT * FROM package_list WHERE user_group = '$grp[id]'");
            array_push($retarr, mysqli_fetch_array($pack_select));
        }
        return $retarr;
    }
}