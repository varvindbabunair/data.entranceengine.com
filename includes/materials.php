<?php
include_once dirname(__FILE__).'/questions.php';
class Materials extends Questions{
    public function __construct() {
        parent::__construct();
    }
    
    function get_note_details($id){
        $note_get_query = $this->query("SELECT * FROM notes_list WHERE id = '$id'");
        $note = mysqli_fetch_array($note_get_query);
        return $note;
    }
    
    function get_video_details($id){
        $video_get_query = $this->query("SELECT * FROM video_list WHERE id = '$id'");
        $video = mysqli_fetch_array($video_get_query);
        return $video;
    }
    
    function get_test_details($id){
        $note_get_query = $this->query("SELECT * FROM test_list WHERE id = '$id'");
        $note = mysqli_fetch_array($note_get_query);
        return $note;
    }
    
    function get_notes($chapterid){
        $note_get_query = $this->query("SELECT * FROM notes_list WHERE chapter = '$chapterid'");
        $note_array = array();
        while ($note = mysqli_fetch_array($note_get_query)){
            array_push($note_array, $note);
        }
        return $note_array;
    }
    
    function get_notes_count($subjectid){
        $chapter_list = $this->get_chapter_list($subjectid);
        $qry = "SELECT id FROM notes_list WHERE";
        foreach ($chapter_list as $chapter){
            $qry .= " chapter = ".$chapter['id']." OR";
        }
        $qry = trim($qry,"OR");
        $result = parent::query($qry);
        return mysqli_num_rows($result);
    }
    
    function get_videos($chapterid){
        $video_get_query = $this->query("SELECT * FROM video_list WHERE chapter = '$chapterid'");
        $video_array = array();
        if(mysqli_num_rows($video_get_query) != 0){
            while ($video = mysqli_fetch_array($video_get_query)){
                array_push($video_array, $video);
            }
            return $video_array;
        }
        return $video_array;
    }
    
    function get_video_count($subjectid){
        $chapter_list = parent::get_chapter_list($subjectid);
        $qry = "SELECT id FROM video_list WHERE";
        foreach ($chapter_list as $chapter){
            $qry .= " chapter = ".$chapter['id']." OR";
        }
        $qry = trim($qry,"OR");
        $result = parent::query($qry);
        return mysqli_num_rows($result);
    }

    function get_test_count(){
        $count_get_query = parent::query("SELECT COUNT(id) as count FROM test_list WHERE 1");
        $count = mysqli_fetch_array($count_get_query);
        return $count['count'];
    }
    
    function get_test_settings($par){
        $parameter_get_query = parent::query("SELECT value FROM test_general_settings WHERE name = '$par'");
        $param = mysqli_fetch_array($parameter_get_query);
        return stripslashes($param['value']);
    }
    
    function get_test_list($par=null){
        $test_list_query = '';
        $test_arr= array();
        switch ($par) {
            case 'all':
                $test_list_qry = "SELECT id,title,tags FROM test_list WHERE 1 ORDER BY id DESC";
                break;
            case (is_array(explode(',', $par))):
                $chapterid = explode(',', $par);
                $qry = "SELECT id FROM question_list WHERE";
                foreach ($chapterid as $chapter) {
                    $qry .= " chapter = '$chapter' OR ";
                }
                $qry = trim($qry,' OR ');
                $test_list_qry = "SELECT id,title,tags FROM test_list WHERE";
                $question_get_query = $this->query($qry);
                while($qn = mysqli_fetch_array($question_get_query)){
                    $test_list_qry .= " questions LIKE '%[\"".$qn['id']."\"%' OR ";
                }
                $test_list_qry = trim($test_list_qry,' OR ');
                break;
            case null :
                $test_list_qry = "SELECT id,title,tags FROM test_list WHERE 1";
        }
        $test_list_query = $this->query($test_list_qry) or die(mysqli_error($this->db_link)) ;
        while($test_list = mysqli_fetch_array($test_list_query)){
            array_push($test_arr, $test_list);
        }
        return $test_arr;
    }
    
    function get_chapters_excluding($subjectid,$exclude) {
        $qry = "SELECT id FROM chapter_list WHERE subject = '$subjectid' AND ";
        $exclude_list = explode(',', $exclude);
        foreach ($exclude_list as $id){
            $qry .= "id != '$id' AND ";
        }
        $qry = trim($qry,"AND ");
        $get_qry = $this->query($qry);
        $retarr = array();
        while($idarr = mysqli_fetch_array($get_qry)){
            array_push($retarr, $idarr);
        }
        return $retarr;
    }
    
    function get_notes_excluding($chapter,$exclude) {
        $qry = "SELECT id FROM notes_list WHERE chapter = '$chapter' AND ";
        $exclude_list = explode(',', $exclude);
        foreach ($exclude_list as $id){
            $qry .= "id != '$id' AND ";
        }
        $qry = trim($qry,"AND ");
        $get_qry = $this->query($qry);
        $retarr = array();
        while($idarr = mysqli_fetch_array($get_qry)){
            array_push($retarr, $idarr);
        }
        return $retarr;
    }
    
    function get_videos_excluding($chapter,$exclude) {
        $qry = "SELECT id FROM video_list WHERE chapter = '$chapter' AND ";
        $exclude_list = explode(',', $exclude);
        foreach ($exclude_list as $id){
            $qry .= "id != '$id' AND ";
        }
        $qry = trim($qry,"AND ");
        $get_qry = $this->query($qry);
        $retarr = array();
        while($idarr = mysqli_fetch_array($get_qry)){
            array_push($retarr, $idarr);
        }
        return $retarr;
    }
    
    function get_tests_excluding($exclude){
        $qry = "SELECT * FROM test_list WHERE ";
        $exclude_list = explode(',', $exclude);
        foreach ($exclude_list as $id){
            $qry .= "id != '$id' AND ";
        }
        $qry = trim($qry,"AND ");
        $get_qry = $this->query($qry);
        $retarr = array();
        while($idarr = mysqli_fetch_array($get_qry)){
            array_push($retarr, $idarr);
        }
        return $retarr;
    }
    
    function get_subjects_excluding($exclude){
        $qry = "SELECT * FROM subject_list WHERE ";
        $exclude_list = explode(',', $exclude);
        foreach ($exclude_list as $id){
            $qry .= "id != '$id' AND ";
        }
        $qry = trim($qry,"AND ");
        $get_qry = $this->query($qry);
        $retarr = array();
        while($idarr = mysqli_fetch_array($get_qry)){
            array_push($retarr, $idarr);
        }
        return $retarr;
    }
    
    function get_section_name($id){
        $qn_get_query = $this->query("SELECT * FROM section_list WHERE id = '$id'");
        $section = mysqli_fetch_array($qn_get_query);
        return $section['title'];
    }
}