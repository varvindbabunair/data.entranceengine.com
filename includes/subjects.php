<?php
include_once dirname(__FILE__).'/class.php';
class Subjects extends Classclass{
    public function __construct() {
        parent::__construct();
    }
    function get_subject_list($param){
        $where_clause = ($param == 'all')?"WHERE 1":"WHERE class = '".$param."'";
        $subject_list = array();
        $subject_get_query = $this->query("SELECT subject_list.*,class_list.title AS class_name FROM subject_list "
                . "INNER JOIN class_list ON subject_list.class = class_list.id ".$where_clause);
        if(mysqli_num_rows($subject_get_query) == 0){
            return null;
        }
        while($subject = mysqli_fetch_array($subject_get_query)){
            if(!isset($subject_list[$subject['class']])){
                $subject_list[$subject['class']] = array();
            }
            array_push($subject_list[$subject['class']], $subject);
        }
        return $subject_list;
    }
    function get_chapter_list($subjectid){
        $chapter_list = array();
        $chapter_get_query = $this->query("SELECT id,title FROM chapter_list WHERE subject = '$subjectid'");
        if(mysqli_num_rows($chapter_get_query) == 0){
            return NULL;
        }
        while ($chapter = mysqli_fetch_array($chapter_get_query)){
            array_push($chapter_list, $chapter);
        }
        return $chapter_list;
    }
    
    function get_chapter_name($id){
        $chapter_get_query = $this->query("SELECT title FROM chapter_list WHERE id = '$id'");
        $chapter = mysqli_fetch_array($chapter_get_query);
        return $chapter['title'];
    }
    
    function get_subject_name($chapter_id){
        $chapter_get_query = $this->query("SELECT subject FROM chapter_list WHERE id = '$chapter_id'");
        $chapter = mysqli_fetch_array($chapter_get_query);
        $subject_get_query = $this->query("SELECT title FROM subject_list WHERE id = '$chapter[subject]'");
        $subject = mysqli_fetch_array($subject_get_query);
        return $subject['title'];
    }
}