<?php

/* 
 * defines the properties of user
 */

class User extends Db{
    public $user_details;
    public $is_loggedin;
            
    function __construct() {
        parent::__construct();
        if(isset($_COOKIE['gr-app-user']) && isset($_COOKIE['gr-app-user-role']) && trim($_COOKIE['gr-app-user-role']) != '' && trim($_COOKIE['gr-app-user']) != '' ){
            $cookie = trim($_COOKIE['gr-app-user']);
            $role = trim($_COOKIE['gr-app-user-role']);
            $user_query = $this->query("SELECT id FROM ".$role."_list WHERE cookie = '$cookie'");
            if(mysqli_num_rows($user_query) != 0){
                $user_det = mysqli_fetch_array($user_query);
                $newKey = md5($user_det['id'].time());
                $this->query("UPDATE ".$role."_list SET cookie = '$newKey', last_login = NOW() WHERE id = '$user_det[id]'");
                setcookie('gr-app-user',$newKey, time() + (86400*30),"/");
                $det_query = $this->query("SELECT * FROM ".$role."_list WHERE id = '$user_det[id]'");
                $this->user_details = mysqli_fetch_array($det_query);
                $this->user_details['userrole'] = $role;
                $this->is_loggedin = TRUE;
            }else{
                $this->is_loggedin = FALSE;
            }
        }
    }
    
    
    function user_role(){
        if($this->user_details['role'] == 'admin'){
            return "admin";
        }else{
            return explode(",", $this->user_details['role']);
        }
    }
    
    function login($email,$password){
        if(isset($_REQUEST['instid']) && $_REQUEST['instid'] != ''){
            if(isset($_REQUEST['usertype']) && $_REQUEST['usertype'] == 'student'){
                $user_query = $this->query("SELECT * FROM student_list WHERE regn_number = '$email' AND password = '$password' AND institute = '$_REQUEST[instid]'");
                $role = 'student';
            }else if(isset($_REQUEST['usertype']) && $_REQUEST['usertype'] == 'faculty'){
                $user_query = $this->query("SELECT * FROM faculty_list WHERE email = '$email' AND password = '$password' AND institute = '$_REQUEST[instid]'");
                $role = 'faculty';
            }else{
                $user_query = $this->query("SELECT * FROM student_list WHERE regn_number = '$email' AND password = '$password' AND institute = '$_REQUEST[instid]'");
                $role = 'student';
                if(mysqli_num_rows($user_query) == 0){
                    $user_query = $this->query("SELECT * FROM faculty_list WHERE email = '$email' AND password = '$password' AND institute = '$_REQUEST[instid]'");
                    $role = 'faculty';
                }
            }
            
        }else{
            $user_query = $this->query("SELECT * FROM admin_list WHERE email = '$email' AND password = '$password'");
            $role = 'admin';
        }
        if(mysqli_num_rows($user_query) != 0){
            $user_det = mysqli_fetch_array($user_query);
            $newKey = md5($user_det['id'].time());
            $this->query("UPDATE ".$role."_list SET cookie = '$newKey', last_login = NOW() WHERE id = '$user_det[id]'");
            setcookie('gr-app-user',$newKey, time() + (86400 * 30),"/");
            setcookie('gr-app-user-role',$role, time() + (86400 * 30),"/");
            $det_query = parent::query("SELECT * FROM ".$role."_list WHERE id = '$user_det[id]'");
            $this->user_details = mysqli_fetch_array($det_query);
            $this->user_details['userrole'] = $role;
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function get_name($id){
        if($id == $this->user_details['id']){
            return $this->user_details['name'];
        }elseif($id !=0){
            $user_query = $this->query("SELECT name FROM admin_list WHERE id = '$id'");
            $user = mysqli_fetch_array($user_query);
            return $user['name'];
        }else{
            return NULL;
        }
    }
    
    function get_users($role){
        if(isset($this->user_details['institute'])){
            $instid = $this->user_details['institute'];
            $user_get_query = $this->query("SELECT * FROM ".$role."_list WHERE institute = '$instid'");
        }else{
            $user_get_query = $this->query("SELECT * FROM ".$role."_list WHERE 1");
        }
        
        $user_list = array();
        while($user = mysqli_fetch_array($user_get_query)){
            array_push($user_list, $user);
        }
        return $user_list;
    }
    
    function get_usercount($role){
        $user_count = $this::query("SELECT COUNT('id') as count FROM ".$role."_list WHERE 1");
        $count = mysqli_fetch_array($user_count);
        return $count['count'];
    }
    
    function get_institute(){
        if($this->user_details['role'] == 'institute'){
            return $this->user_details['name'];
        }else{
            $instid = $this->user_details['institute'];
            $institute_get_query = $this->query("SELECT name FROM institute_list WHERE id = '$instid'");
            $institute = mysqli_fetch_array($institute_get_query);
            return $institute['name'];
        }
    }
}