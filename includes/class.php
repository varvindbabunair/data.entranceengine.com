<?php
include_once '../config.php';
class Classclass extends Db{
    public function __construct() {
        parent::__construct();
    }
    
    public function all_class() {
        $query = $this->query("SELECT * FROM class_list");
        $ret_arr = array();
        while($class = mysqli_fetch_array($query)){
            array_push($ret_arr, $class);
        }
        return $ret_arr;
    }
    public function class_details($id){
        $query = $this->query("SELECT * FROM class_list WHERE id = '$id'");
        return mysqli_fetch_array($query);
    }
    
    public function class_add($title,$code){
        if($this->query("INSERT INTO class_list (title,code) VALUES ('$title','$code')")){
            return TRUE;
        }else{
            return FALSE;
        }        
    }
    
    public function class_update($id,$title){
        if($this->query("UPDATE class_list SET title = '$title' WHERE id = '$id'")){
            return TRUE;
        }else{
            return false;
        }
    }
    
}