<?php

include_once dirname(__FILE__).'/subjects.php';

class Questions extends Subjects{
    public function __construct() {
        parent::__construct();
    }
    
    function get_questions_inchapter($chapterids,$tags,$exclude){
        $qry = "SELECT * FROM question_list WHERE ";
        if($chapterids == 'all'){
            $qry .= "0";
        }else{
            $chapters = explode(",", $chapterids);
            $qry .= "( ";
            foreach($chapters as $chapter){
                $qry .= "chapter = ". trim($chapter)." OR ";
            }
            $qry = trim($qry, "OR ");
            $qry .= " ) ";
        }
        
        if($tags != 'all'){
            $tags = explode(",", $tags);
            
            $qry .= " AND (";
            
            foreach ($tags as $tag){
                $qry .= " tags LIKE '%". trim($tag)."%' OR";
            }
            $qry = trim($qry, "OR");
            $qry .= " )";
        }
        
        if($exclude !=""){
            $excludes = explode(",", $exclude);
            
            $qry .= " AND (";
            
            foreach ($excludes as $exclude){
                $qry .= " id != '". trim($exclude)."' AND";
            }
            $qry = trim($qry, "AND");
            $qry .= " )";
        }
        
        
        $question_get_query = parent::query($qry) or die(mysqli_error($this->db_link));
        $returnsrting = "";
        while ($question = mysqli_fetch_array($question_get_query)){
            $returnsrting .= '<li style="overflow:hidden" id="qn_'.$question['id'].'" class="ui-state-default">'
                    . '<div class="col-lg-1 order" style="display:none;"></div>';
            $qn = $question['question_type'] == 'image'? '<img style="width:100%" src="images/questions/'.$question['question'].'" />' : $question['question'];
            
            $returnsrting .= '<div class="col-lg-8">'.$qn.'</div>'
                    . '<div  style="display:none;" class="col-lg-3 pull-right mark">'
                    . '<div class="input-group input-group-sm">
                        <input id="qn_'.$question['id'].'_plus" val="" type="number" minval="0" style="width:50%" class="form-control" placeholder="+" title="Marks for correct Answer other than global" />
                        <input id="qn_'.$question['id'].'_minus" val="" type="number" minval="0" style="width:50%" class="form-control" placeholder="-" title="Negative Marks for wrong Answer other than global" />
                      </div>'
                    . '</div>'
                    . '</li>';
        }
        
        return $returnsrting;
    }
    
    function get_all_questions($par){
        $questions = array();
        $whereclause = ($par == 'all')? 'WHERE 1': 'validated_by = 0';
        $question_get_query = parent::query("SELECT * FROM question_list");
        while($question = mysqli_fetch_array($question_get_query)){
            array_push($questions, $question);
        }
        return $questions;
    }
            
    function get_numberof_questions($par=null){
        if($par == null || $par == 'all'){
            $number_query = $this->query("SELECT COUNT(id) as count FROM question_list");
        }elseif ($par == 'unallocated') {
            $number_query = $this->query("SELECT COUNT(id) as count FROM question_list WHERE chapter = '0'");
        }
        $number = mysqli_fetch_array($number_query);
        return $number['count'];
    }
    
    function get_tag_list(){
        $tags="";
        $tag_get_query = parent::query("SELECT DISTINCT tags FROM question_list");
        while($tag = mysqli_fetch_array($tag_get_query)){
            $tags .= ",".$tag['tags'];
        }
        $tag_arr = explode(",",trim($tags,","));
        return array_filter(array_unique($tag_arr));
    }
    
    function get_tagof($id){
        $tag_get_query = parent::query("SELECT tags FROM question_list WHERE id = '$id'");
        $tag = mysqli_fetch_array($tag_get_query);
        return explode(",", $tag);
    }
    
    function get_noof_test($id){
        $num_get_query = parent::query("SELECT COUNT(questions) as count FROM test_list WHERE questions LIKE '%[\"$id%'");
        $num_get = mysqli_fetch_array($num_get_query);
        return $num_get['count'];
    }
    
    function get_noof_package($qnid){
        $num_get_query = parent::query("SELECT id FROM test_list WHERE questions LIKE '%[\"$qnid\"%'");
        $subject_like_query = "";
        while($testid = mysqli_fetch_array($num_get_query)){
            $subject_like_query .= " subject_list LIKE '%[\"test\",\"".$testid['id']."\"]%' OR";
            $subject_like_query .= " chapter_list LIKE '%[\"test\",\"".$testid['id']."\"]%' OR";
            $subject_like_query .= " material_list LIKE '%[\"test\",\"".$testid['id']."\"]%' OR";
        }
        $subject_like_query = trim($subject_like_query, " OR");
        $cnt_get_query = "SELECT COUNT(id) as count FROM package_list WHERE ".$subject_like_query;
        if(mysqli_num_rows($num_get_query) == 0){
            return 0;
        }else{
            $query = parent::query($cnt_get_query);
            $num_get = mysqli_fetch_array($query);
            return $num_get['count'];
        }
    }
    
    function get_question($id){
        $qn_get_query = $this->query("SELECT * FROM question_list WHERE id = '$id'");
        return mysqli_fetch_array($qn_get_query);
    }
    
    function get_section_name($id){
        if($id == 0){
            return "Unspecified";
        }else{
            $qn_get_query = $this->query("SELECT * FROM section_list WHERE id = '$id'");
            $section = mysqli_fetch_array($qn_get_query);
            return $section['name'];
        }
        
    }
    function get_topic_name($id){
        if($id == 0){
            return 'Unspecified';
        }else{
            $qn_get_query = $this->query("SELECT * FROM topic_list WHERE id = '$id'");
            $section = mysqli_fetch_array($qn_get_query);
            return $section['title'];
        }
    }
    function get_subject_name($id){
        if($id == 0){
            return 'Unspecified';
        }else{
            $qn_get_query = $this->query("SELECT * FROM subject_list WHERE id = '$id'");
            $section = mysqli_fetch_array($qn_get_query);
            return $section['title'];
        }
    }
}