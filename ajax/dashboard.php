<?php
include '../config.php';
$db = new Db();

if (isset($_POST['packageid']) && trim($_POST['packageid']) != '') {
	if(isset($_POST['action']) && trim($_POST['action']) != ''){
		if($_POST['action'] == 'view' || $_POST['action'] == 'upgrade'){
			$package_query = $db->query("SELECT id,image,title,description,features,pricing,upgrade_price FROM package_list WHERE id = '$_POST[packageid]' AND user_group = '$_POST[viewGroup]'");
			$package = mysqli_fetch_array($package_query);

			?>

			<div class="col-lg-4">
				<img style="width: 100%;"  src="images/packages/<?php echo $package['image']; ?>" />
			</div>
			<div class="col-lg-8">
				<h3><strong><?php echo $package['title']; ?></strong></h3>
				<p>
				<?php echo $package['description'];  ?>
				</p>
				
				<div style="width: 100%;overflow: hidden;margin-top: 30px;">
					<button class="btn btn-warning"><strong><span class="fa fa-inr"></span> 
                                            <?php echo ($_POST['action'] != 'upgrade')?$package['pricing']:$package['upgrade_price']; ?></strong>
                                        </button>
				</div>
			</div>
			<div class="col-lg-12">
				<ul class="nav nav-tabs">
				    <li class="active"><a href="#">Features</a></li>
				</ul>
				<div style="width: 100%;overflow: hidden;padding-top: 20px;border:1px solid #ccc;margin-top: -1px;">
				<?php echo $package['features']; ?>
				</div>
			</div>


			<?php
		}
	}else{
		echo '<p style="text-align:center;"><span class="fa fa-exclamation-triangle"></span> Oops some error occured...</p>';
	}
}
?>