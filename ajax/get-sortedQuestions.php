<?php
session_start();
include '../config.php';
$db = new Db();
$topicId = $_POST['class'];
$chapter_id = $_SESSION['chapter_id'];
$chapterID = $_POST['chapter_var'];

if (isset($topicId)) {
    $question_list = $db->query("SELECT question_list.id,question_list.reference,question_list.question_type, question_list.question,question_list.tags, class_list.title as class_name, subject_list.title as subject_name, chapter_list.title as chapter_name, admin_list.name as added_user
    FROM question_list 
    INNER JOIN class_list ON question_list.class = class_list.id 
    INNER JOIN subject_list ON question_list.subject = subject_list.id 
    INNER JOIN chapter_list ON question_list.chapter = chapter_list.id 
    INNER JOIN admin_list ON question_list.added_by = admin_list.id 
    WHERE topic = $topicId
    AND chapter = $chapter_id");
} else {
    $question_list = $db->query("SELECT question_list.id,question_list.reference,question_list.question_type, question_list.question,question_list.tags, class_list.title as class_name, subject_list.title as subject_name, chapter_list.title as chapter_name, admin_list.name as added_user
    FROM question_list 
    INNER JOIN class_list ON question_list.class = class_list.id 
    INNER JOIN subject_list ON question_list.subject = subject_list.id 
    INNER JOIN chapter_list ON question_list.chapter = chapter_list.id 
    INNER JOIN admin_list ON question_list.added_by = admin_list.id 
    WHERE chapter = $chapter_id");
}

while ($questions = mysqli_fetch_array($question_list)) {
    echo "<tr>";
    echo "<td>" .$questions['id'] . "</td>";
    echo "<td>" .$questions['reference'] . "</td>";
    echo "<td>" .$questions['question'] . "</td>";
    echo "<td>" .$questions['added_user'] . "</td>";
    echo '<td> <a href="?page=edit-question&id=' . $questions['id'] . '"><button class="btn btn-primary btn-xs" ><span  class="fa fa-edit"></span> Edit</button></a>';
    echo '<button class="btn btn-xs btn-primary qn-viewSorted" qnid="' .$questions['id'] .'" data-toggle="modal" data-target="#qnviewSortedModal" ><span class="fa fa-eye"></span> View</button>';
    echo "</tr>";
}
// session_unset();
?>