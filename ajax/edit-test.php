<?php

include '../config.php';
include '../includes/materials.php';

$tests = new Materials();

if(isset($_POST['tag']) && isset($_POST['subject'])){
    $tags = $_POST['tag'];
    if($tags !=""){
        $tags = implode(",", $tags);
    }else{
        $tags = "all";
    }
    $subjects = $_POST['subject'];
    if($subjects !=""){
        $subjects = implode(",", $subjects);
    }else{
        $subjects = "all";
    }
    if(isset($_POST['exclude'])){
        $exclude = $_POST['exclude'];
        $exclude_arr = explode("&", $exclude);
        $exclude_val = "";
        foreach ($exclude_arr as $exclude){
            $exclude_val .= trim($exclude,"qn[]=").",";
        }
        trim($exclude_val,",");
    } else {
        $exclude_val = "";
    }
    
    echo $tests->get_questions_inchapter($subjects,$tags,$exclude_val);
}

if(isset($_POST['editid']) && $_POST['editid'] != '' ){
    $testname = addslashes($_POST['testname']);
    $sectionrule = $_POST['sectionrule'];
    $sectiontitle = addslashes($_POST['sectiontitle']);
    $questions = addslashes(str_replace('\\', '', $_POST['questions']));
    $questions = str_replace('\"[', '[', $questions);
    $questions = str_replace(']\"', ']', $questions);
    $questions = str_replace('"[', '[', $questions);
    $questions = str_replace(']"', ']', $questions);
    $instructions = addslashes($_POST['instruction']);
    $tags = addslashes($_POST['tags']);
    $id = $_POST['editid'];
    $group = $_POST['group'];
    
    $test_insert_query = $tests->query("UPDATE test_list SET questions = '$questions',section_title = '$sectiontitle',section_rule = '$sectionrule',instruction = '$instructions',title = '$testname',tags = '$tags',group = '$group' WHERE id = '$id'") or die(mysqli_error($tests->db_link));
    
    echo 'Successfully saved changes';
//    echo $questions;
    
}