<?php
include '../config.php';
include '../includes/questions.php';
include '../includes/user.php';

$question = new Questions();
$user = new User();

if(!isset($_POST['qnid'])){
    die('<h4 style="text-align:center;">Some Error Occured</h4>');
}

$id = $_POST['qnid'];
$qn = $question->query("SELECT * FROM theory_list WHERE id = $id");
$question_info = mysqli_fetch_array($qn);
$code = '';

?>
<div id="question-meta">
    <table class="table table-bordered">
        <tr><th>ID:</th><td><?= $question_info['id'] ?></td><th>Ref No:</th><td><?= $question_info['reference'] ?></td><th>Subject:</th><td><?= $question->get_subject_name($question_info['subject']) ?></td></tr>
        <tr><th>Chapter:</th><td colspan="5"><?= $question->get_chapter_name($question_info['chapter']) ?></td></tr>
        
        <tr><th>Author:</th><td colspan="2"><?= $user->get_name($question_info['added_by']); ?></td><th>Validated by:</th><td colspan="2"><?= $user->get_name($question_info['validated_by']); ?></td></tr>
        
    </table>
</div>
<div id="question">
    <h4 class="modal-title"><u>Theory :</u></h4>
	<?php
	echo $question_info['theory'];


 ?></div>
<div class="clearfix"></div>
