<?php
include '../config.php';
$db = new Db();
if(!isset($_POST['class'])){
    die('error..');
}
$class = $_POST['class'];
$subject_select_query = $db->query("SELECT * FROM subject_list WHERE class = '$class'");
echo '<h4 style="border-bottom: 2px solid #ccc;padding:10px;margin-bottom:0px;">Subjects</h4>
<ul style="list-style-type:none;padding:0px;margin:0px;height:250px;overflow-y:scroll;">';
while($subjects = mysqli_fetch_array($subject_select_query)){
?>
<li class="subject-list" subject_var="<?= $subjects['id'] ?>" ><img style="width: 20px;" src="images/subjects/<?= $subjects['image'] ?>" /> <?= $subjects['title'] ?><span class="fa fa-caret-right pull-right" style="margin-top: 5px;"></span></li>
<?php
}
?>
    <li style="padding: 10px;" >
        <form enctype="multipart/form-data" class="subject-add-form">
            <h5 style="border-bottom: 1px solid #ccc;"><span class="fa fa-plus"></span> <strong>Add Subject</strong></h5>
            Choose Icon :
            <input class="form-control" name="subject-icon" type="file" />
            <input type="hidden" name="class" value="<?= $_POST['class'] ?>" />
            Subject Name :
            <input type="text" class="form-control" name="subject-name" />
            <br>
            <button class="btn btn-primary form-control btn-sm add-subject"><span class="fa fa-plus"></span> Add Subject</button>
        </form>
    </li>
</ul>