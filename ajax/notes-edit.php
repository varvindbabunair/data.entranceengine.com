<?php
include '../config.php';
include '../includes/materials.php';

$notes = new Materials();
$editid = (isset($_POST['id']))?$_POST['id']:'';
if($editid == ''){
    die('Some Error Occured');
}
$edit_note = $notes->get_note_details($editid);

?>

Select Chapter :
<select name="chapter" class="form-control">
    <option>Select from Chapter List</option>
  <?php
  $subject_list = $notes->get_subject_list();
  foreach($subject_list as $subjects){
      echo '<optgroup label="'.$subjects['title'].'">';
      $chapter_list = $notes->get_chapter_list($subjects['id']);
      foreach($chapter_list as $chapter){
          echo '<option value="'.$chapter['id'].'"  ';
          echo ($chapter['id'] == $edit_note['chapter'])?'selected':'';
          echo ' >'.$chapter['title'].'</option>';
      }
      echo '</optgroup>';
  }
  ?>
</select>



Notes Title:
<input name="title" type="text" class="form-control" value="<?php echo $edit_note['title']; ?>" />
<input name="edit-id" type="hidden" class="form-control" value="<?php echo $editid; ?>" />
Tags :
<input name="tags" class="form-control tags" value="<?php echo $edit_note['tags']; ?>" />
<script>
    $('.tags').tagsinput();
  </script>

Notes :
<textarea class="form-control" name="editor-edit"><?php echo $edit_note['note']; ?></textarea>