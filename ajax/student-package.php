<?php
include '../includes/packages.php';
$packages = new Packages();
$exclude = NULL;
?>
<div style="width:100%;">
    
    <h3>Subscribed Packages</h3>
    <hr>
    <?php
    $subscribed_packages = $packages->subscribed_packages($_POST['student']);
    if($subscribed_packages){
        echo '<div style="width:100%;overflow:hidden;">';
        foreach($subscribed_packages as $package){
        ?>
        <div class="col-lg-12">
            <div class="col-lg-2">
                <img src="images/packages/default.png" style="width:100%" />
            </div>
            <div class="col-lg-8">
                <h4 style="font-weight:bold;"><?php echo $package['title']; ?></h4>
                <p><?php echo $package['description']; ?></p>
                <p><strong>Days Left: <?php echo $packages->get_days_left($package['id'],$_POST['student']); ?></strong></p>
            </div>
            
        </div>
        <?php
        $exclude .= $package['id'].',';
        $upgrade_packages = $packages->get_upgrade_packages($package['id']);
            if(sizeof($upgrade_packages) != 0){
                ?>
    <div class="col-lg-12">
        <h4 style="font-weight:bold;"><u>Upgrade Options</u></h4>
        <?php
                        foreach ($upgrade_packages as $up_option){
                            ?>
        <div class="col-lg-4">
            <h4 style="font-weight:bold;"><?php echo $up_option['title']; ?></h4>
            <div class="col-lg-3">
                <img src="images/packages/default.png" style="width:100%" />
            </div>
            <div class="col-lg-9">
                
                <p>
                    <?php echo substr($up_option['description'], 0,100); ?>
                    <?php echo (strlen($up_option['description'])>100)?'...':''; ?>
                </p>
                <div class="col-lg-12" style="text-align: center">
                    <form action="submit/student-users.php" method="POST">
                        <input type="hidden" name="package-id" value="<?php echo $up_option['id']; ?>" />
                        <input type="hidden" name="student-id" value="<?php echo $_POST['student']; ?>" />
                        <input type="hidden" name="upgrade-from" value="<?php echo $package['id']; ?>" />
                        <input type="hidden" name="purchase-type" value="upgrade" />
                        <button class="btn btn-xs btn-success package-view upgrade" upgradefrom="<?php echo $package['id']; ?>" viewid="<?php echo $up_option['id']; ?>" ><span class="fa fa-eye"></span> Upgrade Package</button>
                    </form>
                </div>
            </div>
        </div>
        <?php
                        }
        ?>
    </div>
    <?php
            }
        }
        echo '</div>';
    }else{
    ?>
    <strong>No packages subscribed.</strong>
    <?php
    }
    ?>
</div>

<div style="width:100%">
    <h3>Availiable Packages</h3>
    <hr>
    <?php
    $package_list = $packages->get_package_list(trim($exclude,','),$_POST['student']);
    foreach($package_list as $package){
    ?>
    <div  class="packages">
        <div class="col-lg-4">
            <img src="images/packages/default.png" style="width:100%" />
        </div>
        <div class="col-lg-8">
            <h4 id="package-title-<?php echo $package['id']; ?>" style="font-weight:bold;" ><?php echo $package['title']; ?></h4>
            <p class="description">
            <?php echo substr($package['description'], 0,120); ?>
            <?php echo (strlen($package['description'])>120)?'...':''; ?>
            </p>
        </div>
        <div class="col-lg-12" style="text-align: center">
            <form action="submit/student-users.php" method="POST">
                <input type="hidden" name="package-id" value="<?php echo $package['id']; ?>" />
                <input type="hidden" name="student-id" value="<?php echo $_POST['student']; ?>" />
                <input type="hidden" name="purchase-type" value="activate" />
            <button class="btn btn-xs btn-success package-view" type="submut" viewid="" ><span class="fa fa-eye"></span> Activate Package</button>
            </form>
        </div>
    </div>
    <?php
    }
    ?>
</div>
