<?php
include '../config.php';
$db = new Db();
if(isset($_POST['sections']) && $_POST['sections'] != ''){
    $str = '';
    $topic = $_POST['topic'];
    $title_arr = explode(',', $_POST['sections']);
    foreach($title_arr as $title){
        $str .= "('".addslashes(trim($title))."','".$topic."'), ";
    }
    $str = trim($str, ', ');
    
    $insert_query = $db->query("INSERT INTO section_list (name,topic) VALUES ".$str);
    
    if($insert_query){
        echo 'success';
    }else{
        echo mysqli_error($db->db_link);
    }
    
}