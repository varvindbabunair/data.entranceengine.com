<?php
include '../config.php';
$dir = "../media";

// Open a known directory, and proceed to read its contents
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        $images = array();

        while (($file = readdir($dh)) !== false) {
            if (!is_dir($dir.$file)) {
                $images[] = $file;
            }
        }

        closedir($dh);

        
        foreach($images as $image){
            echo '<div style="width:23%;margin:1%;float:left;overflow:hidden;"><img style="max-width:100%;max-height:100px;" src="media/'.$image.'" /></div>';
        }
    }
}