<?php
include '../config.php';
$db = new Db();

if(!isset($_POST['id']) || $_POST['id'] ==''){
    die('<h3 style="text-align:center;"><span class="fa fa-exclamation"></span> Some Error Occured</h3>');
}
$id = $_POST['id'];
$package_qry = $db->query("SELECT * FROM package_list WHERE id = '$id'");

$package = mysqli_fetch_array($package_qry);

?>
<div class="col-lg-12">
    <h3><?php echo $package['title']; ?></h3>
    <hr>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
    <img style="width: 100%" src="images/packages/<?php echo $package['image']; ?>" />
</div>
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
    <p><?php echo $package['description']; ?></p>
    
    <p>
        <button class="btn btn-lg btn-warning">Package Price: <span class="fa fa-inr"></span> <?php echo $package['pricing']; ?></button>
    </p>
        <?php
    if($package['base'] != '0'){
    ?>
    <p>
        <button class="btn btn-lg btn-warning">Upgrade Price: <span class="fa fa-inr"></span> <?php echo $package['upgrade_price']; ?></button>
    </p>
        <?php
    }
    if($package['include_taxes'] == '0'){
    ?>
    <p>Prices are excluding taxes</p>
    <?php
    }else{
    ?>
    <p>Prices are including taxes</p>
    <?php } ?>
</div>
<div class="col-lg-12">
    <h4>Features</h4>
    <hr>
</div>
<div class="col-lg-12">
    <?php 
    echo $package['features'];
    ?>
</div>