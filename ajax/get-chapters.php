<?php
include '../config.php';
$db = new Db();
$class = $_POST['class'];
	$subject_select_query = $db->query("SELECT * FROM chapter_list WHERE subject = '$class'");
	echo '<h4 style="border-bottom: 2px solid #ccc;padding:10px;margin-bottom:0px;">Chapters</h4>
<ul style="list-style-type:none;padding:0px;margin:0px;height:250px;overflow-y:scroll;">';
	while($subjects = mysqli_fetch_array($subject_select_query)){
	?>
		<li class="chapter-list" subject_var="<?= $subjects['id'] ?>" ><img style="width: 20px;" src="images/chapters/<?= $subjects['image'] ?>" /> <?= $subjects['title'] ?><span class="fa fa-caret-right pull-right" style="margin-top: 5px;"></span></li>
<?php
        }
?>
    <li style="padding: 10px;" >
        <form enctype="multipart/form-data" class="chapter-add-form">
            <h5 style="border-bottom: 1px solid #ccc;"><span class="fa fa-plus"></span> <strong>Add Chapter</strong></h5>
            Choose Icon :
            <input class="form-control" name="chapter-icon" type="file" />
            <input type="hidden" class="subject-select" name="subject" value="<?= $_POST['class'] ?>" />
            Chapter Name :
            <input type="text" class="form-control" name="chapter-name" />
            <br>
            <button class="btn btn-primary form-control btn-sm add-chapter"><span class="fa fa-plus"></span> Add Chapter</button>
        </form>
    </li>
	</ul>