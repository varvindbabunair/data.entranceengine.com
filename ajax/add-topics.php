<?php
include '../config.php';
$db = new Db();
if(isset($_POST['topics']) && $_POST['topics'] != ''){
    $str = '';
    $chapter = $_POST['chapter'];
    $title_arr = explode(',', $_POST['topics']);
    foreach($title_arr as $title){
        $str .= "('".addslashes(trim($title))."','".$chapter."'), ";
    }
    $str = trim($str, ', ');
    
    $insert_query = $db->query("INSERT INTO topic_list (title,chapter) VALUES ".$str);
    
    if($insert_query){
        echo 'success';
    }else{
        echo mysqli_error($db->db_link);
    }
    
}