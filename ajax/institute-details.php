<?php
include '../config.php';
$db = new Db();

if(!isset($_POST['id']) || $_POST['id'] == ''){
    die('<h3>Some Error Occured</h3>');
}
$id = $_POST['id'];

$admin_query = $db->query("SELECT * from institute_list WHERE id = '$id'");
$admin = mysqli_fetch_array($admin_query);
$users = $db->query("SELECT * FROM faculty_list WHERE institute = $id");
$group_fetch_query = $db->query("SELECT * FROM student_group WHERE institute = $id");
?>
<ul class="nav nav-tabs">
    <li class="inst-nav active" target=".basic-detail-view"><a href="#">Basic Details</a></li>
    <li class="inst-nav" target=".admin-detail-view"><a href="#">Faculty / Admins <span class="badge"><?= mysqli_num_rows($users) ?></span></a></li>
    <li class="inst-nav" target=".batch-detail-view"><a href="#">Student Batches <span class="badge"><?= mysqli_num_rows($group_fetch_query) ?></a></li>
</ul>
<div class="inst-nav-view basic-detail-view" style="overflow:hidden;border:1px solid #ccc;margin-top: -1px;">
    <div style="overflow: hidden;margin:10px;">
        <button class="btn btn-sm btn-primary pull-right">Edit Basic Details</button>
    </div>
    <div class="col-sm-4">
        <img class="thumbnail" style="max-width: 100%;" src="images/institutes/<?= $admin['logo'] ?>">
    </div>
    <div class="col-sm-8">
        <h3><?php echo $admin['name']; ?></h3>
        <p><span class="fa fa-envelope"></span> <?php echo $admin['email']; ?></p>
        <p><span class="fa fa-phone"></span> <?php echo $admin['phone']; ?></p>
        <p><span class="fa fa-map-marker"></span> <?php echo $admin['address']; ?></p>
    </div>
    <table class="table table-hover">
        <tr><th>Subscriptions</th><td>
            <?= ($admin['qn_subs'] == 1)?'Questions<br>':'' ?>
            <?= ($admin['syn_subs'] == 1)?'Synopsis<br>':'' ?>
            <?= ($admin['vid_subs'] == 1)?'Video<br>':'' ?>
            <?= ($admin['digi_subs'] == 1)?'Digital Material<br>':'' ?>
            </td></tr>
    </table>
</div>
<div class="inst-nav-view admin-detail-view" style="overflow:hidden;border:1px solid #ccc;margin-top: -1px;display: none;">
    <div style="overflow: hidden;margin:10px;">
        <button class="btn btn-sm btn-primary pull-right">Add Faculty</button>
    </div>
    <table class="table table-condensed table-hover">
            <thead><th>Name</th><th>Contact</th><th>Role</th><th>Options</th></thead>
        <?php
        
        foreach ($users as $individual){
            ?>
        <tr>
            <td><?php echo $individual['name']; ?></td>
            <td><span class="fa fa-envelope"></span> <?php echo $individual['email']; ?><br><span class="fa fa-phone"></span> <?php echo $individual['phone']; ?></td>
            <td>
                <?= ($individual['status'] == 'admin')?'Institute Administrator':'Teaching Faculty' ?>
            </td>
            <td>
                <button class="btn btn-xs btn-primary view-user" editid="<?php echo $individual['id']; ?>" data-toggle="modal" data-target="#userEditModal"><span class="fa fa-edit"></span> Edit</button>
                <button class="btn btn-xs btn-primary delete-user" removeid="<?php echo $individual['id']; ?>" data-toggle="modal" data-target="#userRemoveModal"><span class="fa fa-remove"></span> Delete</button>
                <button class="btn btn-xs btn-primary reset-user" resetid="<?php echo $individual['id']; ?>" data-toggle="modal" data-target="#userResetModal"><span class="fa fa-lock"></span> Reset Password</button>
            </td>
        </tr>
            <?php
        }
        ?>
        </table>
</div>

    <div class="inst-nav-view batch-detail-view" style="overflow:hidden;border:1px solid #ccc;margin-top: -1px;display: none;">
        <div style="overflow: hidden;margin:10px;">
        <button class="btn btn-sm btn-primary pull-right">Add Batches</button>
    </div>
        <table class="table table-condensed">
                <tr><th>#</th><th>Group Name</th><th>Options</th></tr>
            <?php
            
            $cnt = 1;
            while ($group = mysqli_fetch_array($group_fetch_query)){
                echo '<tr>';
                echo '<td>'.$cnt.'</td>';
                echo '<td>'.$group['name'].'</td>';
                echo '<td><button class="btn btn-xs btn-primary">Edit</button> <button class="btn btn-xs btn-primary">Delete</button></td>';
                echo '</tr>';
                $cnt++;
            }
            ?>
            </table>
    </div>

    <input type="hidden" value="<?php echo $admin['id']; ?>" name="id" />
