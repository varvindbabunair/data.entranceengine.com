<?php
include '../config.php';
$db = new Db();

$physics11 = $db->query("SELECT title, id FROM chapter_list WHERE subject = 1");
$physics12 = $db->query("SELECT title, id FROM chapter_list WHERE subject = 5");
$chemistry11 = $db->query("SELECT title, id FROM chapter_list WHERE subject = 2");
$chemistry12 = $db->query("SELECT title, id FROM chapter_list WHERE subject = 6");
$maths11 = $db->query("SELECT title, id FROM chapter_list WHERE subject = 4");
$maths12 = $db->query("SELECT title, id FROM chapter_list WHERE subject = 9");
$biology11 = $db->query("SELECT title, id FROM chapter_list WHERE subject = 3");
$biology12 = $db->query("SELECT title, id FROM chapter_list WHERE subject = 8");


$class = $_POST['class'];	
	if ($class == 1) {
		while($biology11Chap = mysqli_fetch_array($biology11)){
			
		$physics11Chap = mysqli_fetch_array($physics11);
		$chemistry11Chap = mysqli_fetch_array($chemistry11);
		// $biology11Chap = mysqli_fetch_array($biology11);
		$maths11Chap = mysqli_fetch_array($maths11);

		?>
		<tr>
			<td style="cursor:pointer" class="chap-list" data-toggle="modal" data-target="#chviewModal" chapter_id="<?= $physics11Chap['id'] ?>"><?= $physics11Chap['title'] ?>
			<?php
				$physics11ChapId = $physics11Chap['id'];
				$physics11ChapNumb = $db->query("SELECT * FROM question_list WHERE `chapter` = $physics11ChapId");
				$phy11Num = mysqli_num_rows($physics11ChapNumb);
				echo "<span class='badge badge-info pull-right'>".$phy11Num."</span>";
			?>
			</td>
			<td style="cursor:pointer" class="chap-list" data-toggle="modal" data-target="#chviewModal" chapter_id="<?= $chemistry11Chap['id'] ?>"><?= $chemistry11Chap['title'] ?>
			<?php
				$chemistry11ChapId = $chemistry11Chap['id'];
				$chemistry11ChapNumb = $db->query("SELECT * FROM question_list WHERE `chapter` = $chemistry11ChapId");
				$chem11Num = mysqli_num_rows($chemistry11ChapNumb);
				echo "<span class='badge pull-right'>".$chem11Num."</span>";
			?>
			</td>
			<td style="cursor:pointer" class="chap-list" data-toggle="modal" data-target="#chviewModal" chapter_id="<?= $biology11Chap['id'] ?>"><?= $biology11Chap['title'] ?>
			<?php
				$biology11ChapId = $biology11Chap['id'];
				$biology11ChapNumb = $db->query("SELECT * FROM question_list WHERE `chapter` = $biology11ChapId");
				$bio11Num = mysqli_num_rows($biology11ChapNumb);
				echo "<span class='badge pull-right'>".$bio11Num."</span>";
			?>
			</td>
			<td style="cursor:pointer" class="chap-list" data-toggle="modal" data-target="#chviewModal" chapter_id="<?= $maths11Chap['id'] ?>"><?= $maths11Chap['title'] ?>
			<?php
				$maths11ChapId = $maths11Chap['id'];
				$maths11ChapNumb = $db->query("SELECT * FROM question_list WHERE `chapter` = $maths11ChapId");
				$mat11Num = mysqli_num_rows($maths11ChapNumb);
				echo "<span class='badge pull-right'>".$mat11Num."</span>";
			?>
			</td>
		</tr>
	<?php
		}
	}elseif ($class == 2) {
		while($biology12Chap = mysqli_fetch_array($biology12)){
				
			$physics12Chap = mysqli_fetch_array($physics12);
			$chemistry12Chap = mysqli_fetch_array($chemistry12);
			// $biology12Chap = mysqli_fetch_array($biology12);
			$maths12Chap = mysqli_fetch_array($maths12);
		
			?>
			<tr>
			<td style="cursor:pointer" class="chap-list" data-toggle="modal" data-target="#chviewModal" chapter_id="<?= $physics12Chap['id'] ?>"><?= $physics12Chap['title'] ?>
			<?php
				$physics12ChapId = $physics12Chap['id'];
				$physics12ChapNumb = $db->query("SELECT * FROM question_list WHERE `chapter` = $physics12ChapId");
				$phy12Num = mysqli_num_rows($physics12ChapNumb);
				echo "<span id='count' class='badge pull-right' data-count='$phy12Num'>".$phy12Num."</span>";
			?>
			</td>
			<td style="cursor:pointer" class="chap-list" data-toggle="modal" data-target="#chviewModal" chapter_id="<?= $chemistry12Chap['id'] ?>"><?= $chemistry12Chap['title'] ?>
			<?php
				$chemistry12ChapId = $chemistry12Chap['id'];
				$chemistry12ChapNumb = $db->query("SELECT * FROM question_list WHERE `chapter` = $chemistry12ChapId");
				$chem12Num = mysqli_num_rows($chemistry12ChapNumb);
				echo "<span class='badge pull-right'>".$chem12Num."</span>";
			?>
			</td>
			<td style="cursor:pointer" class="chap-list" data-toggle="modal" data-target="#chviewModal" chapter_id="<?= $biology12Chap['id'] ?>"><?= $biology12Chap['title'] ?>
			<?php
				$biology12ChapId = $biology12Chap['id'];
				$biology12ChapNumb = $db->query("SELECT * FROM question_list WHERE `chapter` = $biology12ChapId");
				$bio12Num = mysqli_num_rows($biology12ChapNumb);
				echo "<span class='badge pull-right'>".$bio12Num."</span>";
			?>
			</td>
			<td style="cursor:pointer" class="chap-list" data-toggle="modal" data-target="#chviewModal" chapter_id="<?= $maths12Chap['id'] ?>"><?= $maths12Chap['title'] ?>
			<?php
				$maths12ChapId = $maths12Chap['id'];
				$maths12ChapNumb = $db->query("SELECT * FROM question_list WHERE `chapter` = $maths12ChapId");
				$mat12Num = mysqli_num_rows($maths12ChapNumb);
				echo "<span class='badge pull-right'>".$mat12Num."</span>";
			?>
			</td>
			</tr>
	<?php
		}
	}


	?>