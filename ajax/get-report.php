<?php

include '../config.php';
$db = new Db();
$days = $_POST['days'];
if($_POST['user'] == 'all'){
    $add_qry = $db->query("SELECT question_list.add_date, COUNT(question_list.id) as total_added FROM question_list WHERE question_list.add_date > DATE_SUB(CURDATE(), INTERVAL $days DAY)  GROUP BY question_list.add_date ORDER BY add_date ASC");
    $val_qry = $db->query("SELECT question_list.validate_date, COUNT(question_list.id) as total_validated FROM question_list WHERE question_list.add_date > DATE_SUB(CURDATE(), INTERVAL $days DAY) AND validated_by != 0 GROUP BY question_list.validate_date ORDER BY validate_date ASC");
                
}else{
    $user = $_POST['user'];
    $add_qry = $db->query("SELECT question_list.add_date, COUNT(question_list.id) as total_added FROM question_list WHERE question_list.add_date > DATE_SUB(CURDATE(), INTERVAL $days DAY) AND added_by = $user GROUP BY question_list.add_date ORDER BY add_date ASC");
    $val_qry = $db->query("SELECT question_list.validate_date, COUNT(question_list.id) as total_validated FROM question_list WHERE question_list.add_date > DATE_SUB(CURDATE(), INTERVAL $days DAY) AND validated_by = $user AND validated_by != 0 GROUP BY question_list.validate_date ORDER BY validate_date ASC");
     
}
                $data = array();
                while ($add = mysqli_fetch_array($add_qry)){
                    $data[$add['add_date']]['added'] = $add['total_added'];
                }
                while ($add = mysqli_fetch_array($val_qry)){
                    $data[$add['validate_date']]['validated'] = $add['total_validated'];
                }
                ?>
                <div id="myfirstchart" style="height: 250px;"></div>
                <script>
                new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'myfirstchart',
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.
                    
                    data: [
                        <?php
                        $str = "";
                foreach ($data as $key=>$value){
                    $str .= "{dates:'".date('d M Y', strtotime($key))."', entered: ";
                    $str .= (isset($data[$key]['added']))?$value['added']:0;
                    $str .= ", validated: ";
                    $str .= (isset($data[$key]['validated']))?$value['validated']:0;
                    $str .= "},";
                    
                }
                echo trim($str, ',');
                      ?>         
                    ],
                    
                    // The name of the data record attribute that contains x-values.
                    xkey: 'dates',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['entered','validated'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Questions Entered','Questions Validated']
                  });
                </script>