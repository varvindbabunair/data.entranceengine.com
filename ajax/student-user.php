<?php
if(!isset($_POST['limit']) || !isset($_POST['group']) || !isset($_POST['search'])){
    die('<h3 style="text-align:center;">Some Error Occured...</h3>');
}

$limit = $_POST['limit'];
$group = $_POST['group'];
$search = $_POST['search'];

include '../config.php';
include '../includes/packages.php';
include '../includes/user.php';
$db = new Db();
$user = new User();
$packages = new Packages();

$query = "SELECT * FROM student_list WHERE ";

if($group != 'all'){
    $query .= "student_group = '$group' AND ";
}

if(trim($search) != ''){
    $query .= "(username LIKE '%".$search."%' OR address LIKE '%".$search."%' OR city LIKE '%".$search."%' OR state LIKE '%".$search."%' OR phone LIKE '%".$search."%' OR email LIKE '%".$search."%')";
}

if($group == 'all' && trim($search) == '' ){
    $query .= "1 ";
}

$query = trim($query,"AND ");

$query .= " AND institute = '".$user->user_details['institute']."' LIMIT ".$limit."";
$student_get_query = $db->query($query) or die(mysqli_error($db->db_link));
?>

<table class="table table-condensed">
    <thead>
    <th>Id</th><th>Name</th><th>Username</th><th>Group</th><th>Contact</th><th>Last login</th><th>Options</th>
    </thead>
    <tbody>
        <?php while($student_details = mysqli_fetch_array($student_get_query)){ ?>
        <tr>
            <td><?php echo $student_details['id']; ?></td>
            <td><?php echo $student_details['name']; ?></td>
            <td><?php echo $student_details['username']; ?></td>
            <td><?php 
            $grp_qry = $db->query("SELECT * FROM student_group WHERE id = '".$student_details['student_group']."'");
            $grp = mysqli_fetch_array($grp_qry);
            echo $grp['name']; ?></td>
            <td><?php echo $student_details['address']; ?><br><?php echo $student_details['city']; ?>,<?php echo $student_details['state']; ?> - <?php echo $student_details['postal_code']; ?>
                <br>
                <span class="fa fa-phone"></span> <?php echo $student_details['phone']; ?><br>
                <span class="fa fa-envelope"></span> <?php echo $student_details['email']; ?><br>
            </td>
            <td><?php echo $student_details['last_login']; ?></td>
            
            <td>
                <button class="btn btn-primary btn-xs viewDetails" target-user="<?php echo $student_details['id']; ?>" ><span class="fa fa-eye"></span> View</button>
                <button class="btn btn-primary btn-xs editDetails" target-user="<?php echo $student_details['id']; ?>" ><span class="fa fa-edit"></span> Edit</button>
                <button class="btn btn-primary btn-xs removeStudent" target-user="<?php echo $student_details['id']; ?>" target-name="<?php echo $student_details['name']; ?>" ><span class="fa fa-remove"></span> Remove</button>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>