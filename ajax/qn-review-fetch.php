<?php
include '../config.php';
include '../includes/questions.php';
include '../includes/user.php';
$question = new Questions();
$user = new User();

if(!isset($_POST['qnid'])){
    die('<h4 style="text-align:center;">Some Error Occured</h4>');
}

$id = $_POST['qnid'];
$uid = $user->user_details['id'];
$reviews = $question->query("SELECT * FROM review_list WHERE faculty = '$uid' AND question = '$id' AND status = 0");
echo '<h5 style="border-bottom:1px solid #999;">Previous Reviews</h5>';
if(mysqli_num_rows($reviews) == 0){
	echo '<p> No previous reviews </p>';
}
while($review = mysqli_fetch_array($reviews)){
	echo '<div class="well';

	echo '" >';
	echo $review['review'];
	echo '<p style="text-align:right;font-size:12px;">Reviewed On : '.$review['date'].'</p>';
	echo '</div>';
}
?>

<div style="overflow: hidden;">
      <form method="POST" action="submit/review.php">
        <textarea name="review" class="form-control"></textarea>
        <input type="hidden" name="question" value="<?= $id ?>" />
        <input type="hidden" name="user" value="<?= $uid ?>" />
        <h4 style="text-align: center;"><input type="submit" class="btn btn-primary" value="Submit Review" /></h4>
    </form>
</div>