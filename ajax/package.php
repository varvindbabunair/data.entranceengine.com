<?php
if(!isset($_POST['content'])){
    die('');
}

include '../config.php';
$db = new Db();

$content = explode('-', trim($_POST['content'],'#'));

if($content[0] == 'test' ){
    $select_query = "SELECT title,instruction FROM test_list WHERE id = '".$content[1]."'";
}elseif ($content[0] == 'notes') {
    $select_query = "SELECT title,note FROM notes_list WHERE id = '".$content[1]."'";
}elseif ($content[0] == 'video') {
    $select_query = "SELECT title,video_link,note FROM video_list WHERE id = '".$content[1]."'";
}else{
    die();
}

$content_query = $db->query($select_query);
$contents = mysqli_fetch_array($content_query);
echo '<h3 style="font-weight:bold;text-align:center;">'.$contents['title'].'</h3>';

if ($content[0] == 'video') {
    echo '<iframe id="idIframe" style="width:100%;border:0px;overflow:hidden;min-height:400px;" class="embed-responsive-item" src="'.$contents['video_link'].'" allowfullscreen=""></iframe>';
    echo ''
    . '<script type="text/javascript">
$("iframe").load(function() {
$(this).contents().find("video").css("width","100%");
$(this).contents().find("video").css("height","");
var hght = $(this).contents().find("video").height()+10;
    $(this).height(hght);
});
</script> '
            . '';
}

if ($content[0] == 'video' || $content[0] == 'notes' ) {
    echo $contents['note'];
}

if ($content[0] == 'test' ) {
    echo $contents['instruction'];
    
    $answer_select_query = $db->query("SELECT id,test_time,answer FROM answer_list WHERE test = '".$content[1]."' AND user = '".$_POST['user']."' ORDER BY test_time DESC");
    echo '<div class="col-lg-12" style="text-align:center;"><button class="exam-open btn btn-primary" ';
    echo 'examid="'.$content[1].'"';
    echo ' > <span class="fa fa-edit"></span> Begin Exam</button></div>';
    
    ?>
    
<form id="examForm<?php echo $content[1]; ?>" method="POST" action="pages/student/test.php" target="examWindow" >
    <input type="hidden" name="user" value="<?php echo $_POST['user']; ?>" />
    <input type="hidden" name="examid" value="<?php echo $content[1]; ?>" />
    <input type="hidden" name="session" value="<?php echo $_COOKIE['exam-user']; ?>" />
    <input type="hidden" name="package" value="<?php echo $_POST['package']; ?>" />
</form>
<?php
    
    echo '<hr>';
    echo '<h3 style="text-align:center;font-weight:bold;border-bottom:1px solid #ccc;margin-top:80px;">Previous Attempts</h3>';
    
    
    
        ?>
<table class="table table-condensed">
<?php
        $cnt = mysqli_num_rows($answer_select_query);
        while($answers = mysqli_fetch_array($answer_select_query)){
            echo '<tr>';
            echo '<td> Attempt #'.$cnt.'</td>';
            echo '<td>'.date_format(date_create($answers['test_time']),' jS F Y g:ia').'</td>';
            echo '<td>';
            ?>
    <form method="POST" action="pages/student/answersheet.php" target="answerSheetWindow" >
        <input type="hidden" name="answers" value="<?php echo str_replace('"', '\'', $answers['answer']); ?>" />
        <input type="hidden" name="test" value="<?php echo $content[1]; ?>" />
        <button class="btn btn-primary btn-xs" onclick="viewAnswersheet();"><span class="fa fa-list-alt"></span> View Answersheet</button>
    </form>
    <?php
            echo '</td>';
            echo '<td>';
            ?>
    <form method="POST" action="submit/test.php" target="resultWindow" >
        <input type="hidden" name="answer-string" value="<?php echo str_replace('"', '\'', $answers['answer']); ?>" />
        <input type="hidden" name="test" value="<?php echo $content[1]; ?>" />
        <button class="btn btn-primary btn-xs" onclick="viewResult();"><span class="fa fa-check"></span> View Results</button>
    </form>
    <?php
            echo '</td>';
            echo '</tr>';
            $cnt--;
        }
        ?>
</table>
<?php
       
    
}

?>