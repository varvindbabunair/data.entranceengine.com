<?php

include '../config.php';
$db = new Db();

if(isset($_POST['type'])){
    $testname = addslashes($_POST['title']);
    $sectionrule = $_POST['sectionrule'];
    $sectiontitle = addslashes($_POST['sectiontitle']);
    $questions = json_encode(array(explode(',', $_POST['questions'])), false);
    $tags = addslashes($_POST['tags']);
    $group = $_POST['group'];
    $subject = $_POST['subject'];
    $chapter = $_POST['chapter'];
    $type = $_POST['type'];

    $topic = 0;
    if($type === 'chapter')
    {
        $test_insert_query = $db->query("INSERT INTO app_chapter_test_list 
(title, section_title, tags, section_rule, chapter, questions, subject) 
VALUES 
('$testname', '$sectiontitle', '$tags','$sectionrule',$chapter, '$questions', $subject)") or die(mysqli_error($db->db_link));

    }
    else if ($type === 'topic')
    {
        $topic = $_POST['topic'];
        $test_insert_query = $db->query("INSERT INTO app_topic_test_list 
(title, section_title, tags, section_rule, chapter, topic, questions, subject)
VALUES 
('$testname', '$sectiontitle', '$tags','$sectionrule',$chapter, $topic, '$questions', $subject)") or die(mysqli_error($db->db_link));

    }

    echo 'Successfully created test';
    
}
$questionId = $_POST['questions'];
$questionIdTest = explode(',', $questionId);

if ($test_insert_query) {
    for ($i= 0; $i <= count($questionIdTest); $i++) { 
        $db->query("UPDATE question_list SET app_test_status = 1 WHERE id = '$questionIdTest[$i]'");
    }
    
    }
include '../includes/materials.php';

$tests = new Materials();

if(isset($_POST['tag']) && isset($_POST['subject'])){
    $tags = $_POST['tag'];
    if($tags !=""){
        $tags = implode(",", $tags);
    }else{
        $tags = "all";
    }
    $subjects = $_POST['subject'];
    if($subjects !=""){
        $subjects = implode(",", $subjects);
    }else{
        $subjects = "all";
    }
    if(isset($_POST['exclude'])){
        $exclude = $_POST['exclude'];
        $exclude_arr = explode("&", $exclude);
        $exclude_val = "";
        foreach ($exclude_arr as $exclude){
            $exclude_val .= trim($exclude,"qn[]=").",";
        }
        trim($exclude_val,",");
    } else {
        $exclude_val = "";
    }
    
    echo $tests->get_questions_inchapter($subjects,$tags,$exclude_val);
}

if(isset($_POST['testsubmit']) && $_POST['testsubmit'] == 'test' ){
    $testname = addslashes($_POST['testname']);
    $sectionrule = $_POST['sectionrule'];
    $sectiontitle = addslashes($_POST['sectiontitle']);
    $questions = addslashes(str_replace('\\', '', $_POST['questions']));
    $questions = str_replace('\"[', '[', $questions);
    $questions = str_replace(']\"', ']', $questions);
    $questions = str_replace('"[', '[', $questions);
    $questions = str_replace(']"', ']', $questions);
    $instructions = addslashes($_POST['instruction']);
    $user_group = $_POST['usergroup'];
    $tags = addslashes($_POST['tags']);
    $group = $_POST['group'];
    
    $test_insert_query = $tests->query("INSERT INTO test_list (questions,section_title,section_rule,instruction,title,tags,user_group) VALUES ('$questions','$sectiontitle','$sectionrule','$instructions','$testname','$tags','$user_group')") or die(mysqli_error($tests->db_link));
    
    echo 'Successfully created test';
    
}