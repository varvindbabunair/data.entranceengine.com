<?php
if(!isset($_POST['examid']) || trim($_POST['examid']) == '' || !isset($_POST['session']) || trim($_POST['session']) =='' ){
    die('<h4 style="text-align:center;margin-top:20%;font-weight:bolder;"><span style="color:red;" class="fa fa-exclamation-triangle"></span> &nbsp; OOps... Some error occured</h4>');
}

if(!isset($_POST['user']) || trim($_POST['user']) == ''){
    die('<h4 style="text-align:center;margin-top:20%;font-weight:bolder;"><span style="color:red;" class="fa fa-exclamation-triangle"></span> &nbsp; OOps... Some error occured</h4>');
}

include dirname(__FILE__).'/../config.php';

$db= new Db();

include '../includes/user.php';
$user = new User();


$test_query = $db->query("SELECT * FROM test_list WHERE id = '".$_POST['examid']."'");
$exam_info = mysqli_fetch_array($test_query);

$questions = json_decode($exam_info['questions']);

$where_query ='';
$plusmark = array();
$minusmark = array();
foreach($questions as $section_list){
    foreach($section_list as $question){
        $where_query .= " OR id = '".$question[0]."'";
        $plusmark[$question[0]] = $question[1];
        $minusmark[$question[0]] = $question[2];
    }
}

$where_query = trim($where_query," OR ");
$question_get_query = $db->query("SELECT id,question_type,question,answer_option FROM question_list WHERE ".$where_query);

$questions_details = array();

while($question_info = mysqli_fetch_array($question_get_query)){
    $questions_details[$question_info['id']] = $question_info;
}


?>

<nav class="navbar navbar-default">
    <div class="container">
        <a class="navbar-brand" href="#"><?= $user->get_institute() ?></a>
    </div>
</nav>

<div class="container">
    <?php
    $sections = json_decode($exam_info['section_title']);
    if(!empty($sections)){
    ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid #ccc;padding-bottom:10px;">
        <p style="margin-top:-10px;background-color: #fff;float: left;padding:0px 10px;">Sections</p>
        <br>
        <?php
            $sectionnum = 0;
            foreach ($sections as $section){
        ?>
        
        
        <div class="btn-group">
            <button section-target="#section-<?php echo implode('-', explode(' ',$section)); ?>" drop-target="#drop-<?php echo implode('-', explode(' ',$section)); ?>" data-target=".section-<?php echo implode('-', explode(' ',$section)); ?>" type="button" class="btn dropdownbtn <?php echo ($sectionnum == 0)? 'btn-primary':'btn-default'; ?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php echo $section; ?> <span class="caret"></span>
            </button>
            <ul id="drop-<?php echo implode('-', explode(' ',$section)); ?>" class="dropdown-menu">
                <li><a href="#"><button class="btn btn-xs btn-default"></button> Unattended</a></li>
                <li><a href="#"><button class="btn btn-xs btn-danger"></button> Unanswered</a></li>
                <li><a href="#"><button class="btn btn-xs btn-success"></button> Answered</a></li>
                <li><a href="#"><button class="btn btn-xs btn-info"></button> Marked for Review</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#"><strong>Total : <span class="total-cnt"></span></strong></a></li>
            </ul>
          </div>
        <?php
        $sectionnum++;
            }
        ?>
    </div>
    <?php
    }
    ?>
    
    <div style="width:100%;overflow: hidden;border:1px solid #ccc;">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="border-right:1px solid #ccc;padding-top:20px;padding-bottom:20px;">
        <?php
        $sectionnum = 0;
        foreach ($sections as $section){
        ?>
        <div attended="<?php echo ($sectionnum == 0)?'true':'false'; ?>" id="section-<?php echo implode('-', explode(' ',$section)); ?>" class="section question-section section-<?php echo implode('-', explode(' ',$section)); ?>"  style="width:100%;display: <?php echo ($sectionnum == 0)?'block':'none'; ?>" id="section-<?php echo implode('-', explode(' ',$section)); ?>" >
            <?php
            $questionnum = 1;
            foreach($questions[$sectionnum] as $question){
            ?>
            
            <div class="question" question-id="<?php echo $question[0]; ?>" time-spent="0" style="width:100%;display: <?php echo ($questionnum == 1)?'block':'none'; ?>" id="question-<?php echo $question[0]; ?>" >
                <div style="width:100%;overflow: hidden;">
                <strong>Question Number : <?php echo $questionnum; ?></strong>
                <strong class="pull-right">Mark for correct Answer : <?php echo $plusmark[$question[0]]; ?></strong>
                <br>
                <strong class="pull-right">Negative Mark : <?php echo $minusmark[$question[0]]; ?></strong>
                
                </div>
                
                <hr style="margin-top:10px;">
                <?php echo ($questions_details[$question[0]]['question_type'] == 'text')?$questions_details[$question[0]]['question']:'<img src="../../images/questions/'.$questions_details[$question[0]]['question'].'" />'; ?>
                <ul style="list-style-type: none;margin-top:30px;">
                    <?php
                    $answer_list = json_decode($questions_details[$question[0]]['answer_option']);
                    foreach ($answer_list as $answer){
                        echo '<li class="answer" data-target="#palette-'.$question[0].'" ><input  name="answer-'.$question[0].'" type="radio" value="'.$answer[0].'"  /> &nbsp;';
                        if($answer[1] == 'image'){
                            echo '<img src="../../images/answers/'.$answer[2].'" />';
                        }else if($answer[1] == 'equation'){
                            echo '<img src="http://latex.codecogs.com/gif.latex?'.stripslashes($answer[2]).'" />';
                        }else if($answer[1] == 'src'){
                            echo '<img src="'.  stripslashes($answer[2]).'" />';
                        }else{
                            echo stripslashes($answer[2]);
                        }
                        echo '</li>';
                    }
                    ?>
                </ul>
            </div>
            <?php
                $questionnum++;
            }
            ?>
        </div>
        <?php
        $sectionnum++;
        }
        ?>
        
        <hr>
        <button id="clear-answer"  class="btn btn-default">Clear Response</button>
        <button id="review-answer"  class="btn btn-default">Mark for Review and Next</button>
        <button id="view-next" class="btn btn-success pull-right">Save and Next</button>
        
    </div>
    <?php $time_rule = json_decode($exam_info['section_rule']); ?>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="border-left:1px solid #ccc;padding-top:20px;">
        <div style="width:100%;font-size:22px;">
            <strong id="countdowntimer" class=""><span class="fa fa-clock-o"></span> Time Left : <span id="left-time"></span> / <?php echo $time_rule[1]; ?> min</strong>
        </div>
        
        <script type="text/javascript" >
    $(function(){
        jQuery('#left-time').countdowntimer({
            minutes : <?php echo $time_rule[1]; ?>,
            seconds : 00,
            timeUp : timeIsUp,
            beforeExpiryTime : "00:00:05:00",
            beforeExpiryTimeFunction :  beforeExpiryFunc
        });
        function timeIsUp() {
            alert('Your time is up. Test will now submit');
            testExpired();
        }
        function beforeExpiryFunc() {
		$('#countdowntimer').addClass('redText');
	}
    });
            </script>
        <hr>
        
        <?php
        $sectionnum = 0;
        foreach ($sections as $section){
            ?>
        
        <div class="section section-<?php echo implode('-', explode(' ',$section)); ?>" style="width:100%;text-align:center;display: <?php echo ($sectionnum == 0)?'block':'none'; ?>" >
            <h5 style="text-align:left;">You are viewing <b><?php echo $section; ?></b> section </h5><br>
            
            <h5 ><strong>Question Palette : </strong></h5>
        <?php
            $questionnum = 1;
            foreach($questions[$sectionnum] as $question){
        ?>
            <button id="palette-<?php echo $question[0]; ?>" data-target="#question-<?php echo $question[0]; ?>" class="palette-btn btn btn-sm <?php echo ($questionnum == 1)?'btn-danger':'btn-default'; ?>"><?php echo $questionnum; ?></button>
        <?php
                $questionnum++;
            }
            ?>
        </div>
        <?php
        $sectionnum++;
        }
        ?>
        
        <hr>
        <h4><strong>Legends :</strong></h4>
        <div style="width:50%;float:left;">
        <p><button class="btn btn-xs btn-default">&nbsp;&nbsp;</button> Unattended</p>
        <p><button class="btn btn-xs btn-danger">&nbsp;&nbsp;</button> Unanswered </p>
        </div>
        <div style="width:50%;float:right;">
        <p><button class="btn btn-xs btn-success">&nbsp;&nbsp;</button> Answered </p>
        <p><button class="btn btn-xs btn-info">&nbsp;&nbsp;</button> Marked for Review </p>
        </div>
        <div style="width:100%;overflow: hidden">
            <hr>
        </div>
        <div style="margin-bottom:20px;text-align: center;overflow: hidden;width:100%">
            <button  id="submitExam" class="btn btn-primary">Submit</button>
        </div>
        <form action="../../submit/test.php" method="POST" id="test-submit" >
            <input type="hidden" name="answer-string" value="" />
            <input type="hidden" name="test-session" value="<?php echo $_POST['session']; ?>" />
            <input type="hidden" name="user" value="<?php echo $_POST['user']; ?>" />
            <input type="hidden" name="test" value="<?php echo $_POST['examid']; ?>" />
        </form>
    </div>
    </div>
</div>
