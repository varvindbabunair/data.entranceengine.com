<?php
include '../config.php';
$db = new Db();
$class = $_POST['class'];

$topic_select_query = $db->query("SELECT * FROM topic_list WHERE chapter = '$class'");
echo '<h4 style="border-bottom: 2px solid #ccc;padding:10px;margin-bottom:0px;">Topics</h4>
<ul style="list-style-type:none;padding:0px;margin:0px;height:250px;overflow-y:scroll;">';
while($topics = mysqli_fetch_array($topic_select_query)){
?>
    <li class="topic-list" subject_var="<?= $topics['id'] ?>" ><?= $topics['title'] ?>
        <span class="fa fa-caret-right pull-right" style="margin-top: 5px;"></span>
    </li>
<?php
}
?>
</ul>