<?php
if(!isset($_POST['editid']) || trim($_POST['editid']) == ''){
    die('<p style="text-align:center;font-weight:bold;font-size:20px;"><span class="fa fa-exclamation-triangle"></span> Oops.... Some error occured while processing your request.</p>');
}

$id = trim($_POST['editid']);
include '../config.php';
include '../includes/questions.php';

$questions = new Questions();

$question_info = $questions->query("SELECT * FROM question_list WHERE id = '$id'");
$question = mysqli_fetch_array($question_info);

?>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
    Choose a Chapter :
    <select name="chapter" class="form-control">
        <option value="">Choose from the list</option>
        <?php
        $subject_list = $questions->get_subject_list();
        foreach($subject_list as $subject){
        ?>
        <optgroup label="<?php echo $subject['title'] ?>">
        <?php
        $chapter_list = $questions->get_chapter_list($subject['id']);
        foreach ($chapter_list as $chapter){
        ?>
            <option value="<?php echo $chapter['id']; ?>"  <?php echo ($chapter['id'] == $question['chapter'])?'selected':''; ?> ><?php echo $chapter['title']; ?></option>
        <?php
        }
        ?>
        </optgroup>
        <?php
        }
        ?>
    </select>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
    Question Type:
    <select class="form-control question-type" name="question-type">
        <option value="text" <?php echo ($question['question_type'] == 'text')?'selected':''; ?> >Text</option>
        <option value="image" <?php echo ($question['question_type'] == 'image')?'selected':''; ?> >Image</option>
    </select>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    Question :
    <?php
    if($question['question_type'] == 'text'){
    ?>
    <textarea name="text-question" style="display:none;" class="form-control text-question" id="text-question"><?php echo $question['question']; ?></textarea>
    <input type="file" class="image-question" style="display:none" name="image-question" accept=".jpg,.png" />
    <?php
    }elseif($question['question_type'] == 'image'){
    ?>
    <textarea name="text-question" style="display:none;" class="form-control text-question" id="text-question"></textarea>
    <br>
    <img class="image-question" src="images/questions/<?php echo $question['question']; ?>" />
    <input type="file" class="image-question" style="<?php echo ($question['question_type'] == 'text')?'display:none;':''; ?>" name="image-question" accept=".jpg,.png" />
    
    <?php
    }
    ?>
</div>
    <div class="col-lg-6">
    Section :
    <?php
    $section_get = $questions->query("SELECT * FROM section_list WHERE chapter = '$question[chapter]'");
    ?>
    <select class="form-control section-select" name="section">
        <?php
        while($section = mysqli_fetch_array($section_get)){
            echo '<option value="'.$section['id'].'" ';
            echo ($section['id'] == $question['section'])?'selected':'';
            echo '>'.$section['title'].'</option>';
        }
        ?>
    </select>
    </div>
    <div class="col-lg-6">
    Tags :
    <input class="tags" id="tags-input" name="tags" data-role="tagsinput" required="" value="<?php echo $question['tags']; ?>" />
    </div>
<div class="col-lg-12">
    
    <h4>Answer</h4>
    <table class="table option-table"><tbody>
            <?php
            $answers = json_decode($question['answer_option']);
            $answer_cnt = sizeof($answers);
            $cnt=0;
            foreach($answers as $answer){
                ?>
                <tr>
                    <td>Option <?php echo $answer[0]; ?></td>
                    <td><select name="answer-type-<?php echo strtolower($answer[0]); ?>" typeof="<?php echo $answer[0]; ?>" class="form-control answer-type"><option <?php echo ($answer[1] == 'text')?'selected=""':''; ?> value="text">Text</option><option <?php echo ($answer[1] == 'image')?'selected=""':''; ?> value="image">Image</option></select></td>
                    <td>
                        <input name="text-answer-<?php echo strtolower($answer[0]); ?>" type="text" class="form-control option-text option-text-<?php echo $answer[0]; ?>" style="<?php echo ($answer[1] == 'text')?'':'display:none'; ?>" value="<?php echo ($answer[1] == 'text')?$answer[2]:''; ?>" /> 
                        <input name="image-answer-<?php echo strtolower($answer[0]); ?>" class="option-image option-image-<?php echo $answer[0]; ?>" style="<?php echo ($answer[1] == 'image')?'':'display:none'; ?>" type="file" accept=".jpg,.png" />
                        <?php 
                        echo ($answer[1] == 'image')?'<img style="max-width:200px;" class="option-image" src="images/answers/'.$answer[2].'" />':'';
                        ?>
                    </td>
                    <td>
                        <input type="radio" name="correct-answer" value="<?php echo $answer[0]; ?>" <?php echo ($answer[0] == $question['correct_answer'])?'checked=""':''; ?> />
                        <?php
                        if($cnt == $answer_cnt-1){
                            ?>
                        <span style="margin-left:10px;" class="fa fa-plus add-option" title="Add Option"></span><span style="margin-left:10px;" class="fa fa-remove remove-option" title="Remove Option"></span>
                        <?php
                        }
                        ?>
                    </td>
                    
                </tr>
            <?php
            $cnt++;
            }
            ?>
        
        </tbody></table>

</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
    Hint Type:
    <select class="form-control hint-type" name="hint-type">
        <option value="text" <?php echo ($question['hint_type'] == 'text')?'selected':''; ?> >Text</option>
        <option value="image" <?php echo ($question['hint_type'] == 'image')?'selected':''; ?> >Image</option>
    </select>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h4>Hint</h4>
    <?php
    if($question['hint_type'] == 'text'){
    ?>
    <!--<textarea name="text-hint" class="form-control text-hint" id="text-hint" style=""><?php echo ($question['hint_type'] == 'image')? '<img src="images/hints/'.$question['hint'].'" />':$question['hint']; ?></textarea>-->
    <textarea name="text-hint" class="form-control text-hint" id="text-hint"><?php echo $question['hint']; ?></textarea>
    <?php
    }elseif($question['hint_type'] == 'image'){
    ?>
    <textarea name="text-hint" class="form-control text-hint" id="text-hint"></textarea>
    <img class="image-hint" src="images/hints/<?php echo $question['hint']; ?>" />
    <input type="file" class="image-hint" style="<?php echo ($question['hint_type'] == 'text')?'display:none;':''; ?>" name="image-hint" accept=".jpg,.png" />
    
    <?php
    }
    ?>
    
</div>
                