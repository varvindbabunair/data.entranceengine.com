<?php
include '../config.php';
$db = new Db();
$class = $_POST['class'];

$subject_select_query = $db->query("SELECT * FROM section_list WHERE topic = '$class'");
echo '<h4 style="border-bottom: 2px solid #ccc;padding:10px;margin-bottom:0px;">Sections</h4>
<ul style="list-style-type:none;padding:0px;margin:0px;height:250px;overflow-y:scroll;">';
while($subjects = mysqli_fetch_array($subject_select_query)){
?>
        <li class="section-list" ><?= $subjects['name'] ?></li>
<?php
}
?>
    <li style="padding: 10px;" >
        <form enctype="multipart/form-data" class="section-add-form">
            <h5 style="border-bottom: 1px solid #ccc;"><span class="fa fa-plus"></span> <strong>Add Section</strong></h5>
            <input type="hidden" class="section-select" name="topic" value="<?= $_POST['class'] ?>" />
            Topics :
            <textarea class="form-control" name="sections"></textarea>
            <br>
            <button class="btn btn-primary form-control btn-sm add-section"><span class="fa fa-plus"></span> Add Sections</button>
        </form>
    </li>
</ul>