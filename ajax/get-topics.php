<?php
include '../config.php';
$db = new Db();
$class = $_POST['class'];

$topic_select_query = $db->query("SELECT * FROM topic_list WHERE chapter = '$class'");
echo '<h4 style="border-bottom: 2px solid #ccc;padding:10px;margin-bottom:0px;">Topics</h4>
<ul style="list-style-type:none;padding:0px;margin:0px;height:250px;overflow-y:scroll;">';
while($topics = mysqli_fetch_array($topic_select_query)){
?>
    <li class="topic-list" subject_var="<?= $topics['id'] ?>" ><?= $topics['title'] ?>
        <span class="fa fa-caret-right pull-right" style="margin-top: 5px;"></span>
    </li>
<?php
}
?>
        
    <li style="padding: 10px;" >
        <form enctype="multipart/form-data" class="topic-add-form">
            <h5 style="border-bottom: 1px solid #ccc;"><span class="fa fa-plus"></span> <strong>Add Topic</strong></h5>
            <input type="hidden" class="topic-select" name="chapter" value="<?= $_POST['class'] ?>" />
            Topics :
            <textarea class="form-control" name="topics"></textarea>
            <br>
            <button class="btn btn-primary form-control btn-sm add-topic"><span class="fa fa-plus"></span> Add Topics</button>
        </form>
    </li>
</ul>