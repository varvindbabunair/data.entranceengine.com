<?php
include '../config.php';
include '../includes/materials.php';

$videos = new Materials();
$editid = (isset($_POST['id']))?$_POST['id']:'';
if($editid == ''){
    die('Some Error Occured');
}
$edit_video = $videos->get_video_details($editid);

?>

Select Chapter :
<select name="chapter" class="form-control">
    <option>Select from Chapter List</option>
  <?php
  $subject_list = $videos->get_subject_list();
  foreach($subject_list as $subjects){
      echo '<optgroup label="'.$subjects['title'].'">';
      $chapter_list = $videos->get_chapter_list($subjects['id']);
      foreach($chapter_list as $chapter){
          echo '<option value="'.$chapter['id'].'"  ';
          echo ($chapter['id'] == $edit_video['chapter'])?'selected':'';
          echo ' >'.$chapter['title'].'</option>';
      }
      echo '</optgroup>';
  }
  ?>
</select>
Video Title:
<input name="edit-id" type="hidden" class="form-control" value="<?php echo $edit_video['id']; ?>" />
<input name="title" type="text" class="form-control" value="<?php echo $edit_video['title']; ?>" />
Video Link / Upload:
<div class="input-group">
    <input type="text" class="form-control" name="vid-link" id="basic-url" aria-describedby="basic-addon3" placeholder="Paste video link">
  <span class="input-group-addon" id="basic-addon3"> OR </span>
  <input type="file" name="upload_vid" class="vid form-control" accept=".mp4,.mpeg4" />

</div>
Tags :
<input name="tags" class="form-control tags" value="<?php echo $edit_video['tags']; ?>" />
<script>
    $('.tags').tagsinput();
  </script>

  Description :
  <textarea class="form-control" name="description"><?php echo $edit_video['description']; ?></textarea>
Notes :
<textarea class="form-control" name="editor-video"><?php echo $edit_video['note']; ?></textarea>