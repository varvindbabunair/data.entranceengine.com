<?php
class Db{
    //credentials to be edited by the user
    private $db_host = 'localhost';
    public $db_name = 'u706553391_ecfy';
    private $db_user = 'u706553391_ecfy';
    private $db_password = 'P@ssw0rd@123#';
    
    
    
    //do not edit beyond this point
    public $db_link;
    
    public function __construct() {
        $this->db_link = mysqli_connect($this->db_host, $this->db_user, $this->db_password,  $this->db_name) or die('error in database connection please edit the config file');
    }
    
    public function query($query){
        $result = mysqli_query($this->db_link, $query);
        return $result;
    }
}
//Global Variables
/*************************************/
//SITE_URL is assigned the url of homepage of site
$main_page = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
define('SITE_URL', 'http://'.$_SERVER['SERVER_NAME'].'/'.$main_page[0] );
//HOME_DIR is the path of home directory
define('HOME_DIR', dirname(__FILE__));
?>
